#!/bin/bash

# Format: update_properties.sh <langcode>

echo Generating properties file from po

msgconv --properties-output -o src/main/resources/i18n/$1.properties src/main/resources/i18n/$1.po
