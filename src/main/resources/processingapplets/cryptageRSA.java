package processingapplets;

import java.awt.Frame;
import java.math.BigInteger;
import java.util.Random;
import processing.core.PApplet;
import processing.core.PFont;

public class cryptageRSA extends PApplet {
   int pq_size;
   static int prime_certainty = 20;
   BigInteger p;
   BigInteger q;
   BigInteger n;
   BigInteger e;
   BigInteger d;
   BigInteger A;
   BigInteger EncMessBits;
   PFont pfont;
   int space;
   int myOr = this.color(255, 100, 0);
   int myGreen = this.color(0, 100, 30);
   int myGreenA = this.color(0, 200, 100);
   int myRed = this.color(255, 0, 0);
   int myBlue = this.color(120);
   int myBlueA = this.color(100);
   cryptageRSA.rectButton[] T0 = new cryptageRSA.rectButton[4];
   cryptageRSA.TextButton[] T1 = new cryptageRSA.TextButton[4];
   cryptageRSA.rectButton[] T2 = new cryptageRSA.rectButton[3];
   cryptageRSA.rectButton[] T3 = new cryptageRSA.rectButton[5];
   cryptageRSA.TextButton[] T4 = new cryptageRSA.TextButton[4];
   cryptageRSA.TextButton[] T5 = new cryptageRSA.TextButton[2];
   cryptageRSA.TextButton T6;
   boolean locked = false;
   boolean info = false;
   String[] ListN1 = new String[]{"1. Génère   P  et  Q", "2. Calcule N", "3. Génère E", "4. Calcule D"};
   String[] ListN2 = new String[]{"Clé privée = ", "Clé publique = ", "Clé publique = "};
   String[] ListN3 = new String[]{"Traduction  du  message  en  chiffres", "Encryptage du message", "E N V O I   du message encrypté", "Décryptage du message"};
   String[] ListN4 = new String[]{"Tu es Alice et tu es chargé de générer deux 'clés', \nune 'clé publique' et 'une clé privée'.\nTu ne transmettras que la clé publique à Bob. \nBob encryptera son message secret à l'aide de la clé publique,\net seul toi pourras le décrypter au moyen de la clé privée!", "Tu es Bob. Tu vas recevoir une 'clé', dite 'publique', \nqui te permettra d'encrypter un message secret.\nTransmet ton message encrypté à Alice qui essayera de le décrypter! "};
   String lastInput = new String();

   public void setup() {
      this.frame = new Frame();
      this.size(900, 650);
      this.space = 50;
      this.frameRate(30.0F);
      this.pfont = this.createFont("Arial Bold", 12.0F, true);
      this.p = new BigInteger("0");
      this.q = new BigInteger("0");
      this.n = new BigInteger("0");
      this.e = new BigInteger("0");
      this.d = new BigInteger("0");
      this.A = new BigInteger("0");
      this.frame.setTitle("Alice");

      int var1;
      for(var1 = 0; var1 < this.T1.length; ++var1) {
         this.T0[var1] = new cryptageRSA.rectButton(this.space, this.height / 2 - this.height / 4 + var1 * 40, 160, 25, this.myGreen);
         this.T1[var1] = new cryptageRSA.TextButton(this.width / 2 - (this.space + 160), this.height / 2 - this.height / 4 + var1 * 40, 160, 25, this.color(255), this.myGreen, this.myGreenA, this.ListN1[var1]);
      }

      for(var1 = 0; var1 < this.T2.length; ++var1) {
         if (var1 == 2) {
            this.T2[var1] = new cryptageRSA.rectButton(this.width / 2 + this.space, this.height / 2 - this.height / 4, 200, 25, this.myRed);
         } else {
            this.T2[var1] = new cryptageRSA.rectButton((var1 + 1) % 2 * this.space + PApplet.parseInt((float)((var1 + 1) / 2)) * (this.width / 2 - (this.space + 160)) - var1 * 40, this.height / 2 + this.height / 20, 160 + var1 * 40, 25, this.myRed);
         }

         this.T2[var1].setText(this.ListN2[var1]);
      }

      for(var1 = 0; var1 < this.T3.length; ++var1) {
         if (var1 < 3) {
            if (var1 == 0) {
               this.T3[var1] = new cryptageRSA.rectButton(this.width / 2 + this.space, this.height / 2 - this.height / 6 + var1 * 90, this.width / 2 - this.space * 2, 30, this.myBlue);
            } else {
               this.T3[var1] = new cryptageRSA.rectButton(this.width / 2 + this.space, this.height / 2 - this.height / 6 + var1 * 90 + PApplet.parseInt((float)(var1 / 2)) * 30, this.width / 2 - this.space * 2, 60, this.myBlue);
            }
         } else {
            this.T3[var1] = new cryptageRSA.rectButton(this.space, this.height / 2 - this.height / 4 + PApplet.parseInt((float)(var1 / 2)) * 30 + var1 * 90, this.width / 2 - this.space * 2, 30 + var1 % 2 * 30, this.myBlue);
         }
      }

      for(var1 = 0; var1 < this.T4.length; ++var1) {
         if (var1 < 2) {
            this.T4[var1] = new cryptageRSA.TextButton(this.width - (this.space + 250), this.height / 2 - this.height / 6 + 55 + var1 * 120, 250, 25, this.color(255), this.myBlue, this.myBlueA, this.ListN3[var1]);
         } else if (var1 == 2) {
            this.T4[var1] = new cryptageRSA.TextButton(this.width - (this.space + 250), this.height / 2 - this.height / 6 + 115 + var1 * 90, 250, 25, this.color(255), this.myRed, this.myOr, this.ListN3[var1]);
         } else {
            this.T4[var1] = new cryptageRSA.TextButton(this.width / 2 - (this.space + 250), this.height / 2 - this.height / 6 + 60 + var1 * 90, 250, 25, this.color(255), this.myBlue, this.myBlueA, this.ListN3[var1]);
         }
      }

      for(var1 = 0; var1 < this.T5.length; ++var1) {
         this.T5[var1] = new cryptageRSA.TextButton(10 + var1 * this.width / 2, 12, 70, 22, this.color(255), this.color(var1 * 153), this.myOr, "- info -");
      }

      this.T6 = new cryptageRSA.TextButton(this.width / 2 - this.space - 170, this.height / 2 + this.height / 10, 170, 25, this.color(255), this.myRed, this.myOr, "Masquer toute information");
      this.lastInput = "Ecrit ton message, il s'inscrira ici ";
      this.T3[0].setText(this.lastInput);
   }

   public void draw() {
      this.background(0);
      this.fill(153);
      this.rect((float)(this.width / 2 + 1), 0.0F, (float)(this.width / 2), (float)this.height);
      this.update(this.mouseX, this.mouseY);

      int var1;
      for(var1 = 0; var1 < this.T1.length; ++var1) {
         this.T0[var1].display();
         this.T1[var1].display();
      }

      for(var1 = 0; var1 < this.T2.length; ++var1) {
         this.T2[var1].display();
      }

      for(var1 = 0; var1 < this.T3.length; ++var1) {
         this.T3[var1].display();
      }

      for(var1 = 0; var1 < this.T4.length; ++var1) {
         this.T4[var1].display();
      }

      for(var1 = 0; var1 < this.T5.length; ++var1) {
         this.T5[var1].display();
      }

      this.T6.display();
   }

   public void keyPressed() {
      if (this.key != '\uffff') {
         if (this.lastInput.length() > 62) {
            this.lastInput = this.lastInput.substring(0, 62);
         }

         if (this.key == '\b') {
            if (this.lastInput.length() > 1) {
               this.lastInput = this.lastInput.substring(0, this.lastInput.length() - 2);
               this.T3[0].setText(this.lastInput);
               this.T3[0].display();
            }
         } else {
            this.lastInput = this.lastInput + this.key;
            this.T3[0].setText(this.lastInput);
            this.T3[0].display();
         }
      }

      BigInteger[] var1;
      if (this.key == '1') {
         var1 = new BigInteger[3];
         var1 = createKeys();
         println("Private Key is: " + var1[0] + " // Public Key is: " + var1[1] + " " + var1[2]);
      }

      BigInteger var2;
      if (this.key == '2') {
         var1 = new BigInteger[3];
         var1 = createKeys();
         var2 = encrypt("oh oh je suis le père Noel!!!", var1[1], var1[2]);
         println("Public Key is: " + var1[1] + " " + var1[2] + " // encrypted message: " + var2);
      }

      if (this.key == '3') {
         var1 = new BigInteger[3];
         var1 = createKeys();
         var2 = encrypt("oh oh je suis le père Noel!!!", var1[1], var1[2]);
         String var3 = decrypt(var2, var1);
         println("Public Key is: " + var1[1] + " " + var1[2] + " // encrypted message: " + var2);
         println("Private Key is: " + var1[0] + " // decrypted message: " + var3);
      }

   }

   public void update(int var1, int var2) {
      int var3;
      for(var3 = 0; var3 < this.T5.length; ++var3) {
         this.T5[var3].update();
         if (this.T5[var3].over) {
            this.myInfo(this.ListN4[var3], 0 + var3 * this.width / 2, 110 - var3 * 40);
         }
      }

      if (!this.locked) {
         for(var3 = 0; var3 < this.T1.length; ++var3) {
            this.T1[var3].update();
         }

         for(var3 = 0; var3 < this.T4.length; ++var3) {
            this.T4[var3].update();
         }

         this.T6.update();
      } else {
         this.locked = false;
      }

      if (this.mousePressed) {
         int var4;
         for(var3 = 0; var3 < this.T1.length; ++var3) {
            if (this.T1[var3].pressed()) {
               this.T1[var3].select = true;
               if (var3 == 0) {
                  this.reset();
                  this.T0[var3].setText("  P = " + this.p + "      Q = " + this.q);
               } else if (var3 == 1) {
                  if (this.p != this.A) {
                     this.calculate_n();
                  }

                  this.T0[var3].setText("  N = " + this.n);
               } else if (var3 == 2) {
                  if (this.p != this.A) {
                     this.launch_e();
                  }

                  this.T0[var3].setText("  E = " + this.e);
               } else if (var3 == 3 && this.e != this.A) {
                  this.launch_d();
                  this.T0[var3].setText("  D = " + this.d);
                  this.T2[0].setText(this.ListN2[0] + this.d);
                  this.T2[1].setText(this.ListN2[1] + "(" + this.n + "; " + this.e + ")");
                  this.T2[2].setText(this.ListN2[2] + "(" + this.n + "; " + this.e + ")");
               }
            }

            for(var4 = 0; var4 < this.T1.length - 1; ++var4) {
               if (var4 != var3) {
                  this.T1[var4].select = false;
               }
            }
         }

         if (this.T6.pressed()) {
            this.T6.select = true;

            for(var3 = 0; var3 < this.T0.length; ++var3) {
               this.T0[var3].setText("");
            }

            this.T2[0].setText("");
         }

         for(var3 = 0; var3 < this.T4.length; ++var3) {
            if (this.T4[var3].pressed()) {
               this.T4[var3].select = true;
               if (var3 == 0) {
                  this.translate_m();
               } else if (var3 == 1) {
                  if (!this.n.equals(this.A)) {
                     this.encrypt_m();
                  }
               } else if (var3 == 2) {
                  if (!this.n.equals(this.A)) {
                     this.send_m();
                     this.lastInput = "";
                     this.T3[0].setText(this.lastInput);
                     this.T2[0].setText(this.ListN2[0]);
                  }
               } else if (var3 == 3 && !this.n.equals(this.A)) {
                  this.decrypt_m();
               }
            }

            for(var4 = 0; var4 < this.T4.length - 1; ++var4) {
               if (var4 != var3) {
                  this.T4[var4].select = false;
               }
            }
         }
      }

   }

   public void myInfo(String var1, int var2, int var3) {
      this.fill(255);
      this.rect((float)var2, 20.0F, (float)(this.width / 2), (float)var3);
      this.fill(this.myOr);
      this.text(var1, (float)(var2 + 20), 40.0F);
   }

   public void reset() {
      this.pq_size = PApplet.parseInt(this.random(4.0F, 10.0F));
      this.p = new BigInteger(this.pq_size + 1, prime_certainty, new Random());
      this.q = new BigInteger(this.pq_size - 1, prime_certainty, new Random());
      if (this.p == null) {
         this.p = new BigInteger(this.pq_size + 1, prime_certainty, new Random());
      }

      if (this.q == null) {
         this.q = new BigInteger(this.pq_size - 1, prime_certainty, new Random());
      }

   }

   public void calculate_n() {
      this.n = this.p.multiply(this.q);
   }

   public void launch_e() {
      this.e = generate_e(this.p, this.q, 16);
   }

   public void launch_d() {
      this.d = calculate_d(this.p, this.q, this.e);
   }

   public void translate_m() {
      BigInteger var1 = newBigInteger(this.lastInput.getBytes());
      this.T3[1].setText(var1 + " ");
   }

   public void encrypt_m() {
      BigInteger var1 = newBigInteger(this.lastInput.getBytes());
      this.EncMessBits = encrypt(var1, this.e, this.n);
      this.T3[2].setText(this.EncMessBits + " ");
   }

   public void decrypt_m() {
      BigInteger var1 = decrypt(this.EncMessBits, this.d, this.n);
      String var2 = new String(var1.toByteArray());
      this.T3[4].setText(var2 + " ");
   }

   public void send_m() {
      this.T3[3].setText(this.EncMessBits + " ");
   }

   public static BigInteger generate_e(BigInteger var0, BigInteger var1, int var2) {
      new BigInteger("0");
      BigInteger var4 = var1.subtract(new BigInteger("1"));
      var4 = var4.multiply(var0.subtract(new BigInteger("1")));
      int var5 = 0;

      BigInteger var3;
      do {
         var3 = (new BigInteger(var2, 0, new Random())).setBit(0);
         ++var5;
      } while(var5 < 100 && var3.gcd(var4).compareTo(new BigInteger("1")) != 0);

      return var3;
   }

   public static BigInteger calculate_d(BigInteger var0, BigInteger var1, BigInteger var2) {
      BigInteger var4 = var1.subtract(new BigInteger("1"));
      var4 = var4.multiply(var0.subtract(new BigInteger("1")));
      BigInteger var3 = var2.modInverse(var4);
      return var3;
   }

   public static BigInteger encrypt(BigInteger var0, BigInteger var1, BigInteger var2) {
      BigInteger var3 = new BigInteger("0");
      int var5 = 0;

      for(BigInteger var4 = (new BigInteger("2")).pow(var2.bitLength() - 1).subtract(new BigInteger("1")); var0.compareTo(var4) == 1; ++var5) {
         var3 = var0.and(var4).modPow(var1, var2).shiftLeft(var5 * var2.bitLength()).or(var3);
         var0 = var0.shiftRight(var2.bitLength() - 1);
      }

      var3 = var0.modPow(var1, var2).shiftLeft(var5 * var2.bitLength()).or(var3);
      return var3;
   }

   public static BigInteger decrypt(BigInteger var0, BigInteger var1, BigInteger var2) {
      BigInteger var3 = new BigInteger("0");

      try {
         int var5 = 0;

         for(BigInteger var4 = (new BigInteger("2")).pow(var2.bitLength()).subtract(new BigInteger("1")); var0.compareTo(var4) == 1; ++var5) {
            var3 = var0.and(var4).modPow(var1, var2).shiftLeft(var5 * (var2.bitLength() - 1)).or(var3);
            var0 = var0.shiftRight(var2.bitLength());
         }

         var3 = var0.modPow(var1, var2).shiftLeft(var5 * (var2.bitLength() - 1)).or(var3);
      } catch (NullPointerException var6) {
         System.out.println("Attention à bien définir les paramètres avant d'utiliser la fonction de décrytage !");
      }

      return var3;
   }

   public static BigInteger[] createKeys() {
      BigInteger[] var0 = new BigInteger[3];
      int var1 = (int)(4.0D + 6.0D * Math.random());
      BigInteger var2 = new BigInteger(var1 + 1, prime_certainty, new Random());
      BigInteger var3 = new BigInteger(var1 - 1, prime_certainty, new Random());
      if (var2 == null) {
         var2 = new BigInteger(var1 + 1, prime_certainty, new Random());
      }

      if (var3 == null) {
         var3 = new BigInteger(var1 - 1, prime_certainty, new Random());
      }

      BigInteger var4 = var2.multiply(var3);
      BigInteger var5 = generate_e(var2, var3, 16);
      BigInteger var6 = calculate_d(var2, var3, var5);
      var0[0] = var6;
      var0[1] = var5;
      var0[2] = var4;
      return var0;
   }

   public static BigInteger encrypt(String var0, BigInteger var1, BigInteger var2) {
      BigInteger var3 = null;
      BigInteger var4 = newBigInteger(var0.getBytes());
      var3 = encrypt(var4, var1, var2);
      return var3;
   }

   public static String decrypt(BigInteger var0, BigInteger[] var1) {
      String var2 = null;
      BigInteger var3 = decrypt(var0, var1[0], var1[2]);
      var2 = new String(var3.toByteArray());
      return var2;
   }

   private static BigInteger newBigInteger(byte[] var0) {
      try {
         return new BigInteger(var0);
      } catch (Exception var1) {
         return new BigInteger("0");
      }
   }

   public static void main(String[] var0) {
      PApplet.main(new String[]{"--bgcolor=#DFDFDF", "processingapplets.cryptageRSA"});
   }

   class Button {
      int x;
      int y;
      int L;
      int h;
      int basecolor;
      int highlightcolor;
      int currentcolor;
      int fcolor;
      String value;
      boolean over = false;
      boolean pressed = false;
      boolean select = false;

      public void update() {
         if (this.over()) {
            this.currentcolor = this.highlightcolor;
         } else {
            this.currentcolor = this.basecolor;
         }

      }

      public boolean pressed() {
         if (this.over) {
            cryptageRSA.this.locked = true;
            return true;
         } else {
            cryptageRSA.this.locked = false;
            return false;
         }
      }

      public boolean over() {
         return true;
      }

      public boolean overText(int var1, int var2, int var3, int var4) {
         return cryptageRSA.this.mouseX >= var1 && cryptageRSA.this.mouseX <= var1 + var3 && cryptageRSA.this.mouseY >= var2 - var4 / 2 && cryptageRSA.this.mouseY <= var2 + var4 / 2;
      }
   }

   class TextButton extends cryptageRSA.Button {
      TextButton(int var2, int var3, int var4, int var5, int var6, int var7, int var8, String var9) {
         super();
         this.x = var2;
         this.y = var3;
         this.L = var4;
         this.h = var5;
         this.fcolor = var6;
         this.highlightcolor = var8;
         this.basecolor = var7;
         this.currentcolor = this.basecolor;
         this.value = var9;
      }

      public boolean over() {
         if (this.overText(this.x, this.y, this.L, this.h)) {
            this.over = true;
            return true;
         } else {
            this.over = false;
            return false;
         }
      }

      public void display() {
         if (this.x < cryptageRSA.this.width / 2) {
            cryptageRSA.this.stroke(153);
         } else {
            cryptageRSA.this.stroke(190);
         }

         cryptageRSA.this.strokeWeight(0.8F);
         cryptageRSA.this.fill(this.currentcolor);
         cryptageRSA.this.rect((float)this.x, (float)(this.y - 17), (float)this.L, (float)this.h);
         cryptageRSA.this.fill(this.fcolor);
         cryptageRSA.this.textFont(cryptageRSA.this.pfont);
         cryptageRSA.this.text(this.value, (float)(this.x + 10), (float)this.y);
         cryptageRSA.this.noStroke();
      }
   }

   class rectButton extends cryptageRSA.Button {
      rectButton(int var2, int var3, int var4, int var5, int var6) {
         super();
         this.x = var2;
         this.y = var3;
         this.L = var4;
         this.h = var5;
         this.value = "null";
         this.currentcolor = var6;
      }

      public void display() {
         cryptageRSA.this.noStroke();
         cryptageRSA.this.fill(this.currentcolor);
         cryptageRSA.this.rect((float)this.x, (float)(this.y - 17), (float)this.L, (float)this.h);
         if (!this.value.equals("null")) {
            cryptageRSA.this.fill(255);
            cryptageRSA.this.textFont(cryptageRSA.this.pfont);
            if (this.value.length() < 45) {
               cryptageRSA.this.text(" " + this.value.substring(0, this.value.length()), (float)this.x, (float)this.y);
            } else {
               cryptageRSA.this.textFont(cryptageRSA.this.pfont, 10.0F);
               if (PApplet.parseInt((float)(this.value.length() / 58)) == 0) {
                  cryptageRSA.this.text(" " + this.value.substring(0, this.value.length()), (float)this.x, (float)this.y);
               } else {
                  int var1 = 0;

                  for(int var2 = 0; var2 < PApplet.parseInt((float)(this.value.length() / 58)); ++var2) {
                     cryptageRSA.this.text(" " + this.value.substring(var2 * 58, (var2 + 1) * 58 - 1), (float)this.x, (float)(this.y + var2 * 15));
                     var1 = var2 + 1;
                  }

                  cryptageRSA.this.text(" " + this.value.substring(var1 * 58, this.value.length()), (float)this.x, (float)(this.y + var1 * 15));
               }
            }
         }

      }

      public void setText(String var1) {
         this.value = var1;
      }
   }
}
