package processingapplets;

import java.awt.Frame;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import processing.core.PApplet;
import processing.core.PFont;
import processing.core.PVector;

public class grapheEtChemins extends PApplet {
   PFont font;
   boolean mouseDown = false;
   boolean info = false;
   boolean pathSelect = false;
   String[] listN = new String[]{"Ahmed", "Barbara", "Charlotte", "Diego", "Elliot", "Ida", "Jelena", "Pontus", "Réda", "Samiha"};
   double[] listP = new double[]{10.0D, 15.0D, 20.0D, 10.0D, 20.0D};
   int indN = 0;
   int indP = 0;
   grapheEtChemins.Graph myGraph;
   String firstSelect;
   String secondSelect;
   String start = null;
   String end = null;
   ArrayList path;
   ArrayList restricted;
   ArrayList opened;
   ArrayList closed;
   int pathC = this.color(0, 124, 30);
   grapheEtChemins.HScrollbar hs1;
   int topWidth;
   public static final int WIDTH = 900;
   public static final int HEIGHT = 500;

   public void setup() {
      this.frame = new Frame();
      this.size(900, 500);
      this.smooth();
      this.myGraph = new grapheEtChemins.Graph();
      this.path = new ArrayList();
      this.restricted = new ArrayList();
      this.opened = new ArrayList();
      this.closed = new ArrayList();
      this.hs1 = new grapheEtChemins.HScrollbar(0, 15, this.width, 15, 4);
   }

   public void draw() {
      this.background(130);
      this.font = this.createFont("Arial Bold", 14.0F, true);
      float var1 = this.hs1.getPos() - (float)(this.width / 2);
      this.textAlign(37);
      this.fill(0.0F, 40.0F, 63.0F);
      this.textFont(this.font, 13.0F);
      this.text(" - I  N  S  T  R  U  C  T  I  O  N  S - \n > Noeud: \n    . ajout = clic droit \n    . déplacement: 'm'\n    . suppression: clic gauche + 'd' \n > Lien: \n    . ajout/suppression: clic centre + glisse \n > Générer: \n    . tous les noeuds = 'a': \n    . tous les liens = 'l': \n    . quelques liens = 'r' \n > Afficher la pondération des liens: 'i' \n > Trouver le plus court chemin entre 2 noeuds: \n    . clic gauche + 'b' pour le noeud de départ \n    . clic gauche + 'e' pour le noeud d'arrivée\n > Fermer l'application: ESC ", var1 * 2.0F, 40.0F);
      this.hs1.update();
      this.hs1.display();
      if (!this.pathSelect) {
         this.start = null;
      }

      grapheEtChemins.Node var2;
      if (this.firstSelect != null) {
         var2 = (grapheEtChemins.Node)this.myGraph.nodes.get(this.firstSelect);
         this.noStroke();
         this.fill(255);
         this.ellipse((float)var2.x, (float)var2.y, 30.0F, 30.0F);
         this.stroke(0);
      }

      if (this.start != null) {
         var2 = (grapheEtChemins.Node)this.myGraph.nodes.get(this.start);
         this.noStroke();
         this.fill(0.0F, 124.0F, 30.0F);
         this.ellipse((float)var2.x, (float)var2.y, 30.0F, 30.0F);
         this.stroke(0);
      }

      grapheEtChemins.Node var4;
      String var5;
      Iterator var6;
      grapheEtChemins.Node var7;
      for(int var10 = 0; var10 < this.path.size(); ++var10) {
         String var3 = (String)this.path.get(var10);
         var4 = (grapheEtChemins.Node)this.myGraph.nodes.get(var3);
         this.fill(this.pathC);
         this.ellipse((float)var4.x, (float)var4.y, 30.0F, 30.0F);
         var6 = ((Iterable)this.myGraph.nodes.keySet()).iterator();

         while(var6.hasNext()) {
            var5 = (String)var6.next();
            var7 = (grapheEtChemins.Node)this.myGraph.nodes.get(var5);
            if (this.myGraph.isLink(var3, var5) && this.path.indexOf(var5) > -1) {
               this.strokeWeight(15.0F);
               this.stroke(this.pathC);
               this.line((float)var4.x, (float)var4.y, (float)var7.x, (float)var7.y);
               this.strokeWeight(1.0F);
               this.noStroke();
            }
         }
      }

      Iterator var11 = ((Iterable)this.myGraph.nodes.keySet()).iterator();

      while(var11.hasNext()) {
         String var12 = (String)var11.next();
         var4 = (grapheEtChemins.Node)this.myGraph.nodes.get(var12);
         this.fill(255.0F, 150.0F, 0.0F);
         this.stroke(0);
         this.strokeWeight(1.0F);
         this.ellipse((float)var4.x, (float)var4.y, 20.0F, 20.0F);
         this.noStroke();
         this.textFont(this.font, (float)(this.width * 14 / 800));
         this.text(var4.n, (float)(var4.x + 12), (float)var4.y);
         var6 = ((Iterable)this.myGraph.nodes.keySet()).iterator();

         while(var6.hasNext()) {
            var5 = (String)var6.next();
            if (this.myGraph.isLink(var12, var5) && !var12.equals(var5)) {
               var7 = (grapheEtChemins.Node)this.myGraph.nodes.get(var5);
               double var8 = this.myGraph.getLink(var12, var5);
               this.fill(0);
               this.strokeWeight(3.0F);
               this.stroke(0);
               this.line((float)var4.x, (float)var4.y, (float)var7.x, (float)var7.y);
               this.noStroke();
               if (this.info) {
                  this.textFont(this.font, (float)(this.width * 10 / 800));
                  this.fill(180);
                  this.text(" " + (float)var8, (float)abs((var4.x + var7.x) / 2), (float)(abs((var4.y + var7.y) / 2) + 10));
               }

               this.strokeWeight(1.0F);
            }
         }
      }

   }

   public void mousePressed() {
      this.path.clear();
      this.mouseDown = true;
      if (this.mouseButton == 39) {
         if (this.indN < this.listN.length) {
            println("ind " + this.indN);
            this.myGraph.addNode(this.listN[this.indN], this.mouseX, this.mouseY);
            ++this.indN;
         } else {
            this.indN = 0;
            this.myGraph.addNode(this.listN[this.indN], this.mouseX, this.mouseY);
            ++this.indN;
         }
      }

      if (this.mouseButton == 3 && this.myGraph.nodes.size() != 0) {
         this.firstSelect = this.myGraph.getClosestNode((float)this.mouseX, (float)this.mouseY);
      }

      if (this.keyPressed) {
         if (this.key == 'b' && this.myGraph.nodes.size() > 1) {
            this.pathSelect = true;
            this.start = this.myGraph.getClosestNode((float)this.mouseX, (float)this.mouseY);
         }

         if (this.key == 'e' && this.start != null) {
            this.pathSelect = true;
            this.end = this.myGraph.getClosestNode((float)this.mouseX, (float)this.mouseY);
            println("start: " + this.start + " // end: " + this.end);
            if (this.end != this.start) {
               this.myGraph.dijkstra(this.start, this.end);
            }

            this.start = null;
            this.end = null;
         }

         if (this.key == 'd') {
            this.path.clear();
            if (this.myGraph.nodes.size() != 0) {
               String var1 = this.myGraph.getClosestNode((float)this.mouseX, (float)this.mouseY);
               this.myGraph.removeNode(var1);
            }
         }
      }

   }

   public void mouseReleased() {
      this.mouseDown = false;
      if (this.firstSelect != null) {
         this.secondSelect = this.myGraph.getClosestNode((float)this.mouseX, (float)this.mouseY);
         if (this.secondSelect != this.firstSelect) {
            if (this.myGraph.isLink(this.firstSelect, this.secondSelect)) {
               this.myGraph.removeLink(this.firstSelect, this.secondSelect);
            } else {
               this.myGraph.addLink(this.firstSelect, this.secondSelect);
            }
         }
      }

      this.firstSelect = null;
      this.secondSelect = null;
   }

   public void keyPressed() {
      String var1;
      Iterator var2;
      String var3;
      Iterator var4;
      if (this.key == 's') {
         this.path.clear();
         var2 = ((Iterable)this.myGraph.nodes.keySet()).iterator();

         while(var2.hasNext()) {
            var1 = (String)var2.next();
            var4 = ((Iterable)this.myGraph.nodes.keySet()).iterator();

            while(var4.hasNext()) {
               var3 = (String)var4.next();
               if (this.myGraph.isLink(var1, var3) && !var1.equals(var3)) {
                  this.myGraph.removeLink(var1, var3);
               }
            }
         }
      }

      if (this.key == 'm') {
         this.path.clear();
         var1 = this.myGraph.getClosestNode((float)this.mouseX, (float)this.mouseY);
         this.myGraph.addNode(var1, this.mouseX, this.mouseY);
         Iterator var9 = ((Iterable)this.myGraph.nodes.keySet()).iterator();

         while(var9.hasNext()) {
            String var7 = (String)var9.next();
            if (this.myGraph.isLink(var1, var7) && !var1.equals(var7)) {
               this.myGraph.removeLink(var1, var7);
               this.myGraph.addLink(var1, var7);
            }
         }
      }

      int var10;
      if (this.key == 'a') {
         this.path.clear();

         for(int var6 = 0; var6 < this.listN.length; ++var6) {
            int var8 = (int)this.random((float)(this.width - 100)) + 50;
            var10 = (int)this.random((float)(this.height - 100)) + 50;
            this.myGraph.addNode(this.listN[var6], var8, var10);
         }
      }

      if (this.key == 'l') {
         var2 = ((Iterable)this.myGraph.nodes.keySet()).iterator();

         while(var2.hasNext()) {
            var1 = (String)var2.next();
            var4 = ((Iterable)this.myGraph.nodes.keySet()).iterator();

            while(var4.hasNext()) {
               var3 = (String)var4.next();
               if (!this.myGraph.isLink(var1, var3)) {
                  this.myGraph.addLink(var1, var3);
               }
            }
         }
      }

      if (this.key == 'r' && this.myGraph.nodes.size() == this.listN.length) {
         var2 = ((Iterable)this.myGraph.nodes.keySet()).iterator();

         while(var2.hasNext()) {
            var1 = (String)var2.next();
            var4 = ((Iterable)this.myGraph.nodes.keySet()).iterator();

            while(var4.hasNext()) {
               var3 = (String)var4.next();
               if (this.myGraph.isLink(var1, var3) && !var1.equals(var3)) {
                  this.myGraph.removeLink(var1, var3);
               }
            }
         }

         var2 = ((Iterable)this.myGraph.nodes.keySet()).iterator();

         while(var2.hasNext()) {
            var1 = (String)var2.next();
            var10 = 0;
            var4 = null;

            while(var10 < this.listN.length / 4) {
               int var5 = (int)this.random((float)this.listN.length);
               String var11 = this.listN[var5];
               if (!this.myGraph.isLink(var1, var11)) {
                  ++var10;
                  this.myGraph.addLink(var1, var11);
               }
            }
         }
      }

      if (this.key == 'i') {
         if (this.info) {
            this.info = false;
         } else {
            this.info = true;
         }
      }

   }

   public void keyReleased() {
      if (this.key == 'p') {
         this.pathSelect = false;
      }

   }

   public static void main(String[] var0) {
      PApplet.main(new String[]{"--bgcolor=#DFDFDF", "processingapplets.grapheEtChemins"});
   }

   class Graph {
      HashMap nodes = new HashMap();

      public void addNode(String var1, int var2, int var3) {
         if (this.nodes.containsKey(var1)) {
            grapheEtChemins.Node var4 = (grapheEtChemins.Node)this.nodes.get(var1);
            if (var4 != null) {
               var4.moveTo(var2, var3);
            }
         } else {
            this.nodes.put(var1, grapheEtChemins.this.new Node(var1, var2, var3));
         }

      }

      public grapheEtChemins.Node getNode(String var1) {
         grapheEtChemins.Node var2 = (grapheEtChemins.Node)this.nodes.get(var1);
         return var2;
      }

      public String getClosestNode(float var1, float var2) {
         float var3 = 9999.0F;
         grapheEtChemins.Node var10000 = (grapheEtChemins.Node)this.nodes.get(grapheEtChemins.this.listN[0]);
         String var4 = null;
         if (this.nodes.size() != 0) {
            Iterator var6 = ((Iterable)this.nodes.keySet()).iterator();

            while(var6.hasNext()) {
               String var5 = (String)var6.next();
               grapheEtChemins.Node var7 = (grapheEtChemins.Node)this.nodes.get(var5);
               float var8 = grapheEtChemins.dist(var1, var2, (float)var7.x, (float)var7.y);
               if (var8 < var3) {
                  var3 = var8;
                  var4 = var5;
               }
            }
         }

         return var4;
      }

      public void removeNode(String var1) {
         grapheEtChemins.Node var2 = (grapheEtChemins.Node)this.nodes.get(var1);
         if (var2 != null) {
            Iterator var4 = ((Iterable)this.nodes.keySet()).iterator();

            while(var4.hasNext()) {
               String var3 = (String)var4.next();
               this.removeLink(var1, var3);
            }

            this.nodes.remove(var1);
         }

      }

      public String[] getAllNodes() {
         String[] var1 = new String[50];
         int var2 = 0;

         for(Iterator var4 = ((Iterable)this.nodes.keySet()).iterator(); var4.hasNext(); ++var2) {
            String var3 = (String)var4.next();
            var1[var2] = var3;
         }

         return var1;
      }

      public String[] getNodes(String var1) {
         String[] var2 = new String[10];
         grapheEtChemins.Node var3 = (grapheEtChemins.Node)this.nodes.get(var1);
         if (var3 != null) {
            int var4 = 0;
            Iterator var6 = ((Iterable)this.nodes.keySet()).iterator();

            while(var6.hasNext()) {
               String var5 = (String)var6.next();
               if (this.isLink(var1, var5)) {
                  var2[var4] = var5;
                  ++var4;
               }
            }
         }

         return var2;
      }

      public void addLink(String var1, String var2, double var3) {
         if (!var1.equals(var2)) {
            grapheEtChemins.Node var5 = (grapheEtChemins.Node)this.nodes.get(var1);
            grapheEtChemins.Node var6 = (grapheEtChemins.Node)this.nodes.get(var2);
            if (var5 != null && var6 != null) {
               var5.links.put(var2, grapheEtChemins.this.new Link(var1, var2, var3));
               var6.links.put(var1, grapheEtChemins.this.new Link(var2, var1, var3));
            }
         }

      }

      public void addLink(String var1, String var2) {
         if (!var1.equals(var2)) {
            grapheEtChemins.Node var3 = (grapheEtChemins.Node)this.nodes.get(var1);
            grapheEtChemins.Node var4 = (grapheEtChemins.Node)this.nodes.get(var2);
            if (var3 != null && var4 != null) {
               double var5 = (double)(PVector.dist(var3.position, var4.position) / 100.0F);
               var3.links.put(var2, grapheEtChemins.this.new Link(var1, var2, var5));
               var4.links.put(var1, grapheEtChemins.this.new Link(var2, var1, var5));
            }
         }

      }

      public void removeLink(String var1, String var2) {
         grapheEtChemins.Node var3 = (grapheEtChemins.Node)this.nodes.get(var1);
         grapheEtChemins.Node var4 = (grapheEtChemins.Node)this.nodes.get(var2);
         if (var3 != null && var4 != null && this.isLink(var1, var2)) {
            var3.links.remove(var2);
            var4.links.remove(var1);
         }

      }

      public boolean isLink(String var1, String var2) {
         grapheEtChemins.Node var3 = (grapheEtChemins.Node)this.nodes.get(var1);
         grapheEtChemins.Node var4 = (grapheEtChemins.Node)this.nodes.get(var2);
         if (var3 != null && var4 != null) {
            boolean var5 = false;
            Iterator var7 = ((Iterable)var3.links.keySet()).iterator();

            String var6;
            while(var7.hasNext()) {
               var6 = (String)var7.next();
               if (var6.equals(var2)) {
                  var5 = true;
               }
            }

            var7 = ((Iterable)var4.links.keySet()).iterator();

            while(var7.hasNext()) {
               var6 = (String)var7.next();
               if (var6.equals(var1)) {
                  var5 = true;
               }
            }

            return var5;
         } else {
            return false;
         }
      }

      public double getLink(String var1, String var2) {
         double var3 = 0.0D;
         if (this.isLink(var1, var2)) {
            grapheEtChemins.Node var5 = (grapheEtChemins.Node)this.nodes.get(var1);
            grapheEtChemins.Link var6 = (grapheEtChemins.Link)var5.links.get(var2);
            var3 = var6.p;
         }

         return var3;
      }

      public void dijkstra(String var1, String var2) {
         if (this.nodes.get(var1) != null && this.nodes.get(var2) != null) {
            grapheEtChemins.this.path.clear();
            Iterator var4 = ((Iterable)this.nodes.keySet()).iterator();

            grapheEtChemins.Node var5;
            while(var4.hasNext()) {
               String var3 = (String)var4.next();
               var5 = (grapheEtChemins.Node)this.nodes.get(var3);
               var5.init();
            }

            grapheEtChemins.this.opened.clear();
            grapheEtChemins.this.closed.clear();
            grapheEtChemins.this.opened.add(var1);
            grapheEtChemins.Node var10 = (grapheEtChemins.Node)this.nodes.get(var1);
            var10.g = 0.0D;

            String var11;
            while(grapheEtChemins.this.opened.size() > 0) {
               var11 = (String)grapheEtChemins.this.opened.remove(0);
               grapheEtChemins.this.closed.add(var11);
               if (var11 == var2) {
                  break;
               }

               var5 = (grapheEtChemins.Node)this.nodes.get(var11);
               Iterator var7 = ((Iterable)var5.links.keySet()).iterator();

               while(var7.hasNext()) {
                  String var6 = (String)var7.next();
                  grapheEtChemins.Node var8 = (grapheEtChemins.Node)this.nodes.get(var6);
                  grapheEtChemins.Link var9 = (grapheEtChemins.Link)var5.links.get(var6);
                  if (var8.walkable && !this.arrayListContains(grapheEtChemins.this.closed, var6)) {
                     if (!this.arrayListContains(grapheEtChemins.this.opened, var6)) {
                        grapheEtChemins.this.opened.add(var6);
                        var8.parent = var5;
                        var8.setG(var9);
                     } else if (var8.g > var5.g + var9.p) {
                        var8.parent = var5;
                        var8.setG(var9);
                     }
                  }
               }
            }

            var11 = var2;

            double var12;
            grapheEtChemins.Node var13;
            for(var12 = 0.0D; !var11.equals(var1); var11 = var13.n) {
               grapheEtChemins.this.path.add(var11);
               var13 = (grapheEtChemins.Node)this.nodes.get(var11);
               var12 += var13.g;
               grapheEtChemins.println(var12);
               var13 = var13.parent;
            }

            grapheEtChemins.this.path.add(var1);
            var13 = (grapheEtChemins.Node)this.nodes.get(var1);
            var12 += var13.g;
            grapheEtChemins.println(var12);
         }
      }

      public boolean arrayListContains(ArrayList var1, String var2) {
         for(int var3 = 0; var3 < var1.size(); ++var3) {
            String var4 = (String)var1.get(var3);
            if (var4 == var2) {
               return true;
            }
         }

         return false;
      }
   }

   class HScrollbar {
      int swidth;
      int sheight;
      int xpos;
      int ypos;
      float spos;
      float newspos;
      int sposMin;
      int sposMax;
      int loose;
      boolean over;
      boolean locked;
      boolean show = false;
      float ratio;

      HScrollbar(int var2, int var3, int var4, int var5, int var6) {
         this.swidth = var4;
         this.sheight = var5;
         int var7 = var4 - var5;
         this.ratio = (float)var4 / (float)var7;
         this.xpos = var2 - 3 * this.swidth / 4;
         this.ypos = var3 - this.sheight / 2;
         this.spos = (float)this.xpos;
         this.newspos = this.spos;
         this.sposMin = this.xpos;
         this.sposMax = this.xpos + this.swidth - this.sheight;
         this.loose = var6;
      }

      public void update() {
         if (this.over()) {
            this.newspos = (float)(grapheEtChemins.this.width / 2);
         } else {
            this.newspos = 0.0F;
         }

         if (grapheEtChemins.abs(this.newspos - this.spos) > 1.0F) {
            this.spos += (this.newspos - this.spos) / (float)this.loose;
         }

      }

      public boolean over() {
         return grapheEtChemins.this.mouseX > this.xpos && grapheEtChemins.this.mouseX < this.xpos + this.swidth && grapheEtChemins.this.mouseY > this.ypos && grapheEtChemins.this.mouseY < this.ypos + this.sheight;
      }

      public void display() {
         grapheEtChemins.this.fill(255);
         grapheEtChemins.this.strokeWeight(0.1F);
         if (this.over()) {
            grapheEtChemins.this.fill(0.0F, 40.0F, 63.0F);
         } else {
            grapheEtChemins.this.fill(255.0F, 150.0F, 0.0F);
         }

         grapheEtChemins.this.rect((float)(this.sheight / 2), (float)this.ypos, (float)(this.sheight * 5), (float)this.sheight);
         grapheEtChemins.this.fill(130);
         grapheEtChemins.this.textFont(grapheEtChemins.this.font, 11.0F);
         grapheEtChemins.this.text("I N F O >>>", (float)(this.sheight / 2 + this.sheight / 5), (float)(this.ypos + 4 * this.sheight / 5));
      }

      public float getPos() {
         return this.spos * this.ratio;
      }
   }

   class Link {
      String n0;
      String n1;
      double p;

      Link(String var2, String var3, double var4) {
         this.n0 = var2;
         this.n1 = var3;
         this.p = var4;
      }
   }

   class Node {
      int x;
      int y;
      String n;
      grapheEtChemins.Node parent = null;
      double g = 0.0D;
      boolean walkable = true;
      HashMap links;
      PVector position;

      Node(String var2, int var3, int var4) {
         this.x = var3;
         this.y = var4;
         this.n = var2;
         this.position = new PVector((float)this.x, (float)this.y);
         this.links = new HashMap();
      }

      public void moveTo(int var1, int var2) {
         this.x = var1;
         this.y = var2;
         this.position = new PVector((float)this.x, (float)this.y);
      }

      public void init() {
         this.parent = null;
         this.g = 0.0D;
      }

      public void setG(grapheEtChemins.Link var1) {
         this.g = this.parent.g + var1.p;
      }
   }
}
