package processingapplets;

import ddf.minim.AudioInput;
import ddf.minim.AudioOutput;
import ddf.minim.AudioPlayer;
import ddf.minim.Minim;
import ddf.minim.analysis.FFT;
import ddf.minim.effects.LowPassSP;
import ddf.minim.signals.SawWave;
import ddf.minim.signals.SineWave;
import ddf.minim.signals.SquareWave;
import ddf.minim.signals.WhiteNoise;
import java.awt.Frame;
import java.util.Arrays;
import processing.core.PApplet;
import processing.core.PFont;
import processing.opengl.PGraphics3D;
import processing.core.PImage;

public class exploSonore extends PApplet {
   Minim minim;
   AudioInput in;
   AudioOutput out;
   AudioPlayer player;
   FFT fft;
   int w;
   PImage fade;
   float rWidth;
   float rHeight;
   Frame frame;
   int myOr = this.color(255, 100, 0);
   int myRed = this.color(255, 0, 0);
   int myBlue = this.color(153);
   int width_;
   int height_;
   boolean isOpen;
   int buttonValue = 1;
   exploSonore.TextButton[] T1 = new exploSonore.TextButton[8];
   boolean locked = false;
   String[] ListN = new String[]{"sinus", "carré", "scie", "bruit", " extrait ", " extrait filtré ", " S T O P ", " - Info - "};
   String sig = "null";
   int count = 0;
   exploSonore.signal signal1;
   exploSonore.signal signal2;
   exploSonore.signal signal3;
   exploSonore.record record1;
   PFont f;
   int c = 0;

   public void setup() {
      this.frameRate(60.0F);
      this.frame = new Frame();
      this.f = this.createFont("Arial Bold", 14.0F, true);
      this.size(800, 600, "processing.core.PGraphics3D");
      this.frame.setTitle("A N A L Y S E   D U   C O N T E N U   F R E Q U E N T I E L");
      this.minim = new Minim(this);
      this.in = this.minim.getLineIn(2, 512);
      this.out = this.minim.getLineOut(2);
      this.signal1 = new exploSonore.signal();
      this.signal2 = new exploSonore.signal();
      this.signal3 = new exploSonore.signal();
      this.record1 = new exploSonore.record();
      this.launchFFT();
      Arrays.fill(((PGraphics3D)this.g).zbuffer, Float.MAX_VALUE);
      this.fill(0);
      this.rect(0.0F, (float)(this.height - 100), (float)this.width, 100.0F);
      this.fill(this.myBlue);

      for(int var1 = 0; var1 < this.T1.length; ++var1) {
         if (var1 < 6) {
            this.T1[var1] = new exploSonore.TextButton(5 + var1 % 4 * 2 * this.width / 3 / this.T1.length, this.height - 100 + 60 * PApplet.parseInt((float)(var1 / 4)), 60 + PApplet.parseInt((float)(var1 / 5)) * 35, 25, this.color(255), this.myBlue, this.myRed, this.myOr, this.ListN[var1]);
         } else if (var1 == 6) {
            this.T1[var1] = new exploSonore.TextButton(5 + var1 % 4 * 2 * this.width / 3 / this.T1.length + 60, this.height - 40, 70, 30, this.color(255), this.myRed, this.myOr, this.color(0), this.ListN[var1]);
         } else {
            this.T1[var1] = new exploSonore.TextButton(5 + var1 % 4 * 2 * this.width / 3 / this.T1.length + this.width / 2 + 120, this.height - 155, 60, 25, this.color(255), this.color(0), this.myOr, this.color(0), this.ListN[var1]);
         }
      }

      this.width_ = this.frame.getWidth();
      this.height_ = this.frame.getHeight();
   }

   public void draw() {
      this.background(0);
      this.pushMatrix();
      if (this.signal1.sounding) {
         ++this.c;
         this.fft = new FFT(this.out.bufferSize(), this.out.sampleRate());
         this.fft.logAverages(60, 6 * this.width / 640);
         this.drawFFT("out");
         if (this.c % 2 == 0) {
            this.drawSignal("out");
         }
      } else if (this.record1.sounding) {
         this.fft = new FFT(this.player.bufferSize(), this.player.sampleRate());
         this.fft.logAverages(60, 6 * this.width / 640);
         this.drawFFT("player");
         this.drawSignal("player");
      } else {
         this.fft = new FFT(this.in.bufferSize(), this.in.sampleRate());
         this.fft.logAverages(60, 6 * this.width / 640);
         this.drawFFT("in");
         this.drawSignal("in");
      }

      this.popMatrix();
      Arrays.fill(((PGraphics3D)this.g).zbuffer, Float.MAX_VALUE);
      this.fill(0);
      this.rect(0.0F, (float)(this.height - 150), (float)this.width, 150.0F);
      this.fill(this.myBlue);
      this.textFont(this.f, 12.0F);
      this.text("S I G N A U X  N U M E R I Q U E S", 6.0F, (float)(this.height - 125));
      this.text("E N R E G I S T R E M E N T", 6.0F, (float)(this.height - 65));
      this.update(this.mouseX, this.mouseY);

      for(int var1 = 0; var1 < this.T1.length; ++var1) {
         this.T1[var1].display();
      }

      this.myInfo();
   }

   public void keyPressed() {
      if (this.key == '0') {
         this.signal1.setSignal("sinus", 1000.0F, 0.0F, true);
      }

      if (this.key == '1') {
         this.signal1.setSignal("sinus", 1000.0F, 0.0F, true);
         this.signal2.setSignal("sinus", 4000.0F, 0.0F, true);
      }

      if (this.key == '2') {
         this.signal1.setSignal("carré", 1000.0F, 0.0F, true);
      }

      if (this.key == '3') {
         this.signal1.setSignal("scie", 1000.0F, 0.0F, true);
      }

      if (this.key == '4') {
         this.signal1.setSignal("bruit", 1000.0F, 0.0F, true);
      }

      if (this.key == 'e') {
         this.record1.setRecord("org/javascool/proglets/processingapplets.exploSonore/Ahmed_Ex2.wav");
      }

      if (this.key == 'f') {
         this.record1.setFilter("org/javascool/proglets/processingapplets.exploSonore/Ahmed_Ex2.wav", 500.0F);
      }

      if (this.key == 's') {
         this.StopAnySound();
      }

   }

   public void update(int var1, int var2) {
      int var3;
      if (!this.locked) {
         for(var3 = 0; var3 < this.T1.length; ++var3) {
            this.T1[var3].update();
         }
      } else {
         this.locked = false;
      }

      if (this.mousePressed) {
         for(var3 = 0; var3 < this.T1.length; ++var3) {
            if (this.T1[var3].pressed() && !this.T1[var3].select) {
               this.T1[var3].select = true;
               if (var3 < 4) {
                  this.signal1.setSignal(this.T1[var3].value, 1000.0F, 0.0F, true);
               } else if (var3 == 4) {
                  this.record1.setRecord("org/javascool/proglets/processingapplets.exploSonore/Ahmed_Ex2.wav");
               } else if (var3 == 5) {
                  this.record1.setRecord("org/javascool/proglets/processingapplets.exploSonore/Ahmed_Ex2.wav");
                  this.record1.applyFilter();
               } else if (var3 == 6) {
                  this.StopAnySound();
               }

               for(int var4 = 0; var4 < this.T1.length - 1; ++var4) {
                  if (var4 != var3) {
                     this.T1[var4].select = false;
                  }
               }
            }
         }
      }

   }

   public void myInfo() {
      if (this.T1[7].over()) {
         this.fill(255);
         this.rect(0.0F, (float)(this.height - 145), (float)this.width, 130.0F);
         this.fill(this.myOr);
         this.text(" Parles, siffles, chuchotes.., et tu verras ce qui se passe sur l'analyseur de contenu sonore.. \n Tu peux aussi jouer une signal ou un enregistrement de ton choix. \n Pour ajuster la fréquence et l'amplitude du signal, bouges la souris sur la fenetre de l'analyseur. \n Pour faire varier le volume de l'enregistrement sonore, tu peux procéder pareillement, \n tandis que le contenu fréquentiel peut s'ajuster par un filtre. Expérimentes! \n ", 50.0F, (float)(this.height - 110));
      }

   }

   public void stop() {
      this.out.close();
      if (this.count != 0) {
         this.record1.ferme();
      }

      this.minim.stop();
      super.stop();
   }

   public void mouseMoved() {
      if (this.signal1.sounding) {
         this.signal1.changeValue();
         this.signal1.printV();
      } else if (this.record1.sounding) {
         this.record1.changeValue();
         this.record1.printV();
      }

   }

   public void StopAnySound() {
      if (this.signal1.sounding) {
         this.signal1.switchOff();
         this.sig = "null";
      }

      if (this.record1.sounding) {
         this.record1.switchOff();
      }

      if (this.signal2.sounding) {
         this.signal2.switchOff();
      }

      if (this.signal3.sounding) {
         this.signal3.switchOff();
      }

   }

   public void launchFFT() {
      if (this.in != null) {
         this.fft = new FFT(this.in.bufferSize(), this.in.sampleRate());
         this.stroke(0);
         this.fft.logAverages(60, 6);
         this.w = this.width / this.fft.avgSize();
         this.strokeWeight((float)this.w);
         this.strokeCap(1);
         this.background(0);
         this.fade = this.get(0, 0, this.width, this.height);
         this.rWidth = (float)this.width * 0.99F;
         this.rHeight = (float)this.height * 0.99F;
      }

   }

   public void drawFFT(String var1) {
      this.strokeWeight(10.0F);
      this.tint(250, 250.0F);
      this.image(this.fade, ((float)this.width - this.rWidth) / 2.0F, ((float)this.height - this.rHeight) / 2.0F, this.rWidth, this.rHeight);
      this.noTint();
      float var2;
      if (var1.equals("out")) {
         this.fft.forward(this.out.mix);
         var2 = this.signal1.volume + 20.0F;
      } else if (var1.equals("player")) {
         this.fft.forward(this.player.mix);
         var2 = this.record1.volume + 20.0F;
      } else {
         this.fft.forward(this.in.mix);
         var2 = 20.0F;
      }

      this.stroke(240.0F, 240.0F, 240.0F);

      int var3;
      for(var3 = 0; var3 < this.fft.avgSize(); ++var3) {
         this.line((float)(var3 * this.w + this.w / 2), (float)(2 * this.height / 3), (float)(var3 * this.w + this.w / 2), (float)(2 * this.height / 3) - this.fft.getAvg(var3) * var2);
      }

      this.fade = this.get(0, 0, this.width, this.height);
      this.stroke(250.0F, 70.0F, 0.0F);
      this.textFont(this.f, 14.0F);
      this.fill(255);
      this.text("100", (float)(this.width / 10), (float)(2 * this.height / 3 + this.height / 30));
      this.text("125", (float)(this.width / 6), (float)(2 * this.height / 3 + this.height / 30));
      this.text("250", (float)(this.width / 5 + this.width / 12), (float)(2 * this.height / 3 + this.height / 30));
      this.text("500", (float)(this.width / 2 - this.width / 12), (float)(2 * this.height / 3 + this.height / 30));
      this.text("1000", (float)(this.width / 2 + this.width / 36), (float)(2 * this.height / 3 + this.height / 30));
      this.text("2000", (float)(2 * this.width / 3 - this.width / 100), (float)(2 * this.height / 3 + this.height / 30));
      this.text("4000", (float)(4 * this.width / 5 - this.width / 50), (float)(2 * this.height / 3 + this.height / 30));
      this.text("8000  Hz", (float)(this.width - this.width / 10), (float)(2 * this.height / 3 + this.height / 30));

      for(var3 = 0; var3 < this.fft.avgSize(); ++var3) {
         this.line((float)(var3 * this.w + this.w / 2), (float)(2 * this.height / 3), (float)(var3 * this.w + this.w / 2), (float)(2 * this.height / 3) - this.fft.getAvg(var3) * var2);
      }

      this.strokeWeight(0.5F);
   }

   public void drawSignal(String var1) {
      this.stroke(255);
      this.strokeWeight(1.0F);
      int var2;
      if (var1.equals("out")) {
         var2 = this.out.bufferSize();
      } else if (var1.equals("player")) {
         var2 = this.player.bufferSize();
      } else {
         var2 = this.in.bufferSize();
      }

      for(int var3 = 0; var3 < var2 - 1; ++var3) {
         float var4 = map((float)var3, 0.0F, (float)var2, 0.0F, (float)this.width);
         float var5 = map((float)(var3 + 1), 0.0F, (float)var2, 0.0F, (float)this.width);
         if (var1.equals("out")) {
            this.line(var4, 40.0F + this.out.left.get(var3) * (this.signal1.volume + 20.0F) * 5.0F, var5, 40.0F + this.out.left.get(var3 + 1) * (this.signal1.volume + 20.0F) * 5.0F);
            this.line(var4, 120.0F + this.out.right.get(var3) * (this.signal1.volume + 20.0F) * 5.0F, var5, 120.0F + this.out.right.get(var3 + 1) * (this.signal1.volume + 20.0F) * 5.0F);
         } else if (var1.equals("player")) {
            this.line(var4, 40.0F + this.player.left.get(var3) * (this.record1.volume + 20.0F) * 3.0F, var5, 40.0F + this.player.left.get(var3 + 1) * (this.record1.volume + 20.0F) * 3.0F);
            this.line(var4, 120.0F + this.player.right.get(var3) * (this.record1.volume + 20.0F) * 3.0F, var5, 120.0F + this.player.right.get(var3 + 1) * (this.record1.volume + 20.0F) * 3.0F);
         } else {
            this.line(var4, 40.0F + this.in.left.get(var3) * 100.0F, var5, 40.0F + this.in.left.get(var3 + 1) * 100.0F);
            this.line(var4, 120.0F + this.in.right.get(var3) * 100.0F, var5, 120.0F + this.in.right.get(var3 + 1) * 100.0F);
         }
      }

      this.strokeWeight(0.5F);
   }

   public static void main(String[] var0) {
      PApplet.main(new String[]{"--bgcolor=#DFDFDF", "processingapplets.exploSonore"});
   }

   class Button {
      int x;
      int y;
      int L;
      int h;
      int basecolor;
      int highlightcolor;
      int selectcolor;
      int currentcolor;
      int fcolor;
      String value;
      boolean over = false;
      boolean pressed = false;
      boolean select = false;

      public void update() {
         if (this.over()) {
            this.currentcolor = this.highlightcolor;
         } else if (this.select) {
            this.currentcolor = this.selectcolor;
         } else {
            this.currentcolor = this.basecolor;
         }

      }

      public boolean pressed() {
         if (this.over) {
            exploSonore.this.locked = true;
            return true;
         } else {
            exploSonore.this.locked = false;
            return false;
         }
      }

      public boolean over() {
         return true;
      }

      public boolean overText(int var1, int var2, int var3, int var4) {
         return exploSonore.this.mouseX >= var1 && exploSonore.this.mouseX <= var1 + var3 && exploSonore.this.mouseY >= var2 - var4 / 2 && exploSonore.this.mouseY <= var2 + var4 / 2;
      }
   }

   class TextButton extends exploSonore.Button {
      TextButton(int var2, int var3, int var4, int var5, int var6, int var7, int var8, int var9, String var10) {
         super();
         this.x = var2;
         this.y = var3;
         this.L = var4;
         this.h = var5;
         this.fcolor = var6;
         this.basecolor = var7;
         this.highlightcolor = var8;
         this.selectcolor = var9;
         this.currentcolor = this.basecolor;
         this.value = var10;
      }

      public boolean over() {
         if (this.overText(this.x, this.y, this.L, this.h)) {
            this.over = true;
            return true;
         } else {
            this.over = false;
            return false;
         }
      }

      public void display() {
         exploSonore.this.stroke(255);
         exploSonore.this.strokeWeight(1.0F);
         exploSonore.this.fill(this.currentcolor);
         exploSonore.this.rect((float)this.x, (float)(this.y - PApplet.parseInt((float)(this.h / 2 + this.h / 4))), (float)this.L, (float)this.h);
         exploSonore.this.noStroke();
         exploSonore.this.fill(this.fcolor);
         exploSonore.this.textFont(exploSonore.this.f);
         exploSonore.this.text(this.value, (float)(this.x + (this.L - this.value.length() * 6) / 2), (float)this.y);
      }
   }

   class record {
      float volume;
      float Fc = 100.0F;
      LowPassSP lpf;
      boolean sounding = false;
      boolean filtering = false;

      record() {
         this.lpf = new LowPassSP(100.0F, exploSonore.this.out.sampleRate());
      }

      public void setRecord(String var1) {
         if (this.sounding) {
            this.switchOff();
            if (this.filtering) {
               this.removeFilter();
            }
         } else if (exploSonore.this.signal1.sounding) {
            exploSonore.this.signal1.switchOff();
         }

         if (var1 != null) {
            try {
               ++exploSonore.this.count;
               exploSonore.this.player = exploSonore.this.minim.loadFile(var1);
               this.changeValue();
               exploSonore.this.player.loop();
               this.sounding = true;
            } catch (Exception var2) {
            }
         }

      }

      public void applyFilter() {
         if (this.sounding) {
            if (!this.filtering) {
               this.filtering = true;
               exploSonore.this.player.addEffect(this.lpf);
               this.lpf.setFreq(this.Fc);
            } else {
               this.removeFilter();
            }
         }

      }

      public void setFilter(String var1, float var2) {
         if (this.sounding) {
            this.switchOff();
            if (this.filtering) {
               this.removeFilter();
            }
         } else if (exploSonore.this.signal1.sounding) {
            exploSonore.this.signal1.switchOff();
         }

         if (var1 != null) {
            try {
               ++exploSonore.this.count;
               exploSonore.this.player = exploSonore.this.minim.loadFile(var1);
               this.changeValue();
               exploSonore.this.player.loop();
               this.sounding = true;
               if (var2 > 100.0F && var2 < 10000.0F) {
                  exploSonore.this.player.addEffect(this.lpf);
                  this.lpf.setFreq(var2);
                  this.filtering = true;
               }
            } catch (Exception var3) {
            }
         }

      }

      public void removeFilter() {
         this.filtering = false;
         exploSonore.this.player.clearEffects();
      }

      public void changeValue() {
         this.volume = exploSonore.map((float)exploSonore.this.mouseY, 0.0F, (float)exploSonore.this.height, 0.0F, -20.0F);
         exploSonore.this.player.setGain(this.volume);
         if (this.filtering) {
            this.Fc = exploSonore.map((float)exploSonore.this.mouseX, 0.0F, (float)exploSonore.this.width, 500.0F, 5000.0F);
            this.lpf.setFreq(this.Fc);
         }

      }

      public void printV() {
         float var1 = (this.volume + 20.0F) / 20.0F;
         exploSonore.this.fill(0);
         exploSonore.this.rect(0.0F, (float)(exploSonore.this.height - 175), (float)(exploSonore.this.width / 2), 30.0F);
         exploSonore.this.fill(exploSonore.this.myOr);
         exploSonore.this.text("Vol.: " + var1 + " ", 10.0F, (float)(exploSonore.this.height - 155));
         if (this.filtering) {
            exploSonore.this.fill(0);
            exploSonore.this.rect(0.0F, (float)(exploSonore.this.height - 175), (float)(exploSonore.this.width / 2), 30.0F);
            exploSonore.this.fill(exploSonore.this.myOr);
            exploSonore.this.text(" Freq. de coupure: " + this.Fc + " Hz  -  Vol.: " + var1 + " ", 10.0F, (float)(exploSonore.this.height - 155));
         }

      }

      public void switchOff() {
         exploSonore.this.player.pause();
         this.sounding = false;
      }

      public void ferme() {
         exploSonore.this.player.close();
      }
   }

   class signal {
      float volume;
      float frequence;
      SineWave sinus_;
      SquareWave square_;
      SawWave saw_;
      WhiteNoise wnoise_;
      String type;
      boolean sounding = false;
      boolean change = true;

      signal() {
         exploSonore.this.out.sound();
      }

      public void setSignal(String var1, float var2, float var3, boolean var4) {
         this.type = var1;
         if (this.sounding) {
            this.switchOff();
         } else if (exploSonore.this.record1.sounding) {
            exploSonore.this.record1.switchOff();
         }

         this.change = var4;
         if (var1.equals("sinus")) {
            this.sinus_ = new SineWave(var2, var3, exploSonore.this.out.sampleRate());
            this.sinus_.portamento(2000);
            this.changeValue();
            exploSonore.this.out.addSignal(this.sinus_);
         } else if (var1.equals("carré")) {
            this.square_ = new SquareWave(var2, var3, exploSonore.this.out.sampleRate());
            this.square_.portamento(2000);
            this.changeValue();
            exploSonore.this.out.addSignal(this.square_);
         } else if (var1.equals("scie")) {
            this.saw_ = new SawWave(var2, var3, exploSonore.this.out.sampleRate());
            this.saw_.portamento(2000);
            this.changeValue();
            exploSonore.this.out.addSignal(this.saw_);
         } else if (var1.equals("bruit")) {
            this.wnoise_ = new WhiteNoise(var3);
            this.changeValue();
            exploSonore.this.out.addSignal(this.wnoise_);
         }

         this.sounding = true;
      }

      public void changeValue() {
         if (this.change) {
            this.frequence = exploSonore.map((float)exploSonore.this.mouseX, 0.0F, (float)exploSonore.this.width, 100.0F, 4000.0F);
            this.volume = exploSonore.map((float)exploSonore.this.mouseY, 0.0F, (float)exploSonore.this.height, 0.2F, 0.0F);
            if (this.type.equals("sinus")) {
               this.sinus_.setFreq(this.frequence);
               this.sinus_.setAmp(this.volume);
            } else if (this.type.equals("carré")) {
               this.square_.setFreq(this.frequence);
               this.square_.setAmp(this.volume);
            } else if (this.type.equals("scie")) {
               this.saw_.setFreq(this.frequence);
               this.saw_.setAmp(this.volume);
            } else if (this.type.equals("bruit")) {
               this.wnoise_.setAmp(this.volume);
            }
         }

      }

      public void printV() {
         float var1 = this.volume / 0.4F;
         exploSonore.this.fill(0);
         exploSonore.this.rect(0.0F, (float)(exploSonore.this.height - 175), (float)(exploSonore.this.width / 2), 30.0F);
         exploSonore.this.fill(exploSonore.this.myOr);
         if (this.type.equals("bruit")) {
            exploSonore.this.text("Vol.: " + var1 + " ", 10.0F, (float)(exploSonore.this.height - 155));
         } else {
            exploSonore.this.text(" Freq.: " + this.frequence + " Hz  -  Vol.: " + var1 + " ", 10.0F, (float)(exploSonore.this.height - 155));
         }

      }

      public void switchOff() {
         exploSonore.this.out.noSound();
         exploSonore.this.out.clearSignals();
         this.sounding = false;
         this.change = true;
      }
   }
}
