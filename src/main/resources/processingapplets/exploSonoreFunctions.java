package processingapplets;

import org.javascool.macros.Macros;

public class exploSonoreFunctions {
   private static final long serialVersionUID = 1L;

   private static exploSonore getPanel() {
      return (exploSonore)Macros.getProgletPane();
   }

   public static void playSignal(int var0, String var1, double var2, double var4) {
      switch(var0) {
      case 1:
         getPanel().signal1.setSignal(var1, (float)var2, (float)var4, false);
         break;
      case 2:
         getPanel().signal2.setSignal(var1, (float)var2, (float)var4, false);
         break;
      case 3:
         getPanel().signal3.setSignal(var1, (float)var2, (float)var4, false);
      }

   }

   public static void playRecord(String var0, double var1) {
      getPanel().record1.setRecord(var0);
      getPanel().record1.setFilter(var0, (float)var1);
   }

   public static void playStop() {
      getPanel().StopAnySound();
   }
}
