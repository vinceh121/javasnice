package processingapplets;

import java.math.BigInteger;

public class cryptageRSAFunctions {
   public static BigInteger[] createKeys() {
      return cryptageRSA.createKeys();
   }

   public static BigInteger encrypt(String var0, BigInteger var1, BigInteger var2) {
      return cryptageRSA.encrypt(var0, var1, var2);
   }

   public static String decrypt(BigInteger var0, BigInteger[] var1) {
      return cryptageRSA.decrypt(var0, var1);
   }
}
