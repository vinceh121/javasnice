package processingapplets;

import org.javascool.macros.Macros;

public class grapheEtCheminsFunctions {
   private static final long serialVersionUID = 1L;

   private static grapheEtChemins getPane() {
      return (grapheEtChemins)Macros.getProgletPane();
   }

   public static void addNode(String var0, int var1, int var2) {
      getPane().myGraph.addNode(var0, var1, var2);
   }

   public static String getClosestNode(double var0, double var2) {
      String var4 = null;
      var4 = getPane().myGraph.getClosestNode((float)var0, (float)var2);
      return var4;
   }

   public static void removeNode(String var0) {
      getPane().myGraph.removeNode(var0);
   }

   public static String[] getAllNodes() {
      String[] var0 = new String[50];
      var0 = getPane().myGraph.getAllNodes();
      return var0;
   }

   public static String[] getLinkedNodes(String var0) {
      String[] var1 = new String[50];
      var1 = getPane().myGraph.getNodes(var0);
      return var1;
   }

   public static void addLink(String var0, String var1, double var2) {
      getPane().myGraph.addLink(var0, var1, var2);
   }

   public static void removeLink(String var0, String var1) {
      getPane().myGraph.removeLink(var0, var1);
   }

   public static boolean isLink(String var0, String var1) {
      boolean var2 = false;
      var2 = getPane().myGraph.isLink(var0, var1);
      return var2;
   }

   public static double getLink(String var0, String var1) {
      double var2 = 0.0D;
      var2 = getPane().myGraph.getLink(var0, var1);
      return var2;
   }

   public static void dijkstra(String var0, String var1) {
      getPane().myGraph.dijkstra(var0, var1);
   }
}
