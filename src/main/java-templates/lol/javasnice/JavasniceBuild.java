package lol.javasnice;

import java.lang.reflect.Field;

public final class JavasniceBuild {
	public static final String VERSION = "${project.version}", GIT_COMMIT = "${git.commit.id.abbrev}",
			GIT_COMMIT_DATE = "${git.commit.time}", GIT_TAG = "${git.tags}";

	public static String buildOptions() {
		final StringBuilder sb = new StringBuilder();
		for (final Field f : JavasniceBuild.class.getDeclaredFields()) {
			sb.append(f.getName());
			sb.append(" = ");
			try {
				sb.append(f.get(""));
			} catch (final IllegalArgumentException | IllegalAccessException e) {
				e.printStackTrace();
				sb.append(e.toString());
			}
			sb.append('\n');
		}
		return sb.toString();
	}
}
