package lol.javasnice;

import java.io.IOException;
import java.text.MessageFormat;
import java.util.Properties;

import javax.swing.JOptionPane;

import org.javascool.tools.UserConfig;

public final class I18N {
	public static final String[] AVAILABLE_LOCALES = new String[] { "en", "fr", "ru", "gcf" };
	private static Properties props = new Properties();
	private static String locale = "en";

	public static void init() {
		I18N.locale = UserConfig.getConf().getString("general.locale", "en");
		if (!I18N.locale.equals("en")) {
			try {
				I18N.props.load(I18N.class.getResourceAsStream("/i18n/" + I18N.locale + ".properties"));
			} catch (final IOException e) {
				e.printStackTrace();
				JOptionPane.showMessageDialog(null, "Could not load localization files");
			}
		}
	}

	public static String gettext(final String key) {
		if ("en".equals(I18N.locale)) {
			return key;
		}
		final String str = I18N.props.getProperty(key);
		return str == null ? key : str;
	}

	public static String ngettext(final String key, final Object... args) {
		return MessageFormat.format(I18N.gettext(key), args);
	}

	public static String getLocale() {
		return I18N.locale;
	}
}
