package lol.javasnice;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JToggleButton;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;

import org.javascool.macros.Macros;
import org.javascool.tools.UserConfig;
import org.javascool.widgets.ToolBar;

public class SettingsFrame extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6849527666830451046L;
	private static SettingsFrame instance = new SettingsFrame();
	private final String[] AVAILABLE_THEMES = new String[] { "/org/fife/ui/rsyntaxtextarea/themes/dark.xml",
			"/org/fife/ui/rsyntaxtextarea/themes/default-alt.xml", "/org/fife/ui/rsyntaxtextarea/themes/default.xml",
			"/org/fife/ui/rsyntaxtextarea/themes/eclipse.xml", "/org/fife/ui/rsyntaxtextarea/themes/idea.xml",
			"/org/fife/ui/rsyntaxtextarea/themes/monokai.xml", "/org/fife/ui/rsyntaxtextarea/themes/vs.xml" };

	private SettingsFrame() {
		this.setSize(new Dimension(669, 354));
		final GridBagLayout layout = new GridBagLayout();
		this.getContentPane().setLayout(layout);
		this.setTitle(I18N.gettext("Settings"));

		final JToggleButton btntglOwo = new JToggleButton("oopsie, this shwouldn happen", OwoPrintStream.isOwo());
		SettingsFrame.setOwoBtnText(btntglOwo);
		btntglOwo.setIcon(Macros.getIcon("icons/owoDisabwed.png"));
		btntglOwo.setDisabledIcon(Macros.getIcon("icons/owoDisabwed.png"));
		btntglOwo.setSelectedIcon(Macros.getIcon("icons/owoEnyabwed.png"));
		btntglOwo.setPressedIcon(Macros.getIcon("icons/owoPwessed.png"));
		btntglOwo.addActionListener(e -> {
			OwoPrintStream.setOwo(btntglOwo.isSelected());
			UserConfig.getConf().setProperty("fun.owo", btntglOwo.isSelected());
			SettingsFrame.setOwoBtnText(btntglOwo);
		});
		final GridBagConstraints gbcTglOwo = new GridBagConstraints();
		gbcTglOwo.insets = new Insets(0, 0, 5, 5);
		gbcTglOwo.fill = GridBagConstraints.HORIZONTAL;
		gbcTglOwo.gridx = 0;
		gbcTglOwo.gridy = 0;
		this.getContentPane().add(btntglOwo, gbcTglOwo);

		final JComboBox<LookAndFeelInfo> lafCb = new JComboBox<>();
		lafCb.setModel(new DefaultComboBoxModel<>(UIManager.getInstalledLookAndFeels()));
		for (final LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
			if (info.getName().equals(UIManager.getLookAndFeel().getName())) {
				lafCb.setSelectedItem(info);
			}
		}
		lafCb.setRenderer(new DefaultListCellRenderer() {
			private static final long serialVersionUID = -124297384660399618L;

			@Override
			public Component getListCellRendererComponent(final JList<?> list, final Object value, final int index,
					final boolean isSelected, final boolean cellHasFocus) {
				super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
				this.setText(((LookAndFeelInfo) value).getName());
				return this;
			}
		});
		lafCb.addActionListener(e -> {
			UserConfig.getConf().setProperty("ui.laf", ((LookAndFeelInfo) lafCb.getSelectedItem()).getClassName());
			JOptionPane.showMessageDialog(null,
					I18N.gettext(
							"Succeshfuwwy changed youw WookAndFweel senpaï. Pwease restart to see the new pwettiness"),
					I18N.gettext("OwO, new theme?"), JOptionPane.INFORMATION_MESSAGE,
					Macros.getIcon("icons/owoPwessed.png"));
		});
		final GridBagConstraints gbcLafCb = new GridBagConstraints();
		gbcLafCb.insets = new Insets(0, 0, 5, 0);
		gbcLafCb.fill = GridBagConstraints.HORIZONTAL;
		gbcLafCb.gridx = 1;
		gbcLafCb.gridy = 0;
		this.getContentPane().add(lafCb, gbcLafCb);

		final JComboBox<String> cbEditorTheme = new JComboBox<>();
		cbEditorTheme.setModel(new DefaultComboBoxModel<>(this.AVAILABLE_THEMES));
		cbEditorTheme.setSelectedItem(UserConfig.getConf().getString("ui.editor_theme", this.AVAILABLE_THEMES[0]));
		cbEditorTheme.setRenderer(new DefaultListCellRenderer() {
			private static final long serialVersionUID = 5239799364668302157L;

			@Override
			public Component getListCellRendererComponent(final JList<?> list, final Object value, final int index,
					final boolean isSelected, final boolean cellHasFocus) {
				super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
				final String s = (String) value;
				this.setText(s.substring(s.lastIndexOf('/') + 1, s.lastIndexOf('.')));
				return this;
			}
		});
		cbEditorTheme.addActionListener(
				e -> UserConfig.getConf().setProperty("ui.editor_theme", cbEditorTheme.getSelectedItem()));
		final GridBagConstraints gcbCbEditorTheme = new GridBagConstraints();
		gcbCbEditorTheme.insets = new Insets(0, 0, 5, 0);
		gcbCbEditorTheme.fill = GridBagConstraints.HORIZONTAL;
		gcbCbEditorTheme.gridx = 1;
		gcbCbEditorTheme.gridy = 1;
		this.getContentPane().add(cbEditorTheme, gcbCbEditorTheme);

		final JCheckBox cbUpdates = new JCheckBox(I18N.gettext("Check version on startup"));
		cbUpdates.setSelected(UserConfig.getConf().getBoolean("general.check_update", false));
		cbUpdates.addActionListener(
				e -> UserConfig.getConf().setProperty("general.check_update", cbUpdates.isSelected()));
		final GridBagConstraints gcbCbUpdates = new GridBagConstraints();
		gcbCbUpdates.insets = new Insets(0, 0, 5, 5);
		gcbCbUpdates.fill = GridBagConstraints.HORIZONTAL;
		gcbCbUpdates.gridx = 0;
		gcbCbUpdates.gridy = 1;
		this.getContentPane().add(cbUpdates, gcbCbUpdates);

		final JComboBox<String> cbLocale = new JComboBox<>();
		cbLocale.setModel(new DefaultComboBoxModel<>(I18N.AVAILABLE_LOCALES));
		cbLocale.setSelectedItem(UserConfig.getConf().getString("general.locale", "en"));
		cbLocale.addActionListener(e -> {
			UserConfig.getConf().setProperty("general.locale", cbLocale.getSelectedItem());
			JOptionPane.showMessageDialog(null, I18N.gettext("Locale changed, you should restart now"));
		});
		cbLocale.setRenderer(new DefaultListCellRenderer() {
			private static final long serialVersionUID = -6561563130422217384L;

			@Override
			public Component getListCellRendererComponent(final JList<?> list, final Object value, final int index,
					final boolean isSelected, final boolean cellHasFocus) {
				super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
				final String loc = (String) value;
				this.setText(loc.toUpperCase());
				if (loc.equals("en")) {
					this.setIcon(Macros.getIcon("https://www.countryflags.io/gb/shiny/16.png"));
				} else if (loc.equals("gcf")) {
					this.setIcon(Macros.getIcon("https://www.countryflags.io/gp/shiny/16.png"));
				} else {
					this.setIcon(Macros.getIcon("https://www.countryflags.io/" + loc + "/shiny/16.png"));
				}

				return this;
			}

		});
		final GridBagConstraints gbc_cbLocale = new GridBagConstraints();
		gbc_cbLocale.insets = new Insets(0, 0, 0, 5);
		gbc_cbLocale.gridx = 0;
		gbc_cbLocale.gridy = 2;
		this.getContentPane().add(cbLocale, gbc_cbLocale);

		final JComboBox<Integer> cbToolbar = new JComboBox<>();
		cbToolbar.setModel(new DefaultComboBoxModel<>(new Integer[] { 0, 1, 2 }));
		cbToolbar.setSelectedItem(UserConfig.getConf().getInt("ui.toolbar_btns", ToolBar.BUTTON_ICON_ONLY));
		cbToolbar.setRenderer(new DefaultListCellRenderer() {
			private static final long serialVersionUID = -6062516518147190389L;

			@Override
			public Component getListCellRendererComponent(final JList<?> list, final Object value, final int index,
					final boolean isSelected, final boolean cellHasFocus) {
				final DefaultListCellRenderer cell = (DefaultListCellRenderer) super.getListCellRendererComponent(list,
						value, index, isSelected, cellHasFocus);
				switch ((Integer) value) {
				case ToolBar.BUTTON_ICON_ONLY:
					cell.setText("Icon only");
					break;
				case ToolBar.BUTTON_TEXT_ICON:
					cell.setText("Text and icon");
					break;
				case ToolBar.BUTTON_TEXT_ONLY:
					cell.setText("Text only");
					break;
				default:
					cell.setText("Invalid: " + value);
					break;
				}
				return cell;
			}
		});
		cbToolbar.addActionListener(
				e -> UserConfig.getConf().setProperty("ui.toolbar_btns", cbToolbar.getSelectedItem()));
		final GridBagConstraints gbc_cbToolbar = new GridBagConstraints();
		gbc_cbLocale.insets = new Insets(0, 0, 0, 5);
		gbc_cbLocale.gridx = 0;
		gbc_cbLocale.gridy = 3;
		this.getContentPane().add(cbToolbar, gbc_cbToolbar);
	}

	private static void setOwoBtnText(final JToggleButton btn) {
		if (btn.isSelected()) {
			btn.setText(I18N.gettext("disabwe supewiow mwode >~<"));
		} else {
			btn.setText(I18N.gettext("enyabwe weebish mwode (・`ω´・)"));
		}
	}

	public static void toggle() {
		SettingsFrame.instance.setVisible(!SettingsFrame.instance.isVisible());
	}
}
