package lol.javasnice;

import java.awt.BorderLayout;
import java.io.IOException;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.WindowConstants;

import org.apache.bcel.classfile.ClassParser;
import org.apache.bcel.classfile.JavaClass;

public class ByteCodeFrame extends JFrame {
	private static ByteCodeFrame instance = new ByteCodeFrame();
	private static final long serialVersionUID = 4642266129902273562L;

	private final JTextPane pane;
	private final JScrollPane scroll;

	private ByteCodeFrame() {
		this.setLayout(new BorderLayout(5, 5));
		this.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
		this.setSize(400, 400);

		this.pane = new JTextPane();
		this.pane.setEditable(false);

		this.scroll = new JScrollPane();
		this.scroll.setViewportView(this.pane);
		this.add(this.scroll, BorderLayout.CENTER);

	}

	public void setByteCode(final String filepath) throws IOException {
		final StringBuilder sb = new StringBuilder();
		sb.append("Expanded byte code of file '" + filepath + "':\n");
		final ClassParser parse = new ClassParser(filepath);
		final JavaClass cls = parse.parse();
		sb.append(cls.toString());
		this.pane.setText(sb.toString());
	}

	public void toggleVisibility() {
		this.setVisible(!this.isVisible());
	}

	public static ByteCodeFrame getInstance() {
		return ByteCodeFrame.instance;
	}
}
