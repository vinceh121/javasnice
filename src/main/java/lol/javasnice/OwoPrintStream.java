package lol.javasnice;

import java.io.OutputStream;
import java.io.PrintStream;

import me.vinceh121.owoifinator.Owoifinator;
import me.vinceh121.owoifinator.modes.OwoLatin;

public class OwoPrintStream extends PrintStream {
	private final Owoifinator owo;
	private static boolean owoMode = false;

	public OwoPrintStream(final OutputStream fileName, final boolean autoflush) {
		super(fileName, autoflush);
		this.owo = new Owoifinator(new OwoLatin());
	}

	@Override
	public void write(final byte[] buf, final int off, final int len) {
		if (OwoPrintStream.owoMode) {
			final String out = this.owo.processText(new String(buf));
			super.write(out.getBytes(), off, len);
		} else {
			super.write(buf, off, len);
		}
	}

	public static void setOwo(final boolean owo) {
		OwoPrintStream.owoMode = owo;
	}

	public static boolean isOwo() {
		return OwoPrintStream.owoMode;
	}

}
