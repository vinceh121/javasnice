package org.javascool.core;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.regex.Pattern;

import org.javascool.tools.FileManager;
import org.javascool.tools.UserConfig;

import lol.javasnice.ByteCodeFrame;

public class ProgletEngine {
	private static final String[] defaultProglets = { "abcdAlgos", "algoDeMaths", "analogNumerique", "codagePixels",
			"commSerie", "detectionForme", "dichotomie", "gogleMaps", "javaProg", "manuelIsn", "opengl", "syntheSons",
			"ticTacToe", "tortueLogo", "txtCode", "visages", "wwwIOI" };
	private ArrayList<Proglet> proglets;
	private static ProgletEngine engine = null;
	private Thread thread = null;
	private Runnable runnable = null;
	private Proglet currentProglet = null;

	public static ProgletEngine getInstance() {
		if (ProgletEngine.engine == null) {
			ProgletEngine.engine = new ProgletEngine();
		}

		return ProgletEngine.engine;
	}

	private ProgletEngine() {
		final String javascoolJar = Utils.javascoolJar();

		/*
		 * String[] var2 = FileManager.list(javascoolJar,
		 * "org.javascool.proglets.[^\\.]+.proglet.(pml|json)"); int var3 = var2.length;
		 *
		 * for(int var4 = 0; var4 < var3; ++var4) { String dir = var2[var4]; String name
		 * = dir.replaceFirst("jar:[^!]*!(.*)proglet.(pml|json)", "$1");
		 *
		 * try { Proglet proglet = (new Proglet()).load(name);
		 * this.proglets.add(proglet); } catch (Exception var8) {
		 * System.err.println("Erreur lors de la détection dans le jar de la proglet " +
		 * name + " en " + dir + " (" + var8 + ")"); } }
		 */
		this.proglets = new ArrayList<>();
		for (final String str : ProgletEngine.defaultProglets) {
			try {
				// System.out.println("STR: " + str);
				final Proglet proglet = new Proglet().load("proglets/" + str);
				this.proglets.add(proglet);
			} catch (final Exception var9) {
				System.out.println("Error while scanning proglet ("
						+ var9
						+ " with "
						+ javascoolJar
						+ "\n . . you can still use javasnice");
			}
		}

		if (!this.proglets.isEmpty()) {
			Collections.sort(this.proglets, (p1, p2) -> p1.getName().compareTo(p2.getName()));
		} else {
			String message = "Error: this jar doesn't contain proglets";
			message = message + "\njavascoolJar = " + javascoolJar + "\n";

			String name;
			for (final Iterator<?> var11 = System.getProperties().stringPropertyNames().iterator(); var11
					.hasNext(); message = message + "  " + name + " = " + System.getProperty(name) + "\n") {
				name = (String) var11.next();
			}

			throw new IllegalStateException(message);
		}
	}

	public boolean doCompile(final String program) {
		this.doStop();
		final Jvs2Java jvs2java = this.getProglet() != null ? this.getProglet().getJvs2java() : new Jvs2Java();
		final String javaCode = jvs2java.translate(program);

		String javaFile;
		try {
			final File buildDir = UserConfig.createTempDir("javac");
			javaFile = buildDir + File.separator + jvs2java.getClassName() + ".java";
			FileManager.save(javaFile, javaCode);
		} catch (final Exception var9) {
			try {
				javaFile = new File(jvs2java.getClassName() + ".java").getAbsolutePath();
				System.out.println("Saving local file: " + javaFile);
				FileManager.save(javaFile, javaCode);
			} catch (final Exception var8) {
				System.out.println("Warning: local folder '"
						+ System.getProperty("user.dir")
						+ "' can't be used to save files. You need to select an appropriate folder.");
				return false;
			}
		}

		if (Java2Class.compile(javaFile)) {
			try {
				ByteCodeFrame.getInstance().setByteCode(javaFile.replaceAll(Pattern.quote(".java"), ".class"));
			} catch (final IOException e) {
				e.printStackTrace();
			}
			try {
				this.runnable = Java2Class.load(javaFile);
				return true;
			} catch (final Exception var7) {
				var7.printStackTrace();
				System.out.println("Could not find required files in temporary directory '"
						+ new File(javaFile).getParent()
						+ "'");
				return false;
			}
		} else {
			this.runnable = null;
			return false;
		}
	}

	public void doRun() {
		this.doStop();
		if (this.runnable != null) {
			(this.thread = new Thread(() -> {
				try {
					ProgletEngine.this.runnable.run();
					ProgletEngine.this.thread = null;
				} catch (final Throwable var2) {
					Jvs2Java.report(var2);
				}

			})).start();
		}

	}

	public void doStop(final String message) {
		if (message != null) {
			System.out.println("Interrupt cause: " + message);
		}

		if (this.thread != null) {
			this.thread.interrupt();
			this.thread = null;
		}

	}

	public void doStop() {
		this.doStop((String) null);
	}

	public boolean isRunning() {
		return this.thread != null;
	}

	public Runnable getProgletRunnable() {
		return this.runnable;
	}

	public Proglet setProglet(final String proglet) {
		if (this.currentProglet != null) {
			this.currentProglet.stop();
		}

		this.currentProglet = null;
		final Iterator<Proglet> var2 = this.getProglets().iterator();

		while (var2.hasNext()) {
			final Proglet p = var2.next();
			if (p.getName().equals(proglet)) {
				this.currentProglet = p;
			}
		}

		if (this.currentProglet == null) {
			throw new IllegalArgumentException("Tried to use undefined proglet: " + proglet);
		} else {
			this.currentProglet.start();
			return this.currentProglet;
		}
	}

	public Proglet getProglet(final String proglet) {
		final Iterator<Proglet> var2 = this.getProglets().iterator();

		Proglet p;
		do {
			if (!var2.hasNext()) {
				return null;
			}

			p = var2.next();
		} while (!p.getName().equals(proglet));

		return p;
	}

	public Proglet getProglet() {
		return this.currentProglet;
	}

	public Iterable<Proglet> getProglets() {
		return this.proglets;
	}

	public int getProgletCount() {
		return this.proglets.size();
	}
}
