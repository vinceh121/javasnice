package org.javascool.core;

import java.io.File;
import java.lang.reflect.InvocationTargetException;

import org.javascool.gui.Core;
import org.javascool.tools.FileManager;

public class Jvs2Java extends Translator {
	private Translator progletTranslator = null;
	private String progletPackageName = null;
	private Proglet proglet = null;
	private static int uid = 0;

	public Jvs2Java setProglet(final Proglet proglet) {
		this.progletTranslator = proglet.getTranslator();
		this.progletPackageName = proglet.hasFunctions() ? "org.javascool.proglets." + proglet.getName() : null;
		this.proglet = proglet;
		return this;
	}

	public String translate(final String jvsCode, String jvsName) {
		String text = jvsCode.replace(' ', ' ');
		if (!text.replaceAll("[ \n\r\t]+", " ").matches(".*void[ ]+main[ ]*\\([ ]*\\).*")) {
			if (text.replaceAll("[ \n\r\t]+", " ").matches(".*main[ ]*\\([ ]*\\).*")) {
				System.out.println(
						"Warning: you should specify return types in a declarative language if you want stuff to work");
				text = text.replaceFirst("main[ ]*\\([ ]*\\)", "void main()");
			} else {
				System.out.println("Warning: you didn't even declare the main method now seriously?");
				text = "\nvoid main() {\n" + text + "\n}\n";
			}
		}

		final String[] lines = text.split("\n");
		final StringBuilder head = new StringBuilder();
		final StringBuilder tail = new StringBuilder();
		final StringBuilder body = new StringBuilder();

		final String[] var9 = lines;
		final int var10 = lines.length;

		for (int var11 = 0; var11 < var10; ++var11) {
			final String line = var9[var11];
			if (line.matches("^\\s*(import|package)[^;]*;\\s*$")) {
				head.append(line);
				body.append("//").append(line).append("\n");
				if (line.matches("^\\s*package[^;]*;\\s*$")) {
					System.out.println("Warning: do not specify packages, it might fuck everything up");
				}
			} else if (line.matches("^\\s*include[^;]*;\\s*$")) {
				final String name = line.replaceAll("^\\s*include([^;]*);\\s*$", "$1").trim();
				body.append("/* include ").append(name).append("; */ ");

				try {
					final String include = FileManager.load(name + ".jvs");
					final String[] var15 = include.split("\n");
					final int var16 = var15.length;

					for (int var17 = 0; var17 < var16; ++var17) {
						final String iline = var15[var17];
						if (iline.matches("^\\s*import[^;]*;\\s*$")) {
							head.append(iline);
						} else if (!iline.matches("^\\s*package[^;]*;\\s*$")) {
							body.append(iline);
						}
					}
				} catch (final Exception var19) {
					body.append(" - Could not read the included file!!");
				}

				body.append("\n");
			} else {
				body.append(line).append("\n");
			}

		}

		head.append("import static java.lang.Math.*;"); // Use Math.x you lazy cunts
		head.append("import static org.javascool.macros.Macros.*;");
		head.append("import static org.javascool.macros.Stdin.*;");
		head.append("import static org.javascool.macros.Stdout.*;");
		if (this.progletPackageName != null) {
			head.append("import static ").append(this.progletPackageName).append(".Functions.*;");
		}

		if (this.progletTranslator != null) {
			head.append(this.progletTranslator.getImports());
		}

		++Jvs2Java.uid;
		head.append("public class JvsToJavaTranslated").append(Jvs2Java.uid).append(" implements Runnable{");
		head.append("  private static final long serialVersionUID = ").append(Jvs2Java.uid).append("L;");
		head.append("  public void run() {");
		head.append("   try{ main(); } catch(Throwable e) { ");
		head.append(
				"    if (e.toString().matches(\".*Interrupted.*\"))println(\"\\n-------------------\\nProgram stopped!\\n-------------------\\n\");");
		head.append(
				"    else println(\"\\n-------------------\\nError while running proglet\\n\"+org.javascool.core.Jvs2Java.report(e)+\"\\n-------------------\\n\");}");
		head.append("  }");
		if (this.proglet != null) {
			if (jvsName == null) {
				jvsName = this.proglet.getName() + " demo";
			}

			final String main
					= "public static void main(String[] usage) {    new org.javascool.widgets.MainFrame().reset(\""
							+ jvsName
							+ "\", "
							+ (this.proglet.getDimension().width + 200)
							+ ", "
							+ (this.proglet.getDimension().height + 100)
							+ ", "
							+ (this.proglet.getPane() != null
									? "org.javascool.core.ProgletEngine.getInstance().setProglet(\""
											+ this.proglet.getName()
											+ "\").getProgletPane()"
									: "org.javascool.widgets.Console.getInstance()")
							+ ").setRunnable(new JvsToJavaTranslated"
							+ Jvs2Java.uid
							+ "());"
							+ "}";
			head.append(main);
		}

		// note, there was addition of sleep in while here
		String finalBody = body.toString();
		if (this.progletTranslator != null) {
			finalBody = this.progletTranslator.translate(finalBody);
		}

		return head.toString() + finalBody + "\n\n" + tail.toString() + "}";
	}

	@Override
	public String translate(final String jvsCode) {
		return this.translate(jvsCode, (String) null);
	}

	public String getClassName() {
		return "JvsToJavaTranslated" + Jvs2Java.uid;
	}

	public static String report(final Throwable error) {
		if (Core.DEBUG) {
			error.printStackTrace(System.out);
		}
		if (error instanceof InvocationTargetException) {
			return Jvs2Java.report(error.getCause());
		} else {
			String s = "";
			/*
			 * if (error instanceof ArrayIndexOutOfBoundsException) { s = s +
			 * "Erreur lors de l'utilisation d'un tableau, l'index utilisé (" +
			 * error.toString().split(": ")[1] + ") n'est pas valide.\n\n"; } else if (error
			 * instanceof NullPointerException) { s = s +
			 * "Utilisation d'une variable non initialisée (pointeur de valeur nul).\n\n"; }
			 * else { s = s + error.toString() + "\n\n"; }
			 */
			s += error.toString() + "\n\n";

			s = s + "========\nStacktrace, in case of emergency, copy/paste in stackoverflow\n";

			for (int i = 0; i < 4 && i < error.getStackTrace().length; ++i) {
				String s_i = "" + error.getStackTrace()[i];
				if (s_i.startsWith("JvsToJavaTranslated")) {
					s_i = s_i.replaceAll("JvsToJavaTranslated[0-9]*", "");
					s_i = s_i.replaceFirst("\\.", "");
					s_i = s_i.replaceFirst("\\.java:", ") l\\.");
					s_i = s_i.substring(0, s_i.length() - 1);
					final String[] s_is = s_i.split(" ");
					s = s + s_is[1] + ": " + s_is[0] + "\n";
				}
			}

			return s;
		}
	}

	public static void build(final String name, final String jvsFile, final String javaFile) {
		System.out.println("Compiling " + new File(javaFile).getName() + "..");
		final Proglet proglet = new Proglet().load("proglets/" + name);
		final Jvs2Java jvs2java = proglet.getJvs2java();
		final String javaCode = jvs2java.translate(FileManager.load(jvsFile),
				new File(jvsFile).getName().replaceFirst("\\.jvs$", ""));
		FileManager.save(javaFile, (javaCode.trim() + "\n").replaceAll("\n", System.getProperty("line.separator")));
		System.out.println(" archived with success :\nFile '" + javaFile + "' is now available");
	}

	public static void build(final String name, final String jvsFile) {
		Jvs2Java.build(name, jvsFile, jvsFile.replaceFirst("\\.jvs$", ".java"));
	}

	public static void build(final String jvsFile) {
		Jvs2Java.build(Utils.javascoolProglet(), jvsFile, jvsFile.replaceFirst("\\.jvs$", ".java"));
	}
}
