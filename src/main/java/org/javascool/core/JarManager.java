package org.javascool.core;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.jar.JarEntry;
import java.util.jar.JarInputStream;
import java.util.jar.JarOutputStream;

import org.javascool.tools.FileManager;

public class JarManager {
	private JarManager() {
	}

	public static void jarExtract(final String jarFile, final String destDir) {
		try {
			final JarInputStream jip = new JarInputStream(new FileInputStream(jarFile));

			JarEntry je;
			while ((je = jip.getNextJarEntry()) != null) {
				if (!je.isDirectory() && !je.getName().contains("META-INF")) {
					final File dest = new File(destDir + File.separator + je.getName());
					dest.getParentFile().mkdirs();
					JarManager.copyStream(jip, new FileOutputStream(dest));
				}
			}

			jip.close();
		} catch (final Exception var7) {
			throw new IllegalStateException(var7);
		}
	}

	public static void jarCreate(final String jarFile, final String mfData, String srcDir) {
		try {
			final File parent = new File(jarFile).getParentFile();
			if (parent != null) {
				parent.mkdirs();
			}

			new File(jarFile).delete();
			srcDir = new File(srcDir).getCanonicalPath();
			if (mfData != null) {
				new File(srcDir + File.separator + "META-INF").mkdirs();
				FileManager.save(srcDir + File.separator + "META-INF" + File.separator + "MANIFEST.MF", mfData);
			}

			final JarOutputStream target = new JarOutputStream(new FileOutputStream(jarFile));
			JarManager.copyFileToJar(new File(srcDir), target, new File(srcDir));
			target.close();
		} catch (final Exception var5) {
			var5.printStackTrace();
			throw new RuntimeException(var5);
		}
	}

	public static void copyFiles(final String srcDir, final String dstDir, final boolean recurse) throws IOException {
		if (new File(srcDir).isDirectory()) {
			if (!new File(srcDir).getName().equals(".svn")) {
				final String[] var3 = FileManager.list(srcDir);
				final int var4 = var3.length;

				for (int var5 = 0; var5 < var4; ++var5) {
					final String s = var3[var5];
					final String d = dstDir + File.separator + new File(s).getAbsoluteFile().getName();
					if (recurse) {
						JarManager.copyFiles(s, d, true);
					} else if (!new File(s).isDirectory()) {
						JarManager.copyFile(s, d);
					}
				}
			}
		} else {
			JarManager.copyFile(srcDir, dstDir);
		}

	}

	public static void copyFiles(final String srcDir, final String dstDir) throws IOException {
		JarManager.copyFiles(srcDir, dstDir, true);
	}

	public static void copyFile(final String srcFile, final String dstFile) throws IOException {
		new File(dstFile).getParentFile().mkdirs();
		JarManager.copyStream(new FileInputStream(srcFile), new FileOutputStream(dstFile));
	}

	public static void copyResource(final String srcRes, final String dstFile) throws IOException {
		JarManager.copyStream(JarManager.class.getClassLoader().getResourceAsStream(srcRes),
				new FileOutputStream(new File(dstFile)));
	}

	private static void copyFileToJar(final File source, final JarOutputStream target, final File root)
			throws IOException {
		try {
			if (source.isDirectory()) {
				String name = source.getPath()
						.replace(root.getAbsolutePath() + File.separator, "")
						.replace(File.separator, "/");
				if (!name.isEmpty() && !source.equals(root)) {
					if (!name.endsWith("/")) {
						name = name + "/";
					}

					final JarEntry entry = new JarEntry(name);
					entry.setTime(source.lastModified());
					target.putNextEntry(entry);
					target.closeEntry();
				}

				final File[] var11 = source.listFiles();
				final int var6 = var11.length;

				for (int var7 = 0; var7 < var6; ++var7) {
					final File nestedFile = var11[var7];
					JarManager.copyFileToJar(nestedFile, target, root);
				}
			} else {
				final JarEntry entry = new JarEntry(source.getPath()
						.replace(root.getAbsolutePath() + File.separator, "")
						.replace(File.separator, "/"));
				entry.setTime(source.lastModified());
				target.putNextEntry(entry);
				JarManager.copyStream(new BufferedInputStream(new FileInputStream(source)), target);
			}

		} catch (final Throwable var9) {
			var9.printStackTrace(System.out);
			throw new IllegalStateException(var9);
		}
	}

	private static void copyStream(final InputStream in, final OutputStream out) throws IOException {
		final InputStream i = in instanceof JarInputStream ? in : new BufferedInputStream(in, 2048);
		final OutputStream o = out instanceof JarOutputStream ? out : new BufferedOutputStream(out, 2048);
		final byte[] data = new byte[2048];

		int c;
		while ((c = i.read(data, 0, 2048)) != -1) {
			o.write(data, 0, c);
		}

		if (o instanceof JarOutputStream) {
			((JarOutputStream) o).closeEntry();
		} else {
			o.close();
		}

		if (i instanceof JarInputStream) {
			((JarInputStream) i).closeEntry();
		} else {
			i.close();
		}

	}

	public static void rmDir(final File dir) {
		if (dir.isDirectory()) {
			final File[] var1 = dir.listFiles();
			final int var2 = var1.length;

			for (int var3 = 0; var3 < var2; ++var3) {
				final File f = var1[var3];
				JarManager.rmDir(f);
			}
		}

		dir.delete();
	}
}
