package org.javascool.core;

import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.charset.Charset;
import java.util.Iterator;

import org.javascool.gui.Core;
import org.javascool.macros.Macros;

public class Utils {
	private static String javascoolJar = null;
	private static String javascoolJarEnc = null;

	private Utils() {
	}

	public static String javascoolJar() {
		if (Utils.javascoolJar != null) {
			return Utils.javascoolJar;
		} else {
			final String url = Macros.getResourceURL("org/javascool/core/Utils.class")
					.toString()
					.replaceFirst("jar:file:([^!]*)!.*", "$1");
			if (url.endsWith(".jar")) {
				try {
					String jar = URLDecoder.decode(url, "UTF-8");
					if (new File(jar).exists()) {
						Utils.javascoolJar = jar;
						return jar;
					} else {
						jar = URLDecoder.decode(url, Charset.defaultCharset().name());
						if (new File(jar).exists()) {
							Utils.javascoolJarEnc = Charset.defaultCharset().name();
							return jar;
						} else {
							final Iterator<?> var2 = Charset.availableCharsets().keySet().iterator();

							while (var2.hasNext()) {
								final String enc = (String) var2.next();
								jar = URLDecoder.decode(url, enc);
								if (new File(jar).exists()) {
									Utils.javascoolJarEnc = enc;
									if (Core.DEBUG) {
										System.err.println(
												"Notice: javascool file " + jar + " correct decoding as " + enc);
									}
									Utils.javascoolJar = jar;
									return jar;
								}
								if (Core.DEBUG) {
									System.err.println("Notice: javascool file " + jar + " wrong decoding as " + enc);
								}
							}

							throw new RuntimeException("Encoding problem");
						}
					}
				} catch (final UnsupportedEncodingException var4) {
					throw new RuntimeException("Spurious defaultCharset: this is a caveat");
				}
			} else {
				return "";
			}
		}
	}

	public static String javascoolJarEnc() {
		Utils.javascoolJar();
		return Utils.javascoolJarEnc;
	}

	public static String javascoolProglet() {
		if (ProgletEngine.getInstance().getProgletCount() == 1) {
			final Iterator<?> var0 = ProgletEngine.getInstance().getProglets().iterator();
			if (var0.hasNext()) {
				final Proglet proglet = (Proglet) var0.next();
				return proglet.getName();
			}
		}

		return null;
	}

	public static boolean javaStart(final String command, final int timeout) {
		try {
			String javaCommand = System.getProperty("java.home") + File.separator + "bin" + File.separator + "java";
			if (System.getProperty("os.name").toUpperCase().contains("WIN")) {
				javaCommand = javaCommand + ".exe";
			}

			return Utils.exec(javaCommand + (command.indexOf(9) == -1 ? " " : "\t") + command, timeout) == 0;
		} catch (final Exception var3) {
			System.out.println("Could not start command '$java " + command + "' : " + var3);
			return false;
		}
	}

	private static int exec(final String command, final int timeout) throws IOException {
		final Process process = Runtime.getRuntime().exec(command.trim().split(command.indexOf(9) == -1 ? " " : "\t"));
		final InputStreamReader stdout = new InputStreamReader(process.getInputStream());
		final InputStreamReader stderr = new InputStreamReader(process.getErrorStream());
		final long now = System.currentTimeMillis();

		while (true) {
			try {
				Thread.sleep(300L);
			} catch (final Exception var8) {}

			while (stdout.ready()) {
				System.out.print((char) stdout.read());
			}

			while (stderr.ready()) {
				System.err.print((char) stderr.read()); // Keep stderr
			}

			try {
				return process.exitValue();
			} catch (final Exception var9) {
				if (timeout > 0 && System.currentTimeMillis() > now + timeout * 1000) {
					throw new IllegalStateException("Timeout T > " + timeout + "s when running '" + command + "'");
				}
			}
		}
	}
}
