package org.javascool.core;

import java.awt.Component;
import java.awt.Dimension;
import java.io.File;

import javax.swing.JFrame;

import org.javascool.macros.Macros;
import org.javascool.tools.FileManager;
import org.javascool.tools.Pml;
import org.javascool.widgets.MainFrame;

import lol.javasnice.I18N;

public class Proglet {
	private final Pml pml = new Pml();
	private final MainFrame popupframe = null;
	private Jvs2Java jvs2java = null;

	public Proglet load(String location) {
		if (!location.endsWith("/")) {
			location = location + "/";
		}

		try {
			this.pml.load(location + "proglet.pml", true);
			this.pml.set("jvs-version", 4);
		} catch (final Exception var8) {
			try {
				this.pml.load(location + "proglet.json", true);
				this.pml.set("help-location", location + I18N.getLocale() + "_help.html");
				this.pml.set("jvs-version", 5);
			} catch (final Exception var7) {
				throw new IllegalStateException("Proglet " + location + " is not available and can't be loaded");
			}
		}

		this.pml.set("location", location);

		try {
			this.pml.set("name", new File(location).getName());
		} catch (final Exception var6) {
			throw new IllegalArgumentException(var6 + " : " + location + " is a malformed URL");
		}

		if (FileManager.exists(Macros.getResourceURL(location + "completion.xml"))) {
			this.pml.set("completion", location + "completion.xml");
		}

		if (this.pml.isDefined("icon")
				&& FileManager.exists(Macros.getResourceURL(location + this.pml.getString("icon")))) {
			this.pml.set("icon-location", location + this.pml.getString("icon"));
		} else {
			this.pml.set("icon-location", "icons/scripts.png");
		}

		try {
			Class.forName("org.javascool.proglets." + this.pml.getString("name") + ".Functions");
			this.pml.set("has-functions", true);
		} catch (final Throwable var5) {
			this.pml.set("has-functions", false);
		}

		if (!this.pml.isDefined("help-location")) {
			this.pml.set("help-location", this.pml.getString("location") + "help.htm");
		}

		try {
			this.pml.set("jvs-translator",
					Class.forName("org.javascool.proglets." + this.pml.getString("name") + ".Translator")
							.getDeclaredConstructor()
							.newInstance());
		} catch (final Throwable var4) {}

		return this;
	}

	@Override
	public String toString() {
		return this.pml.toString();
	}

	public String getName() {
		return this.pml.getString("name");
	}

	public String getTitle() {
		final String title = this.pml.getString(I18N.getLocale() + "_title");
		if (title != null) {
			return title;
		} else {
			return I18N.gettext("This proglet's name isn't translated for your language.");
		}
	}

	public String getIcon() {
		return this.pml.getString("icon-location");
	}

	public String getHelp() {
		return this.pml.getString("help-location");
	}

	public Dimension getDimension() {
		return new Dimension(this.pml.getInteger("width", 500), this.pml.getInteger("height", 500));
	}

	public String getCompletion() {
		return this.pml.getString("completion", "");
	}

	public boolean hasFunctions() {
		return this.pml.getBoolean("has-functions");
	}

	public boolean hasPane() {
		return this.pml.getBoolean("pane-defined");
	}

	public Component getPane() {
		this.setPane();
		return (Component) this.pml.getObject("java-pane");
	}

	public Component getProgletPane() {
		this.setPane();
		return (Component) this.pml.getObject("java-proglet-pane");
	}

	private void setPane() {
		if (!this.pml.isDefined("pane-defined")) {
			this.pml.set("pane-defined", true);
			// if (this.isProcessing()) {
			// boolean popup = true;
			//
			// try {
			// int width = this.pml.getInteger("width", 500);
			// int height = this.pml.getInteger("height", 500);
			// Applet applet = (Applet) Class.forName("" + this.pml.getString("name") +
			// "").newInstance();
			// applet.init();
			// applet.setMinimumSize(new Dimension(width, height));
			// applet.setMaximumSize(new Dimension(width, height));
			// if (popup) {
			// this.popupframe = (new MainFrame() {
			// /**
			// *
			// */
			// private static final long serialVersionUID = 5551382711913578928L;
			//
			// @Override
			// public boolean isClosable() {
			// return false;
			// }
			// }).asPopup().reset(this.getName(), this.getIcon(), width, height, applet);
			// this.pml.set("java-pane", (String) null);
			// } else {
			// this.pml.set("java-pane", applet);
			// }
			//
			// this.pml.set("java-proglet-pane", applet);
			// } catch (ClassNotFoundException var7) {
			// } catch (Throwable var8) {
			// var8.printStackTrace();
			// System.out.println("Error while load processing proglet: " + var8);
			// }
			// } else {
			try {
				final Component pane
						= (Component) Class.forName("org.javascool.proglets." + this.pml.getString("name") + ".Panel")
								.getDeclaredConstructor()
								.newInstance();
				if (pane instanceof JFrame) {
					((JFrame) pane).setVisible(true);
					this.pml.set("java-pane", (String) null);
				} else {
					this.pml.set("java-pane", pane);
				}

				this.pml.set("java-proglet-pane", pane);
			} catch (final ClassNotFoundException var5) {} catch (final Throwable var6) {
				var6.printStackTrace();
				System.out.println("Could not load proglet: " + var6);
			}
		}
		// }

	}

	public Translator getTranslator() {
		return (Translator) this.pml.getObject("jvs-translator");
	}

	public Jvs2Java getJvs2java() {
		if (this.jvs2java == null) {
			this.jvs2java = new Jvs2Java();
			this.jvs2java.setProglet(this);
		}

		return this.jvs2java;
	}

	public boolean isProcessing() {
		return this.pml.getBoolean("processing");
	}

	public void start() {
		if (this.popupframe != null) {
			this.popupframe.setVisible(true);
		}

		// try {
		// if (this.getPane() != null && this.getPane() instanceof Applet) {
		// ((Applet) this.getPane()).start();
		// }
		// } catch (Throwable var2) {
		// System.out.println("Error while starting applet/proglet");
		// }

	}

	public void stop() {
		// try {
		// if (this.getPane() != null && this.getPane() instanceof Applet) {
		// ((Applet) this.getPane()).stop();
		// }
		// } catch (Throwable var2) {
		// System.out.println("Error while stopping applet/proglet");
		// }

		if (this.popupframe != null) {
			this.popupframe.setVisible(false);
		}

	}
}
