package org.javascool.core;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import javax.tools.Diagnostic;
import javax.tools.Diagnostic.Kind;
import javax.tools.DiagnosticCollector;
import javax.tools.JavaCompiler;
import javax.tools.JavaCompiler.CompilationTask;
import javax.tools.JavaFileObject;
import javax.tools.StandardJavaFileManager;
import javax.tools.ToolProvider;

import org.apache.commons.lang3.SystemUtils;
import org.javascool.gui.Core;
import org.javascool.macros.Macros;

import lol.javasnice.I18N;

public class Java2Class {
	private Java2Class() {
	}

	public static boolean compile(final String javaFile, final boolean allErrors) {
		final String[] javaFiles = new String[] { javaFile };
		return Java2Class.compile(javaFiles, allErrors);
	}

	public static boolean compile(final String javaFile) {
		return Java2Class.compile(javaFile, false);
	}

	public static boolean compile(final String[] javaFiles) {
		return Java2Class.compile(javaFiles, false);
	}

	public static boolean compile(final String[] javaFiles, final boolean allErrors) {
		return javaFiles.length == 0 ? false : Java2Class.compile1(javaFiles, allErrors);
	}

	// private static boolean compile3(String[] javaFiles, boolean allErrors) {
	// int options = 3;
	// String[] args = new String[options + javaFiles.length];
	// args[0] = "javac";
	// args[1] = "-g";
	// args[2] = "-source 8";
	// System.arraycopy(javaFiles, 0, args, options, javaFiles.length);
	//
	// System.out.println("Compiler args: " + Arrays.toString(args));
	//
	// StringWriter out = new StringWriter();
	//
	// try {
	// Process proc = Runtime.getRuntime().exec(args);
	// proc.waitFor();
	// System.out.println("Javac exited with code: " + proc.exitValue());
	// InputStream in = proc.getInputStream();
	// BufferedReader bf = new BufferedReader(new InputStreamReader(in));
	// String line;
	// while ((line = bf.readLine()) != null) {
	// out.append(line);
	// }
	// } catch (IOException | InterruptedException e) {
	// e.printStackTrace();
	// return false;
	// }
	//
	// String sout = out.toString().trim();
	//
	// if (sout.indexOf("^") != -1 && !allErrors) {
	// sout = sout.substring(0, sout.indexOf("^") + 1);
	// }
	//
	// if (javaFiles.length > 1) {
	// String[] var7 = javaFiles;
	// int var8 = javaFiles.length;
	//
	// int var9;
	// String javaFile;
	// for (var9 = 0; var9 < var8; ++var9) {
	// javaFile = var7[var9];
	// sout = sout.replaceAll(Pattern.quote((new File(javaFile)).getParent() +
	// File.separator), "\n");
	// }
	//
	// var7 = javaFiles;
	// var8 = javaFiles.length;
	//
	// for (var9 = 0; var9 < var8; ++var9) {
	// javaFile = var7[var9];
	// sout = sout.replaceAll("(" + Pattern.quote((new File(javaFile)).getName()) +
	// "):([0-9]+):",
	// "$1 : erreur de syntaxe ligne $2 :\n ");
	// }
	// } else {
	// sout = sout.replaceAll("(" + Pattern.quote((new
	// File(javaFiles[0])).getPath()) + "):([0-9]+):",
	// "\n Erreur de syntaxe ligne $2 :\n ");
	// sout = sout.replaceAll(Pattern.quote((new
	// File(javaFiles[0])).getName().replaceFirst("java$", "")), "");
	// }
	//
	// sout = sout.replaceAll("/\\*(.*)@<nojavac.*@nojavac>\\*/", "$1");
	// sout = sout.replaceAll("not a statement",
	// "L'instruction n'est pas valide.\n (Il se peut qu'une variable indiquée
	// n'existe pas)");
	// sout = sout.replaceAll("';' expected",
	// "Un ';' est attendu (il peut manquer, ou une parenthèse être incorrecte,
	// ..)");
	// sout = sout.replaceAll("cannot find
	// symbol\\s*symbol\\s*:\\s*([^\\n]*)[^:]*:\\s*(.*)",
	// "Il y a un symbole non-défini à cette ligne : «$1» (utilisez-vous la bonne
	// proglet ?)");
	// sout = sout.replaceAll("illegal start of expression",
	// "($0) L'instruction (ou la précédente) est tronquée ou mal écrite");
	// sout = sout.replaceAll("class, interface, or enum expected",
	// "($0) Il y a probablement une erreur dans les accolades (peut-être trop de
	// '}')");
	// sout = sout.replaceAll("'.class' expected",
	// "($0) Il manque des accolades ou des parenthèses pour définir
	// l'instruction");
	// sout =
	// sout.replaceAll("incompatible\\Wtypes\\W*found\\W*:\\W([A-Za-z\\.]*)\\Wrequired:\\W([A-Za-z\\.]*)",
	// "Vous avez mis une valeur de type $1 alors qu'il faut une valeur de type
	// $2");
	// if (sout.indexOf("Note:") != -1) {
	// sout = sout.substring(0, sout.indexOf("Note:")).trim();
	// }
	//
	// if (sout.length() > 0) {
	// System.out.println(sout);
	// }
	//
	// return sout.length() == 0;
	// }

	// private static boolean compile2(String[] javaFiles, boolean allErrors) {
	// int options = 2;
	// String[] args = new String[options + javaFiles.length];
	// args[0] = "-g";
	// args[1] = "-nowarn";
	// System.arraycopy(javaFiles, 0, args, options, javaFiles.length);
	// StringWriter out = new StringWriter();
	//
	// Method javac;
	// try {
	// javac =
	// Class.forName("com.sun.tools.javac.Main").getDeclaredMethod("compile",
	// Class.forName("[Ljava.lang.String;"), Class.forName("java.io.PrintWriter"));
	// } catch (Exception var12) {
	// throw new IllegalStateException("Impossible d'accéder au compilateur javac :
	// " + var12);
	// }
	//
	// try {
	// javac.invoke((Object) null, args, new PrintWriter(out));
	// } catch (Exception var11) {
	// throw new IllegalStateException("Erreur système lors du lancement du
	// compilateur javac : " + var11);
	// }
	//
	// String sout = out.toString().trim();
	// if (sout.length() > 0) {
	// System.err.println("Notice: Java compilation message : " + sout);
	// }
	//
	// if (sout.indexOf("^") != -1 && !allErrors) {
	// sout = sout.substring(0, sout.indexOf("^") + 1);
	// }
	//
	// if (javaFiles.length > 1) {
	// String[] var7 = javaFiles;
	// int var8 = javaFiles.length;
	//
	// int var9;
	// String javaFile;
	// for (var9 = 0; var9 < var8; ++var9) {
	// javaFile = var7[var9];
	// sout = sout.replaceAll(Pattern.quote((new File(javaFile)).getParent() +
	// File.separator), "\n");
	// }
	//
	// var7 = javaFiles;
	// var8 = javaFiles.length;
	//
	// for (var9 = 0; var9 < var8; ++var9) {
	// javaFile = var7[var9];
	// sout = sout.replaceAll("(" + Pattern.quote((new File(javaFile)).getName()) +
	// "):([0-9]+):",
	// "$1 : erreur de syntaxe ligne $2 :\n ");
	// }
	// } else {
	// sout = sout.replaceAll("(" + Pattern.quote((new
	// File(javaFiles[0])).getPath()) + "):([0-9]+):",
	// "\n Erreur de syntaxe ligne $2 :\n ");
	// sout = sout.replaceAll(Pattern.quote((new
	// File(javaFiles[0])).getName().replaceFirst("java$", "")), "");
	// }
	//
	// sout = sout.replaceAll("/\\*(.*)@<nojavac.*@nojavac>\\*/", "$1");
	// sout = sout.replaceAll("not a statement",
	// "L'instruction n'est pas valide.\n (Il se peut qu'une variable indiquée
	// n'existe pas)");
	// sout = sout.replaceAll("';' expected",
	// "Un ';' est attendu (il peut manquer, ou une parenthèse être incorrecte,
	// ..)");
	// sout = sout.replaceAll("cannot find
	// symbol\\s*symbol\\s*:\\s*([^\\n]*)[^:]*:\\s*(.*)",
	// "Il y a un symbole non-défini à cette ligne : «$1» (utilisez-vous la bonne
	// proglet ?)");
	// sout = sout.replaceAll("illegal start of expression",
	// "($0) L'instruction (ou la précédente) est tronquée ou mal écrite");
	// sout = sout.replaceAll("class, interface, or enum expected",
	// "($0) Il y a probablement une erreur dans les accolades (peut-être trop de
	// '}')");
	// sout = sout.replaceAll("'.class' expected",
	// "($0) Il manque des accolades ou des parenthèses pour définir
	// l'instruction");
	// sout =
	// sout.replaceAll("incompatible\\Wtypes\\W*found\\W*:\\W([A-Za-z\\.]*)\\Wrequired:\\W([A-Za-z\\.]*)",
	// "Vous avez mis une valeur de type $1 alors qu'il faut une valeur de type
	// $2");
	// if (sout.indexOf("Note:") != -1) {
	// sout = sout.substring(0, sout.indexOf("Note:")).trim();
	// }
	//
	// if (sout.length() > 0) {
	// System.out.println(sout);
	// }
	//
	// return sout.length() == 0;
	// }

	private static boolean compile1(final String[] javaFiles, final boolean allErrors) {
		if (Core.DEBUG) {
			System.out.println("Compiling files: " + Arrays.toString(javaFiles));
		}
		final JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();

		if (compiler == null) {
			System.out.println("Error: the compiler could not be found");
			Macros.message(I18N.ngettext("<h1>No compiler was found</h1>"
					+ "<p>You need to install a version of a JDK that is <b>inferior or equals</b> to your current Java version ({0})."
					+ "I recomment OpenJDK but Oracle JDK is fine. You should also upgrade your Java version while you're at it.</p>",
					SystemUtils.JAVA_VERSION), true);
			throw new IllegalStateException("Error: the compiler could not be found");
		} else {
			System.out.println(compiler.getSourceVersions());
			final DiagnosticCollector<JavaFileObject> diagnostics = new DiagnosticCollector<>();
			final StandardJavaFileManager fileManager = compiler.getStandardFileManager(null, Locale.ENGLISH, null);
			final List<File> sourceFileList = new ArrayList<>();
			for (final String javaFile : javaFiles) {
				sourceFileList.add(new File(javaFile));
			}

			final StringWriter out = new StringWriter();
			final Iterable<? extends JavaFileObject> compilationUnits
					= fileManager.getJavaFileObjectsFromFiles(sourceFileList);
			final CompilationTask task = compiler.getTask(out, fileManager, diagnostics, null, null, compilationUnits);

			/*
			 * Processor proc = new AbstractProcessor() {
			 * 
			 * @Override public SourceVersion getSupportedSourceVersion() { return
			 * SourceVersion.RELEASE_8; }
			 * 
			 * @Override public boolean process(Set<? extends TypeElement> annotations,
			 * RoundEnvironment roundEnv) { return false; } }; List<Processor> listProc =
			 * new ArrayList<>(); listProc.add(proc); task.setProcessors(listProc);
			 */

			final boolean success = task.call();

			if (Core.DEBUG) {
				System.out.println(
						"----- COMPILER OUTPUT -----\n" + out.toString() + "\n----- END COMPILER OUTPUT -----");
			}

			try {
				fileManager.close();
			} catch (final IOException var15) {
				System.out.println("Error while closing the compiler's file manager: " + var15);
			}

			final Iterator<?> var18 = diagnostics.getDiagnostics().iterator();

			Diagnostic<?> diagnostic;
			do {
				if (success) {
					return true;
				}

				diagnostic = (Diagnostic<?>) var18.next();
				final String javaDiagnostic = diagnostic.getMessage(Locale.ENGLISH);
				final String jvsDiagnostic = javaFiles.length > 1 ? javaDiagnostic : javaDiagnostic.split(" ", 2)[1];

				final int line = (int) diagnostic.getLineNumber();
				System.out
						.println("-------------------\nError while compiling at line " + line + ".\n" + jvsDiagnostic);
				System.out.println("Compilation error: line =" + line + " error=" + javaDiagnostic);
				System.out.println("-------------------");
			} while (!diagnostic.getKind().equals(Kind.ERROR) || allErrors);
			return false;
		}
	}

	@SuppressWarnings("resource")
	public static Runnable load(final String path) {
		try {
			final File javaClass = new File(path).getAbsoluteFile();
			final URL[] urls = new URL[] { new URL("file:" + javaClass.getParent() + File.separator) };
			final URLClassLoader loader = new URLClassLoader(urls); // Idk how we can fix that but closing this loader
																	// will
			// prevent inner classes from loading
			final Class<?> j_class = loader.loadClass(javaClass.getName().replaceAll("\\.java", ""));
			final Object o = j_class.getConstructor().newInstance();
			// loader.close();
			if (!(o instanceof Runnable)) {
				throw new IllegalArgumentException("Error: class " + javaClass + " doesn't implement Runnable");
			} else {
				return (Runnable) o;
			}
		} catch (final Throwable var5) {
			throw new RuntimeException("Error: could not load class" + path);
		}
	}
}
