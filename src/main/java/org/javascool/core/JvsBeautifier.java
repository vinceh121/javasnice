package org.javascool.core;

import com.github.javaparser.JavaParser;
import com.github.javaparser.ParseResult;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.printer.PrettyPrinter;

public class JvsBeautifier {
	private static JavaParser parse;
	private static PrettyPrinter pp;

	public static String run(final String text) {
		if (JvsBeautifier.parse == null) {
			JvsBeautifier.parse = new JavaParser();
		}
		if (JvsBeautifier.pp == null) {
			JvsBeautifier.pp = new PrettyPrinter();
		}

		final ParseResult<MethodDeclaration> code = JvsBeautifier.parse.parseMethodDeclaration(text);
		if (code.isSuccessful()) {
			final MethodDeclaration unit = code.getResult().get();
			return JvsBeautifier.pp.print(unit);
		} else {
			System.out.println("Failed to parse code: " + code.getProblems());
			return text;
		}
	}

	// public static String run2(String text) {
	// char[] f = text.trim().replace(' ', ' ').toCharArray();
	// String g = "";
	// String ln = "\n";
	// int bra = 0;
	// int par = 0;
	// int i = 0;
	//
	// while (true) {
	// while (i < f.length) {
	// if (f[i] == '/' && i < f.length - 1 && f[i + 1] == '*') {
	// for (g = g + f[i++]; i < f.length && (f[i - 1] != '*' || f[i] != '/'); g = g
	// + f[i++]) {
	// }
	//
	// if (i < f.length) {
	// g = g + f[i++] + ln;
	// }
	// } else if (f[i] == '/' && i < f.length - 1 && f[i + 1] == '/') {
	// while (i < f.length && f[i] != '\n') {
	// g = g + f[i++];
	// }
	//
	// g = g + ln;
	// ++i;
	// } else if (f[i] == '"') {
	// for (g = g + f[i++]; i < f.length && (f[i - 1] == '\\' || f[i] != '"')
	// && f[i] != '\n'; g = g + f[i++]) {
	// }
	//
	// if (i < f.length) {
	// g = g + f[i++];
	// }
	// } else if (f[i] == '\'') {
	// for (g = g + f[i++]; i < f.length && f[i] != '\'' && f[i] != '\n'; g = g +
	// f[i++]) {
	// }
	//
	// if (i < f.length) {
	// g = g + f[i++];
	// }
	// } else if (f[i] == '@') {
	// while (i < f.length && f[i] != '\n') {
	// g = g + f[i++];
	// }
	//
	// g = g + ln;
	// ++i;
	// } else if (Character.isWhitespace(f[i])) {
	// g = g + ' ';
	// ++i;
	//
	// while (i < f.length && Character.isWhitespace(f[i])) {
	// ++i;
	// }
	// } else {
	// char c0 = g.length() == 0 ? 32 : g.charAt(g.length() - 1);
	// if (f[i] == '(') {
	// ++par;
	// }
	//
	// if (f[i] == ')') {
	// --par;
	// }
	//
	// if (isOperator(f[i])) {
	// if (f[i] != '-' && f[i] != '+' || c0 != 'e' && c0 != 'E') {
	// if (!Character.isWhitespace(c0) && !isOperator(c0)) {
	// g = g + ' ';
	// }
	//
	// g = g + f[i];
	// if (i < f.length - 1 && !Character.isWhitespace(f[i + 1]) && !isOperator(f[i
	// + 1])
	// && !Character.isDigit(f[i + 1])) {
	// g = g + ' ';
	// }
	// } else {
	// g = g + f[i];
	// }
	// } else if (f[i] == '.') {
	// if (g.length() > 0 && Character.isWhitespace(c0)) {
	// g = g.substring(0, g.length() - 1);
	// }
	//
	// for (g = g + f[i]; i < f.length - 1 && Character.isWhitespace(f[i + 1]); ++i)
	// {
	// }
	// } else if (f[i] != ',' && f[i] != ';' && f[i] != ')') {
	// if (f[i] == '(') {
	// if (g.length() > 0 && Character.isWhitespace(c0) && g.length() > 1
	// && Character.isLetterOrDigit(g.charAt(g.length() - 2))) {
	// g = g.substring(0, g.length() - 1);
	// }
	//
	// for (g = g + f[i]; i < f.length - 1 && Character.isWhitespace(f[i + 1]); ++i)
	// {
	// }
	// } else if (f[i] == '}') {
	// for (int n = 0; n < 3; ++n) {
	// if (g.length() > 0 && Character.isWhitespace(g.charAt(g.length() - 1))) {
	// g = g.substring(0, g.length() - 1);
	// }
	// }
	//
	// g = g + f[i];
	// } else {
	// g = g + f[i];
	// }
	// } else {
	// if (g.length() > 0 && Character.isWhitespace(c0)) {
	// g = g.substring(0, g.length() - 1);
	// }
	//
	// g = g + f[i];
	// if (par > 0 && f[i] != ')' && i < f.length - 1 && !Character.isWhitespace(f[i
	// + 1])) {
	// g = g + ' ';
	// }
	//
	// if (f[i] == ')' && i < f.length - 1 && f[i + 1] == '{') {
	// g = g + ' ';
	// }
	// }
	//
	// if (f[i] == '{' || f[i] == '}' || f[i] == ';' && par == 0) {
	// if (f[i] == '{') {
	// ++bra;
	// ln = ln + " ";
	// }
	//
	// if (f[i] == '}') {
	// --bra;
	// }
	//
	// if (ln.length() >= 3 && f[i] == '}') {
	// ln = ln.substring(0, ln.length() - 3);
	// }
	//
	// g = g + ln;
	// boolean nextIsNotImport = f[i] != ';' || bra > 0
	// || !text.substring(i + 1).matches("\\s*(;\\s*)?import(.|\n)*");
	// if (ln.length() == 1 && nextIsNotImport) {
	// g = g + "\n";
	// }
	//
	// ++i;
	//
	// while (i < f.length && Character.isWhitespace(f[i])) {
	// ++i;
	// }
	// } else {
	// ++i;
	// }
	// }
	// }
	//
	// return "\n" + g.replaceAll("\\}\\s*else\\s*(\\{|if)", "} else $1")
	// .replaceAll("(while|if|for|return)\\s*([^a-z_0-9_])", "$1 $2");
	// }
	// }
	//
	// private static boolean isOperator(char c) {
	// switch (c) {
	// case '!':
	// case '%':
	// case '&':
	// case '*':
	// case '+':
	// case '-':
	// case '/':
	// case ':':
	// case '<':
	// case '=':
	// case '>':
	// case '^':
	// case '|':
	// return true;
	// default:
	// return false;
	// }
	// }
}
