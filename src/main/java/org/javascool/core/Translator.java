package org.javascool.core;

public class Translator {
	public String getImports() {
		return "";
	}

	public String translate(final String code) {
		return code;
	}

	public static int[] scanConstruct(final String string, final int offset, final String start, final String stop,
			final String escape, final boolean recurse, final boolean code) {
		final int[] index = new int[] { -1, offset };
		if (0 <= offset && offset < string.length() && stop != null) {
			index[0] = code ? Translator.scanComments(string, offset, false) : Translator.scanSpaces(string, offset);
			if (index[0] >= string.length() || start != null && !string.startsWith(start, index[0])) {
				return new int[] { -1, offset };
			} else {
				index[1] = index[0] + (start == null ? 0 : start.length());

				while (index[1] < string.length() && !string.startsWith(stop, index[1])) {
					if (escape != null && escape.length() > 0 && string.startsWith(escape, index[1])) {
						index[1] += escape.length();
					} else if (recurse && start != null && index[1] > index[0] && string.startsWith(start, index[1])) {
						final int[] next
								= Translator.scanConstruct(string, index[1], start, stop, escape, recurse, code);
						if (next[0] == -1) {
							return new int[] { -1, offset };
						}

						index[1] = next[1];
					} else if (code) {
						index[1] = Translator.scanComments(string, index[1], true);
					} else {
						index[1]++;
					}
				}

				if (index[1] < string.length() && string.startsWith(stop, index[1])) {
					index[1] += stop.length();
					return index;
				} else {
					return new int[] { -1, offset };
				}
			}
		} else {
			return new int[] { -1, offset };
		}
	}

	public static int[] scanInstruction(final String string, final int offset) {
		int[] index = Translator.scanConstruct(string, offset, "{", "}", (String) null, true, true);
		if (index[0] == -1) {
			index = Translator.scanConstruct(string, offset, (String) null, ";", (String) null, false, true);
		}

		return index;
	}

	public static int scanComments(final String string, int offset, final boolean next) {
		final int offset0 = offset;

		int offset1;
		for (boolean loop = true; loop; loop = offset > offset1) {
			offset1 = offset;
			offset = Translator.scanSpaces(string, offset);
			offset = Translator.scanConstruct(string, offset, "/*", "*/", (String) null, false, false)[1];
			offset = Translator.scanConstruct(string, offset, "//", "\n", (String) null, false, false)[1];
			offset = Translator.scanConstruct(string, offset, "\"", "\"", "\\\"", false, false)[1];
		}

		return next && offset == offset0 ? offset + 1 : offset;
	}

	public static int scanSpaces(final String string, int offset) {
		while (offset < string.length() && Character.isWhitespace(string.charAt(offset))) {
			++offset;
		}

		return offset;
	}
}
