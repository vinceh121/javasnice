package org.javascool.core;

import java.io.File;

import org.javascool.tools.FileManager;
import org.javascool.tools.UserConfig;

public class Jvs2Jar {
	private Jvs2Jar() {
	}

	public static boolean build(final String name, final String jvsFile, final String jarFile) {
		System.out.println("Compiling " + new File(jvsFile).getName());
		if (name == null) {
			throw new IllegalArgumentException("Proglet name abigious or undefined");
		} else if (!new File(jvsFile).isFile()) {
			throw new IllegalArgumentException("File " + jvsFile + " doesn't exist");
		} else {
			try {
				System.out.println(" extracting javasnice classes (this could take some time, grab a coffee)");
				final String jarDir = UserConfig.createTempDir("jvs-build-").getAbsolutePath();
				JarManager.jarExtract(Utils.javascoolJar(), jarDir);
				final String[] var4 = new String[] { "org/fife", "com/sun/tools/javac", "com/sun/source/tree",
						"com/sun/source/util" };
				final int var5 = var4.length;

				String javaFile;
				for (int var6 = 0; var6 < var5; ++var6) {
					javaFile = var4[var6];
					JarManager.rmDir(new File(jarDir + File.separator + javaFile.replace('/', File.separatorChar)));
				}

				System.out.println(" load proglet");
				final Proglet proglet = new Proglet().load("org/javascool/proglets/" + name);
				final Jvs2Java jvs2java = proglet.getJvs2java();
				System.out.println(" creating java file");
				final String javaCode = jvs2java.translate(FileManager.load(jvsFile),
						new File(jvsFile).getName().replaceFirst("\\.jvs$", ""));
				javaFile = jarDir + File.separator + jvs2java.getClassName() + ".java";
				FileManager.save(javaFile, javaCode);
				System.out.println(" compiling class file");
				if (!Java2Class.compile(javaFile)) {
					System.out.println("Compiling errors for file '" + jvsFile + ".jar'");
					return false;
				} else {
					System.out.println(" building jar file");
					final String mfData = "Manifest-Version: 1.0\nMain-Class: "
							+ jvs2java.getClassName()
							+ "\n"
							+ "Specification-Title: javasnice\n";
					JarManager.jarCreate(jarFile, mfData, jarDir);
					JarManager.rmDir(new File(jarDir));
					System.out.println(" archived successfully :\nFile '" + jvsFile + ".jar' is now available");
					return true;
				}
			} catch (final Throwable var9) {
				System.out.println(var9);
				var9.printStackTrace();
				return false;
			}
		}
	}

	public static boolean build(final String name, final String jvsFile) {
		return Jvs2Jar.build(name, jvsFile, jvsFile + ".jar");
	}

	public static boolean build(final String jvsFile) {
		return Jvs2Jar.build(Utils.javascoolProglet(), jvsFile, jvsFile + ".jar");
	}
}
