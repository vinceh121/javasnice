package org.javascool.macros;

import java.awt.Color;
import java.awt.Component;
import java.awt.Desktop;
import java.awt.Desktop.Action;
import java.awt.Dimension;
import java.net.URI;
import java.net.URL;
import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.SwingUtilities;
import javax.swing.event.HyperlinkEvent.EventType;

import org.javascool.core.ProgletEngine;
import org.javascool.gui.Core;
import org.javascool.tools.FileManager;
import org.javascool.widgets.Dialog;

import lol.javasnice.I18N;
import me.vinceh121.owoifinator.Owoifinator;
import me.vinceh121.owoifinator.modes.OwoCyrillic;
import me.vinceh121.owoifinator.modes.OwoLatin;
import me.vinceh121.owoifinator.modes.OwoSlavLatin;

public class Macros {
	private static long offset;
	private static Dialog messageDialog;
	private static final boolean DISRESPECT = Calendar.getInstance().get(Calendar.MONTH) == Calendar.APRIL
			&& Calendar.getInstance().get(Calendar.DAY_OF_MONTH) == 1;

	private Macros() {
	}

	public static long[] fibonacci(final int max) {
		final long[] sequence = new long[max + 1];
		for (int i = 0; i <= max; i++) {
			sequence[i] = Macros._fib(i);
		}
		return sequence;
	}

	public static long _fib(final int i) {
		if (i <= 1) {
			return i;
		}
		return Macros._fib(i - 1) + Macros._fib(i - 2);
	}

	public static String owo(final String in) {
		return new Owoifinator(new OwoLatin()).processText(in);
	}

	public static String owoCyrillic(final String in) {
		return new Owoifinator(new OwoCyrillic()).processText(in);
	}

	public static String owoSlavLatin(final String in) {
		return new Owoifinator(new OwoSlavLatin()).processText(in);
	}

	public static int random(final int min, final int max) {
		return (int) Math.floor(min + (max - min) * Math.random());
	}

	public static boolean equal(final String string1, final String string2) {
		return string1.equals(string2);
	}

	public static double now() {
		return System.currentTimeMillis() - Macros.offset;
	}

	public static void sleep(final int delay) {
		try {
			if (delay > 0) {
				Thread.sleep(delay);
			} else {
				Thread.sleep(0L, 10000);
			}

		} catch (final Exception var2) {
			throw new RuntimeException(I18N.gettext("Program stopped!"));
		}
	}

	public static Timer sample(final int delay, final TimerTask runnable) {
		final Timer timer = new Timer();
		timer.schedule(runnable, 0L, delay);
		return timer;
	}

	public static void assertion(final boolean condition, final String message, final Object object) {
		if (Core.DEBUG) {
			System.err.println("#" + condition + " : " + message + " ::" + object);
		}
		if (!condition) {
			System.out.println(
					I18N.ngettext("Stopping program :{\n  Assertion(«{0}») is wrong.\n  Stack trace: {", message));
			final StackTraceElement[] where = Thread.currentThread().getStackTrace();

			for (int i = 2; i < where.length - 3; ++i) {
				System.out.println("     "
						+ where[i].toString()
								.replaceAll("JvsToJavaTranslated[0-9]+\\.", "")
								.replaceAll("java:([0-9]+)", "ligne : $1"));
			}

			System.out.println("  }");
			if (object != null) {
				System.out.println(I18N.ngettext("  Causing object:{\n    class = «{0}»\n    value = «{1}»\n  }",
						object.getClass(), object));
			}

			System.out.println("}");
			ProgletEngine.getInstance().doStop();
			Macros.sleep(500);
		}

	}

	public static void assertion(final boolean condition, final String message) {
		Macros.assertion(condition, message, (Object) null);
	}

	public static void message(final String text, final boolean html) {
		final JEditorPane p = new JEditorPane();
		p.setEditable(false);
		p.setOpaque(false);
		if (html) {
			p.setContentType("text/html; charset=utf-8");
			p.addHyperlinkListener(e -> {
				if (e.getDescription() != null && e.getEventType() == EventType.ACTIVATED) {
					Macros.openURL(e.getDescription());
				} else if (e.getDescription() == null) {
					System.out.println(I18N.gettext("Could not redirect to link"));
				}
			});
		}

		p.setText(text);
		p.setBackground(new Color(200, 200, 200, 0));
		Macros.messageDialog = new Dialog();
		Macros.messageDialog.setTitle(I18N.gettext("Javasnice message"));
		Macros.messageDialog.setMinimumSize(new Dimension(300, 100));
		Macros.messageDialog.setMaximumSize(new Dimension(800, 600));
		Macros.messageDialog.setPreferredSize(new Dimension(800, 600));
		Macros.messageDialog.add(p);
		Macros.messageDialog.add(new JButton(I18N.gettext("OK")) {
			/**
			 *
			 */
			private static final long serialVersionUID = -777885713184033186L;

			{
				this.addActionListener(e -> Macros.messageDialog.close());
			}
		}, "South");
		Macros.messageDialog.open(!SwingUtilities.isEventDispatchThread());
	}

	public static void message(final String text) {
		Macros.message(text, false);
	}

	public static ImageIcon getIcon(final String path) {
		URL icon = Macros.getResourceURL(path);

		if (Macros.DISRESPECT) {
			icon = Macros.getResourceURL("icons/LOL.png");
			System.out.println("THE DOCTOR IS HERE");
		}

		if (icon == null) {
			System.out.println(I18N.ngettext("Warning : getIcon({0}) not found", path));
		}

		return icon == null ? null : new ImageIcon(icon);
	}

	public static void openURL(final String location) {
		try {
			if (Desktop.isDesktopSupported() && Desktop.getDesktop().isSupported(Action.BROWSE)) {
				Desktop.getDesktop().browse(new URI(location));
				if (Core.DEBUG) {
					System.out.println("Note: opening " + location + " in an external browser");
				}
			} else {
				Macros.openURL2(location);
			}
		} catch (final Throwable var2) {
			Macros.openURL2(location);
		}

	}

	private static void openURL2(final String location) {
		if (Core.DEBUG) {
			System.out.println("Note: Opening " + location + " in an external browser (second method)");
		}
		final String url = location;
		final String os = System.getProperty("os.name").toLowerCase();
		final Runtime rt = Runtime.getRuntime();

		try {
			if (os.indexOf("win") >= 0) {
				rt.exec("rundll32 url.dll,FileProtocolHandler " + url);
			} else if (os.indexOf("mac") >= 0) {
				rt.exec("open " + url);
			} else {
				if (os.indexOf("nix") < 0 && os.indexOf("nux") < 0) {
					throw new RuntimeException(
							"Warning: while opening URL " + location + " could not detect OS, will try anyway");
				}

				final String[] browsers = new String[] { "epiphany", "firefox", "mozilla", "konqueror", "netscape",
						"opera", "links", "lynx" };
				final StringBuilder cmd = new StringBuilder();

				for (int i = 0; i < browsers.length; ++i) {
					cmd.append(i == 0 ? "" : " || ").append(browsers[i]).append(" \"").append(url).append("\" ");
				}

				rt.exec(new String[] { "sh", "-c", cmd.toString() });
			}

		} catch (final Exception var7) {
			throw new RuntimeException("Error (" + var7 + ") while opening " + location + " in browser");
		}
	}

	public static URL getResourceURL(final String location, final String base, final boolean reading) {
		return FileManager.getResourceURL(location, base, reading);
	}

	public static URL getResourceURL(final String location, final String base) {
		return Macros.getResourceURL(location, base, true);
	}

	public static URL getResourceURL(final String location, final boolean reading) {
		return Macros.getResourceURL(location, (String) null, reading);
	}

	public static URL getResourceURL(final String location) {
		return Macros.getResourceURL(location, (String) null, true);
	}

	public static Component getProgletPane() {
		Component c = null;

		try {
			c = ProgletEngine.getInstance().getProglet().getProgletPane();
		} catch (final Throwable var2) {}

		if (c == null) {
			throw new IllegalStateException(I18N.gettext("Proglet's pane is undefine. Shouldn't happen."));
		} else {
			return c;
		}
	}

	static {
		final Calendar ref = Calendar.getInstance();
		ref.set(2000, 0, 1, 0, 0, 0);
		Macros.offset = ref.getTimeInMillis();
	}
}
