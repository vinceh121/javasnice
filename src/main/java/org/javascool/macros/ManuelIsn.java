package org.javascool.macros;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.util.Locale;
import java.util.Scanner;
import java.util.regex.Pattern;

public class ManuelIsn {
	private static final Object readMonitor = new Object();
	private static final Pattern DOT;
	private static final Pattern EOLN;
	private static final Pattern ALL;

	public static boolean stringEqual(final String s1, final String s2) {
		return s1.equals(s2);
	}

	public static boolean stringAlph(final String s1, final String s2) {
		return s1.compareTo(s2) <= 0;
	}

	public static String stringNth(final String s, final int n) {
		return s.substring(n, n + 1);
	}

	public static int stringLength(final String s) {
		return s.length();
	}

	public static String asciiString(final int n) {
		final byte[] b = new byte[] { (byte) n };
		return new String(b);
	}

	public static int stringCode(final String s) {
		return s.charAt(0);
	}

	private static Scanner scanner(final Reader in) {
		final Scanner scanner = new Scanner(in);
		scanner.useLocale(Locale.US);
		return scanner;
	}

	public static Scanner openIn(final String name) {
		try {
			final FileInputStream fis = new FileInputStream(name);
			final Scanner scanner = ManuelIsn.scanner(new InputStreamReader(fis));
			return scanner;
		} catch (final FileNotFoundException var3) {
			return ManuelIsn.scanner(new InputStreamReader(System.in));
		}
	}

	public static void closeIn(final Scanner s) {
		s.close();
	}

	public static int readIntFromFile(final Scanner s) {
		synchronized (ManuelIsn.readMonitor) {
			return s.nextInt();
		}
	}

	public static double readDoubleFromChar(final Scanner s) {
		synchronized (ManuelIsn.readMonitor) {
			return s.nextDouble();
		}
	}

	public static String readCharacterFromFile(final Scanner s) {
		synchronized (ManuelIsn.readMonitor) {
			return String.valueOf(s.findWithinHorizon(ManuelIsn.DOT, 1).charAt(0));
		}
	}

	public static String readStringFromFile(final Scanner s) {
		final String r = s.findWithinHorizon(ManuelIsn.EOLN, 0);
		if (r == null) {
			return s.findWithinHorizon(ManuelIsn.ALL, 0);
		} else if (r.length() == 1) {
			return "";
		} else {
			final int pos = r.charAt(r.length() - 2) == '\r' ? r.length() - 2 : r.length() - 1;
			return r.substring(0, pos);
		}
	}

	public static OutputStreamWriter openOut(final String name) {
		try {
			final FileOutputStream fos = new FileOutputStream(name);
			final OutputStreamWriter out = new OutputStreamWriter(fos, "UTF-8");
			return out;
		} catch (final FileNotFoundException var3) {
			return new OutputStreamWriter(System.out);
		} catch (final UnsupportedEncodingException var4) {
			return new OutputStreamWriter(System.out);
		}
	}

	public static void closeOut(final OutputStreamWriter s) {
		try {
			s.close();
		} catch (final IOException var2) {}

	}

	public static void printlnToFile(final OutputStreamWriter s) {
		try {
			s.write(System.getProperty("line.separator"));
		} catch (final IOException var2) {}

	}

	public static void printToFile(final OutputStreamWriter s, final String n) {
		try {
			s.write(n);
		} catch (final IOException var3) {}

	}

	public static void printlnToFile(final OutputStreamWriter s, final String n) {
		ManuelIsn.printToFile(s, n);
		ManuelIsn.printlnToFile(s);
	}

	public static void printToFile(final OutputStreamWriter s, final int n) {
		try {
			s.write(String.valueOf(n));
		} catch (final IOException var3) {}

	}

	public static void printlnToFile(final OutputStreamWriter s, final int n) {
		ManuelIsn.printToFile(s, n);
		ManuelIsn.printlnToFile(s);
	}

	public static void printToFile(final OutputStreamWriter s, final double n) {
		try {
			s.write(String.valueOf(n));
		} catch (final IOException var4) {}

	}

	public static void printlnToFile(final OutputStreamWriter s, final double n) {
		ManuelIsn.printToFile(s, n);
		ManuelIsn.printlnToFile(s);
	}

	static {
		DOT = Pattern.compile(".", 32);
		EOLN = Pattern.compile(".*?(?:\r(?!\n)|\n|\r\n)");
		ALL = Pattern.compile(".*+");
	}
}
