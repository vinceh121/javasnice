package org.javascool.macros;

import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;

import javax.swing.RepaintManager;

import org.javascool.gui.Desktop;
import org.javascool.widgets.Console;

public class Stdout {
	private Stdout() {
	}

	public static void echo(final String string) {
		Console.getInstance();
		System.out.println(string);
	}

	public static void echo(final int string) {
		Stdout.echo("" + string);
	}

	public static void echo(final char string) {
		Stdout.echo("" + string);
	}

	public static void echo(final double string) {
		Stdout.echo("" + string);
	}

	public static void echo(final boolean string) {
		Stdout.echo("" + string);
	}

	public static void echo(final Object string) {
		Stdout.echo("" + string);
	}

	public static void echo() {
		Stdout.echo("");
	}

	public static void println(final String string) {
		Console.getInstance();
		System.out.println(string);
		Desktop.getInstance().focusOnConsolePanel();
	}

	public static void println(final int i) {
		Stdout.println("" + i);
	}

	public static void println(final char i) {
		Stdout.println("" + i);
	}

	public static void println(final double d) {
		Stdout.println("" + d);
	}

	public static void println(final boolean b) {
		Stdout.println("" + b);
	}

	public static void println(final Object o) {
		Stdout.println("" + o);
	}

	public static void println() {
		Stdout.println("");
	}

	public static void print(final String string) {
		Console.getInstance();
		System.out.print(string);
		System.out.flush();
	}

	public static void print(final int i) {
		Stdout.print("" + i);
	}

	public static void print(final char i) {
		Stdout.print("" + i);
	}

	public static void print(final double d) {
		Stdout.print("" + d);
	}

	public static void print(final boolean b) {
		Stdout.print("" + b);
	}

	public static void print(final Object o) {
		Stdout.print("" + o);
	}

	public static void clear() {
		Console.getInstance().clear();
	}

	public static void saveConsoleOutput(final String location) {
		Console.getInstance().saveConsoleOutput(location);
	}

	public static void sendConsoleToPrinter() {
		Stdout.sendToPrinter(Console.getInstance());
	}

	public static void sendProgletToPrinter() {
		Stdout.sendToPrinter(Macros.getProgletPane());
	}

	public static void sendToPrinter(final Component c) {
		new Stdout.Printer(c).print();
	}

	private static class Printer implements Printable {
		private final Component component;

		private Printer(final Component component) {
			this.component = component;
		}

		private void print() {
			final PrinterJob printJob = PrinterJob.getPrinterJob();
			printJob.setPrintable(this);
			if (printJob.printDialog()) {
				try {
					printJob.print();
				} catch (final PrinterException var3) {
					System.out.println("Error printing: " + var3);
				}
			}

		}

		@Override
		public int print(final Graphics g, final PageFormat pf, final int pi) {
			if (pi > 0) {
				return 1;
			} else {
				final Graphics2D g2d = (Graphics2D) g;
				g2d.translate(pf.getImageableX(), pf.getImageableY());
				final double factor = Math.min(pf.getImageableWidth() / this.component.getWidth(),
						pf.getImageableHeight() / this.component.getHeight());
				g2d.scale(factor, factor);
				final RepaintManager currentManager = RepaintManager.currentManager(this.component);
				final boolean db = currentManager.isDoubleBufferingEnabled();
				currentManager.setDoubleBufferingEnabled(false);
				this.component.printAll(g2d);
				currentManager.setDoubleBufferingEnabled(db);
				return 0;
			}
		}
	}
}
