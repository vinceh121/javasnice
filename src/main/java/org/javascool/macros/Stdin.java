package org.javascool.macros;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.javascool.tools.FileManager;
import org.javascool.widgets.Dialog;

import lol.javasnice.I18N;

public class Stdin {
	private static Dialog inputDialog;
	private static char inputSeparator;
	private static String inputQuestion;
	private static String inputString;
	private static Stdin.InputBuffer inputBuffer = new Stdin.InputBuffer();
	private static Runnable keyListenerRunnable = null;
	private static KeyListener keyKeyListener = null;
	private static String lastKey = "";
	private static MouseListener keyMouseListener = null;

	private Stdin() {
	}

	public static String readString(final String question) {
		return Stdin.readString(question, '\n');
	}

	public static String readString(final String question, final char separator) {
		if (Stdin.inputBuffer.isPopable()) {
			return Stdin.inputBuffer.popString(separator);
		} else {
			Stdin.inputQuestion = question;
			Stdin.inputSeparator = separator;
			Stdin.inputString = null;
			Stdin.inputDialog = new Dialog();
			Stdin.inputDialog.setTitle(I18N.gettext("Javasnice read"));
			Stdin.inputDialog.add(new JPanel() {
				/**
				 * 
				 */
				private static final long serialVersionUID = -5892042920567248401L;

				{
					this.add(new JLabel(Stdin.inputQuestion + " "));
					this.add(new JTextField(40) {
						/**
						 * 
						 */
						private static final long serialVersionUID = 1L;

						{
							this.addActionListener(e -> {
								Stdin.inputString = ((JTextField) e.getSource()).getText();
								Stdin.inputDialog.close();
							});
							this.addCaretListener(e -> {
								Stdin.inputString = ((JTextField) e.getSource()).getText();
								if (Stdin.inputSeparator == 0) {
									Stdin.inputDialog.close();
								} else if (Stdin.inputSeparator == ' ' && Stdin.inputString.length() > 0 && Character
										.isWhitespace(Stdin.inputString.charAt(Stdin.inputString.length() - 1))) {
									Stdin.inputString = Stdin.inputString.substring(0, Stdin.inputString.length() - 1);
									Stdin.inputDialog.close();
								}

							});
						}
					});
				}
			});
			Stdin.inputDialog.open(true);
			return Stdin.inputString == null ? "" : Stdin.inputString;
		}
	}

	public static String readString() {
		return Stdin.readString(I18N.gettext("Enter a string:"));
	}

	public static String readWord(final String question) {
		return Stdin.readString(question, ' ');
	}

	public static String readWord() {
		return Stdin.readWord(I18N.gettext("Enter a word:"));
	}

	public static char readChar(final String question) {
		final String s = Stdin.readString(question, '\u0000');
		return s.length() > 0 ? s.charAt(0) : '\u0000';
	}

	public static char readChar() {
		return Stdin.readChar(I18N.gettext("Enter a character:"));
	}

	public static char readCharacter(final String question) {
		return Stdin.readString(question, '\u0000').charAt(0);
	}

	public static char readCharacter() {
		return Stdin.readChar(I18N.gettext("Enter a character:"));
	}

	public static int readInteger(String question) {
		if (Stdin.inputBuffer.isPopable()) {
			return Stdin.inputBuffer.popInteger();
		} else {
			final String s = Stdin.readString(question, ' ');

			try {
				return Integer.decode(s);
			} catch (final Exception var3) {
				if (!question.endsWith(I18N.gettext(" (Pwease input integer)"))) {
					question = question + I18N.gettext(" (Pwease input integer)");
				}

				return s.equals("") ? 0 : Stdin.readInteger(question);
			}
		}
	}

	public static int readInteger() {
		return Stdin.readInteger(I18N.gettext("Enter an integer: "));
	}

	public static int readInt(final String question) {
		return Stdin.readInteger(question);
	}

	public static int readInt() {
		return Stdin.readInteger();
	}

	public static long readLong(String question) {
		if (Stdin.inputBuffer.isPopable()) {
			return Stdin.inputBuffer.popLong();
		} else {
			final String s = Stdin.readString(question, ' ');

			try {
				return Long.decode(s);
			} catch (final Exception var3) {
				if (!question.endsWith(I18N.gettext(" (Pwease input integer)"))) {
					question = question + I18N.gettext(" (Pwease input integer)");
				}

				return s.equals("") ? 0L : Stdin.readLong(question);
			}
		}
	}

	public static long readLong() {
		return Stdin.readLong(I18N.gettext("Enter a long: "));
	}

	public static double readDecimal(String question) {
		if (Stdin.inputBuffer.isPopable()) {
			return Stdin.inputBuffer.popDecimal();
		} else {
			final String s = Stdin.readString(question, ' ');

			try {
				return Double.parseDouble(s);
			} catch (final Exception var3) {
				if (!question.endsWith(I18N.gettext(" (Pwease enter a number)"))) {
					question = question + I18N.gettext(" (Pwease enter a number)");
				}

				return s.equals("") ? 0.0D : Stdin.readDecimal(question);
			}
		}
	}

	public static double readDecimal() {
		return Stdin.readDecimal(I18N.gettext("Enter a decimal number: "));
	}

	public static double readDouble(final String question) {
		return Stdin.readDecimal(question);
	}

	public static double readDouble() {
		return Stdin.readDecimal();
	}

	public static double readFloat(final String question) {
		return Stdin.readDecimal(question);
	}

	public static double readFloat() {
		return Stdin.readDecimal();
	}

	public static boolean readBoolean(final String question) {
		if (Stdin.inputBuffer.isPopable()) {
			return Stdin.inputBuffer.popBoolean();
		} else {
			Stdin.inputQuestion = question;
			Stdin.inputString = null;
			Stdin.inputDialog = new Dialog();
			Stdin.inputDialog.setTitle(I18N.gettext("Javasnice read"));
			Stdin.inputDialog.add(new JPanel() {
				/**
				 * 
				 */
				private static final long serialVersionUID = -3550776051477271733L;

				{
					this.add(new JLabel(Stdin.inputQuestion + " "));
					this.add(new JButton(I18N.gettext("YES")) {
						/**
						 * 
						 */
						private static final long serialVersionUID = 1L;

						{
							this.addActionListener(e -> {
								Stdin.inputString = I18N.gettext("YES");
								Stdin.inputDialog.close();
							});
						}
					});
					this.add(new JButton(I18N.gettext("NO")) {
						/**
						 * 
						 */
						private static final long serialVersionUID = 1L;

						{
							this.addActionListener(e -> {
								Stdin.inputString = I18N.gettext("NO");
								Stdin.inputDialog.close();
							});
						}
					});
				}
			});
			Stdin.inputDialog.open(true);
			return I18N.gettext("YES").equals(Stdin.inputString);
		}
	}

	public static boolean readBoolean() {
		return Stdin.readBoolean(I18N.gettext("Enter a boolean: "));
	}

	public static Boolean readBool(final String question) {
		return Stdin.readBoolean(question);
	}

	public static Boolean readBool() {
		return Stdin.readBoolean();
	}

	public static void clearConsoleInput() {
		Stdin.inputBuffer.clear();
	}

	public static void addConsoleInput(final String string) {
		Stdin.inputBuffer.add(string);
	}

	public static void loadConsoleInput(final String location) {
		Stdin.clearConsoleInput();
		Stdin.addConsoleInput(FileManager.load(location));
	}

	public static void setKeyListener(final Runnable runnable) {
		if (Stdin.keyKeyListener != null) {
			Macros.getProgletPane().removeKeyListener(Stdin.keyKeyListener);
		}

		if (Stdin.keyMouseListener != null) {
			Macros.getProgletPane().removeMouseListener(Stdin.keyMouseListener);
		}

		if (Macros.getProgletPane() != null) {
			Stdin.keyListenerRunnable = runnable;
			if (runnable != null) {
				Macros.getProgletPane().setFocusable(true);
				Macros.getProgletPane().addMouseListener(Stdin.keyMouseListener = new MouseListener() {
					@Override
					public void mousePressed(final MouseEvent e) {
					}

					@Override
					public void mouseReleased(final MouseEvent e) {
					}

					@Override
					public void mouseClicked(final MouseEvent e) {
					}

					@Override
					public void mouseEntered(final MouseEvent e) {
						Macros.getProgletPane().requestFocus();
						Macros.getProgletPane().requestFocusInWindow();
					}

					@Override
					public void mouseExited(final MouseEvent e) {
					}
				});
				Macros.getProgletPane().addKeyListener(Stdin.keyKeyListener = new KeyListener() {
					@Override
					public void keyPressed(final KeyEvent e) {
					}

					@Override
					public void keyReleased(final KeyEvent e) {
						final String s = KeyEvent.getKeyText(e.getKeyCode());
						if ((e.getModifiersEx() & 2) != 0) {
							Stdin.lastKey = "Ctrl+" + s;
						} else {
							final int c = e.getKeyChar();
							if (' ' <= c && c < 127) {
								Stdin.lastKey = "" + e.getKeyChar();
							} else {
								if ("Shift".equals(s) || "Ctrl".equals(s)) {
									return;
								}

								Stdin.lastKey = s;
							}
						}

						if (Stdin.keyListenerRunnable != null) {
							Stdin.keyListenerRunnable.run();
						}

					}

					@Override
					public void keyTyped(final KeyEvent e) {
					}
				});
			}
		}

	}

	public static String getLastKey() {
		return Stdin.lastKey;
	}

	private static class InputBuffer {
		String inputs;

		private InputBuffer() {
			this.inputs = new String();
		}

		public void add(final String string) {
			this.inputs = this.inputs + string.trim() + "\n";
		}

		public void clear() {
			this.inputs = "";
		}

		public boolean isPopable() {
			return this.inputs.length() > 0;
		}

		public String popString(final char separator) {
			Macros.sleep(500);
			int i = separator == 0 ? 1 : this.inputs.indexOf(10);
			if (separator == ' ') {
				final int i1 = this.inputs.indexOf(32);
				if (i1 != -1 && (i == -1 || i1 < i)) {
					i = i1;
				}
			}

			if (i != -1) {
				final String input = this.inputs.substring(0, i);
				this.inputs = this.inputs.substring(separator == 0 ? i : i + 1);
				return input;
			} else {
				return "";
			}
		}

		public int popInteger() {
			try {
				return Integer.decode(this.popString(' '));
			} catch (final Exception var2) {
				return 0;
			}
		}

		public long popLong() {
			try {
				return Long.decode(this.popString(' '));
			} catch (final Exception var2) {
				return 0L;
			}
		}

		public double popDecimal() {
			try {
				return Double.parseDouble(this.popString(' '));
			} catch (final Exception var2) {
				return 0.0D;
			}
		}

		public boolean popBoolean() {
			return this.popString(' ').toLowerCase().matches("[tyvo1].*");
		}

	}
}
