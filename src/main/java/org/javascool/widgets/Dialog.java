package org.javascool.widgets;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JDialog;
import javax.swing.SwingUtilities;

import org.javascool.macros.Macros;

public class Dialog extends JDialog {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3508966264239125706L;
	private boolean pending = false;

	public Dialog() {
		super(MainFrame.getFrame());
		this.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(final WindowEvent e) {
				Dialog.this.pending = false;
			}
		});
	}

	public void open(final boolean modal) {
		this.setDefaultCloseOperation(2);
		this.pack();
		if (MainFrame.getFrame() != null) {
			this.setLocation((MainFrame.getFrame().getWidth() - this.getWidth()) / 2,
					(MainFrame.getFrame().getHeight() - this.getHeight()) / 2);
		}

		this.setVisible(true);
		if (modal && SwingUtilities.isEventDispatchThread()) {
			throw new IllegalStateException("Cannot open modal dialog on event dispatcher thread. Learn how to swing.");
		} else {
			this.pending = modal;

			while (this.pending) {
				Macros.sleep(100);
			}

		}
	}

	public void close() {
		this.dispose();
		this.pending = false;
	}

	public boolean isOpen() {
		return this.pending;
	}
}
