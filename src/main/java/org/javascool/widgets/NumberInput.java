package org.javascool.widgets;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JTextField;

public class NumberInput extends JPanel {
	private static final long serialVersionUID = 1L;
	private final JTextField field;
	private final JSlider slider;
	private Runnable runnable = null;
	private double min;
	private double max;
	private double step;
	private double value;

	public NumberInput() {
		this.setPreferredSize(new Dimension(400, 62));
		this.field = new JTextField(12);
		this.field.addActionListener(evt -> {
			try {
				NumberInput.this.set(Double.parseDouble(NumberInput.this.field.getText()), 'T');
			} catch (final NumberFormatException ignored) {}
		});
		this.add(this.field);
		this.slider = new JSlider();
		this.slider.setFont(new Font("Dialog", Font.PLAIN, 0));
		this.slider.addMouseListener(new MouseListener() {
			@Override
			public void mouseClicked(final MouseEvent e) {
			}

			@Override
			public void mouseEntered(final MouseEvent e) {
			}

			@Override
			public void mouseExited(final MouseEvent e) {
			}

			@Override
			public void mousePressed(final MouseEvent e) {
			}

			@Override
			public void mouseReleased(final MouseEvent e) {
				NumberInput.this
						.set((NumberInput.this.max - NumberInput.this.min) / 100.0D * NumberInput.this.slider.getValue()
								+ NumberInput.this.min, 'S');
			}
		});
		this.add(this.slider);
		this.setText("");
		this.setScale(0.0D, 100.0D, 1.0D);
		this.setValue(0.0D);
	}

	private void set(double value, final char from) {
		value = this.step <= 0.0D ? value : this.min + this.step * Math.rint((value - this.min) / this.step);
		value = value < this.min ? this.min : value > this.max ? this.max : value;
		this.value = value;
		this.field.setText(
				Double.toString(value).toString().replaceFirst("(99999|00000).*$", "").replaceFirst(".0$", ""));
		if (from != 'S') {
			this.slider.setValue(
					(int) (this.max > this.min ? 100.0D * (value - this.min) / (this.max - this.min) : value));
		}

		if (from != ' ' && this.runnable != null) {
			this.runnable.run();
		}

	}

	public final NumberInput setText(final String name) {
		this.setBorder(BorderFactory.createTitledBorder(name));
		return this;
	}

	public final NumberInput setScale(final double min, final double max, final double step) {
		this.min = min;
		this.max = max;
		this.step = step;
		return this;
	}

	public final NumberInput setScale() {
		return this.setScale(0.0D, 100.0D, 1.0D);
	}

	public double getValue() {
		return this.value;
	}

	public void setValue(final double value) {
		this.set(value, ' ');
	}

	public NumberInput setRunnable(final Runnable runnable) {
		this.runnable = runnable;
		return this;
	}
}
