package org.javascool.widgets;

import java.awt.BorderLayout;
import java.awt.Font;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import org.javascool.tools.FileManager;

import lol.javasnice.I18N;
import lol.javasnice.OwoPrintStream;

public class Console extends JPanel {
	private static final long serialVersionUID = 1L;
	private static ArrayList<Console> consoles = new ArrayList<>();
	private static Console console = null;
	private static PrintStream defaultStdout;
	private static boolean redirectedStdout;
	private static PrintStream defaultStderr;
	private static boolean redirectedStderr;
	private final JTextArea outputPane;
	private final ToolBar toolbar;
	private JLabel status;
	private static final String[] prefixes;

	public static Console newInstance(final boolean popup) {
		Console.console = new Console();
		if (popup) {
			new MainFrame().reset(I18N.gettext("Console"), "icons/copyAll.png", 600, 400, Console.console);
		}

		Console.consoles.add(Console.console);
		return Console.console;
	}

	public static Console newInstance() {
		return Console.newInstance(false);
	}

	public static void removeInstance(final Console console) {
		if (console != null) {
			Console.consoles.remove(console);
		}

	}

	public static Console getInstance() {
		return Console.console == null ? Console.newInstance(true) : Console.console;
	}

	private static void redirectStdout(final boolean redirect) {
		if (redirect && !Console.redirectedStdout) {
			final OutputStream out = new OutputStream() {
				@Override
				public void write(final int b) {
					Console.printAll(String.valueOf((char) b));
					Console.defaultStdout.write(b);
				}

				@Override
				public void write(final byte[] b, final int off, final int len) {
					Console.printAll(new String(b, off, len));
					Console.defaultStdout.write(b, off, len);
				}

				@Override
				public void write(final byte[] b) throws IOException {
					this.write(b, 0, b.length);
					Console.defaultStdout.write(b);
				}
			};
			System.setOut(new OwoPrintStream(out, true));
			Console.redirectedStdout = true;
		} else if (Console.redirectedStdout && !redirect) {
			System.setOut(Console.defaultStdout);
			Console.redirectedStdout = false;
		}

	}

	public static void redirectStderr(final boolean redirect) {
		if (redirect && !Console.redirectedStderr) {
			final OutputStream err = new OutputStream() {
				@Override
				public void write(final int b) throws IOException {
					Console.printAll(String.valueOf((char) b));
					Console.defaultStderr.write(b);
				}

				@Override
				public void write(final byte[] b, final int off, final int len) throws IOException {
					Console.printAll(new String(b, off, len));
					Console.defaultStderr.write(b, off, len);
				}

				@Override
				public void write(final byte[] b) throws IOException {
					this.write(b, 0, b.length);
					Console.defaultStderr.write(b);
				}
			};
			System.setErr(new OwoPrintStream(err, true));
			Console.redirectedStderr = true;
		} else if (Console.redirectedStderr && !redirect) {
			System.setErr(Console.defaultStderr);
			Console.redirectedStderr = false;
		}

	}

	private static void printAll(final String text) {
		for (final Object o : Console.consoles) {
			final Console console = (Console) o;
			console.print(text);
		}
	}

	private Console() {
		final BorderLayout layout = new BorderLayout();
		this.setLayout(layout);
		this.outputPane = new JTextArea();
		this.outputPane.setEditable(false);
		this.outputPane.setFont(new Font("Monospaced", 0, 12));
		// float[] bg = Color.RGBtoHSB(46, 46, 46, null); // @Bowser65 did you do this
		// shit?
		// this.outputPane.setBackground(Color.getHSBColor(bg[0], bg[1], bg[2])); //
		// this shit background color definition
		// // here fucking hideous
		final JScrollPane scrolledOutputPane = new JScrollPane(this.outputPane);
		this.add(scrolledOutputPane, "Center");
		this.toolbar = new ToolBar();
		this.toolbar.addTool(I18N.gettext("Clear"), "icons/erase.png", () -> Console.this.clear());
		this.toolbar.addTool(I18N.gettext("Copy All"), "icons/copyAll.png", () -> Console.this.copyAll());
		this.toolbar.addTool(I18N.gettext("Copy Selection"), "icons/copySelection.png",
				() -> Console.this.copySelection());

		this.toolbar.addSeparator();
		this.toolbar.addTool("status", this.status = new JLabel("                                         "));
		this.add(this.toolbar, "North");
		Console.redirectStdout(true);
	}

	public void clear() {
		this.outputPane.setText("");
	}

	private void copyAll() {
		this.outputPane.selectAll();
		this.outputPane.copy();
	}

	private void copySelection() {
		this.outputPane.copy();
	}

	public void show(final String text) {
		this.status.setText(text);
	}

	public void print(final String text) {
		final String[] var2 = Console.prefixes;
		final int var3 = var2.length;

		for (int var4 = 0; var4 < var3; ++var4) {
			final String p = var2[var4];
			if (text.startsWith(p)) {
				return;
			}
		}

		this.outputPane.append(text);
	}

	public String getText() {
		return this.outputPane.getText();
	}

	public ToolBar getToolBar() {
		return this.toolbar;
	}

	public void saveConsoleOutput(final String location) {
		FileManager.save(location, this.getText());
	}

	static {
		Console.defaultStdout = System.out;
		Console.redirectedStdout = false;
		Console.defaultStderr = System.err;
		Console.redirectedStderr = false;
		prefixes = new String[] { "=== Minim Error ===", "=== Likely buffer underrun in AudioOutput.",
				"==== JavaSound Minim Error ====" };
	}
}
