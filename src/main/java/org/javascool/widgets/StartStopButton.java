package org.javascool.widgets;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.javascool.macros.Macros;

import lol.javasnice.I18N;

public abstract class StartStopButton extends JPanel {
	private static final long serialVersionUID = 1L;
	private JButton startButton;
	private JLabel execTime;

	public StartStopButton() {
		this.setOpaque(false);
		this.add(this.startButton = new JButton(I18N.gettext("Stop")));
		this.startButton.addActionListener(e -> new Thread(() -> {
			this.onActionCauseSwingBad();

		}).start());
		this.add(this.execTime = new JLabel(I18N.gettext("  Execution time: 0 min 0 sec")));
		this.doStop();
	}

	public void onActionCauseSwingBad() {
		if (StartStopButton.this.isStarting()) {
			StartStopButton.this.doStop();
		} else {
			StartStopButton.this.doStart();
		}
	}

	private void doStart() {
		if (this.isRunning()) {
			this.stop();
		}

		this.execTime.setText(I18N.gettext("  Execution time: 0 min 0 sec"));
		this.startButton.setText(I18N.gettext("Stop"));
		this.startButton.setIcon(Macros.getIcon("icons/stop.png"));
		this.revalidate();
		final Thread isRunningThread = new Thread(() -> {
			while (StartStopButton.this.isRunning()) {
				Macros.sleep(50);
			}

			StartStopButton.this.doStop();
		});
		final Thread timerThread = new Thread(() -> {
			for (int t = 0; StartStopButton.this.isRunning(); ++t) {
				StartStopButton.this.execTime
						.setText(I18N.ngettext("  Execution time: {0} min {1} sec", t / 60, t % 60));
				StartStopButton.this.execTime.revalidate();
				Macros.sleep(1000);
			}

		});
		this.start();
		isRunningThread.start();
		timerThread.start();
	}

	private void doStop() {
		if (this.isRunning()) {
			this.stop();
		}

		this.startButton.setText(I18N.gettext("Execute"));
		this.startButton.setIcon(Macros.getIcon("icons/play.png"));
		this.revalidate();
	}

	public boolean isRunning() {
		return this.isStarting();
	}

	private boolean isStarting() {
		return I18N.gettext("Stop").equals(this.startButton.getText());
	}

	public abstract void start();

	public abstract void stop();
}
