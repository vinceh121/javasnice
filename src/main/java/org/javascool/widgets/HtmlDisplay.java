package org.javascool.widgets;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.event.HyperlinkEvent.EventType;

import org.javascool.macros.Macros;

import lol.javasnice.I18N;

public class HtmlDisplay extends JPanel {
	private static final long serialVersionUID = 1L;
	private final JEditorPane pane;
	private final JButton home;
	private final JButton prev;
	private final JButton next;
	private static final String STRING_PREFIX = "http://string?";
	private static final String EDITOR_PREFIX = "http://editor?";
	private static final String NEWTAB_PREFIX = "http://newtab?";
	private final HtmlDisplay.URLStack urls = new HtmlDisplay.URLStack();

	public HtmlDisplay() {
		this.setLayout(new BorderLayout());
		final ToolBar bar = new ToolBar();
		this.home = bar.addTool(I18N.gettext("Initial page"), "icons/refresh.png", () -> {
			if (HtmlDisplay.this.urls.hasHome()) {
				HtmlDisplay.this.update(HtmlDisplay.this.urls.getHome(), false);
			}
		});
		this.prev = bar.addTool(I18N.gettext("Previous page"), "icons/prev.png", () -> {
			if (HtmlDisplay.this.urls.hasPrev()) {
				HtmlDisplay.this.update(HtmlDisplay.this.urls.getPrev(), false);
			}
		});
		this.next = bar.addTool(I18N.gettext("Next page"), "icons/next.png", () -> {
			if (HtmlDisplay.this.urls.hasNext()) {
				HtmlDisplay.this.update(HtmlDisplay.this.urls.getNext(), false);
			}

		});
		this.add(bar, "North");
		this.pane = new JEditorPane();
		this.pane.setBackground(new Color(46, 46, 46));
		this.pane.setEditable(false);
		this.pane.setContentType("text/html; charset=utf-8");
		this.pane.addHyperlinkListener(e -> {
			if (e.getEventType() == EventType.ACTIVATED) {
				HtmlDisplay.this.update(e.getDescription(), true);
			}
		});
		final JScrollPane spane = new JScrollPane(this.pane, ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS,
				ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		this.add(spane, "Center");
	}

	public HtmlDisplay setPage(final String location) {
		this.update(location, true);
		return this;
	}

	public HtmlDisplay setPage(final URL location) {
		this.update(location, true);
		return this;
	}

	public HtmlDisplay setText(final String text) {
		try {
			return this.setPage(HtmlDisplay.STRING_PREFIX + URLEncoder.encode(text, "utf-8"));
		} catch (final UnsupportedEncodingException var3) {
			throw new IllegalStateException("Unknown encoding: (" + var3 + ")");
		}
	}

	private void updateButtons() {
		this.home.setEnabled(this.urls.hasHome());
		this.prev.setEnabled(this.urls.hasPrev());
		this.next.setEnabled(this.urls.hasNext());
	}

	public boolean doBrowse(final String location) {
		return false;
	}

	private void browse(final String location) {
		try {
			Desktop.getDesktop().browse(new URI(location));
		} catch (final Exception var3) {
			this.setText("Page adderess: <tt>«"
					+ location.replaceFirst("^(mailto):.*", "$1: ...")
					+ "»</tt> (not available here).");
		}

	}

	private void update(final String location, final boolean push) {
		if (location.startsWith(HtmlDisplay.STRING_PREFIX)) {
			try {
				if (push) {
					this.urls.push(location);
				}

				this.pane.setText(URLDecoder.decode(location.substring(HtmlDisplay.STRING_PREFIX.length()), "utf-8"));
			} catch (final UnsupportedEncodingException var5) {
				throw new IllegalStateException("Unknown encoding: (" + var5 + ")");
			}
		} else if (location.startsWith(HtmlDisplay.EDITOR_PREFIX)) {
			org.javascool.gui.Desktop.getInstance()
					.openFile(this.toURL(location.substring(HtmlDisplay.EDITOR_PREFIX.length())));
		} else if (location.startsWith(HtmlDisplay.NEWTAB_PREFIX)) {
			final URL url = this.toURL(location.substring(HtmlDisplay.NEWTAB_PREFIX.length()));
			final String name = new File(url.getPath()).getName().replaceFirst("\\.[^\\.]*$", "").replace('_', '.');
			org.javascool.gui.Desktop.getInstance()
					.openBrowserTab(url.toString(), name.substring(0, 1).toUpperCase() + name.substring(1));
		} else if (location.matches("^(http|https|rtsp|mailto):.*$")) {
			this.browse(location);
		} else if (!location.matches(".*\\.(htm|html)$") && !location.matches("^#.*")) {
			if (!this.doBrowse(location)) {
				this.setText("URL: <tt>«" + location + "»</tt> could not be shown");
			}
		} else {
			this.update(this.toURL(location), push);
		}

	}

	private void update(final URL url, final boolean push) {
		if (push) {
			this.urls.push(url);
		}

		this.pane.getDocument().putProperty("stream", (Object) null);

		try {
			this.pane.setPage(url);
		} catch (final IOException var4) {
			this.setText("Link: <tt>«" + url + "»</tt> throws an error \"" + var4.toString() + "\"");
		}

		this.updateButtons();
	}

	private void update(final Object link, final boolean push) {
		if (link instanceof URL) {
			this.update((URL) link, push);
		} else {
			this.update(link.toString(), push);
		}

	}

	private URL toURL(final String location) {
		try {
			return this.urls.isEmpty() ? Macros.getResourceURL(location)
					: this.urls.getCurrent() instanceof URL ? new URL((URL) this.urls.getCurrent(), location)
							: new URL(location);
		} catch (final MalformedURLException var5) {
			try {
				return new URL("http://string?Link: <tt>«" + location + "»</tt> is malformed");
			} catch (final MalformedURLException var4) {
				throw new IllegalStateException(var4);
			}
		}
	}

	private class URLStack extends ArrayList<Object> {
		private static final long serialVersionUID = 1L;
		private int current;

		private URLStack() {
			this.current = -1;
		}

		public void push(final Object url) {
			++this.current;

			while (this.current < this.size()) {
				this.remove(this.current);
			}

			this.add(url);
		}

		public Object getCurrent() {
			return this.current >= 0 ? this.get(this.current) : null;
		}

		public boolean hasHome() {
			return this.current >= 0;
		}

		public Object getHome() {
			if (this.hasHome()) {
				this.current = 0;
			}

			return this.getCurrent();
		}

		public boolean hasPrev() {
			return this.current > 0;
		}

		public Object getPrev() {
			if (this.hasPrev()) {
				--this.current;
			}

			return this.getCurrent();
		}

		public boolean hasNext() {
			return this.current < this.size() - 1;
		}

		public Object getNext() {
			if (this.hasNext()) {
				++this.current;
			}

			return this.getCurrent();
		}

		@Override
		public String toString() {
			String s = "";

			for (int i = this.size() - 1; i >= 0; --i) {
				s = s + (i == this.current ? " * " : "   ") + this.get(i) + "\n";
			}

			return s;
		}
	}
}
