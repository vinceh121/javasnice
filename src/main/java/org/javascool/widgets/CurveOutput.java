package org.javascool.widgets;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.JPanel;

public class CurveOutput extends JPanel {
	private static final long serialVersionUID = 1L;
	private ArrayList<ArrayList<point>> curves = new ArrayList<>();
	private ArrayList<line> lines = new ArrayList<>();
	private final ArrayList<rectangle> rectangles = new ArrayList<>();
	private ArrayList<oval> ovals = new ArrayList<>();
	private ArrayList<block> blocks = new ArrayList<>();
	private ArrayList<label> labels = new ArrayList<>();
	private int width;
	private int height;
	private int i0;
	private int j0;
	private int w0;
	private int h0;
	private double Xoffset;
	private double Yoffset;
	private double Xscale;
	private double Yscale;
	private double reticuleX;
	private double reticuleY;
	private static Color[] colors;
	private Runnable runnable;

	public CurveOutput() {
		final CurveOutput.ReticuleMouseListener l = new CurveOutput.ReticuleMouseListener();
		this.addMouseMotionListener(l);
		this.addMouseListener(l);
		this.reset(0.0D, 0.0D, 1.0D, 1.0D);
		this.Xoffset = 0.0D;
		this.Yoffset = 0.0D;
		this.Xscale = 1.0D;
		this.Yscale = 1.0D;
		this.reticuleX = 0.0D;
		this.reticuleY = 0.0D;
		this.runnable = null;
	}

	@Override
	public void paint(final Graphics g) {
		try {
			this.width = this.getWidth();
			this.height = this.getHeight();
			this.i0 = this.width / 2;
			this.j0 = this.height / 2;
			this.w0 = this.i0 - 10;
			this.h0 = this.j0 - 10;
			super.paint(g);
			this.paintBackground(g);
			g.setPaintMode();

			int i;
			for (int c = 0; c < 10; ++c) {
				final ArrayList<?> curve = this.curves.get(c);
				g.setColor(CurveOutput.colors[c]);

				for (i = 1; i < curve.size(); ++i) {
					final CurveOutput.point p0 = (CurveOutput.point) curve.get(i - 1);
					final CurveOutput.point p1 = (CurveOutput.point) curve.get(i);
					g.drawLine(this.x2i(p0.x), this.y2j(p0.y), this.x2i(p1.x), this.y2j(p1.y));
				}
			}

			int y2;
			Iterator<line> var9;
			int j;
			int x2;
			for (var9 = this.lines.iterator(); var9.hasNext(); g.drawLine(i, j, x2, y2)) {
				final CurveOutput.line l = var9.next();
				g.setColor(l.c);
				i = this.x2i(l.x1);
				j = this.y2j(l.y1);
				x2 = this.x2i(l.x2);
				y2 = this.y2j(l.y2);
				if (i == x2 && j == y2) {
					++x2;
				}
			}

			final Iterator<oval> itOval = this.ovals.iterator();

			while (itOval.hasNext()) {
				final CurveOutput.oval l = itOval.next();
				g.setColor(l.c);
				g.drawOval(this.x2i(l.x), this.y2j(l.y), this.x2w(l.w), this.y2h(l.h));
			}

			final Iterator<block> itBlock = this.blocks.iterator();

			while (itBlock.hasNext()) {
				final CurveOutput.block l = itBlock.next();
				g.setColor(l.c_f);
				g.fillRect(this.x2i(l.x), this.y2j(l.y), this.x2w(l.w), this.y2h(l.h));
				if (l.c_b != l.c_f) {
					g.setColor(l.c_b);
					g.drawRect(this.x2i(l.x), this.y2j(l.y), this.x2w(l.w), this.y2h(l.h));
				}
			}

			final Iterator<label> itLabel = this.labels.iterator();

			while (itLabel.hasNext()) {
				final CurveOutput.label l = itLabel.next();
				i = this.x2i(l.x);
				j = this.y2j(l.y);
				g.setColor(l.c);
				g.drawString(l.s, i, j);
				g.drawLine(i - 1, j, i + 1, j);
				g.drawLine(i, j - 1, i, j + 1);
			}

			this.paintReticule(g);
		} catch (final Exception var8) {}

	}

	private void paintReticule(final Graphics g) {
		final int i = this.x2i(this.reticuleX);
		final int j = this.y2j(this.reticuleY);
		g.setColor(Color.white);
		g.setXORMode(Color.black);
		g.drawLine(this.i0 - this.w0, j, this.i0 + this.w0, j);
		g.drawLine(i, this.j0 - this.h0, i, this.j0 + this.h0);
	}

	private void paintBackground(final Graphics g) {
		g.setPaintMode();
		g.setColor(Color.CYAN);
		g.fillRoundRect(1, 1, this.width - 2, this.height - 2, 30, 30);
		g.setColor(Color.BLACK);
		g.drawRoundRect(1, 1, this.width - 2, this.height - 2, 30, 30);
		g.setColor(Color.DARK_GRAY);
		g.fillRect(this.i0 - this.w0, this.j0 - this.h0, 2 * this.w0, 2 * this.h0);
		g.setColor(Color.WHITE);
		g.drawRect(this.i0 - this.w0, this.j0 - this.h0, 2 * this.w0, 2 * this.h0);
		g.drawLine(this.i0, this.j0 - this.h0, this.i0, this.j0 + this.h0);
		g.drawLine(this.i0 - this.w0, this.j0, this.i0 + this.w0, this.j0);
	}

	private int x2i(final double x) {
		return (int) Math.rint(this.i0 + this.w0 * x);
	}

	private int y2j(final double y) {
		return (int) Math.rint(this.j0 - this.h0 * y);
	}

	private int x2w(final double x) {
		return (int) Math.rint(this.w0 * x);
	}

	private int y2h(final double y) {
		return (int) Math.rint(this.h0 * y);
	}

	private double i2x(final int i) {
		return (double) (i - this.i0) / (double) this.w0;
	}

	private double j2y(final int j) {
		return (double) (this.j0 - j) / (double) this.h0;
	}

	public CurveOutput reset(final double Xoffset, final double Yoffset, final double Xscale, final double Yscale) {
		this.Xoffset = Xoffset;
		this.Yoffset = Yoffset;
		this.Xscale = Xscale;
		this.Yscale = Yscale;
		this.curves = new ArrayList<>();

		for (int c = 0; c < 10; ++c) {
			this.curves.add(new ArrayList<point>());
		}

		this.lines = new ArrayList<>();
		this.ovals = new ArrayList<>();
		this.blocks = new ArrayList<>();
		this.labels = new ArrayList<>();
		this.repaint(0, 0, this.getWidth(), this.getHeight());
		return this;
	}

	public CurveOutput reset(final double Xscale, final double Yscale) {
		return this.reset(0.0D, 0.0D, Xscale, Yscale);
	}

	public CurveOutput reset() {
		return this.reset(0.0D, 0.0D, 1.0D, 1.0D);
	}

	public Object add(final double x, final double y, final int c) {
		final CurveOutput.point p = new CurveOutput.point();
		p.x = (x - this.Xoffset) / this.Xscale;
		p.y = (y - this.Yoffset) / this.Yscale;
		if (0 <= c && c < 10) {
			this.curves.get(c).add(p);
		}

		this.repaint(0, 0, this.getWidth(), this.getHeight());
		return p;
	}

	public Object add(final double x1, final double y1, final double x2, final double y2, final int c) {
		final CurveOutput.line l = new CurveOutput.line();
		l.x1 = (x1 - this.Xoffset) / this.Xscale;
		l.y1 = (y1 - this.Yoffset) / this.Yscale;
		l.x2 = (x2 - this.Xoffset) / this.Xscale;
		l.y2 = (y2 - this.Yoffset) / this.Yscale;
		l.c = 0 <= c && c < 10 ? CurveOutput.colors[c] : Color.BLACK;
		this.lines.add(l);
		this.repaint(0, 0, this.getWidth(), this.getHeight());
		return l;
	}

	public Object addRectangle(final double xmin, final double ymin, final double xmax, final double ymax,
			final int c) {
		final CurveOutput.rectangle r = new CurveOutput.rectangle();
		r.l1 = (CurveOutput.line) this.add(xmin, ymin, xmax, ymin, c);
		r.l2 = (CurveOutput.line) this.add(xmax, ymin, xmax, ymax, c);
		r.l3 = (CurveOutput.line) this.add(xmax, ymax, xmin, ymax, c);
		r.l4 = (CurveOutput.line) this.add(xmin, ymax, xmin, ymin, c);
		this.rectangles.add(r);
		return r;
	}

	public Object add(final double x, final double y, final double r, final int c) {
		final CurveOutput.oval l = new CurveOutput.oval();
		l.x = (x - this.Xoffset - r) / this.Xscale;
		l.y = (y - this.Yoffset + r) / this.Yscale;
		l.w = 2.0D * r / this.Xscale;
		l.h = 2.0D * r / this.Yscale;
		l.c = 0 <= c && c < 10 ? CurveOutput.colors[c] : Color.BLACK;
		this.ovals.add(l);
		this.repaint(0, 0, this.getWidth(), this.getHeight());
		return l;
	}

	public Object add(final double x, final double y, final double w, final double h, final int c_f, final int c_b) {
		final CurveOutput.block l = new CurveOutput.block();
		l.x = (x - this.Xoffset) / this.Xscale;
		l.y = (y + h - this.Yoffset) / this.Yscale;
		l.w = w / this.Xscale;
		l.h = h / this.Yscale;
		l.c_f = 0 <= c_f && c_f < 10 ? CurveOutput.colors[c_f] : Color.BLACK;
		l.c_b = 0 <= c_b && c_b < 10 ? CurveOutput.colors[c_b] : l.c_f;
		this.blocks.add(l);
		this.repaint(0, 0, this.getWidth(), this.getHeight());
		return l;
	}

	public Object add(final double x, final double y, final String s, final int c) {
		final CurveOutput.label l = new CurveOutput.label();
		l.x = (x - this.Xoffset) / this.Xscale;
		l.y = (y - this.Yoffset) / this.Yscale;
		l.s = s;
		l.c = 0 <= c && c < 10 ? CurveOutput.colors[c] : Color.BLACK;
		this.labels.add(l);
		this.repaint(0, 0, this.getWidth(), this.getHeight());
		return l;
	}

	public boolean remove(final Object object) {
		if (object instanceof CurveOutput.line && this.lines.contains(object)) {
			this.lines.remove(object);
			return true;
		} else if (object instanceof CurveOutput.rectangle && this.rectangles.contains(object)) {
			final CurveOutput.rectangle r = (CurveOutput.rectangle) object;
			this.lines.remove(r.l1);
			this.lines.remove(r.l2);
			this.lines.remove(r.l3);
			this.lines.remove(r.l4);
			this.rectangles.remove(object);
			return true;
		} else if (object instanceof CurveOutput.oval && this.ovals.contains(object)) {
			this.ovals.remove(object);
			return true;
		} else if (object instanceof CurveOutput.block && this.blocks.contains(object)) {
			this.blocks.remove(object);
			return true;
		} else if (object instanceof CurveOutput.label && this.labels.contains(object)) {
			this.labels.remove(object);
			return true;
		} else {
			if (object instanceof CurveOutput.point) {
				for (int c = 0; c < 10; ++c) {
					if (((ArrayList<?>) this.curves.get(c)).contains(object)) {
						((ArrayList<?>) this.curves.get(c)).remove(object);
						return true;
					}
				}
			}

			return false;
		}
	}

	public double getReticuleX() {
		return this.Xoffset + this.Xscale * this.reticuleX;
	}

	public double getReticuleY() {
		return this.Yoffset + this.Yscale * this.reticuleY;
	}

	public void setReticule(double x, double y) {
		x -= this.Xoffset;
		x /= this.Xscale;
		y -= this.Yoffset;
		y /= this.Yscale;
		this.reticuleX = x < -1.0D ? -1.0D : x > 1.0D ? 1.0D : x;
		this.reticuleY = y < -1.0D ? -1.0D : y > 1.0D ? 1.0D : y;
		this.repaint(0, 0, this.getWidth(), this.getHeight());
	}

	public CurveOutput setRunnable(final Runnable runnable) {
		this.runnable = runnable;
		return this;
	}

	static {
		CurveOutput.colors = new Color[] { Color.BLACK, new Color(150, 75, 0), Color.RED, Color.ORANGE, Color.YELLOW,
				Color.GREEN, Color.BLUE, Color.MAGENTA, Color.GRAY, Color.WHITE };
	}

	private class ReticuleMouseListener implements MouseMotionListener, MouseListener {
		private ReticuleMouseListener() {
		}

		@Override
		public void mouseDragged(final MouseEvent e) {
			CurveOutput.this.paintReticule(CurveOutput.this.getGraphics());
			CurveOutput.this.reticuleX = CurveOutput.this.i2x(e.getX());
			CurveOutput.this.reticuleY = CurveOutput.this.j2y(e.getY());
			CurveOutput.this.paintReticule(CurveOutput.this.getGraphics());
		}

		@Override
		public void mouseReleased(final MouseEvent e) {
			this.mouseDragged(e);
			if (CurveOutput.this.runnable != null) {
				new Thread(CurveOutput.this.runnable).start();
			}

		}

		@Override
		public void mouseMoved(final MouseEvent e) {
		}

		@Override
		public void mousePressed(final MouseEvent e) {
		}

		@Override
		public void mouseClicked(final MouseEvent e) {
		}

		@Override
		public void mouseEntered(final MouseEvent e) {
		}

		@Override
		public void mouseExited(final MouseEvent e) {
		}

	}

	private static class label {
		double x;
		double y;
		String s;
		Color c;

		private label() {
		}

	}

	private static class block {
		double x;
		double y;
		double w;
		double h;
		Color c_f;
		Color c_b;

		private block() {
		}

	}

	private static class oval {
		double x;
		double y;
		double w;
		double h;
		Color c;

		private oval() {
		}

	}

	private static class rectangle {
		CurveOutput.line l1;
		CurveOutput.line l2;
		CurveOutput.line l3;
		CurveOutput.line l4;

		private rectangle() {
		}
	}

	private static class line {
		double x1;
		double y1;
		double x2;
		double y2;
		Color c;

		private line() {
		}

	}

	private static class point {
		double x;
		double y;

		private point() {
		}
	}
}
