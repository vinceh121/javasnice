package org.javascool.widgets;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.plaf.basic.BasicButtonUI;

import org.javascool.macros.Macros;

import lol.javasnice.I18N;

public class TabbedPane extends JTabbedPane {
	private static final long serialVersionUID = 1L;

	public void addTab(final String title, final String icon, final Component component, final String tooltip,
			final boolean closable) {
		this.addTab(title, Macros.getIcon(icon), component, tooltip, closable);
	}

	public void addTab(final String title, final Icon icon, final Component component, final String tooltip,
			final boolean closable) {
		super.addTab(title, icon, component, tooltip);
		if (closable) {
			this.setTabComponentAt(this.indexOfComponent(component), new TabbedPane.TabPanel());
		}

		this.setSelectedComponent(component);
	}

	public void addTab(final String title, final String icon, final Component component, final String tooltip) {
		this.addTab(title, icon, component, tooltip, false);
	}

	@Override
	public void addTab(final String title, final Icon icon, final Component component, final String tooltip) {
		this.addTab(title, icon, component, tooltip, false);
	}

	public void addTab(final String title, final String icon, final Component component) {
		this.addTab(title, icon, component, (String) null, false);
	}

	@Override
	public void addTab(final String title, final Icon icon, final Component component) {
		this.addTab(title, icon, component, (String) null, false);
	}

	public void addTab(final String title, final Component component, final String tooltip, final boolean closable) {
		this.addTab(title, (Icon) null, component, tooltip, closable);
	}

	public void addTab(final String title, final Component component, final String tooltip) {
		this.addTab(title, (Icon) null, component, tooltip, false);
	}

	public void addTab(final String title, final Component component, final boolean closable) {
		this.addTab(title, component, null, closable);
	}

	@Override
	public void addTab(final String title, final Component component) { // closable, false
		this.addTab(title, component, false);
	}

	protected boolean isCloseable(final int index) {
		return true;
	}

	@Override
	public void setTitleAt(final int index, final String title) {
		super.setTitleAt(index, title);
		this.getTabComponentAt(index).invalidate();
	}

	private class TabPanel extends JPanel {
		private static final long serialVersionUID = 1L;

		public TabPanel() {
			super(new FlowLayout(0, 0, 0));
			this.setOpaque(false);
			final JLabel label = new JLabel() {
				private static final long serialVersionUID = 1L;

				@Override
				public String getText() {
					final int i = TabbedPane.this.indexOfTabComponent(TabPanel.this);
					return i == -1 ? null : TabbedPane.this.getTitleAt(i);
				}
			};
			this.add(label);
			label.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 5));
			final JButton button = new TabbedPane.TabPanel.TabButton();
			this.add(button);
			this.setBorder(BorderFactory.createEmptyBorder(2, 0, 0, 0));
		}

		private class TabButton extends JButton implements ActionListener {
			private static final long serialVersionUID = 1L;
			private final MouseListener buttonMouseListener = new MouseAdapter() {
				@Override
				public void mouseEntered(final MouseEvent e) {
					final Component component = e.getComponent();
					if (component instanceof AbstractButton) {
						final AbstractButton button = (AbstractButton) component;
						button.setBorderPainted(false);
					}

				}

				@Override
				public void mouseExited(final MouseEvent e) {
					final Component component = e.getComponent();
					if (component instanceof AbstractButton) {
						final AbstractButton button = (AbstractButton) component;
						button.setBorderPainted(false);
					}

				}
			};

			public TabButton() {
				final int size = 17;
				this.setPreferredSize(new Dimension(size, size));
				this.setToolTipText(I18N.gettext("Close tab"));
				this.setUI(new BasicButtonUI());
				this.setContentAreaFilled(false);
				this.setFocusable(false);
				this.setBorder(BorderFactory.createEtchedBorder());
				this.setBorderPainted(false);
				this.addMouseListener(this.buttonMouseListener);
				this.setRolloverEnabled(true);
				this.addActionListener(this);
			}

			@Override
			public void actionPerformed(final ActionEvent e) {
				final int i = TabbedPane.this.indexOfTabComponent(TabPanel.this);
				if (i != -1 && TabbedPane.this.isCloseable(i)) {
					TabbedPane.this.remove(i);
				}

			}

			@Override
			public void updateUI() {
			}

			@Override
			protected void paintComponent(final Graphics g) {
				super.paintComponent(g);
				final Graphics2D g2 = (Graphics2D) g.create();
				if (this.getModel().isPressed()) {
					g2.translate(1, 1);
				}

				g2.setStroke(new BasicStroke(2.0F));
				g2.setColor(Color.BLACK);
				if (this.getModel().isRollover()) {
					g2.setColor(Color.WHITE);
				}

				final int delta = 6;
				g2.drawLine(delta, delta, this.getWidth() - delta - 1, this.getHeight() - delta - 1);
				g2.drawLine(this.getWidth() - delta - 1, delta, delta, this.getHeight() - delta - 1);
				g2.dispose();
			}
		}
	}
}
