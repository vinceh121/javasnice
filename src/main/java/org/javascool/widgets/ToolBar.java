package org.javascool.widgets;

import java.util.HashMap;

import javax.swing.AbstractButton;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JPopupMenu;
import javax.swing.JToolBar;

import org.javascool.macros.Macros;
import org.javascool.tools.UserConfig;

public class ToolBar extends JToolBar {
	private static final long serialVersionUID = 1L;
	public static final int BUTTON_TEXT_ICON = 0, BUTTON_TEXT_ONLY = 1, BUTTON_ICON_ONLY = 2;
	private final HashMap<String, JComponent> buttons = new HashMap<>();
	private final HashMap<JButton, Runnable> actions = new HashMap<>();
	private int left = 0;
	private int right = 0;

	public ToolBar() {
		this.setFloatable(false);
	}

	public JComponent getTool(final String label) {
		return this.buttons.get(label);
	}

	@Override
	public void removeAll() {
		this.left = this.right = 0;
		this.setVisible(false);
		this.revalidate();
		super.removeAll();
		this.buttons.clear();
		this.actions.clear();
		this.setVisible(true);
		this.revalidate();
	}

	public final JButton addTool(final String label, final String icon, final Runnable action) {
		return this.addTool(label, icon, action, this.left++);
	}

	public final JButton addTool(final String label, final Runnable action) {
		return this.addTool(label, (String) null, action);
	}

	public final JPopupMenu addTool(final String label, final String icon) {
		final ToolBar.PopupMenuRunnable p = new ToolBar.PopupMenuRunnable();
		// p.b =
		this.addTool(label, icon, p);
		return p.j;
	}

	public final JPopupMenu addTool(final String label) {
		return this.addTool(label, (String) null);
	}

	public static void adjustButtonSettings(final AbstractButton button, final String label, final String icon) {
		switch (UserConfig.getConf().getInt("ui.toolbar_btns", ToolBar.BUTTON_ICON_ONLY)) {
		case BUTTON_TEXT_ICON:
			button.setText(label);
			button.setIcon(Macros.getIcon(icon));
			break;
		case BUTTON_TEXT_ONLY:
			button.setText(label);
			break;
		case BUTTON_ICON_ONLY:
			button.setToolTipText(label);
			button.setIcon(Macros.getIcon(icon));
			break;
		default:
			throw new IllegalArgumentException("Invalid toolbar mode");
		}
	}

	private JButton addTool(final String label, final String icon, final Runnable action, final int where) {
		// JButton button = icon == null ? new JButton(label) : new JButton(label,
		// Macros.getIcon(icon));
		final JButton button = new JButton();
		try {
			ToolBar.adjustButtonSettings(button, label, icon);
		} catch (final NullPointerException e) {
			System.out.println("Failed to find icon '" + icon + "' for button '" + label + "'");
		}
		button.addActionListener(e -> ToolBar.this.actions.get(e.getSource()).run());
		this.add(button, where);
		if (this.buttons.containsKey(label)) {
			throw new IllegalArgumentException("Duplicate button name: «" + label + "»");
		} else {
			this.buttons.put(label, button);
			this.actions.put(button, action);
			this.revalidate();
			return button;
		}
	}

	public void addTool(final String label, final JComponent component) {
		this.add(component, this.left++);
		if (this.buttons.containsKey(label)) {
			throw new IllegalArgumentException("Duplicate button name: «" + label + "»");
		} else {
			this.buttons.put(label, component);
			this.revalidate();
		}
	}

	public void removeTool(final String label) {
		if (this.buttons.containsKey(label)) {
			final JComponent c = this.buttons.get(label);
			this.remove(c);
			this.buttons.remove(label);
			if (c instanceof AbstractButton && this.actions.containsKey(c)) {
				this.actions.remove(c);
			}

			this.setVisible(false);
			this.revalidate();
			this.setVisible(true);
			this.revalidate();
		}

	}

	public boolean hasTool(final String label) {
		return this.buttons.containsKey(label);
	}

	public JButton addRightTool(final String label, final Runnable action) {
		if (this.right == 0) {
			this.add(Box.createHorizontalGlue());
		}

		return this.addTool(label, (String) null, action, this.left + ++this.right);
	}

	public final JPopupMenu addRightTool(final String label) {
		final ToolBar.PopupMenuRunnable p = new ToolBar.PopupMenuRunnable();
		// p.b =
		this.addRightTool(label, p);
		return p.j;
	}

	public void addRightTool(final JComponent component) {
		if (this.right == 0) {
			this.add(Box.createHorizontalGlue());
		}

		this.add(component, this.left + ++this.right);
	}

	private class PopupMenuRunnable implements Runnable {
		JPopupMenu j;
		// JButton b;

		private PopupMenuRunnable() {
			this.j = new JPopupMenu();
			this.j.setInvoker(ToolBar.this);
			this.j.setBorderPainted(true);
		}

		@Override
		public void run() {
			this.j.setVisible(true);
		}
	}
}
