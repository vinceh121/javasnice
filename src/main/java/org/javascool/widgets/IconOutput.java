package org.javascool.widgets;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.HashMap;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

import org.javascool.macros.Macros;

public class IconOutput extends JPanel {
	private static final long serialVersionUID = 1L;
	private Color[] image;
	private int width;
	private int height;
	private int i0;
	private int j0;
	private int dij;
	boolean zoom = true;
	private static HashMap<Color, String> colors = new HashMap<>();
	private int clicX = 0;
	private int clicY = 0;
	private Runnable runnable;

	public IconOutput() {
		this.addMouseListener(new MouseListener() {
			@Override
			public void mouseReleased(final MouseEvent e) {
				IconOutput.this.clicX = (e.getX() - IconOutput.this.i0) / IconOutput.this.dij;
				IconOutput.this.clicY = (e.getY() - IconOutput.this.j0) / IconOutput.this.dij;
				if (IconOutput.this.runnable != null) {
					new Thread(IconOutput.this.runnable).start();
				}
			}

			@Override
			public void mousePressed(final MouseEvent e) {
			}

			@Override
			public void mouseClicked(final MouseEvent e) {
			}

			@Override
			public void mouseEntered(final MouseEvent e) {
			}

			@Override
			public void mouseExited(final MouseEvent e) {
			}
		});
		this.runnable = null;
		this.setBackground(Color.GRAY);
		this.setPreferredSize(new Dimension(550, 550));
		this.reset(550, 550);
	}

	@Override
	public void paint(final Graphics g) {
		super.paint(g);
		this.setBounds();
		g.setPaintMode();

		for (int j = 0; j < this.height; ++j) {
			for (int i = 0; i < this.width; ++i) {
				final int ij = i + j * this.width;
				if (0 <= ij && ij < this.image.length) {
					g.setColor(this.image[ij]);
					g.fillRect(this.i0 + i * this.dij, this.j0 + j * this.dij, this.dij, this.dij);
				}
			}
		}

		final Graphics2D g2d = (Graphics2D) g;
		this.paint2D(g2d);
	}

	public void paint2D(final Graphics2D g2d) {
	}

	private void setBounds() {
		final int di = this.width > 0 && this.getWidth() >= this.width && this.zoom ? this.getWidth() / this.width : 1;
		final int dj
				= this.height > 0 && this.getHeight() >= this.height && this.zoom ? this.getHeight() / this.height : 1;
		if (this.dij < 1) {
			this.dij = 1;
		}

		this.dij = di < dj ? di : dj;
		this.i0 = (this.getWidth() - this.width * this.dij) / 2;
		this.j0 = (this.getHeight() - this.height * this.dij) / 2;
	}

	public final IconOutput reset(final int width, final int height, final boolean zoom) {
		// int MAXSIZE = 1000;
		// if (width <= MAXSIZE && height <= MAXSIZE && width * height <= MAXSIZE *
		// MAXSIZE) {
		this.zoom = zoom;
		this.image = new Color[(this.width = width) * (this.height = height)];

		for (int ij = 0; ij < this.width * this.height; ++ij) {
			this.image[ij] = Color.WHITE;
		}

		this.repaint(0, 0, this.getWidth(), this.getHeight());
		return this;
		// } else {
		// throw new IllegalArgumentException("It's too big! (" + width + ", " + height
		// + ")"); // that's what she said
		// }
	}

	public final IconOutput reset(final int width, final int height) {
		return this.reset(width, height, true);
	}

	public IconOutput reset(final String location, final boolean zoom) throws IOException {
		for (int n = 0; n < 3; ++n) {
			final BufferedImage img = ImageIO.read(Macros.getResourceURL(location));
			if (img != null) {
				return this.reset(img, zoom);
			}
		}

		throw new IOException("Unable to load the image " + location);
	}

	public final IconOutput reset(final String location) throws IOException {
		return this.reset(location, true);
	}

	public IconOutput reset(final BufferedImage img, final boolean zoom) {
		this.reset(img.getWidth(), img.getHeight(), zoom);

		for (int j = 0; j < img.getHeight(); ++j) {
			for (int i = 0; i < img.getWidth(); ++i) {
				this.image[i + this.width * j] = new Color(img.getRGB(i, j));
			}
		}

		this.repaint(0, 0, this.getWidth(), this.getHeight());
		return this;
	}

	public final IconOutput reset(final BufferedImage img) {
		return this.reset(img, true);
	}

	public Dimension getDimension() {
		return new Dimension(this.width, this.height);
	}

	public BufferedImage getImage() {
		final BufferedImage img = new BufferedImage(this.width, this.height, 1);

		for (int j = 0; j < img.getHeight(); ++j) {
			for (int i = 0; i < img.getWidth(); ++i) {
				img.setRGB(i, j, this.image[i + this.width * j].getRGB());
			}
		}

		return img;
	}

	public boolean set(final int x, final int y, final String c) {
		return this.set(x, y, IconOutput.getColor(c));
	}

	public boolean set(final int x, final int y, int v) {
		v = v < 0 ? 0 : v > 255 ? 255 : v;
		return this.set(x, y, new Color(v, v, v));
	}

	public boolean set(final int x, final int y, final Color c) {
		if (0 <= x && x < this.width && 0 <= y && y < this.height) {
			this.setBounds();
			final int ij = x + y * this.width;
			this.image[ij] = c;
			this.repaint(this.i0 + x * this.dij, this.j0 + y * this.dij, this.dij, this.dij);
			return true;
		} else {
			return false;
		}
	}

	public int getIntensity(final int x, final int y) {
		if (0 <= x && x < this.width && 0 <= y && y < this.height) {
			final Color c = this.image[x + y * this.width];
			return (c.getRed() + c.getGreen() + c.getBlue()) / 3;
		} else {
			return 0;
		}
	}

	public String getColor(final int x, final int y) {
		if (0 <= x && x < this.width && 0 <= y && y < this.height) {
			final Color c = this.image[x + y * this.width];
			return IconOutput.colors.containsKey(c) ? (String) IconOutput.colors.get(c) : c.toString();
		} else {
			return "undefined";
		}
	}

	public Color getPixelColor(final int x, final int y) {
		if (0 <= x && x < this.width && 0 <= y && y < this.height) {
			final Color c = this.image[x + y * this.width];
			return c;
		} else {
			return Color.BLACK;
		}
	}

	private static Color getColor(final String color) {
		try {
			return (Color) Class.forName("java.awt.Color").getField(color).get((Object) null);
		} catch (final Exception var2) {
			return Color.BLACK;
		}
	}

	private static void putColors(final String color) {
		IconOutput.colors.put(IconOutput.getColor(color), color);
	}

	public int getClicX() {
		return this.clicX;
	}

	public int getClicY() {
		return this.clicY;
	}

	public IconOutput setRunnable(final Runnable runnable) {
		this.runnable = runnable;
		return this;
	}

	static {
		IconOutput.putColors("black");
		IconOutput.putColors("blue");
		IconOutput.putColors("cyan");
		IconOutput.putColors("gray");
		IconOutput.putColors("green");
		IconOutput.putColors("magenta");
		IconOutput.putColors("orange");
		IconOutput.putColors("pink");
		IconOutput.putColors("red");
		IconOutput.putColors("white");
		IconOutput.putColors("yellow");
	}
}
