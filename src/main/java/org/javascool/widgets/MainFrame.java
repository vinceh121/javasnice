package org.javascool.widgets;

import java.awt.Component;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.WindowConstants;

import org.javascool.core.Jvs2Java;
import org.javascool.macros.Macros;

public class MainFrame extends JFrame {
	/**
	 *
	 */
	private static final long serialVersionUID = -3740078629110135076L;
	private Component pane;
	private static int frameCount = 0;
	private StartStopButton runnableStartStopButton = null;
	private Runnable runnable = null;
	private static JFrame firstFrame;

	public MainFrame asPopup() {
		if (System.getProperty("os.name").toLowerCase().indexOf("mac") >= 0) {
			this.setUndecorated(true);
			this.getRootPane().setWindowDecorationStyle(0);
		} else if (System.getProperty("os.name").toLowerCase().indexOf("win") >= 0) {}

		return this;
	}

	public MainFrame reset(final String title, final String icon, final int width, final int height,
			final Component pane) {
		if (title != null) {
			this.setTitle(title);
		}

		if (System.getProperty("os.name").toUpperCase().contains("MAC")) {
			try {
				System.setProperty("com.apple.mrj.application.apple.menu.about.name", title);
			} catch (final Exception ignored) {}
		}

		if (icon != null) {
			final ImageIcon image = Macros.getIcon(icon);
			if (image != null) {
				this.setIconImage(image.getImage());
			}
		}

		this.add(this.setPane(pane));
		// if (pane instanceof Applet) {
		// ((Applet) pane).init();
		// }

		this.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		this.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(final WindowEvent e) {
				MainFrame.this.close();
			}
		});
		this.pack();
		if (width > 0 && height > 0) {
			this.setSize(width, height);
		} else {
			this.setExtendedState(6);
		}

		this.setLocation(100 + MainFrame.frameCount * 250, 100 + MainFrame.frameCount * 150);
		this.setVisible(true);
		// if (pane instanceof Applet) {
		// ((Applet) pane).start();
		// }

		if (MainFrame.firstFrame == null) {
			MainFrame.firstFrame = this;
		}

		++MainFrame.frameCount;
		return this;
	}

	public MainFrame reset(final String title, final int width, final int height, final Component pane) {
		return this.reset(title, null, width, height, pane);
	}

	public MainFrame reset(final String title, final String icon, final Component pane) {
		return this.reset(title, icon, 0, 0, pane);
	}

	public MainFrame reset(final String title, final Component pane) {
		return this.reset(title, null, 0, 0, pane);
	}

	public MainFrame reset(final int width, final int height, final Component pane) {
		return this.reset(pane.getClass().toString(), null, width, height, pane);
	}

	public MainFrame reset(final Component pane) {
		return this.reset(pane.getClass().toString(), null, 0, 0, pane);
	}

	public MainFrame setRunnable(final Runnable runnable) {
		if (runnable == null) {
			if (this.runnableStartStopButton != null) {
				this.remove(this.runnableStartStopButton);
			}

			this.runnableStartStopButton = null;
		} else if (this.runnableStartStopButton == null) {
			this.add(this.runnableStartStopButton = new StartStopButton() {
				private static final long serialVersionUID = 1L;
				private Thread runnableThread = null;

				@Override
				public void start() {
					this.stop();
					(this.runnableThread = new Thread(() -> {
						try {
							MainFrame.this.runnable.run();
							runnableThread = null;
						} catch (final Throwable var2) {
							Jvs2Java.report(var2);
						}

					})).start();
				}

				@Override
				public void stop() {
					if (this.runnableThread != null) {
						this.runnableThread.interrupt();
						this.runnableThread = null;
					}

				}

				@Override
				public boolean isRunning() {
					return this.runnableThread != null;
				}
			}, "North");
		}

		this.runnable = runnable;
		return this;
	}

	public void close(final boolean force) {
		if (force || this.isClosable()) {
			// if (this.pane instanceof Applet) {
			// try {
			// ((Applet) this.pane).stop();
			// } catch (Throwable var4) {
			// }
			//
			// try {
			// ((Applet) this.pane).destroy();
			// } catch (Throwable var3) {
			// }
			// }

			this.setVisible(false);
			this.dispose();
			--MainFrame.frameCount;
			if (MainFrame.frameCount == 0) {
				System.exit(0);
			}
		}

	}

	public void close() {
		this.close(false);
	}

	public boolean isClosable() {
		return true;
	}

	public Component getPane() {
		return this.pane;
	}

	public Component setPane(final Component pane) {
		this.pane = pane;
		return pane;
	}

	public static JFrame getFrame() {
		return MainFrame.firstFrame;
	}

}
