package org.javascool.gui;

import java.awt.BorderLayout;
import java.io.File;
import java.util.HashMap;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.UIManager;

import org.javascool.core.Jvs2Jar;
import org.javascool.core.Jvs2Java;
import org.javascool.core.ProgletEngine;
import org.javascool.tools.UserConfig;
import org.javascool.widgets.Console;

import lol.javasnice.I18N;

class JVSPanel extends JPanel {
	private static final long serialVersionUID = 1L;
	HashMap<String, Boolean> haveToSave = new HashMap<>();
	private Boolean noFileEdited = true;
	private static JVSPanel desktop;

	public static JVSPanel getInstance() {
		if (JVSPanel.desktop == null) {
			JVSPanel.desktop = new JVSPanel();
		}

		return JVSPanel.desktop;
	}

	private JVSPanel() {
		this.setVisible(true);
		this.setLayout(new BorderLayout());
		this.add(JVSStartPanel.getInstance());
		this.revalidate();
		this.repaint();
	}

	public void closeProglet() {
		if (this.closeAllFiles()) {
			this.removeAll();
			this.setOpaque(true);
			this.repaint();
			this.validate();
			this.repaint();
			this.add(JVSStartPanel.getInstance());
			this.repaint();
			this.revalidate();
			this.repaint();
			if (ProgletEngine.getInstance().getProglet() != null) {
				ProgletEngine.getInstance().getProglet().stop();
			}
		}

	}

	public void newFile() {
		final String fileId = JVSFileTabs.getInstance().openNewFile();
		this.haveToSave.put(fileId, false);
	}

	public void compileFile() {
		JVSFileTabs.getInstance().getEditor(JVSFileTabs.getCurrentCompiledFile()).removeLineSignals();
		if (JVSFileTabs.getInstance().saveCurrentFile()) {
			JVSWidgetPanel.getInstance().focusOnConsolePanel();
			if (JVSFileTabs.getInstance().compileFile(JVSFileTabs.getInstance().getCurrentFileId())) {
				JVSToolBar.getInstance().enableStartStopButton();
			} else {
				JVSToolBar.getInstance().disableStartStopButton();
			}
		}

	}

	public void openFile() {
		final JFileChooser fc = new JFileChooser();
		if (UserConfig.getConf().containsKey("io.dir")) {
			fc.setCurrentDirectory(new File(UserConfig.getConf().getString("io.dir")));
		} else if (!System.getProperty("os.name").toLowerCase().contains("nix")
				&& !System.getProperty("os.name").toLowerCase().contains("nux")) {
			if (System.getProperty("home.dir") != null) {
				fc.setCurrentDirectory(new File(System.getProperty("home.dir")));
			}
		} else {
			fc.setCurrentDirectory(new File(System.getProperty("user.dir")));
		}

		final int returnVal = fc.showOpenDialog(Desktop.getInstance().getFrame());
		if (returnVal == 0) {
			final String path = fc.getSelectedFile().getAbsolutePath();
			if (!fc.getSelectedFile().exists()) {
				JVSPanel.Dialog.error("Error", I18N.gettext("Specified file does not exist"));
				return;
			}

			UserConfig.getConf().setProperty("io.dir", fc.getSelectedFile().getParentFile().getAbsolutePath());
			if (this.noFileEdited) {
				this.noFileEdited = false;
			}

			final String fileId = JVSFileTabs.getInstance().open(path);
			this.haveToSave.put(fileId, false);
		}

	}

	public boolean saveFile() {
		if (JVSFileTabs.getInstance().saveCurrentFile()) {
			this.haveToSave.put(JVSFileTabs.getInstance().getCurrentFileId(), false);
			return true;
		} else {
			return false;
		}
	}

	public boolean saveAsFile() {
		if (JVSFileTabs.getInstance().saveAsCurrentFile()) {
			this.haveToSave.put(JVSFileTabs.getInstance().getCurrentFileId(), false);
			return true;
		} else {
			return false;
		}
	}

	public boolean saveAsJavaFile() {
		final boolean saving = this.saveFile();
		final String name = ProgletEngine.getInstance().getProglet().getName();
		final String jvsFile
				= JVSFileTabs.getInstance().getFile(JVSFileTabs.getInstance().getCurrentFileId()).getPath();
		Jvs2Java.build(name, jvsFile);
		return saving;
	}

	public boolean saveAsJarFile() {
		final boolean saving = this.saveFile();
		final String name = ProgletEngine.getInstance().getProglet().getName();
		final String jvsFile
				= JVSFileTabs.getInstance().getFile(JVSFileTabs.getInstance().getCurrentFileId()).getPath();
		Jvs2Jar.build(name, jvsFile);
		return saving;
	}

	public void closeFile() {
		if (this.haveToSave.get(JVSFileTabs.getInstance().getCurrentFileId())) {
			if (this.saveFileIdBeforeClose(JVSFileTabs.getInstance().getCurrentFileId()) == 1) {
				JVSFileTabs.getInstance().closeFile(JVSFileTabs.getInstance().getCurrentFileId());
			}
		} else {
			JVSFileTabs.getInstance().closeFile(JVSFileTabs.getInstance().getCurrentFileId());
		}

		if (JVSFileTabs.getInstance().getOppenedFileCount() == 0) {
			this.newFile();
		}

	}

	public void mustSave(final String fileId) {
		this.noFileEdited = false;
		this.haveToSave.put(fileId, true);
	}

	public void haveNotToSave(final String fileId) {
		this.haveToSave.put(fileId, false);
	}

	public Boolean getHasToSave(final String fileId) {
		return this.haveToSave.get(fileId);
	}

	public void reportCompileError(final int line, final String explication) {
		Console.getInstance().clear();
		JVSWidgetPanel.getInstance().focusOnConsolePanel();
		if (JVSFileTabs.getInstance().getEditor(JVSFileTabs.getCurrentCompiledFile()) != null) {
			JVSFileTabs.getInstance().getEditor(JVSFileTabs.getCurrentCompiledFile()).signalLine(line);
		}

	}

	public Boolean close() {
		String id = "";
		final Boolean[] can_close = new Boolean[this.haveToSave.keySet().toArray().length];
		int i = 0;
		int j = 0;
		Object[] var5 = this.haveToSave.keySet().toArray();
		int var6 = var5.length;

		int var7;
		Object fileId;
		for (var7 = 0; var7 < var6; ++var7) {
			fileId = var5[var7];
			if (this.haveToSave.get(fileId)) {
				++j;
			}
		}

		if (j == 0) {
			final Object[] options = { I18N.gettext("Yes!"), I18N.gettext("Yes") };
			JOptionPane.showOptionDialog(Desktop.getInstance().getFrame(),
					I18N.gettext("Do you really wanna quit Javasnice?"), I18N.gettext("Confirmation"),
					JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[0]);
			return true;
		} else {
			j = 0;
			var5 = this.haveToSave.keySet().toArray();
			var6 = var5.length;

			for (var7 = 0; var7 < var6; ++var7) {
				fileId = var5[var7];
				id = (String) fileId;
				if (this.haveToSave.get(id)) {
					switch (this.saveFileIdBeforeClose(id)) {
					case -1:
						return false;
					case 0:
						can_close[i] = false;
						break;
					case 1:
						can_close[i] = true;
					}

					++j;
				} else {
					can_close[i] = true;
				}

				if (can_close[i]) {
					JVSFileTabs.getInstance().closeFile(id);
				}

				++i;
			}

			try {
				final Boolean[] var10 = can_close;
				var6 = can_close.length;

				for (var7 = 0; var7 < var6; ++var7) {
					final Boolean can_close_r = var10[var7];
					if (can_close_r != null && !can_close_r) {
						return false;
					}
				}
			} catch (final NullPointerException var9) {}

			return true;
		}
	}

	public Boolean closeAllFiles() {
		String id = "";
		final Boolean[] can_close = new Boolean[this.haveToSave.keySet().toArray().length];
		int i = 0;
		int j = 0;
		Object[] var5 = this.haveToSave.keySet().toArray();
		int var6 = var5.length;

		int var7;
		Object fileId;
		for (var7 = 0; var7 < var6; ++var7) {
			fileId = var5[var7];
			if (this.haveToSave.get(fileId)) {
				++j;
			} else {
				JVSFileTabs.getInstance().closeFile((String) fileId);
			}
		}

		if (j == 0) {
			final int n = JOptionPane.showConfirmDialog(Desktop.getInstance().getFrame(),
					"Do you really want to continue?", "Confirmation", 0);
			return n == 0 ? true : false;
		} else {
			j = 0;
			var5 = this.haveToSave.keySet().toArray();
			var6 = var5.length;

			for (var7 = 0; var7 < var6; ++var7) {
				fileId = var5[var7];
				id = (String) fileId;
				if (this.haveToSave.get(id)) {
					switch (this.saveFileIdBeforeClose(id)) {
					case -1:
						return false;
					case 0:
						can_close[i] = false;
						break;
					case 1:
						can_close[i] = true;
					}

					++j;
				} else {
					can_close[i] = true;
				}

				if (can_close[i]) {
					JVSFileTabs.getInstance().closeFile(id);
				}

				++i;
			}

			try {
				final Boolean[] var10 = can_close;
				var6 = can_close.length;

				for (var7 = 0; var7 < var6; ++var7) {
					final Boolean can_close_r = var10[var7];
					if (can_close_r != null && !can_close_r) {
						return false;
					}
				}
			} catch (final NullPointerException var9) {}

			return true;
		}
	}

	public int saveFileIdBeforeClose(final String fileId) {
		final JVSFile file = JVSFileTabs.getInstance().getFile(fileId);
		final int result = JOptionPane.showConfirmDialog(Desktop.getInstance().getFrame(),
				I18N.ngettext("Do you want to save {0} before continuing?", file.getName()));
		if (result == 0) {
			if (JVSFileTabs.getInstance().saveFile(fileId)) {
				this.haveToSave.put(fileId, false);
				return 1;
			} else {
				return 0;
			}
		} else if (result == 1) {
			return 1;
		} else {
			this.haveToSave.put(fileId, true);
			return -1;
		}
	}

	public void loadProglet(final String name) {
		System.gc();
		this.removeAll();
		this.revalidate();
		this.repaint();
		this.setVisible(false);
		this.setVisible(true);
		this.add(JVSToolBar.getInstance(), "North");
		this.add(JVSCenterPanel.getInstance(), "Center");
		this.revalidate();
		JVSCenterPanel.getInstance().revalidate();
		JVSCenterPanel.getInstance().setDividerLocation(this.getWidth() / 2);
		JVSCenterPanel.getInstance().revalidate();
		JVSWidgetPanel.getInstance().setProglet(name);
		this.newFile();
	}

	public void reportRuntimeBug(final String ex) {
		final StackTraceElement[] stack = Thread.currentThread().getStackTrace();
		int line = 0;
		final StackTraceElement[] var4 = stack;
		final int var5 = stack.length;

		for (int var6 = 0; var6 < var5; ++var6) {
			final StackTraceElement elem = var4[var6];
			if (elem.getFileName().startsWith("JvsToJavaTranslated")) {
				line = elem.getLineNumber();
			} else {
				System.out.println(elem.getClassName());
			}
		}

		if (JVSFileTabs.getInstance().getEditor(JVSFileTabs.getCurrentCompiledFile()) != null) {
			JVSFileTabs.getInstance().getEditor(JVSFileTabs.getCurrentCompiledFile()).signalLine(line);
		}

		ProgletEngine.getInstance().doStop();
		JVSPanel.Dialog.error("Runtime error at line " + line, ex);
	}

	public void reportApplicationBug(final String ex) {
		JVSPanel.Dialog.error("Javasnice error: ", ex);
	}

	static {
		UIManager.put("FileChooser.readOnly", Boolean.TRUE);
		JVSPanel.desktop = null;
	}

	public static class Dialog {
		public static void success(final String title, final String message) {
			JOptionPane.showMessageDialog(Desktop.getInstance().getFrame(), message, title, 1);
		}

		public static void error(final String title, final String message) {
			JOptionPane.showMessageDialog(Desktop.getInstance().getFrame(), message, title, 0);
		}
	}
}
