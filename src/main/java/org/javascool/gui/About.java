package org.javascool.gui;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Calendar;

import javax.swing.JLabel;

import org.javascool.macros.Macros;

import lol.javasnice.I18N;

public final class About {
	public static final String title = "Javasnice";
	public static final String logo = "icons/logo.png";
	public static final String revision = "4.20.69";

	public static void showAboutMessage() {
		String message = I18N.gettext("<h1>Javasnice</h1> Javasnice 1 (4.20.69) is a program made by geniuses<br><br>"
				+ "Based on Java's Cool 4.0.1313 made by Philippe VIENNE, Guillaume MATHERON and Inria in collaboration with David Pichardie, Philippe Lucaud, etc.. and the help of Robert Cabane<br><br>"
				+ "Distributed under the GNU GPL V3 license<br>");

		if (Calendar.getInstance().get(Calendar.MONTH) == Calendar.OCTOBER) {
			message += "<a href='https://www.youtube.com/watch?v=PFrPrIxluWk'><img src='https://cdn.discordapp.com/attachments/579635091894960147/639212532988641291/71440841_p0.png'></a>";
		} else if (Calendar.getInstance().get(Calendar.MONTH) == Calendar.APRIL
				&& Calendar.getInstance().get(Calendar.DAY_OF_MONTH) == 1) {
			message += "<img src='https://cdn.discordapp.com/attachments/579635091894960147/639473990175424561/LOL.png'>";
		} else {
			message += "<img src='https://cdn.discordapp.com/attachments/623821622880763916/624878069207007233/bigbrain-small.jpg'>";
		}

		Macros.message(message, true);
	}

	public static JLabel getAboutMessage() {
		final JLabel logoLabel = new JLabel(Macros.getIcon("icons/logo-transparent.png"));
		logoLabel.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(final MouseEvent e) {
				About.showAboutMessage();
			}
		});
		logoLabel.setToolTipText(I18N.gettext("About Javasnice"));
		return logoLabel;
	}
}
