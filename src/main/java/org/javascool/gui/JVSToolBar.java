package org.javascool.gui;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;

import org.javascool.core.ProgletEngine;
import org.javascool.macros.Macros;
import org.javascool.widgets.StartStopButton;
import org.javascool.widgets.ToolBar;

import lol.javasnice.ByteCodeFrame;
import lol.javasnice.I18N;
import lol.javasnice.SettingsFrame;

class JVSToolBar extends ToolBar {
	private static final long serialVersionUID = 1L;
	private JButton compileButton, btnByteCode;
	private StartStopButton runButton;
	private static JVSToolBar jvstb;
	// private JButton settings;
	public boolean canRunShitcode = false;

	public static JVSToolBar getInstance() {
		if (JVSToolBar.jvstb == null) {
			JVSToolBar.jvstb = new JVSToolBar();
		}

		return JVSToolBar.jvstb;
	}

	private JVSToolBar() {
		this.setName(I18N.gettext("Javasnice ToolBar"));
		this.init();
	}

	private void init() {
		if (ProgletEngine.getInstance().getProgletCount() > 1) {
			this.addTool(I18N.gettext("New activity"), "icons/newActivity.png",
					() -> JVSPanel.getInstance().closeProglet());
		}

		this.addTool(I18N.gettext("New file"), "icons/new.png", () -> JVSPanel.getInstance().newFile());
		this.addTool(I18N.gettext("Open file"), "icons/open.png", () -> JVSPanel.getInstance().openFile());
		this.addTool(I18N.gettext("Save"), "icons/save.png", () -> JVSPanel.getInstance().saveFile());
		final JPopupMenu menu = this.addTool(I18N.gettext("Save as..."), "icons/saveas.png");
		final JLabel l = new JLabel(I18N.gettext("Save current Javasnice file:"));
		l.setIcon(Macros.getIcon("icons/saveas.png"));
		menu.add(l);
		menu.add(I18N.gettext("   .. in javasnice (.jvs)")).addActionListener(e -> JVSPanel.getInstance().saveAsFile());
		menu.add(I18N.gettext("   .. java source (.java)"))
				.addActionListener(e -> new Thread(() -> JVSPanel.getInstance().saveAsJavaFile()).start());
		menu.add(I18N.gettext("   .. as executable (.jar)"))
				.addActionListener(e -> new Thread(() -> JVSPanel.getInstance().saveAsJarFile()).start());
		// this.settings =
		this.addTool(I18N.gettext("Settings"), "icons/settings.png", () -> SettingsFrame.toggle());
		this.addTool(I18N.gettext("Run Garbage Collector"), "icons/gc.png", () -> {
			final long free = Runtime.getRuntime().freeMemory();
			System.gc();
			JOptionPane.showMessageDialog(null,
					I18N.ngettext("You now have {0} more bytes free", Runtime.getRuntime().freeMemory() - free));
		});
		this.compileButton = this.addTool(I18N.gettext("Compile"), "icons/compile.png",
				() -> JVSPanel.getInstance().compileFile());
		this.btnByteCode = this.addTool(I18N.gettext("Show bytecode"), "icons/bytecode.png",
				() -> ByteCodeFrame.getInstance().toggleVisibility());
		this.addTool(I18N.gettext("Execute"), this.runButton = new StartStopButton() {
			private static final long serialVersionUID = 1L;

			@Override
			public void start() {
				JVSWidgetPanel.getInstance().focusOnProgletPanel();
				ProgletEngine.getInstance().doRun();
			}

			@Override
			public void stop() {
				ProgletEngine.getInstance().doStop();
			}

			@Override
			public boolean isRunning() {
				return ProgletEngine.getInstance().isRunning();
			}
		});
		this.runButton.setVisible(false);
		this.add(About.getAboutMessage(), 0);
		this.revalidate();
		this.repaint();
	}

	public StartStopButton getRunButton() {
		return this.runButton;
	}

	public void enableCompileButton() {
		this.compileButton.setVisible(true);
		this.btnByteCode.setVisible(true);
		this.revalidate();
	}

	public void disableCompileButton() {
		this.compileButton.setVisible(false);
		this.btnByteCode.setVisible(false);
		this.revalidate();
	}

	public void enableStartStopButton() {
		this.runButton.setVisible(true);
		this.canRunShitcode = true;
		this.revalidate();
	}

	public void disableStartStopButton() {
		this.runButton.setVisible(false);
		this.canRunShitcode = false;
		this.revalidate();
	}
}
