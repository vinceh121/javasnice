package org.javascool.gui;

import java.io.File;
import java.util.HashMap;
import java.util.Iterator;
import java.util.UUID;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.UIManager;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.javascool.core.ProgletEngine;
import org.javascool.tools.UserConfig;

import lol.javasnice.I18N;

class JVSFileTabs extends JVSTabs {
	private static final long serialVersionUID = 1L;
	private static HashMap<String, JVSEditor> editors;
	private static HashMap<String, JVSFile> files;
	static HashMap<String, String> fileIds;
	private static String currentCompiledFile;
	private static JVSFileTabs desktop;

	public static JVSFileTabs getInstance() {
		if (JVSFileTabs.desktop == null) {
			JVSFileTabs.desktop = new JVSFileTabs();
		}

		return JVSFileTabs.desktop;
	}

	public static String getCurrentCompiledFile() {
		return JVSFileTabs.currentCompiledFile;
	}

	private JVSFileTabs() {
	}

	public String openNewFile() {
		return this.openFile(new JVSFile(JVSFile.DEFAULT_CODE));
	}

	public String open(final String url) {
		return this.openFile(new JVSFile(url, true));
	}

	String openFile(final JVSFile file) {
		if (!this.getFileId(file.getName()).equals("")
				&& JVSFileTabs.files.get(this.getFileId(file.getName())).getFile().equals(file.getFile())) {
			this.setSelectedIndex(this.getTabId(this.getFileId(file.getName())));
			return this.getFileId(file.getName());
		} else {
			final String fileId = UUID.randomUUID().toString();
			final JVSEditor editor = new JVSEditor();
			editor.setText(file.getCode());
			editor.getRTextArea().getDocument().addDocumentListener(new DocumentListener() {
				@Override
				public void insertUpdate(final DocumentEvent e) {
				}

				@Override
				public void removeUpdate(final DocumentEvent e) {
				}

				@Override
				public void changedUpdate(final DocumentEvent e) {
					JVSFileTabs.fileUpdateNotification();
				}
			});
			JVSFileTabs.editors.put(fileId, editor);
			JVSFileTabs.files.put(fileId, file);
			String tabTitle = file.getName();
			if (this.indexOfTab(file.getName()) != -1) {
				int i;
				for (i = 1; this.indexOfTab(file.getName() + " " + i) != -1; ++i) {}

				tabTitle = file.getName() + " " + i;
				file.setName(tabTitle);
			}

			this.add(tabTitle, "", JVSFileTabs.editors.get(fileId));
			JVSFileTabs.fileIds.put(tabTitle, fileId);
			JVSPanel.getInstance().haveNotToSave(fileId);
			this.setSelectedIndex(this.getTabId(fileId));
			this.setTabComponentAt(this.getTabId(fileId), new TabPanel(this, fileId));
			return fileId;
		}
	}

	public void closeFile(final String fileId) {
		if (this.getTabId(fileId) != -1) {
			this.getTitleAt(this.getTabId(fileId));

			try {
				String fileName = "";
				final Iterator<String> var4 = JVSFileTabs.fileIds.keySet().iterator();

				while (var4.hasNext()) {
					final String name = var4.next();
					if (JVSFileTabs.fileIds.get(name).equals(fileId)) {
						fileName = name;
					}
				}

				this.removeTabAt(this.getTabId(fileId));
				JVSFileTabs.fileIds.remove(fileName);
				JVSFileTabs.files.remove(fileId);
				JVSFileTabs.editors.remove(fileId);
				JVSPanel.getInstance().haveToSave.remove(fileId);
			} catch (final Exception var6) {
				throw new IllegalStateException(var6);
			}
		}

	}

	public String getCurrentFileId() {
		final String tab_name = this.getTitleAt(this.getSelectedIndex());
		final String fileId = JVSFileTabs.fileIds.get(tab_name);
		return fileId;
	}

	public Boolean saveCurrentFile() {
		return this.saveFile(this.getCurrentFileId());
	}

	public Boolean saveAsCurrentFile() {
		return this.saveAsFile(this.getCurrentFileId());
	}

	public Boolean currentFileIsTmp() {
		return JVSFileTabs.files.get(this.getCurrentFileId()).isTmp();
	}

	public Boolean compileFile(final String fileId) {
		if (JVSFileTabs.fileIds.containsValue(fileId) && !JVSFileTabs.files.get(fileId).isTmp()) {
			JVSFileTabs.currentCompiledFile = fileId;
			if (ProgletEngine.getInstance().doCompile(JVSFileTabs.editors.get(fileId).getText())) {
				System.out.println(I18N.gettext("Compilation succeeded!"));
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	public Boolean saveFile(final String fileId) {
		if (!JVSFileTabs.fileIds.containsValue(fileId)) {
			return true;
		} else if (JVSFileTabs.files.get(fileId).isTmp()) {
			return this.saveAsFile(fileId);
		} else {
			JVSFileTabs.files.get(fileId).setCode(JVSFileTabs.editors.get(fileId).getText());
			JVSFileTabs.files.get(fileId).save();
			return true;
		}
	}

	public Boolean saveAsFile(final String fileId) {
		final JFileChooser fc = new JFileChooser();
		if (UserConfig.getConf().containsKey("io.dir")) {
			fc.setCurrentDirectory(new File(UserConfig.getConf().getString("io.dir")));
		}

		fc.setApproveButtonText(I18N.gettext("Save"));
		fc.setDialogTitle(I18N.gettext("Save"));
		final int returnVal = fc.showSaveDialog(this.getParent());
		if (returnVal == 0) {
			String path = fc.getSelectedFile().getAbsolutePath();
			String name = fc.getSelectedFile().getName();
			UserConfig.getConf().addProperty("io.dir", fc.getSelectedFile().getParent());
			if (!path.endsWith(".jvs")) {
				path = path + ".jvs";
			}

			if (!name.endsWith(".jvs")) {
				name = name + ".jvs";
			}

			if (!this.getFileId(name).equals("")) {
				System.out.println("File is not new ; Test : ");
				System.out.println(JVSFileTabs.files.get(this.getFileId(name)).getFile().getName().equals(name));
				if (JVSFileTabs.files.get(this.getFileId(name)).getFile().getName().equals(name)) {
					JOptionPane.showMessageDialog(Desktop.getInstance().getFrame(),
							I18N.gettext("This file is already open in Java's nice, do something better for once."),
							I18N.gettext("Write error"), 0);
					return this.saveAsFile(fileId);
				}
			}

			JVSFileTabs.files.get(fileId).setPath(path);
			JVSFileTabs.files.get(fileId).setName(fc.getSelectedFile().getName());
			this.editTabName(fileId, name);
			JVSFileTabs.files.get(fileId).setCode(JVSFileTabs.editors.get(fileId).getText());
			if (JVSFileTabs.files.get(fileId).save()) {
				return true;
			} else {
				JOptionPane.showMessageDialog(Desktop.getInstance().getFrame(),
						"File cannot be written here, choose some other place for your shitcode", "Write error", 0);
				return this.saveAsFile(fileId);
			}
		} else {
			return false;
		}
	}

	public int getTabId(final String fileId) {
		return JVSFileTabs.fileIds.containsValue(fileId) ? this.indexOfComponent(JVSFileTabs.editors.get(fileId)) : -1;
	}

	public Boolean editTabName(final String fileId, final String newTitle) {
		String tabTitle = newTitle;
		if (this.indexOfTab(newTitle) != -1) {
			int i;
			for (i = 1; this.indexOfTab(newTitle + " " + i) != -1; ++i) {}

			tabTitle = newTitle + " " + i;
		}

		final String oldTabTitle = this.getTitleAt(this.getTabId(fileId));
		this.setTitleAt(this.getTabId(fileId), tabTitle);
		JVSFileTabs.fileIds.remove(oldTabTitle);
		JVSFileTabs.fileIds.put(tabTitle, fileId);
		return true;
	}

	public String getFileId(final String tabName) {
		return JVSFileTabs.fileIds.containsKey(tabName) ? (String) JVSFileTabs.fileIds.get(tabName) : "";
	}

	public JVSFile getFile(final String id) {
		return JVSFileTabs.files.containsKey(id) ? (JVSFile) JVSFileTabs.files.get(id) : new JVSFile();
	}

	public JVSEditor getEditor(final String fileId) {
		return JVSFileTabs.editors.containsKey(fileId) ? (JVSEditor) JVSFileTabs.editors.get(fileId) : new JVSEditor();
	}

	protected static void fileUpdateNotification() {
		JVSPanel.getInstance().mustSave(JVSFileTabs.getInstance().getCurrentFileId());
	}

	public int getOppenedFileCount() {
		return this.tabs.entrySet().toArray().length;
	}

	static {
		UIManager.put("FileChooser.readOnly", Boolean.TRUE);
		JVSFileTabs.editors = new HashMap<>();
		JVSFileTabs.files = new HashMap<>();
		JVSFileTabs.fileIds = new HashMap<>();
		JVSFileTabs.currentCompiledFile = "";
	}
}
