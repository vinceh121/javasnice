package org.javascool.gui;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import org.javascool.tools.FileManager;

import lol.javasnice.I18N;

final class JVSFile {
	public static final String DEFAULT_CODE = "void main(){\n    \n}\n";
	private String fileContent;
	private String code;
	private String name;
	private String path;
	private File file;
	private String proglet;

	public JVSFile() {
		this("");
	}

	public JVSFile(final String text) {
		this(text, false);
	}

	public JVSFile(final String url, final Boolean fromurl) {
		if (!fromurl) {
			this.code = url;
			this.name = I18N.gettext("New file");
			this.path = "";
			this.proglet = "default";

			try {
				this.file = File.createTempFile("JVS_TMPFILE_", ".jvs");
				this.file.deleteOnExit();
				this.path = this.file.getAbsolutePath();
			} catch (final IOException var4) {
				throw new RuntimeException(var4);
			}
		} else {
			final File file_to_open = new File(url);
			this.name = file_to_open.getName();
			this.path = file_to_open.getAbsolutePath();
			this.fileContent = FileManager.load(this.path);
			if (this.fileContent.matches("^[ \\t\\n\\r]*@proglet:[A-Za-z]*[\\n\\r]*")) {
				this.proglet = this.fileContent.replaceAll("^[ \\t\\n\\r]*@proglet:([A-Za-z]*)[\\n\\r]*.*", "$1");
			}

			this.code = this.fileContent;
			this.file = file_to_open;
		}

		this.refreshData();
	}

	public Boolean isTmp() {
		return this.file.getName().startsWith("JVS_TMPFILE_");
	}

	public Boolean save() {
		try {
			this.refreshData();
			FileManager.save(this.getPath(), this.code);
			return true;
		} catch (final Exception var2) {
			return false;
		}
	}

	public String getName() {
		return this.name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public String getPath() {
		return this.path;
	}

	public void setPath(final String path) {
		this.path = path;
		this.file = new File(path);
	}

	public File getFile() {
		return this.file;
	}

	public String getProglet() {
		return this.proglet.equals("default") ? "" : this.proglet;
	}

	public void setProglet(final String proglet) {
		this.proglet = proglet;
		this.refreshData();
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(final String code) {
		this.code = code;
		this.refreshData();
	}

	private void refreshData() {
		String fileToSave = "";
		fileToSave = fileToSave + this.getCode();
		this.fileContent = fileToSave;
	}

	public static String readFileAsString(final String filePath) throws IOException {
		final byte[] buffer = new byte[(int) new File(filePath).length()];
		final BufferedInputStream f = new BufferedInputStream(new FileInputStream(filePath));
		f.read(buffer);
		f.close();
		return new String(buffer);
	}
}
