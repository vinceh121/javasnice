package org.javascool.gui;

import java.awt.Component;
import java.util.HashMap;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import org.javascool.macros.Macros;

class JVSTabs extends JTabbedPane {
	private static final long serialVersionUID = 1L;
	protected HashMap<String, JPanel> tabs = new HashMap<>();

	public String add(final String name, final String icon, final JPanel panel) {
		return this.add(name, icon, panel, (String) null);
	}

	public String add(final String name, final String icon, final Component panel) {
		this.tabs.put(name, new JPanel());
		if (!icon.equalsIgnoreCase("")) {
			final ImageIcon logo = Macros.getIcon(icon);
			this.addTab(name, logo, panel);
		} else {
			this.addTab(name, (Icon) null, panel);
		}

		this.revalidate();
		return name;
	}

	public String add(final String name, final String icon, final JPanel panel, final String tooltip) {
		this.tabs.put(name, panel);
		if (!icon.equalsIgnoreCase("")) {
			final ImageIcon logo = Macros.getIcon(icon);
			this.addTab(name, logo, panel, tooltip);
		} else {
			this.addTab(name, (Icon) null, panel, tooltip);
		}

		this.revalidate();
		return name;
	}

	public JPanel getPanel(final String name) {
		return this.tabs.get(name);
	}

	public void del(final String name) {
		this.removeTabAt(this.indexOfTab(name));
		this.tabs.remove(name);
	}

	public void switchToTab(final String name) {
		this.setSelectedIndex(this.indexOfTab(name));
	}
}
