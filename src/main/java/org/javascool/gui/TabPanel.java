package org.javascool.gui;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.plaf.basic.BasicButtonUI;

import lol.javasnice.I18N;

class TabPanel extends JPanel {
	private static final long serialVersionUID = 1L;
	private final JTabbedPane pane;
	private final String file;
	private static final MouseListener buttonMouseListener = new MouseAdapter() {
		@Override
		public void mouseEntered(final MouseEvent e) {
			final Component component = e.getComponent();
			if (component instanceof AbstractButton) {
				final AbstractButton button = (AbstractButton) component;
				button.setBorderPainted(false);
			}

		}

		@Override
		public void mouseExited(final MouseEvent e) {
			final Component component = e.getComponent();
			if (component instanceof AbstractButton) {
				final AbstractButton button = (AbstractButton) component;
				button.setBorderPainted(false);
			}

		}
	};

	public TabPanel(final JTabbedPane pane) {
		this(pane, "");
	}

	public TabPanel(final JTabbedPane pane, final String fileId) {
		super(new FlowLayout(FlowLayout.LEFT, 0, 0));
		this.file = fileId;
		if (pane == null) {
			throw new NullPointerException("TabbedPane is null");
		} else {
			this.pane = pane;
			this.setOpaque(false);
			final JLabel label = new JLabel() {
				private static final long serialVersionUID = 1L;

				@Override
				public String getText() {
					final int i = pane.indexOfTabComponent(TabPanel.this);
					return i != -1 ? pane.getTitleAt(i) : null;
				}
			};
			this.add(label);
			label.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 5));
			final JButton button = new TabPanel.TabButton();
			this.add(button);
			this.setBorder(BorderFactory.createEmptyBorder(2, 0, 0, 0));
		}
	}

	private class TabButton extends JButton implements ActionListener {
		private static final long serialVersionUID = 1L;

		public TabButton() {
			final int size = 17;
			this.setPreferredSize(new Dimension(size, size));
			this.setToolTipText(I18N.gettext("Close this tab"));
			this.setUI(new BasicButtonUI());
			this.setContentAreaFilled(false);
			this.setFocusable(false);
			this.setBorder(BorderFactory.createEtchedBorder());
			this.setBorderPainted(false);
			this.addMouseListener(TabPanel.buttonMouseListener);
			this.setRolloverEnabled(true);
			this.addActionListener(this);
		}

		@Override
		public void actionPerformed(final ActionEvent e) {
			if ((TabPanel.this.pane.getTabCount() > 1) && this.confirmClose()) {
				final int i = TabPanel.this.pane.indexOfTabComponent(TabPanel.this);
				if (i != -1) {
					TabPanel.this.pane.remove(i);
				}
			}
		}

		@Override
		public void updateUI() {
		}

		@Override
		protected void paintComponent(final Graphics g) {
			if (TabPanel.this.pane.getTabCount() <= 1) {
				this.setVisible(false);
				this.revalidate();
			} else {
				this.setVisible(true);
				this.revalidate();
				super.paintComponent(g);
				final Graphics2D g2 = (Graphics2D) g.create();
				if (this.getModel().isPressed()) {
					g2.translate(1, 1);
				}

				g2.setStroke(new BasicStroke(2.0F));
				g2.setColor(Color.BLACK);
				if (this.getModel().isRollover()) {
					g2.setColor(Color.WHITE);
				}

				final int delta = 6;
				g2.drawLine(delta, delta, this.getWidth() - delta - 1, this.getHeight() - delta - 1);
				g2.drawLine(this.getWidth() - delta - 1, delta, delta, this.getHeight() - delta - 1);
				g2.dispose();
			}
		}

		private boolean confirmClose() {
			if (TabPanel.this.pane instanceof JVSFileTabs) {
				if (!JVSPanel.getInstance().getHasToSave(TabPanel.this.file) || (JVSPanel.getInstance().saveFileIdBeforeClose(TabPanel.this.file) == 1)) {
					JVSFileTabs.getInstance().closeFile(TabPanel.this.file);
					return true;
				} else {
					return false;
				}
			} else {
				return TabPanel.this.pane.getTabCount() > 1;
			}
		}
	}
}
