package org.javascool.gui;

import java.io.File;
import java.net.URL;

import javax.swing.JFrame;

import org.javascool.core.ProgletEngine;
import org.javascool.macros.Macros;
import org.javascool.tools.FileManager;
import org.javascool.widgets.MainFrame;
import org.javascool.widgets.ToolBar;

public class Desktop {
	private final static Desktop desktop = new Desktop();
	private MainFrame frame;

	public static Desktop getInstance() {
		return Desktop.desktop;
	}

	private Desktop() {
	}

	public JFrame getFrame() {
		if (this.frame == null) {
			this.frame = new MainFrame() {
				private static final long serialVersionUID = 1727893520791296658L;

				@Override
				public boolean isClosable() {
					return Desktop.getInstance().isClosable();
				}
			}.reset("Javasnice", "icons/logo.png", JVSPanel.getInstance());
		}

		return this.frame;
	}

	public ToolBar getToolBar() {
		return JVSToolBar.getInstance();
	}

	public boolean isClosable() {
		final boolean close = JVSPanel.getInstance().close();
		if (close && ProgletEngine.getInstance().getProglet() != null) {
			ProgletEngine.getInstance().getProglet().stop();
		}

		return close;
	}

	public boolean openNewFile() {
		try {
			JVSPanel.getInstance().newFile();
			return true;
		} catch (final Throwable var2) {
			System.out.println("Could not create new file " + var2);
			return false;
		}
	}

	public boolean openFile(final File file) {
		try {
			if (file == null) {
				JVSPanel.getInstance().openFile();
			} else {
				JVSFileTabs.getInstance().open(file.getAbsolutePath());
			}

			return true;
		} catch (final Throwable var3) {
			System.out.println("Could not open file" + var3);
			return false;
		}
	}

	public boolean openFile(final URL url) {
		try {
			if (url.getProtocol().equals("jar")) {
				JVSFileTabs.getInstance().openFile(new JVSFile(FileManager.load(url.toExternalForm())));
				return true;
			} else {
				return this.openFile(new File(url.toURI()));
			}
		} catch (final Exception var3) {
			System.out.println("Could not open file " + var3);
			var3.printStackTrace(System.err);
			return false;
		}
	}

	public boolean openFile(final String file) {
		return this.openFile(Macros.getResourceURL(file));
	}

	public boolean openFile() {
		return this.openFile((File) null);
	}

	public boolean saveCurrentFile() {
		return JVSPanel.getInstance().saveFile();
	}

	public void closeFile() {
		JVSPanel.getInstance().closeFile();
	}

	public void compileFile() {
		JVSPanel.getInstance().compileFile();
	}

	public void closeProglet() {
		JVSPanel.getInstance().closeProglet();
	}

	public boolean openProglet(final String proglet, final boolean closed) {
		if (!closed && !JVSPanel.getInstance().closeAllFiles()) {
			return false;
		} else {
			JVSPanel.getInstance().loadProglet(proglet);
			return true;
		}
	}

	public boolean openProglet(final String proglet) {
		return this.openProglet(proglet, false);
	}

	public void openBrowserTab(final URL url, final String name) {
		this.openBrowserTab(url.toString(), name);
	}

	public void openBrowserTab(final String url, final String name) {
		JVSWidgetPanel.getInstance().openWebTab(url, name);
	}

	public void focusOnConsolePanel() {
		JVSWidgetPanel.getInstance().focusOnConsolePanel();
	}

	public void focusOnProgletPanel() {
		JVSWidgetPanel.getInstance().focusOnProgletPanel();
	}
}
