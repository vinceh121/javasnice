package org.javascool.gui;

import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

import org.fife.ui.rtextarea.RTextArea;

public class EditorWrapper {
	private static JVSFileTabs tabs = JVSFileTabs.getInstance();

	public static String getText() {
		final String fileId = EditorWrapper.tabs.getCurrentFileId();
		return EditorWrapper.tabs.getEditor(fileId).getText();
	}

	public static void setText(final String txt) {
		final String fileId = EditorWrapper.tabs.getCurrentFileId();
		EditorWrapper.tabs.getEditor(fileId).setText(txt);
	}

	public static RTextArea getRTextArea() {
		final String fileId = EditorWrapper.tabs.getCurrentFileId();
		return EditorWrapper.tabs.getEditor(fileId).getRTextArea();
	}

	public static Map<String, String> getOthers() {
		final TreeMap<String, String> others = new TreeMap<>();
		final Iterator<?> iter = JVSFileTabs.fileIds.keySet().iterator();

		while (iter.hasNext()) {
			final String nom_fich = (String) iter.next();
			final String id_fich = JVSFileTabs.fileIds.get(nom_fich);
			if (!id_fich.equals(EditorWrapper.tabs.getCurrentFileId())) {
				final String contenu = EditorWrapper.tabs.getEditor(id_fich).getText();
				others.put(nom_fich, contenu);
			}
		}

		return others;
	}
}
