package org.javascool.gui;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Iterator;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ToolTipManager;

import org.javascool.core.Proglet;
import org.javascool.core.ProgletEngine;
import org.javascool.macros.Macros;

class JVSStartPanel extends JScrollPane {
	private static final long serialVersionUID = 1L;
	private static JVSStartPanel jvssp;

	public static JVSStartPanel getInstance() {
		if (JVSStartPanel.jvssp == null) {
			JVSStartPanel.jvssp = new JVSStartPanel(JVSStartPanel.shortcutPanel());
		}

		return JVSStartPanel.jvssp;
	}

	private JVSStartPanel(final JPanel panel) {
		super(panel, 20, 30);
		this.setMinimumSize(new Dimension(800, 600));
		ToolTipManager.sharedInstance().setInitialDelay(75);
	}

	private static JPanel shortcutPanel() {
		final JPanel shortcuts = new JPanel();
		int i = 0;

		Iterator<?> var2;
		Proglet proglet;
		for (var2 = ProgletEngine.getInstance().getProglets().iterator(); var2.hasNext(); ++i) {
			proglet = (Proglet) var2.next();
		}

		shortcuts.setLayout(new GridLayout(0, i / 3 == 0 ? 1 : i / 3));
		var2 = ProgletEngine.getInstance().getProglets().iterator();

		while (var2.hasNext()) {
			proglet = (Proglet) var2.next();
			shortcuts.add(JVSStartPanel.createShortcut(Macros.getIcon(proglet.getIcon()), proglet.getName(),
					proglet.getTitle(), new JVSStartPanel.ProgletLoader(proglet.getName())));
		}

		return shortcuts;
	}

	private static JPanel createShortcut(final ImageIcon icon, final String name, final String title,
			final Runnable start) {
		final JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, 1));
		panel.add(Box.createVerticalGlue());
		final JButton label = new JButton(name, icon);
		label.setToolTipText(title);
		label.setPreferredSize(new Dimension(160, 160));
		label.setVerticalTextPosition(3);
		label.setHorizontalTextPosition(0);
		label.setAlignmentX(0.5F);
		label.setAlignmentY(0.5F);
		panel.add(label);
		panel.add(Box.createVerticalGlue());
		label.addMouseListener(new MouseListener() {
			@Override
			public void mouseClicked(final MouseEvent e) {
				start.run();
			}

			@Override
			public void mousePressed(final MouseEvent e) {
			}

			@Override
			public void mouseReleased(final MouseEvent e) {
			}

			@Override
			public void mouseEntered(final MouseEvent e) {
			}

			@Override
			public void mouseExited(final MouseEvent e) {
			}
		});
		return panel;
	}

	private static class ProgletLoader implements Runnable {
		private final String proglet;

		ProgletLoader(final String proglet) {
			this.proglet = proglet;
		}

		@Override
		public void run() {
			JVSPanel.getInstance().loadProglet(this.proglet);
		}
	}
}
