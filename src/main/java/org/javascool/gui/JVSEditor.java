package org.javascool.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.AbstractAction;
import javax.swing.ImageIcon;
import javax.swing.JMenu;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JToggleButton;
import javax.swing.KeyStroke;
import javax.swing.ToolTipManager;
import javax.swing.text.BadLocationException;

import org.apache.commons.configuration2.event.Event;
import org.fife.ui.autocomplete.AutoCompletion;
import org.fife.ui.autocomplete.CompletionProvider;
import org.fife.ui.autocomplete.DefaultCompletionProvider;
import org.fife.ui.autocomplete.LanguageAwareCompletionProvider;
import org.fife.ui.rsyntaxtextarea.RSyntaxTextArea;
import org.fife.ui.rsyntaxtextarea.SyntaxConstants;
import org.fife.ui.rsyntaxtextarea.Theme;
import org.fife.ui.rtextarea.Gutter;
import org.fife.ui.rtextarea.RTextScrollPane;
import org.fife.ui.rtextarea.ToolTipSupplier;
import org.javascool.core.JvsBeautifier;
import org.javascool.core.ProgletEngine;
import org.javascool.tools.UserConfig;
import org.javascool.widgets.ToolBar;

import lol.javasnice.I18N;

class JVSEditor extends JPanel {
	private static final long serialVersionUID = 1L;
	private boolean completion = false;
	private final RSyntaxTextArea textPane;
	private final RTextScrollPane scrollPane;
	private final ToolBar toolBar;

	public JVSEditor() {
		this.setLayout(new BorderLayout());
		this.toolBar = new ToolBar();
		this.textPane = this.createTextArea();
		this.textPane.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_JAVA);
		this.setTheme(
				UserConfig.getConf().getString("ui.editor_theme", "/org/fife/ui/rsyntaxtextarea/themes/monokai.xml"));
		UserConfig.getConf()
				.addEventListener(Event.ANY, event -> JVSEditor.this.setTheme(UserConfig.getConf()
						.getString("ui.editor_theme", "/org/fife/ui/rsyntaxtextarea/themes/monokai.xml")));
		this.textPane.requestFocus();
		new JVSEditor.JVSAutoCompletionProvider(this.textPane);
		this.scrollPane = new RTextScrollPane(this.textPane, true);
		final Gutter gutter = this.scrollPane.getGutter();
		gutter.setBorderColor(new Color(25, 25, 25));
		this.toolBar.addTool(I18N.gettext("Reformat code"), "icons/format.png",
				() -> JVSEditor.this.setText(JvsBeautifier.run(JVSEditor.this.getText())));
		final JToggleButton completionButton = new JToggleButton();
		ToolBar.adjustButtonSettings(completionButton, I18N.gettext("Enable autocomplete"), "icons/completion.png");
		completionButton.addActionListener(e -> {
			JVSEditor.this.completion = completionButton.isSelected();
			if (UserConfig.getConf().getInt("ui.toolbar_btns", ToolBar.BUTTON_ICON_ONLY) == ToolBar.BUTTON_TEXT_ICON
					|| UserConfig.getConf()
							.getInt("ui.toolbar_btns", ToolBar.BUTTON_ICON_ONLY) == ToolBar.BUTTON_TEXT_ONLY) {
				if (completionButton.isSelected()) {
					completionButton.setToolTipText(I18N.gettext("Enable autocomplete"));
				} else {
					completionButton.setToolTipText(I18N.gettext("Disable autocomplete"));
				}
			}
			if (completionButton.isSelected()) {
				completionButton.setToolTipText(I18N.gettext("Enable autocomplete"));
			} else {
				completionButton.setToolTipText(I18N.gettext("Disable autocomplete"));
			}
		});
		this.toolBar.addTool(I18N.gettext("Enable/Disable autocomplete"), completionButton);
		this.add(this.toolBar, "North");
		this.add(this.scrollPane, "Center");
		this.setVisible(true);
	}

	private void setTheme(final String url) {
		try {
			final Theme theme = Theme.load(this.getClass().getResourceAsStream(url));
			theme.apply(this.textPane);
		} catch (final Exception t) {
			System.out.println("rip in white theme");
			t.printStackTrace();
		}
	}

	private RSyntaxTextArea createTextArea() {
		final RSyntaxTextArea textArea = new RSyntaxTextArea(25, 70);
		textArea.setCaretPosition(0);
		textArea.requestFocusInWindow();
		textArea.setMarkOccurrences(true);
		textArea.setCodeFoldingEnabled(true);
		textArea.setText("");
		textArea.setTabSize(4);
		textArea.setTabsEmulated(true);
		final JPopupMenu popup = textArea.getPopupMenu();
		int i = 0;

		while (i < popup.getComponentCount()) {
			if (popup.getComponent(i) instanceof JMenu) {
				popup.remove(i);
			} else {
				++i;
			}
		}

		KeyStroke key = KeyStroke.getKeyStroke(83, InputEvent.CTRL_DOWN_MASK);
		if (JVSEditor.isMac()) {
			key = KeyStroke.getKeyStroke(83, InputEvent.META_DOWN_MASK);
		}

		this.registerShortcuts(textArea);
		textArea.getInputMap().put(key, "save");
		textArea.getActionMap().put("save", new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(final ActionEvent e) {
				JVSPanel.getInstance().saveFile();
			}
		});
		return textArea;
	}

	private void registerShortcuts(final RSyntaxTextArea textArea) {
		final int modifier = JVSEditor.isMac() ? InputEvent.META_DOWN_MASK : InputEvent.CTRL_DOWN_MASK;
		final KeyStroke keyN = KeyStroke.getKeyStroke(78, modifier);
		final KeyStroke keyO = KeyStroke.getKeyStroke(79, modifier);
		final KeyStroke keyR = KeyStroke.getKeyStroke(82, modifier);
		final KeyStroke keyQ = KeyStroke.getKeyStroke(81, modifier);
		final KeyStroke keyArrowLeft = KeyStroke.getKeyStroke(37, InputEvent.ALT_DOWN_MASK);
		final KeyStroke keyArrowRight = KeyStroke.getKeyStroke(39, InputEvent.ALT_DOWN_MASK);

		textArea.getInputMap().put(keyN, "new");
		textArea.getInputMap().put(keyO, "open");
		textArea.getInputMap().put(keyR, "run");
		textArea.getInputMap().put(keyQ, "close");
		textArea.getInputMap().put(keyArrowLeft, "arrowLeft");
		textArea.getInputMap().put(keyArrowRight, "arrowRight");

		textArea.getActionMap().put("new", new AbstractAction() {
			/**
			 * 
			 */
			private static final long serialVersionUID = -8964970372883757506L;

			@Override
			public void actionPerformed(final ActionEvent e) {
				JVSPanel.getInstance().newFile();
			}
		});

		textArea.getActionMap().put("open", new AbstractAction() {
			/**
			 * 
			 */
			private static final long serialVersionUID = -4874917256938699000L;

			@Override
			public void actionPerformed(final ActionEvent e) {
				JVSPanel.getInstance().openFile();
			}
		});

		textArea.getActionMap().put("run", new AbstractAction() {
			private static final long serialVersionUID = -4634476719489203442L;

			@Override
			public void actionPerformed(final ActionEvent e) {
				if (!ProgletEngine.getInstance().isRunning()) {
					JVSPanel.getInstance().compileFile();
				}
				JVSToolBar.getInstance().getRunButton().onActionCauseSwingBad();
				/*
				 * if (!ProgletEngine.getInstance().isRunning()) {
				 * JVSPanel.getInstance().compileFile(); if
				 * (JVSToolBar.getInstance().canRunShitcode) {
				 * ProgletEngine.getInstance().doRun(); } }
				 */
			}
		});

		textArea.getActionMap().put("close", new AbstractAction() {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1057591459367629410L;

			@Override
			public void actionPerformed(final ActionEvent e) {
				JVSPanel.getInstance().closeFile();
			}
		});

		textArea.getActionMap().put("arrowLeft", new AbstractAction() {
			/**
			 * 
			 */
			private static final long serialVersionUID = 3576395210062183474L;

			@Override
			public void actionPerformed(final ActionEvent e) {
				JVSEditor.this.selectTabNextTo(true);
			}
		});

		textArea.getActionMap().put("arrowRight", new AbstractAction() {
			/**
			 * 
			 */
			private static final long serialVersionUID = -3841076159962624649L;

			@Override
			public void actionPerformed(final ActionEvent e) {
				JVSEditor.this.selectTabNextTo(false);
			}
		});
	}

	private void selectTabNextTo(final boolean prev) {
		final int newIndex = JVSFileTabs.getInstance().getSelectedIndex() + (prev ? -1 : 1);
		if (newIndex < 0 || newIndex == JVSFileTabs.getInstance().tabs.size()) {
			return;
		}
		JVSFileTabs.getInstance().setSelectedIndex(newIndex);
		this.textPane.grabFocus();
	}

	private static boolean isMac() {
		return System.getProperty("os.name").toUpperCase().contains("MAC");
	}

	public String getText() {
		return this.textPane.getText();
	}

	public void setText(final String text) {
		this.textPane.setText(text);
	}

	public RSyntaxTextArea getRTextArea() {
		return this.textPane;
	}

	public RTextScrollPane getScrollPane() {
		return this.scrollPane;
	}

	public void removeLineSignals() {
		this.getScrollPane().getGutter().removeAllTrackingIcons();
	}

	public void signalLine(final int line) {
		final Gutter gutter = this.getScrollPane().getGutter();
		gutter.setBookmarkingEnabled(true);
		ImageIcon icon = null;

		try {
			final BufferedImage img = ImageIO.read(ClassLoader.getSystemResourceAsStream("icons/error.png"));
			icon = new ImageIcon(img);
		} catch (final IOException var7) {
			var7.printStackTrace();
		}

		try {
			this.getRTextArea().setCaretPosition(this.getRTextArea().getLineStartOffset(line - 1));
			this.getScrollPane().getGutter().addLineTrackingIcon(line - 1, icon);
		} catch (final BadLocationException var6) {
			var6.printStackTrace();
		}

	}

	public static CompletionProvider createCodeCompletionProvider() {
		final DefaultCompletionProvider cp = new DefaultCompletionProvider();
		if (!ProgletEngine.getInstance().getProglet().getCompletion().equals("")) {
			JvsXMLCompletion.readCompletionToProvider(ProgletEngine.getInstance().getProglet().getCompletion(), cp);
		}

		JvsXMLCompletion.readCompletionToProvider("completion-macros.xml", cp);
		final LanguageAwareCompletionProvider lacp = new LanguageAwareCompletionProvider(cp);
		return lacp;
	}

	private class JVSAutoCompletionProvider extends AutoCompletion {
		public JVSAutoCompletionProvider(final RSyntaxTextArea TextPane) {
			super(JVSEditor.createCodeCompletionProvider());
			this.setShowDescWindow(true);
			this.install(TextPane);
			TextPane.setToolTipSupplier((ToolTipSupplier) this.getCompletionProvider());
			ToolTipManager.sharedInstance().registerComponent(TextPane);
			TextPane.addKeyListener(new KeyListener() {
				@Override
				public void keyTyped(final KeyEvent e) {
					final int ch = e.getKeyChar();
					if (ch > '!' && ch != 127 && ch != 129 && ch != 141 && ch != 143 && ch != 144 && ch != 157
							&& ch != 160 && JVSEditor.this.completion) {
						JVSAutoCompletionProvider.this.showPopupWindow();
					} else {
						JVSAutoCompletionProvider.this.hideChildWindows();
					}

				}

				@Override
				public void keyPressed(final KeyEvent e) {
				}

				@Override
				public void keyReleased(final KeyEvent e) {
				}
			});
		}

		public void showPopupWindow() {
			this.refreshPopupWindow();
		}
	}
}
