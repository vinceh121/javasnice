package org.javascool.gui;

import javax.swing.JScrollPane;

import org.javascool.core.Proglet;
import org.javascool.core.ProgletEngine;
import org.javascool.macros.Macros;
import org.javascool.widgets.Console;
import org.javascool.widgets.HtmlDisplay;

import lol.javasnice.I18N;

class JVSWidgetPanel extends JVSTabs {
	private static final long serialVersionUID = 1L;
	private String progletTabId;
	private static JVSWidgetPanel jwp;
	private Console console;

	public static JVSWidgetPanel getInstance() {
		if (JVSWidgetPanel.jwp == null) {
			JVSWidgetPanel.jwp = new JVSWidgetPanel();
		}

		return JVSWidgetPanel.jwp;
	}

	private JVSWidgetPanel() {
		this.add(I18N.gettext("Console"), "", this.console = Console.newInstance());
	}

	public void setProglet(final String name) {
		this.removeAll();
		Console.removeInstance(this.console);
		this.add(I18N.gettext("Console"), "", this.console = Console.newInstance());
		final Proglet proglet = ProgletEngine.getInstance().setProglet(name);
		if (proglet.getPane() != null) {
			this.progletTabId = this.add("Proglet " + name, "", new JScrollPane(proglet.getPane()));
		}

		if (proglet.getHelp() != null) {
			this.add(I18N.gettext("Proglet Help"), "",
					new HtmlDisplay().setPage(Macros.getResourceURL(proglet.getHelp())));
			// this.switchToTab("Aide de la proglet");
		}

		final HtmlDisplay memo = new HtmlDisplay();
		memo.setPage(ClassLoader.getSystemResource("memo-macros." + I18N.getLocale() + ".htm"));
		this.add(I18N.gettext("Memo"), "", memo);
	}

	public void focusOnProgletPanel() {
		if (this.progletTabId != null) {
			this.switchToTab(this.progletTabId);
		}

	}

	public void focusOnConsolePanel() {
		this.setSelectedIndex(this.indexOfTab(I18N.gettext("Console")));
	}

	public void openWebTab(final String url, final String tabName) {
		if (this.indexOfTab(tabName) >= 0) {
			this.switchToTab(tabName);
		} else {
			final HtmlDisplay memo = new HtmlDisplay();
			memo.setPage(url);
			this.add(tabName, "", memo);
			this.setTabComponentAt(this.indexOfTab(tabName), new TabPanel(this));
			this.setSelectedComponent(memo);
		}
	}

	public Console getConsoleInstance() {
		return this.console;
	}
}
