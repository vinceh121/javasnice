package org.javascool.gui;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.fife.ui.autocomplete.BasicCompletion;
import org.fife.ui.autocomplete.Completion;
import org.fife.ui.autocomplete.CompletionProvider;
import org.fife.ui.autocomplete.DefaultCompletionProvider;
import org.fife.ui.autocomplete.ShorthandCompletion;
import org.javascool.macros.Macros;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

class JvsXMLCompletion extends DefaultHandler {
	private final List<Completion> completions;
	private CompletionProvider provider;
	private ClassLoader completionCL;
	private String name;
	private StringBuffer desc;
	private String shortDesc;
	private StringBuffer code;
	private boolean doingKeywords;
	private boolean inKeyword;
	private boolean gettingDoc;
	private boolean gettingCode;
	private boolean inCompletionTypes;
	private static ClassLoader DEFAULT_COMPLETION_CLASS_LOADER;

	public JvsXMLCompletion(final CompletionProvider provider) {
		this(provider, (ClassLoader) null);
	}

	public JvsXMLCompletion(final CompletionProvider provider, final ClassLoader cl) {
		this.provider = provider;
		this.completionCL = cl;
		if (this.completionCL == null) {
			this.completionCL = JvsXMLCompletion.DEFAULT_COMPLETION_CLASS_LOADER;
		}

		this.completions = new ArrayList<>();
		this.desc = new StringBuffer();
		this.code = new StringBuffer();
	}

	@Override
	public void characters(final char[] ch, final int start, final int length) {
		if (this.gettingDoc) {
			this.desc.append(ch, start, length);
		}

		if (this.gettingCode) {
			this.code.append(ch, start, length);
		}

	}

	private BasicCompletion createCompletion() {
		BasicCompletion bc = new BasicCompletion(this.provider, this.name);
		if (this.code.length() > 0) {
			bc = new ShorthandCompletion(this.provider, this.name, this.code.toString());
			this.code = new StringBuffer();
		}

		if (this.desc.length() > 0) {
			bc.setSummary(this.desc.toString());
			this.desc = new StringBuffer();
		}

		if (this.shortDesc.length() > 0) {
			bc.setShortDescription(this.shortDesc.toString());
			this.shortDesc = "";
		}

		return bc;
	}

	@Override
	public void endElement(final String uri, final String localName, final String qName) {
		if ("keywords".equals(qName)) {
			this.doingKeywords = false;
		} else if (this.doingKeywords) {
			if ("keyword".equals(qName)) {
				final Completion c = this.createCompletion();
				this.completions.add(c);
				this.inKeyword = false;
			} else if (this.inKeyword) {
				if ("doc".equals(qName)) {
					this.gettingDoc = false;
				}

				if ("code".equals(qName)) {
					this.gettingCode = false;
				}
			}
		} else if (this.inCompletionTypes && "completionTypes".equals(qName)) {
			this.inCompletionTypes = false;
		}

	}

	public void reset(final CompletionProvider provider) {
		this.provider = provider;
		this.completions.clear();
		this.doingKeywords = this.inKeyword = this.gettingDoc = false;
		this.desc = new StringBuffer();
		this.code = new StringBuffer();
	}

	public static void setDefaultCompletionClassLoader(final ClassLoader cl) {
		JvsXMLCompletion.DEFAULT_COMPLETION_CLASS_LOADER = cl;
	}

	@Override
	public void startElement(final String uri, final String localName, final String qName, final Attributes attrs) {
		if ("keywords".equals(qName)) {
			this.doingKeywords = true;
		} else if (this.doingKeywords) {
			if ("keyword".equals(qName)) {
				this.name = attrs.getValue("name");
				this.shortDesc = attrs.getIndex("title") > -1 ? attrs.getValue("title") : "";
				this.inKeyword = true;
			} else if (this.inKeyword) {
				if ("doc".equals(qName)) {
					this.gettingDoc = true;
				}

				if ("code".equals(qName)) {
					this.gettingCode = true;
				}
			}
		}

	}

	public List<Completion> getCompletions() {
		return this.completions;
	}

	public static DefaultCompletionProvider readCompletionToProvider(final String file,
			final DefaultCompletionProvider cp) {
		final SAXParserFactory factory = SAXParserFactory.newInstance();
		final JvsXMLCompletion handler = new JvsXMLCompletion(cp, ClassLoader.getSystemClassLoader());
		final BufferedInputStream bin
				= new BufferedInputStream(ClassLoader.getSystemClassLoader().getResourceAsStream(file));

		try {
			final SAXParser saxParser = factory.newSAXParser();
			saxParser.parse(bin, handler);
			final List<Completion> completions = handler.getCompletions();
			cp.addCompletions(completions);
		} catch (final Exception var15) {
			Macros.message("Error while reading auto-completion XML. Error : " + var15.getMessage(), false);
		} finally {
			try {
				bin.close();
			} catch (final IOException var14) {
				throw new RuntimeException(var14.toString());
			}
		}

		return cp;
	}
}
