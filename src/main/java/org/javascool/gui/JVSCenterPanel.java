package org.javascool.gui;

import javax.swing.JSplitPane;

class JVSCenterPanel extends JSplitPane {
	private static final long serialVersionUID = 1L;
	private static JVSCenterPanel jvssplitpane;

	public static JVSCenterPanel getInstance() {
		if (JVSCenterPanel.jvssplitpane == null) {
			JVSCenterPanel.jvssplitpane = new JVSCenterPanel();
		}

		return JVSCenterPanel.jvssplitpane;
	}

	private JVSCenterPanel() {
		super(JSplitPane.HORIZONTAL_SPLIT);
		this.setOneTouchExpandable(true);
		this.setLeftComponent(JVSFileTabs.getInstance());
		this.setRightComponent(JVSWidgetPanel.getInstance());
		this.setVisible(true);
		this.validate();
	}
}
