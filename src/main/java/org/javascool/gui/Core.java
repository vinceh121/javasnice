package org.javascool.gui;

import java.net.URL;

import javax.swing.JOptionPane;
import javax.swing.UIManager;

import org.apache.commons.io.IOUtils;
import org.javascool.core.Utils;
import org.javascool.macros.Macros;
import org.javascool.tools.UserConfig;
import org.json.JSONArray;

import lol.javasnice.I18N;
import lol.javasnice.JavasniceBuild;

public class Core {
	public static final String help = "memo-macros.htm";
	public static boolean DEBUG = false;

	public static void main(final String[] usage) {
		System.out.println("Build: " + JavasniceBuild.buildOptions());
		I18N.init();
		if (usage.length > 0 && (usage[0].equals("-h") || usage[0].equals("-help") || usage[0].equals("--help"))) {
			System.out.println(I18N.gettext("Java's Nice Core - starts the modded interface to annoy teachers"));
			System.out.println("\tjava -jar javascool.jar");
			System.exit(0);
		}

		Core.DEBUG = UserConfig.getConf().getBoolean("general.debug", true);

		if (!System.getProperty("os.name").toLowerCase().startsWith("mac")) {
			UIManager.put("FileChooser.readOnly", Boolean.TRUE);
		}

		System.out.println(I18N.gettext("Javasnice is starting...\nTime to annoy teachers"));
		UIManager.installLookAndFeel("Darcula", "com.bulenkov.darcula.DarculaLaf");
		UIManager.installLookAndFeel("Material", "mdlaf.MaterialLookAndFeel");
		try {
			UIManager.setLookAndFeel(UserConfig.getConf().getString("ui.laf", "com.bulenkov.darcula.DarculaLaf"));
		} catch (final Throwable t) {
			System.out.println(I18N.gettext("uh looks like it won't look good ¯\\_(ツ)_/¯"));
			t.printStackTrace();
		}

		System.setProperty("http.agent", "Mozilla/5.0 (X11; Linux x86_64; rv:70.0) Gecko/20100101 Firefox/70.0");
		Core.checkUpdate();
		Desktop.getInstance().getFrame();
		String pname = usage.length > 0 ? usage[usage.length - 1] : null;

		try {
			if (pname.matches(".*javascool.*")) {
				pname = null;
			} else {
				Desktop.getInstance().openProglet(pname, true);
			}
		} catch (final Exception var3) {
			pname = null;
		}

		if (pname == null) {
			pname = Utils.javascoolProglet();
			if (pname != null) {
				if (Core.DEBUG) {
					System.out.println("Opening proglet '" + pname + "'");
				}
				Desktop.getInstance().openProglet(pname, true);
			}
		}

	}

	public static void checkUpdate() {
		if (UserConfig.getConf().getBoolean("general.check_update", true)) {
			new Thread((Runnable) () -> {
				try {
					System.out.println("Checking version...");
					final URL url = new URL("https://gitlab.com/api/v4/projects/14399976/repository/tags/");
					final JSONArray tags = new JSONArray(IOUtils.toString(url, "UTF-8"));
					final String latestTag = tags.getJSONObject(0).getString("name");

					if (latestTag.compareTo(JavasniceBuild.GIT_TAG) >= 0) {
						System.out.println("You have the upstream version");
					} else {
						Macros.message(I18N.ngettext(
								"<h2>Your Javasnice version is outdated.</h2>"
										+ "Your version <span>{0}"
										+ "</span><br>The latest version's summary <span>{1}</span>",
								JavasniceBuild.GIT_TAG, latestTag), true);
					}
				} catch (final Exception e) {
					e.printStackTrace();
					JOptionPane.showMessageDialog(null, "Could not check version: " + e.toString());
				}
			}, "VersionChecker").start();
		}

	}
}
