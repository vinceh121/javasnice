package org.javascool.proglets.txtCode;

import java.awt.Color;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.Socket;

import org.javascool.tools.socket.SocketClient;
import org.javascool.tools.socket.SocketServer;

/**
 * Définit les fonctions de la proglet.
 *
 * @see <a href="Functions.java.html">source code</a>
 * @serial exclude
 * @author Christophe Béasse
 */
public class Functions {

	// @factory
	private Functions() {
	}

	/** Renvoie l'instance de la proglet. */
	/*
	 * private static Panel getPane() { return (Panel) getProgletPane(); }
	 */

	/*
	 * Méthodes liées au filedump
	 */

	public static void focusOnConsolePanel() {
		org.javascool.gui.Desktop.getInstance().focusOnConsolePanel();
	}

	private static DataInputStream fileR = null;
	private static DataOutputStream fileW = null;

	/**
	 * Ouverture du fichier en lecture.
	 * 
	 * @param nomFichier nom du fichier à ouvrir
	 */
	public static void openFileReader(final String nomFichier) {
		try {
			Functions.fileR = new DataInputStream(new BufferedInputStream(new FileInputStream(nomFichier)));
		} catch (final FileNotFoundException e) {}
	}

	/**
	 * Lecture du code suivant dans le fichier.
	 * 
	 * @return valeur du code lu.
	 */
	public static int readNextCode() {
		int c = -1;

		if (Functions.fileR == null) {
			throw new RuntimeException("The reader isn't open!");
		}

		try {
			c = Functions.fileR.readUnsignedByte();
		} catch (final EOFException e) {} catch (final IOException e) {}
		return c;
	}

	/**
	 * Fermeture du fichier ouvert en lecture.
	 */

	public static void closeFileReader() {
		try {
			Functions.fileR.close();
		} catch (final IOException e) {}
	}

	public static void filedump(final String nomFichier) {

		final int[] buffer = new int[16];

		Functions.openFileReader(nomFichier);
		int c;
		int offset = 0;
		int i = 0;
		Functions.affiche("=======================================\n");
		Functions.affiche("txtCode fileDump :[" + nomFichier + "]\n");
		Functions.affiche("=======================================\n");
		Functions.sautDeLigne();
		Functions.affiche("      00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F \n");
		while ((c = Functions.readNextCode()) != -1) {
			buffer[i++] = c;
			if (i == 16) {
				Functions.afficheligne(buffer, i, offset);
				offset++;
				i = 0;
			} // End if
		} // End while

		Functions.afficheligne(buffer, i, offset);

		Functions.closeFileReader();
	}

	static void afficheligne(final int[] buffer, final int i, final int offset) {
		if (i > 0) {
			Functions.affiche(String.format("%04x", offset) + "  ");
			for (int j = 0; j < i; j++) {
				Functions.affiche(Functions.code2HexStr(buffer[j]));
				Functions.affiche(" ");
			}
			for (int j = i; j < 16; j++) {
				Functions.affiche("   ");
			}
			Functions.affiche(" ");
			for (int j = 0; j < i; j++) {
				if (buffer[j] > 31 && buffer[j] < 127) {
					Functions.affiche(Functions.code2CarStr(buffer[j]));
				} else {
					Functions.affiche(".");
				}
			}
			Functions.sautDeLigne();
		} // End if

	}

	/**
	 * Ouverture du fichier en Ecriture.
	 * 
	 * @param nomFichier nom du fichier à ouvrir
	 */
	public static void openFileWriter(final String nomFichier) {
		try {
			Functions.fileW = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(nomFichier)));
		} catch (final IOException e) {}
	}

	/**
	 * Ecriture du code suivant dans le fichier ouvert en écriture.
	 * 
	 * @param c code à ecrire
	 */
	public static void writeNextCode(final int c) {

		if (Functions.fileW == null) {
			throw new RuntimeException("The writer isn't oopen!");
		}

		try {
			Functions.fileW.writeByte(c);
		} catch (final IOException e) {}
	}

	/**
	 * Fermeture du fichier ouvert en Ecriture.
	 */
	public static void closeFileWriter() {
		try {
			Functions.fileW.close();
		} catch (final IOException e) {}
	}

	public static void affiche(final String str) {
		Panel.textArea.append(str);
	}

	public static void affiche(final char c) {
		Panel.textArea.append(Character.toString(c));
	}

	public static void affiche(final int n) {
		Panel.textArea.append(Integer.toString(n));
	}

	public static void afficheCodeAuFormatHex(final int c) {
		Panel.textArea.append(Functions.code2HexStr(c));
	}

	public static void afficheCodeAuFormatDec(final int c) {
		Panel.textArea.append(Integer.toString(c));
	}

	public static void afficheCodeAuFormatBin(final int c) {
		Panel.textArea.append(Integer.toBinaryString(c));
	}

	public static void afficheCodeAuFormatCar(final int c) {
		Panel.textArea.append(Functions.code2CarStr(c));
	}

	public static void sautDeLigne() {
		Panel.textArea.append("\n");
	}

	public static String code2HexStr(final int code) {
		if (code < 16) {
			return "0" + Integer.toHexString(code);
		} else {
			return Integer.toHexString(code);
		}
	}

	public static String code2CarStr(final int code) {

		return Character.toString((char) code);

	}

	/** Remets à zéro la zone d'affichage du proglet */
	public static void resetConsole() {

		Panel.textArea.setText(" ");
		Panel.textArea.setForeground(Color.BLACK);

	}

	/*
	 * Méthodes liées à la connection serveur.
	 */
	private static SocketServer server = new SocketServer();

	/**
	 * Ouverture du socket server.
	 * 
	 * @see SocketServer#open(int)
	 */
	public static void openSocketServer(final int numport) {
		Functions.server.open(numport);
	}

	/** Permet de récupérer un message via le socket server. */
	public static String getMessageViaSocketServer() {
		return Functions.server.getMessage();
	}

	/** Permet d'écrire un message sur le socket server. */
	public static void sendMessageViaSocketServer(final String text) {
		Functions.server.sendMessage(text);
	}

	/** Renvoie la socket elle-même pour accéder aux fonctions bas-niveau. */
	public static Socket getSocketServer() {
		return Functions.server.getSocket();
	}

	/** Fermeture du socket server. */
	public static void closeSocketServer() {
		Functions.server.close();
	}

	/*
	 * Méthodes liées à la connection serveur.
	 */
	private static SocketClient client = new SocketClient();

	/**
	 * Ouverture du socket client.
	 * 
	 * @see SocketClient#open(String, int)
	 */
	public static void openSocketClient(final String hostname, final int numport) {
		Functions.client.open(hostname, numport);
	}

	/** Permet de récupérer un message via le socket client. */
	public static String getMessageViaSocketClient() {
		return Functions.client.getMessage();
	}

	/** Permet d'écrire un message sur le socket client. */
	public static void sendMessageViaSocketClient(final String text) {
		Functions.client.sendMessage(text);
	}

	/** Renvoie la socket elle-même pour accéder aux fonctions bas-niveau. */
	public static Socket getSocketClient() {
		return Functions.client.getSocket();
	}

	/** Fermeture du socket client. */
	public static void closeSocketClient() {
		Functions.client.close();
	}

} // class functions
