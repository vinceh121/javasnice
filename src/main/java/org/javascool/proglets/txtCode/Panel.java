package org.javascool.proglets.txtCode;

import java.awt.BorderLayout;
import java.awt.Font;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

/**
 * Définit le panneau graphique de la proglet.
 *
 * @see <a href="Panel.java.html">source code</a>
 * @serial exclude
 * @author Christophe Béasse <oceank2@gmail.com>
 */
public class Panel extends JPanel {

	private static final long serialVersionUID = 1L;

	// Construction de la proglet
	public Panel() {
		final Font f = new Font("Courier New", Font.PLAIN, 11);
		Panel.textArea.setEditable(false);
		Panel.textArea.setEnabled(false);
		Panel.textArea.setFont(f);
		Panel.textArea.setRows(30);
		Panel.textArea.setColumns(80);

		this.add(new JScrollPane(Panel.textArea), BorderLayout.CENTER);

	}

	public static JTextArea textArea = new JTextArea();
}
