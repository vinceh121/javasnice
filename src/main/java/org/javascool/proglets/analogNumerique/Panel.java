package org.javascool.proglets.analogNumerique;

import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JPanel;

import org.javascool.macros.Macros;
import org.javascool.widgets.NumberInput;

import lol.javasnice.I18N;

/**
 * Définit une proglet javascool qui permet d'implémenter un algorithme
 * dichotomiqiue de conversion analogique-numérique.
 *
 * @see <a href="Panel.java.html">code source</a>
 * @serial exclude
 */
public class Panel extends JPanel {
	private static final long serialVersionUID = 1L;
	/** Tension inconnue d'entrée. */
	public NumberInput value;
	/** Etiquette du comparateur et de la sortie. */
	public JLabel out, cmp;

	// @bean
	public Panel() {
		super(new BorderLayout());
		this.setPreferredSize(new Dimension(560, 450));
		// Adds the figure
		final JLayeredPane pane = new JLayeredPane();
		pane.setPreferredSize(new Dimension(540, 300));
		final JLabel fig = new JLabel();
		fig.setIcon(Macros.getIcon("org/javascool/proglets/analogNumerique/conv.png"));
		fig.setBounds(3, 0, 540, 300);
		pane.add(fig, 1, 0);
		this.out = new JLabel("????");
		this.out.setBounds(270, 78, 100, 50);
		pane.add(this.out, 2, 0);
		this.cmp = new JLabel("?");
		this.cmp.setBounds(190, 178, 100, 50);
		pane.add(this.cmp, 2, 1);
		this.add(pane, BorderLayout.NORTH);
		// Adds the input
		this.add(this.value = new NumberInput(), BorderLayout.CENTER);
		this.value.setText(I18N.gettext("unknown voltage")).setScale(0, 1023, 1);
		this.value.setValue(300);
		final JPanel border = new JPanel();
		border.setPreferredSize(new Dimension(560, 190));
		this.add(border, BorderLayout.SOUTH);
	}

}
