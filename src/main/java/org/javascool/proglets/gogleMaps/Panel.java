/*******************************************************************************
* David.Pichardie@inria.fr, Copyright (C) 2011.           All rights reserved. *
*******************************************************************************/

package org.javascool.proglets.gogleMaps;

import javax.swing.JPanel;

/**
 * Définit une proglet javascool qui permet de tracer des chemins sur une carte
 * de France.
 *
 * @see <a href="Panel.java.html">code source</a>
 * @see <a href="GogleMapPanel.java.html">GogleMapPanel.java</a>, <a href=
 *      "GogleMapCalculChemins.java.html">GogleMapCalculChemins.java</a>,
 *      <a href="GogleMapParcours.java.html">GogleMapParcours.java</a>
 * @serial exclude
 */
public class Panel extends JPanel {
	private static final long serialVersionUID = 1L;

	// @bean
	public Panel() {
		this.add(this.main = new GogleMapPanel());
		Functions.voisins = this.main.arcs;
		Functions.latitudes = this.main.latitudes;
		Functions.longitudes = this.main.longitudes;
		Functions.villes = this.main.latitudes.keySet();
	}

	GogleMapPanel main;
}
