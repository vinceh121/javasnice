/*******************************************************************************
* David.Pichardie@inria.fr, Copyright (C) 2011.           All rights reserved. *
*******************************************************************************/

package org.javascool.proglets.gogleMaps;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Set;

import org.javascool.macros.Macros;

class GogleMapParcours {
	private static int numVisite;

	private static void afficheVille(final GogleMapPanel g, final String s) {
		final double latitude = Functions.latitudes.get(s);
		final double longitude = Functions.longitudes.get(s);
		g.affichePoint(longitude, latitude);
	}

	// affiche une ville de nom s en indiquant le num�ro num
	static void afficheVilleAvecNumero(final GogleMapPanel g, final String s, final int num) {
		final double latitude = Functions.latitudes.get(s);
		final double longitude = Functions.longitudes.get(s);
		g.affichePoint(longitude, latitude, num);
	}

	static void afficheRouteDirecte(final GogleMapPanel g, final String ville1, final String ville2) {
		g.afficheRoute(Functions.longitudes.get(ville1), Functions.latitudes.get(ville1),
				Functions.longitudes.get(ville2), Functions.latitudes.get(ville2), 1);
	}

	// Affiche toutes les routes directes
	static void afficheToutesRoutesDirectes(final GogleMapPanel g) {
		for (final String depart : g.arcs.keySet()) {
			for (final String arrivee : g.arcs.get(depart)) {
				if (depart.compareTo(arrivee) > 0) {
					GogleMapParcours.afficheRouteDirecte(g, depart, arrivee);
				}
			}
		}
	}

	static void parcoursRec(final GogleMapPanel g, final Set<String> vu, final String ville1) {
		// Macros.sleep(500);
		vu.add(ville1);
		GogleMapParcours.afficheVilleAvecNumero(g, ville1, GogleMapParcours.numVisite++);
		for (final String ville2 : g.arcs.get(ville1)) {
			GogleMapParcours.afficheVille(g, ville2);
		}
		Macros.sleep(500);
		for (final String ville2 : g.arcs.get(ville1)) {
			if (!vu.contains(ville2)) {
				GogleMapParcours.parcoursRec(g, vu, ville2);
			}
		}
	}

	static void parcoursProfondeur(final GogleMapPanel g, final String depart) {
		final Set<String> vu = new HashSet<>();
		GogleMapParcours.numVisite = 1;
		GogleMapParcours.parcoursRec(g, vu, depart);
	}

	static void parcoursLargeur(final GogleMapPanel g, final String depart) {
		final Set<String> vu = new HashSet<>();
		final Queue<String> aVoir = new LinkedList<>();
		aVoir.offer(depart);
		int countVisite = 1;
		while (!aVoir.isEmpty()) {
			final String ville1 = aVoir.remove();
			vu.add(ville1);
			GogleMapParcours.afficheVilleAvecNumero(g, ville1, countVisite++);
			Macros.sleep(500);
			for (final String ville2 : g.arcs.get(ville1)) {
				if (!vu.contains(ville2)) {
					GogleMapParcours.afficheVille(g, ville2);
					if (!aVoir.contains(ville2)) {
						aVoir.offer(ville2);
					}
				}
			}
		}
	}
}
