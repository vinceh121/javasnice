/*******************************************************************************
* David.Pichardie@inria.fr, Copyright (C) 2011.           All rights reserved. *
*******************************************************************************/

package org.javascool.proglets.gogleMaps;

import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.swing.SwingUtilities;

import org.javascool.macros.Macros;

/**
 * Définit les fonctions de la proglet qui permettent de tracer des chemins sur
 * une carte de France.
 *
 * @see <a href="Functions.java.html">code source</a>
 * @serial exclude
 */
public class Functions {
	// @factory
	private Functions() {
	}

	/** Renvoie l'instance de la proglet pour accéder à ses éléments. */
	private static Panel getPane() {
		return (Panel) Macros.getProgletPane();
	}

	/** Définit l'intensité d'uen route. */
	public enum IntensiteRoute {
		LEGER(1), MOYEN(2), FORT(3);

		private int value;

		IntensiteRoute(final int i) {
			this.value = i;
		}
	}

	/** Calcule la distance entre deux villes. */
	public static double getDistance(final String ville1, final String ville2) {
		final double x1 = Functions.latitudes.get(ville1), y1 = Functions.longitudes.get(ville1);
		final double x2 = Functions.latitudes.get(ville2), y2 = Functions.longitudes.get(ville2);
		return Functions.distanceEuclidienne(y1, x1, y2, x2);
	}

	public final static IntensiteRoute LEGER = IntensiteRoute.LEGER;
	public final static IntensiteRoute MOYEN = IntensiteRoute.MOYEN;
	public final static IntensiteRoute FORT = IntensiteRoute.FORT;

	/**
	 * Met en évidence un point de la carte désigné par ses coordonnées
	 * géographiques (longitude,latitude).
	 */
	public static void affichePointSurCarte(final double longitude, final double latitude, final int idx) {
		Functions.getPane().main.affichePoint(longitude, latitude, idx);
	}

	/**
	 * Met en évidence un point de la carte désigné par ses coordonnées
	 * géographiques (longitude,latitude) en numérotant ce point avec le nombre
	 * index.
	 */
	public static void affichePointSurCarte(final double longitude, final double latitude) {
		Functions.getPane().main.affichePoint(longitude, latitude);
	}

	/**
	 * Trace une ligne droite entre un point de coordonnées géographiques
	 * (longitude1,latitude1) et un autre de coordonnées géographiques
	 * (longitude2,latitude2).
	 */
	public static void afficheRouteSurCarte(final double longitude1, final double latitude1, final double longitude2,
			final double latitude2, final IntensiteRoute intensite) {
		Functions.getPane().main.afficheRoute(longitude1, latitude1, longitude2, latitude2, intensite.value);
	}

	/**
	 * Trace une ligne droite entre un point de coordonnées géographiques
	 * (longitude1,latitude1) et un autre de coordonnées géographiques
	 * (longitude2,latitude2) en prenant une couleur d'intensité intensite.
	 */
	public static void afficheRouteSurCarte(final double longitude1, final double latitude1, final double longitude2,
			final double latitude2) {
		Functions.getPane().main.afficheRoute(longitude1, latitude1, longitude2, latitude2);
	}

	/**
	 * Calcule la distance (en km) sur la sphère terrestre entre un point de
	 * coordonnées géographiques (longitude1,latitude1) et un autre de coordonnées
	 * géographiques (longitude2,latitude2).
	 */
	public static int distanceEuclidienne(final double longitude1, final double latitude1, final double longitude2,
			final double latitude2) {
		return Functions.getPane().main.distanceEuclidienne(longitude1, latitude1, longitude2, latitude2);
	}

	/** Efface la carte. */
	public static void effaceCarte() {
		Functions.getPane().main.clearMap();
	}

	/** Table de toutes les villes. */
	public static Set<String> villes;
	/** Table des latitudes associée à chaque nom de ville. */
	public static Map<String, Double> latitudes;
	/** Table des longitudes associée à chaque nom de ville. */
	public static Map<String, Double> longitudes;
	/** Table des voisins de chaque ville. */
	public static Map<String, List<String>> voisins;

	/**
	 * Calcule un chemin sous forme d'une liste de noms de ville afin de relier la
	 * ville de nom depart à celle de nom arrivee en suivant uniquement des routes
	 * de la table voisins de la proglet.
	 */
	public static List<String> plusCourtCheminGogleMap(final String depart, final String arrivee) {
		return GogleMapCalculChemins.plusCourtChemin(Functions.getPane().main, depart, arrivee);
	}

	/** Parcours en largeur de la carte à partir d'un point de départ. */
	public static void parcoursEnLargeur(final String depart) {
		SwingUtilities.invokeLater(() -> {
			Functions.getPane().main.clearMap();
			GogleMapParcours.afficheToutesRoutesDirectes(Functions.getPane().main);
			GogleMapParcours.parcoursLargeur(Functions.getPane().main, depart);
		});
	}
}
