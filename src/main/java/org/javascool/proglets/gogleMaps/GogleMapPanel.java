/*******************************************************************************
* David.Pichardie@inria.fr, Copyright (C) 2011.           All rights reserved. *
*******************************************************************************/

package org.javascool.proglets.gogleMaps;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Line2D;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.SwingWorker;

import org.javascool.macros.Macros;

class GogleMapPanel extends JPanel implements ActionListener {
	private static final long serialVersionUID = 1L;

	Map<String, List<String>> arcs;
	Map<String, Double> latitudes;
	Map<String, Double> longitudes;
	private Image ici_bleu;
	private Image ici_rouge;
	private Image france;
	private final Set<PointAAfficher> pointsAfficheAvecNumero;
	private final Set<PointAAfficher> pointsAfficheSansNumero;
	private final Set<ArcAAfficher> arcsAffiche;
	private CartePanel carte;
	static private String buttonDFSString = "Parcours en profondeur";
	static private String buttonBFSString = "Parcours en largeur";
	private final JButton buttonDFS;
	private final JButton buttonBFS;
	private final GogleMapPanel me = this;

	void clearMap() {
		this.pointsAfficheAvecNumero.clear();
		this.pointsAfficheSansNumero.clear();
		this.arcsAffiche.clear();
		this.carte.repaint();
	}

	private void drawRoad(final Graphics g, final double longitude1, final double latitude1, final double longitude2,
			final double latitude2) {
		final int xi = GogleMapPanel.getX(longitude1) + 9;
		final int yi = GogleMapPanel.getY(latitude1) + 29;
		final int xj = GogleMapPanel.getX(longitude2) + 9;
		final int yj = GogleMapPanel.getY(latitude2) + 29;
		final Graphics2D g2 = (Graphics2D) g;
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g2.setStroke(new BasicStroke(6));
		g2.draw(new Line2D.Double(xi, yi, xj, yj));
	}

	void drawPoint(final Graphics2D g, final int x, final int y, final int indice) {
		// System.out.println("x ="+x+" y="+y+" i="+indice);
		g.drawImage(indice != -1 ? this.ici_bleu : this.ici_rouge, x, y, null);
		if (indice != -1 && indice < 10) {
			g.drawString("" + indice, x + 7, y + 13);
		} else if (indice != -1) {
			g.drawString("" + indice, x + 4, y + 13);
		}
		// g.setColor(Color.BLACK);
		// g.fillRect(x+5, y+25, 12,12);
	}

	public void affichePoint(final double longitude, final double latitude, final int idx) {
		this.pointsAfficheAvecNumero.add(new PointAAfficher(longitude, latitude, idx));
		this.carte.repaint();
	}

	public void affichePoint(final double longitude, final double latitude) {
		this.pointsAfficheSansNumero.add(new PointAAfficher(longitude, latitude, -1));
		this.carte.repaint();
	}

	// @param intensite: entier entre 1 et 5 pour l'intensite du tracé
	public void afficheRoute(final double longitude1, final double latitude1, final double longitude2,
			final double latitude2, final int intensite) {
		this.arcsAffiche.add(new ArcAAfficher(longitude1, latitude1, longitude2, latitude2, intensite));
		this.carte.repaint();
	}

	public void afficheRoute(final double longitude1, final double latitude1, final double longitude2,
			final double latitude2) {
		this.arcsAffiche.add(new ArcAAfficher(longitude1, latitude1, longitude2, latitude2, 2));
		this.carte.repaint();
	}

	static int getX_Icon(final double d) {
		return GogleMapPanel.getX(d);
	}

	static int getX(final double d) {
		return GogleMapPanel.inv_lin_x(d);
	}

	static int getY_Icon(final double d) {
		return GogleMapPanel.getY(d);
	}

	static int getY(final double d) {
		return GogleMapPanel.inv_lin_y(d);
	}

	static int scaleX(final int x) {
		return (int) Math.round(.981 * x) - 9;
	}

	static int scaleY(final int y) {
		return (int) Math.round(.98 * y) - 3;
	}

	static double linTransf(final int x, final int x0, final int x1, final double y0, final double y1) {
		// y - y0 = (y1-y0)/(x1-x0) * (x - x0)
		return y0 + (y1 - y0) / ((double) x1 - (double) x0) * ((double) x - (double) x0);
	}

	static double errTransf(final int err, final int x0, final int x1, final double y0, final double y1) {
		return (y1 - y0) / ((double) x1 - (double) x0) * err;
	}

	static int invLinTransf(final double x, final double x0, final double x1, final int y0, final int y1) {
		// y - y0 = (y1-y0)/(x1-x0) * (x - x0)
		return (int) Math.round(y0 + (y1 - y0) / (x1 - x0) * (x - x0));
	}

	static double lin_y(final int latitude) {
		return GogleMapPanel.linTransf(latitude, 192, 179, 48.392168, 48.586601);
	}

	static double lin_x(final int longitude) {
		return GogleMapPanel.linTransf(longitude, 6, 546, -4.486885, 7.745018);
	}

	static double err_y(final int err) {
		return GogleMapPanel.linTransf(err, 192, 179, 48.392168, 48.586601);
	}

	static double err_x(final int err) {
		return GogleMapPanel.linTransf(err, 6, 546, -4.486885, 7.745018);
	}

	static int inv_lin_y(final double latitude) {
		return GogleMapPanel.invLinTransf(latitude, 48.392168, 48.586601, 192, 179);
	}

	static int inv_lin_x(final double longitude) {
		return GogleMapPanel.invLinTransf(longitude, -4.486885, 7.745018, 6, 546);
	}

	static double square(final double x) {
		return x * x;
	}

	int distanceEuclidienne(final double longitude1, final double latitude1, final double longitude2,
			final double latitude2) {
		final double longitude = (longitude1 - longitude2) * Math.PI / 180;
		double aux = Math.cos(latitude1 * Math.PI / 180) * Math.cos(latitude2 * Math.PI / 180) * Math.cos(longitude);
		aux = aux + Math.sin(latitude1 * Math.PI / 180) * Math.sin(latitude2 * Math.PI / 180);
		return (int) Math.round(6378 * Math.acos(aux));
	}

	final void ajoute(final int i, final String ville, final double _latitude, final double _longitude) {
		this.latitudes.put(ville, _latitude);
		this.longitudes.put(ville, _longitude);
	}

	private class ParcoursEnLargeur extends SwingWorker<Void, Void> {
		@Override
		protected Void doInBackground() {
			GogleMapPanel.this.me.clearMap();
			GogleMapParcours.afficheToutesRoutesDirectes(GogleMapPanel.this.me);
			GogleMapParcours.parcoursLargeur(GogleMapPanel.this.me, "Paris");
			GogleMapPanel.this.me.buttonBFS.setEnabled(true);
			GogleMapPanel.this.me.buttonDFS.setEnabled(true);
			return null;
		}
	}

	private class ParcoursEnProfondeur extends SwingWorker<Void, Void> {
		@Override
		protected Void doInBackground() {
			GogleMapPanel.this.me.clearMap();
			GogleMapParcours.afficheToutesRoutesDirectes(GogleMapPanel.this.me);
			GogleMapParcours.parcoursProfondeur(GogleMapPanel.this.me, "Paris");
			GogleMapPanel.this.me.buttonBFS.setEnabled(true);
			GogleMapPanel.this.me.buttonDFS.setEnabled(true);
			return null;
		}
	}

	@Override
	public void actionPerformed(final ActionEvent e) {
		final String action = e.getActionCommand();
		if (action.equals(GogleMapPanel.buttonBFSString)) {
			this.buttonBFS.setEnabled(false);
			this.buttonDFS.setEnabled(false);
			new ParcoursEnLargeur().execute();
		} else if (action.equals(GogleMapPanel.buttonDFSString)) {
			this.buttonBFS.setEnabled(false);
			this.buttonDFS.setEnabled(false);
			new ParcoursEnProfondeur().execute();
		}
	}

	GogleMapPanel() {
		super(new BorderLayout());

		this.buttonDFS = new JButton(GogleMapPanel.buttonDFSString);
		this.buttonBFS = new JButton(GogleMapPanel.buttonBFSString);
		this.buttonDFS.setActionCommand(GogleMapPanel.buttonDFSString);
		this.buttonBFS.setActionCommand(GogleMapPanel.buttonBFSString);
		this.buttonDFS.addActionListener(this);
		this.buttonBFS.addActionListener(this);
		final JPanel groupBoutons = new JPanel(new GridLayout(1, 0));
		groupBoutons.add(this.buttonDFS);
		groupBoutons.add(this.buttonBFS);
		this.add(this.carte = new CartePanel(), BorderLayout.CENTER);
		this.add(groupBoutons, BorderLayout.NORTH);

		try {
			this.ici_bleu = Macros.getIcon("org/javascool/proglets/gogleMaps/ici_bleu.png").getImage();
			this.ici_rouge = Macros.getIcon("org/javascool/proglets/gogleMaps/ici_rouge.png").getImage();
			this.france = Macros.getIcon("org/javascool/proglets/gogleMaps/carteDeFrance.png").getImage();
		} catch (final Exception e) {
			System.out.println("Erreur au read : " + e);
		}
		this.latitudes = new HashMap<>();
		this.longitudes = new HashMap<>();

		this.ajoute(0, "Dunkerque", 51.069360, 2.376571);
		this.ajoute(1, "Calais", 50.979622, 1.855583);
		this.ajoute(2, "Lille", 50.650582, 3.056121);
		this.ajoute(3, "Béthune", 50.545887, 2.648391);
		this.ajoute(4, "Lens", 50.381367, 3.056121);
		this.ajoute(5, "Valenciennes", 50.366410, 3.531806);
		this.ajoute(6, "Amiens", 49.887806, 2.308616);
		this.ajoute(7, "Le Havre", 49.483984, 0.134056);
		this.ajoute(8, "Rouen", 49.439114, 1.108078);
		this.ajoute(9, "Reims", 49.259638, 4.007492);
		this.ajoute(10, "Thionville", 49.364333, 6.182052);
		this.ajoute(11, "Metz", 49.110074, 6.182052);
		this.ajoute(12, "Strasbourg", 48.586601, 7.745017);
		this.ajoute(13, "Nancy", 48.691295, 6.204704);
		this.ajoute(14, "Paris", 48.855815, 2.353920);
		this.ajoute(15, "Caen", 49.169900, -0.386932);
		this.ajoute(16, "Troyes", 48.287473, 4.052795);
		this.ajoute(17, "Brest", 48.392168, -4.486885);
		this.ajoute(18, "Lorient", 47.749043, -3.376953);
		this.ajoute(19, "Rennes", 48.107996, -1.678077);
		this.ajoute(20, "Le Mans", 48.003301, 0.202011);
		this.ajoute(21, "Orléans", 47.913563, 1.900886);
		this.ajoute(22, "Tours", 47.405046, 0.700347);
		this.ajoute(23, "Angers", 47.479828, -0.568145);
		this.ajoute(24, "Nantes", 47.240526, -1.564819);
		this.ajoute(25, "Saint-Nazaire", 47.285395, -2.199066);
		this.ajoute(26, "Dijon", 47.330264, 5.049469);
		this.ajoute(27, "Mulhouse", 47.763999, 7.337287);
		this.ajoute(28, "Montbéliard", 47.509741, 6.793647);
		this.ajoute(29, "Besançon", 47.270439, 6.023490);
		this.ajoute(30, "Annemasse", 46.268361, 6.227355);
		this.ajoute(31, "Annecy", 45.969233, 6.159400);
		this.ajoute(32, "Chambéry", 45.670105, 5.932884);
		this.ajoute(33, "Grenoble", 45.296196, 5.706367);
		this.ajoute(34, "Lyon", 45.834626, 4.800300);
		this.ajoute(35, "Saint-Etienne", 45.550454, 4.369918);
		this.ajoute(36, "Valence", 45.071850, 4.890907);
		this.ajoute(37, "Nice", 43.950121, 7.269332);
		this.ajoute(38, "Toulon", 43.426648, 5.932884);
		this.ajoute(39, "Marseille", 43.591168, 5.343940);
		this.ajoute(40, "Avigon", 44.174467, 4.822952);
		this.ajoute(41, "Nîmes", 44.084729, 4.347267);
		this.ajoute(42, "Montpellier", 43.860383, 3.871582);
		this.ajoute(43, "Perpignan", 43.037782, 2.874908);
		this.ajoute(44, "Toulouse", 43.860383, 1.425201);
		this.ajoute(45, "Pau", 43.591168, -0.364280);
		this.ajoute(46, "Bayonne", 43.755688, -1.496864);
		this.ajoute(47, "Bordeaux", 44.997068, -0.593449);
		this.ajoute(48, "Clermont-Ferrand", 45.879495, 3.078773);
		this.ajoute(49, "Limoges", 45.909408, 1.243988);
		this.ajoute(50, "Angoulême", 45.744887, 0.156707);
		this.ajoute(51, "La Rochelle", 46.238448, -1.157089);
		this.ajoute(52, "Poitiers", 46.627314, 0.315269);

		this.arcs = new HashMap<>();
		for (final String ville : this.latitudes.keySet()) {
			this.arcs.put(ville, new ArrayList<String>());
		}
		this.ajouteArc("Brest", "Lorient");
		this.ajouteArc("Brest", "Rennes");
		this.ajouteArc("Lorient", "Rennes");
		this.ajouteArc("Rennes", "Nantes");
		this.ajouteArc("Nantes", "Saint-Nazaire");
		this.ajouteArc("Rennes", "Le Mans");
		this.ajouteArc("Le Mans", "Paris");
		this.ajouteArc("Paris", "Orléans");
		this.ajouteArc("Le Mans", "Tours");
		this.ajouteArc("Orléans", "Limoges");
		this.ajouteArc("Le Mans", "Angers");
		this.ajouteArc("Nantes", "La Rochelle");
		this.ajouteArc("La Rochelle", "Angoulême");
		this.ajouteArc("Nantes", "Angoulême");
		this.ajouteArc("Angers", "Nantes");
		this.ajouteArc("Poitiers", "Angoulême");
		this.ajouteArc("Tours", "Poitiers");
		this.ajouteArc("Angoulême", "Bordeaux");
		this.ajouteArc("Bordeaux", "Bayonne");
		this.ajouteArc("Bayonne", "Pau");
		this.ajouteArc("Pau", "Toulouse");
		this.ajouteArc("Bordeaux", "Toulouse");
		this.ajouteArc("Toulouse", "Perpignan");
		this.ajouteArc("Toulouse", "Montpellier");
		this.ajouteArc("Montpellier", "Nîmes");
		this.ajouteArc("Nîmes", "Avigon");
		this.ajouteArc("Avigon", "Marseille");
		this.ajouteArc("Marseille", "Toulon");
		this.ajouteArc("Toulon", "Nice");
		this.ajouteArc("Avigon", "Valence");
		this.ajouteArc("Valence", "Grenoble");
		this.ajouteArc("Grenoble", "Chambéry");
		this.ajouteArc("Chambéry", "Annecy");
		this.ajouteArc("Annecy", "Annemasse");
		this.ajouteArc("Valence", "Saint-Etienne");
		this.ajouteArc("Lyon", "Saint-Etienne");
		this.ajouteArc("Lyon", "Grenoble");
		this.ajouteArc("Clermont-Ferrand", "Saint-Etienne");
		this.ajouteArc("Clermont-Ferrand", "Limoges");
		this.ajouteArc("Limoges", "Angoulême");
		this.ajouteArc("Paris", "Troyes");
		this.ajouteArc("Troyes", "Dijon");
		this.ajouteArc("Dijon", "Besançon");
		this.ajouteArc("Dijon", "Lyon");
		this.ajouteArc("Besançon", "Montbéliard");
		this.ajouteArc("Montbéliard", "Mulhouse");
		this.ajouteArc("Mulhouse", "Strasbourg");
		this.ajouteArc("Strasbourg", "Nancy");
		this.ajouteArc("Nancy", "Paris");
		this.ajouteArc("Troyes", "Nancy");
		this.ajouteArc("Nancy", "Metz");
		this.ajouteArc("Metz", "Thionville");
		this.ajouteArc("Metz", "Reims");
		this.ajouteArc("Reims", "Paris");
		this.ajouteArc("Paris", "Rouen");
		this.ajouteArc("Rouen", "Le Havre");
		this.ajouteArc("Caen", "Rennes");
		this.ajouteArc("Rouen", "Caen");
		this.ajouteArc("Calais", "Dunkerque");
		this.ajouteArc("Dunkerque", "Béthune");
		this.ajouteArc("Lille", "Béthune");
		this.ajouteArc("Béthune", "Lens");
		this.ajouteArc("Lens", "Valenciennes");
		this.ajouteArc("Lens", "Lille");
		this.ajouteArc("Lens", "Paris");
		this.ajouteArc("Amiens", "Paris");
		this.ajouteArc("Amiens", "Lens");
		this.ajouteArc("Reims", "Lens");
		this.ajouteArc("Lens", "Lille");

		this.pointsAfficheAvecNumero = new TreeSet<>();
		this.pointsAfficheSansNumero = new TreeSet<>();
		this.arcsAffiche = new HashSet<>();
	}
	// int searchPoint(double x, double y) {
	// int error = 6;
	// for (int i=0; i<nb_villes; i++)
	// if (Math.abs(inv_lin_x(longitude[i])-x)<=error &&
	// Math.abs(inv_lin_y(latitude[i])-y)<=error)
	// return i;
	// return -1;
	// }

	final void ajouteArc(final String depart, final String arrivee) {
		this.ajouteArcAux(depart, arrivee);
		this.ajouteArcAux(arrivee, depart);
	}

	void ajouteArcAux(final String depart, final String arrivee) {
		this.arcs.get(depart).add(arrivee);
	}

	class CartePanel extends JPanel {
		private static final long serialVersionUID = 1L;

		CartePanel() {
			this.setPreferredSize(new Dimension(640, 640));
		}

		@Override
		protected void paintComponent(final Graphics g) {
			super.paintComponent(g);
			g.drawImage(GogleMapPanel.this.france, 0, 0, null);
			for (final ArcAAfficher a : GogleMapPanel.this.arcsAffiche) {
				g.setColor(new Color(1.f, 0.f, 0.f, a.intensite * .3f));
				GogleMapPanel.this.drawRoad(g, a.longitude1, a.latitude1, a.longitude2, a.latitude2);
			}
			final int screenRes = Toolkit.getDefaultToolkit().getScreenResolution();
			final int fontSize = (int) Math.round(10.0 * screenRes / 72.0);
			final Font font = new Font("Arial", Font.BOLD, fontSize);
			final Graphics2D g2d = (Graphics2D) g;
			g2d.setFont(font);
			g2d.setColor(Color.WHITE);
			g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
			for (final PointAAfficher p : GogleMapPanel.this.pointsAfficheSansNumero) {
				GogleMapPanel.this.drawPoint(g2d, GogleMapPanel.getX(p.x), GogleMapPanel.getY(p.y), p.idx);
			}
			for (final PointAAfficher p : GogleMapPanel.this.pointsAfficheAvecNumero) {
				GogleMapPanel.this.drawPoint(g2d, GogleMapPanel.getX(p.x), GogleMapPanel.getY(p.y), p.idx);
			}
		}
	}
}

class PointAAfficher implements Comparable<PointAAfficher> {
	double x;
	double y;
	int idx;

	PointAAfficher(final double _x, final double _y, final int _idx) {
		this.x = _x;
		this.y = _y;
		this.idx = _idx;
	}

	@Override
	public int compareTo(final PointAAfficher o) {
		final PointAAfficher p = o; // (PointAAfficher) o;
		final int cmp1 = (int) Math.round(1000 * (p.x - this.x));
		if (cmp1 != 0) {
			return cmp1;
		} else {
			return (int) Math.round(1000 * (p.y - this.y));
		}
	}
}

class ArcAAfficher {
	double longitude1;
	double latitude1;
	double longitude2;
	double latitude2;
	int intensite;

	ArcAAfficher(final double _longitude1, final double _latitude1, final double _longitude2, final double _latitude2,
			final int _intensite) {
		this.longitude1 = _longitude1;
		this.latitude1 = _latitude1;
		this.longitude2 = _longitude2;
		this.latitude2 = _latitude2;
		this.intensite = _intensite;
	}
}
