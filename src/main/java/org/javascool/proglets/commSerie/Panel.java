package org.javascool.proglets.commSerie;

import java.awt.BorderLayout;

import javax.swing.JPanel;

/**
 * Définit une proglet javascool qui permet d'utiliser toute les classes des
 * swings.
 *
 * @see <a href="Panel.java.html">code source</a>
 * @serial exclude
 */
public class Panel extends JPanel {
	private static final long serialVersionUID = 1L;

	SerialInterfacePanel spanel;
	SerialInterface serial;

	public Panel() {
		this.setLayout(new BorderLayout());
		this.removeAll();
	}

	public void removeAll(final String displayMode) {
		super.removeAll();
		if (displayMode.length() > 0) {
			this.add(this.spanel = new SerialInterfacePanel(displayMode), BorderLayout.NORTH);
		}
		this.serial = this.spanel.getSerialInterface();
	}

	@Override
	public void removeAll() {
		this.removeAll("CD");
	}

}
