package org.javascool.proglets.commSerie;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import lol.javasnice.I18N;

/**
 * Interface graphique: actuellement un convertisseur décimal en héxadécimal
 **/
public class ConvertisseurPanel extends JPanel {
	private static final long serialVersionUID = 1L;
	// Pour saisir la chaîne de caractères
	private final JTextField enASCII;

	// Pour afficher les codes ASCII de la chaîne de caractères
	private final JTextField enHEx;

	// Le constructeur
	public ConvertisseurPanel() {
		this.setLayout(new BorderLayout());

		// Incrustation du nom dans le bas de la fenêtre
		final String texte = "<html><b><font color=\"#0000FF\">   "
				+ I18N.gettext("Converter ASCII->hexadecimal")
				+ "  </font></b></html>";
		final JLabel lblAuteur = new JLabel(texte, SwingConstants.RIGHT);
		this.add("South", lblAuteur);

		/*** Panel des zones de texte **/

		final JPanel panelHaut = new JPanel();
		panelHaut.setLayout(new BoxLayout(panelHaut, BoxLayout.Y_AXIS));
		this.add("Center", panelHaut);

		final JPanel panelDonnees = new JPanel();
		panelDonnees.setLayout(new BoxLayout(panelDonnees, BoxLayout.Y_AXIS));
		panelDonnees.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0),
				BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), I18N.gettext("Input a string "))));
		panelHaut.add(panelDonnees);

		/*** La saisie de la chaîne de caractères **/

		final JPanel panelASCII = new JPanel(new FlowLayout());
		panelDonnees.add(panelASCII);

		final JLabel lblChaine = new JLabel(I18N.gettext("String: "));
		panelASCII.add(lblChaine);

		this.enASCII = new JTextField();
		lblChaine.setLabelFor(this.enASCII);

		// les propriétés de la zone de saisie
		this.enASCII.setEditable(true); // pour la saisie
		this.enASCII.setBackground(Color.black);
		this.enASCII.setForeground(Color.green);
		this.enASCII.setHorizontalAlignment(SwingConstants.CENTER);
		this.enASCII.setPreferredSize(new Dimension(250, 35));
		panelASCII.add(this.enASCII);

		/*** L'affichage des codes ASCII de la chaîne de caractères **/

		final JPanel panelHEX = new JPanel(new FlowLayout());
		panelDonnees.add(panelHEX);

		final JLabel lblHEX = new JLabel(I18N.gettext(" ASCII Codes                    : "));
		panelHEX.add(lblHEX);

		this.enHEx = new JTextField();
		lblHEX.setLabelFor(this.enHEx);

		// les propriétés de la zone d'affichage
		this.enHEx.setEditable(false);
		this.enHEx.setBackground(Color.cyan);
		this.enHEx.setForeground(Color.blue);
		this.enHEx.setHorizontalAlignment(SwingConstants.CENTER);
		this.enHEx.setPreferredSize(new Dimension(250, 35));
		panelHEX.add(this.enHEx);

		final Font font1 = new Font("taille1", Font.BOLD, 16);
		this.enASCII.setFont(font1);

		final Font font2 = new Font("taille2", Font.BOLD, 14);
		this.enHEx.setFont(font2);

		/*** Panel des Boutons **/

		final JPanel panelBoutons = new JPanel();
		panelBoutons.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0),
				BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "")));
		panelHaut.add(panelBoutons);

		// Bouton EFFACER
		final JButton cmdEffacer = new JButton(I18N.gettext("Clear"));
		cmdEffacer.setPreferredSize(new Dimension(130, 30));
		cmdEffacer.addActionListener(evt -> ConvertisseurPanel.this.effacer());
		panelBoutons.add(cmdEffacer);

		// Bouton CONVERTIR
		final JButton cmdConvertir = new JButton(I18N.gettext("Convert"));
		cmdConvertir.setPreferredSize(new Dimension(130, 30));
		cmdConvertir.addActionListener(evt -> ConvertisseurPanel.this.convertir());
		panelBoutons.add(cmdConvertir);
	}

	/*** convertit un tableau d'entiers en chaîne de codes Hexa (code ASCII) **/

	static String byteArrayToHexString(final byte[] bArray) {
		final StringBuffer buffer = new StringBuffer();
		for (final byte b : bArray) {
			buffer.append(Integer.toHexString(b));
			buffer.append(" ");
		}
		return buffer.toString().toUpperCase();
	}

	/*** Assure la conversion ASCII des caractères saisis **/

	private void convertir() {
		final String chaine = this.enASCII.getText();

		// traduction en tableau de code ASCII :
		final byte[] bytes = chaine.getBytes();

		this.enHEx.setText(ConvertisseurPanel.byteArrayToHexString(bytes)); // on affiche les codes ASCII
	}

	// Pour effacer les champs d'affichage
	private void effacer() {
		this.enHEx.setText("");
		this.enASCII.setText("");
	}

	/**
	 * Ouvre le panel en application.
	 * 
	 * @param usage <tt>java -cp javascool-proglets.jar org.javascool.proglets.commSerie.ConvertisseurPanel</tt>
	 */
	public static void main(final String[] usage) {
		new org.javascool.widgets.MainFrame().reset(I18N.gettext("Hex ASCII codes of string"), 600, 300,
				new ConvertisseurPanel());
	}
} // fin Panel()
