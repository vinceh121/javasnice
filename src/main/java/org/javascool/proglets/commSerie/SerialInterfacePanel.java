package org.javascool.proglets.commSerie;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;

import lol.javasnice.I18N;

/**
 * Définit un panneau graphique permettant de piloter une interface série.
 * <p>
 * <img src=
 * "http://javascool.gforge.inria.fr/documents/sketchbook/commSerie/screenshot.png"
 * alt="screenshot">
 * </p>
 *
 * @see <a href="SerialInterfacePanel.java.html">source code</a>
 * @serial exclude
 */

public class SerialInterfacePanel extends JPanel {
	private static final long serialVersionUID = 1L;

	/**
	 * Construit un panneau de contôle pour l'interface série donné.
	 * 
	 * @param serialInterface Interface série à piloter. Si null, crée une interface
	 *                        série.
	 * @param displayMode     Précise si:
	 *                        <ul>
	 *                        <li>"C" : le panneau de contrôle des paramètres et
	 *                        d'ouverture/fermeture du port est affiché</li>
	 *                        <li>"D" : le panneau de dialogue entrée/sortie avec le
	 *                        port est affiché</li>
	 *                        <li>"CD" : les deux panneaux sont affichés
	 *                        (défaut)</li>
	 *                        <li>"" : rien n'est affiché.</li>
	 *                        </ul>
	 */
	public SerialInterfacePanel(final SerialInterface serialInterface, final String displayMode) {
		this.serial = serialInterface == null ? new SerialInterface() : serialInterface;
		this.setBorder(BorderFactory.createTitledBorder(I18N.gettext("Serial port controller")));
		this.setLayout(new BorderLayout());
		if (0 <= displayMode.indexOf("C")) {
			this.add(new JPanel() {
				private static final long serialVersionUID = 1L;
				{
					this.add(new JComboBox/* !<String> */<Object>(SerialInterface.getPortNames()) {
						private static final long serialVersionUID = 1L;
						{
							this.setBorder(BorderFactory.createTitledBorder(I18N.gettext("Port name")));
							this.setPreferredSize(new Dimension(100, 70));
							this.addActionListener(e -> SerialInterfacePanel.this.serial
									.setName((String) ((JComboBox<?>) e.getSource()).getSelectedItem()));
						}
					});
					this.add(new JComboBox/* !<Integer> */<Object>(
							new Integer[] { 19200, 9600, 4800, 2400, 1200, 600, 300 }) {
						private static final long serialVersionUID = 1L;
						{
							this.setBorder(BorderFactory.createTitledBorder(I18N.gettext("Baud rate (b/s)")));
							this.setPreferredSize(new Dimension(100, 70));
							this.addActionListener(e -> SerialInterfacePanel.this.serial
									.setRate((Integer) ((JComboBox<?>) e.getSource()).getSelectedItem()));
						}
					});
					this.add(new JComboBox/* !<String> */<Object>(
							new String[] { I18N.gettext("aucun"), I18N.gettext("pair"), I18N.gettext("impair") }) {
						private static final long serialVersionUID = 1L;
						{
							this.setBorder(BorderFactory.createTitledBorder(I18N.gettext("Parity bit")));
							this.setPreferredSize(new Dimension(100, 70));
							this.addActionListener(e -> {
								final String v = (String) ((JComboBox<?>) e.getSource()).getSelectedItem();
								SerialInterfacePanel.this.serial.setParity(I18N.gettext("pair").equals(v) ? 'E'
										: I18N.gettext("impair").equals(v) ? 'O' : 'N');
							});
						}
					});
					this.add(new JComboBox/* !<Integer> */<Object>(new Integer[] { 8, 7 }) {
						private static final long serialVersionUID = 1L;
						{
							this.setBorder(BorderFactory.createTitledBorder(I18N.gettext("Word length")));
							this.setPreferredSize(new Dimension(100, 70));
							this.addActionListener(e -> SerialInterfacePanel.this.serial
									.setSize((Integer) ((JComboBox<?>) e.getSource()).getSelectedItem()));
						}
					});
					this.add(new JComboBox/* !<Double> */<Object>(new Double[] { 1.0, 1.5, 2.0 }) {
						private static final long serialVersionUID = 1L;
						{
							this.setBorder(BorderFactory.createTitledBorder(I18N.gettext("Stop bits")));
							this.setPreferredSize(new Dimension(100, 70));
							this.addActionListener(e -> SerialInterfacePanel.this.serial
									.setStop((Double) ((JComboBox<?>) e.getSource()).getSelectedItem()));
						}
					});
					this.add(new JButton() {
						private static final long serialVersionUID = 1L;
						{
							final String open = I18N.gettext("OPEN"), close = I18N.gettext("CLOSE");
							this.setBorder(BorderFactory.createTitledBorder(I18N.gettext("OPEN∕CLOSE")));
							this.setPreferredSize(new Dimension(100, 70));
							this.setText(open);
							this.addActionListener(e -> {
								final JButton b = (JButton) e.getSource();
								if (open.equals(b.getText())) {
									b.setText(close);
									System.out.println(I18N.ngettext("Opening serial interface : {0}",
											SerialInterfacePanel.this.serial));
									SerialInterfacePanel.this.serial.open();
								} else {
									b.setText(open);
									SerialInterfacePanel.this.serial.close();
								}
							});
						}
					});
				}
			}, BorderLayout.NORTH);
		}
		if (0 <= displayMode.indexOf("D")) {
			this.add(new Box(BoxLayout.X_AXIS) {
				private static final long serialVersionUID = 1L;
				{
					this.add(new Box(BoxLayout.Y_AXIS) {
						private static final long serialVersionUID = 1L;
						{
							this.add(new JScrollPane(SerialInterfacePanel.this.writeChar = new JTextArea(1, 8) {
								private static final long serialVersionUID = 1L;
								{
									this.addKeyListener(new KeyListener() {
										@Override
										public void keyPressed(final KeyEvent e) {
										}

										@Override
										public void keyReleased(final KeyEvent e) {
										}

										@Override
										public void keyTyped(final KeyEvent e) {
											final char c = e.getKeyChar();
											SerialInterfacePanel.this.external = false;
											SerialInterfacePanel.this.serial.write(c);
											SerialInterfacePanel.this.external = true;
										}
									});
								}
							}, ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER,
									ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS) {
								private static final long serialVersionUID = 1L;
								{
									this.setBorder(BorderFactory.createTitledBorder(I18N.gettext("Send a character:")));
								}
							});
							this.add(new JScrollPane(SerialInterfacePanel.this.writeHexa = new JTextArea(1, 8) {
								private static final long serialVersionUID = 1L;
								{
									this.setBackground(new Color(200, 200, 200));
									this.setEditable(false);
								}
							}, ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER,
									ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS) {
								private static final long serialVersionUID = 1L;
								{
									this.setBorder(
											BorderFactory.createTitledBorder(I18N.gettext("Character's ASCII code:")));
								}
							});
							this.add(new JButton(I18N.gettext("Clear")) {
								private static final long serialVersionUID = 1L;
								{
									this.addActionListener(e -> {
										SerialInterfacePanel.this.writeChar.setText("");
										SerialInterfacePanel.this.writeHexa.setText("");
									});
								}
							});
						}
					});
					this.add(new Box(BoxLayout.Y_AXIS) {
						private static final long serialVersionUID = 1L;
						{
							this.add(new JScrollPane(SerialInterfacePanel.this.readChar = new JTextArea(1, 8) {
								private static final long serialVersionUID = 1L;
								{
									this.setBackground(new Color(200, 200, 200));
									this.setEditable(false);
								}
							}, ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER,
									ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS) {
								private static final long serialVersionUID = 1L;
								{
									this.setBorder(
											BorderFactory.createTitledBorder(I18N.gettext("Recieved character:")));
								}
							});
							this.add(new JScrollPane(SerialInterfacePanel.this.readHexa = new JTextArea(1, 8) {
								private static final long serialVersionUID = 1L;
								{
									this.setBackground(new Color(200, 200, 200));
									this.setEditable(false);
								}
							}, ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER,
									ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS) {
								private static final long serialVersionUID = 1L;
								{

									this.setBorder(
											BorderFactory.createTitledBorder(I18N.gettext("Character's ASCII code:")));
								}
							});
							this.add(new JButton(I18N.gettext("Clear")) {
								private static final long serialVersionUID = 1L;
								{
									this.addActionListener(e -> {
										SerialInterfacePanel.this.readChar.setText("");
										SerialInterfacePanel.this.readHexa.setText("");
									});
								}
							});
						}
					});
				}
			}, BorderLayout.CENTER);
			this.serial.setReader(c -> {
				SerialInterfacePanel.this.readChar.setText(SerialInterfacePanel.this.readChar.getText() + (char) c);
				SerialInterfacePanel.this.readHexa
						.setText(SerialInterfacePanel.this.readHexa.getText() + " " + Integer.toString(c, 16));
			});
			this.serial.setWriter(c -> {
				if (SerialInterfacePanel.this.external) {
					SerialInterfacePanel.this.writeChar
							.setText(SerialInterfacePanel.this.writeChar.getText() + (char) c);
				}
				SerialInterfacePanel.this.writeHexa
						.setText(SerialInterfacePanel.this.writeHexa.getText() + " " + Integer.toString(c, 16));
			});
		}

		// Permet d'afficher les messages de la console dans l'interface.
		{
			final JPanel c = org.javascool.widgets.Console.newInstance();
			c.setPreferredSize(new Dimension(600, 200));
			this.add(c, BorderLayout.SOUTH);
		}
	}

	/**
	 * @see#SerialInterfacePanel(SerialInterface,String)
	 */

	public SerialInterfacePanel(final SerialInterface serial) {
		this(serial, "CD");
	}

	/**
	 *
	 * 
	 * @see #SerialInterfacePanel(SerialInterface, String)
	 */

	public SerialInterfacePanel(final String displayMode) {
		this(null, displayMode);
	}

	/**
	 * @see #SerialInterfacePanel(SerialInterface, String)
	 */
	public SerialInterfacePanel() {
		this(null, "CD");
	}

	private final SerialInterface serial;
	private JTextArea writeChar, writeHexa, readChar, readHexa;
	private boolean external = true;

	/** Renvoie l'interface série, pour pouvoir accéder à ses fonctions. */
	public SerialInterface getSerialInterface() {
		return this.serial;
	}

	/**
	 * Renvoie la liste des des noms de ports séries disponibles ce qui teste
	 * l'installation des librairies.
	 * 
	 * @param usage <tt>java -cp javascool-proglets.jar org.javascool.proglets.commSerie.SerialInterfacePanel</tt>
	 */
	public static void main(final String[] usage) {
		new org.javascool.widgets.MainFrame().reset("Serial interface", 800, 600, new SerialInterfacePanel());
	}
}
