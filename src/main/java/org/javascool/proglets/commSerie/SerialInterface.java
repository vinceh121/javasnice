package org.javascool.proglets.commSerie;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;

import com.fazecast.jSerialComm.SerialPort;
import com.fazecast.jSerialComm.SerialPortDataListener;
import com.fazecast.jSerialComm.SerialPortEvent;

import lol.javasnice.I18N;

/**
 * Adaptation for Javasnice of the PSerial - class for serial port goodness Part
 * of the Processing project - http://processing.org Copyright (c) 2004-05 Ben
 * Fry & Casey Reas Définit les primitives pour s'interfacer avec un port série.
 * <p>
 * Les erreurs d'entrée-sortie sont affichée dans la console
 * (<tt>System.out</tt>).
 *
 * @see <a href="SerialInterface.java.html">code source</a>
 * @serial exclude
 */
public class SerialInterface {
	private String name = "COM1";

	private int rate = 9600;

	private char parity = 'N';

	private int size = 8;

	private double stop = 1;

	private SerialPort port = null;

	private OutputStream output = null;

	private InputStream input = null;

	private Reader reader = null;

	private Writer writer = null;

	private final ArrayList<Integer> data = new ArrayList<>();

	private SerialPortDataListener listener = null;

	/**
	 * Définit le nom du port série.
	 * 
	 * @param name Le nom du port, "COM1" par défaut.
	 * @return Cet objet permettant la construction
	 *         <tt>new SerialInterface().setName(..</tt>.
	 */
	public SerialInterface setName(final String name) {
		this.name = name;
		return this;
	}

	/** Renvoie le nom du port série. */
	public String getName() {
		return this.name;
	}

	/**
	 * Définit le débit du port série.
	 * 
	 * @param rate Le débit du port en bauds (bits par seconde), 9600 par défaut.
	 * @return Cet objet permettant la construction
	 *         <tt>new SerialInterface().setRate(..</tt>.
	 */
	public SerialInterface setRate(final int rate) {
		this.rate = rate;
		return this;
	}

	/** Renvoie le débit du port série. */
	public int getRate() {
		return this.rate;
	}

	/**
	 * Définit la parité du port série.
	 * 
	 * @param parity La parité du port : 'N' (par défaut) si pas de parité, 'E' pour
	 *               even si parité paire, 'O' pour odd si parité impaire.
	 * @return Cet objet permettant la construction
	 *         <tt>new SerialInterface().setParity(..</tt>.
	 */
	public SerialInterface setParity(final char parity) {
		this.parity = parity;
		return this;
	}

	/** Renvoie la parité du port série. */
	public int getParity() {
		return this.parity;
	}

	/**
	 * Définit la taille des mots du port série.
	 * 
	 * @param size La taille des mots : 8 (par défaut) ou 7..
	 * @return Cet objet permettant la construction
	 *         <tt>new SerialInterface().setSize(..</tt>.
	 */
	public SerialInterface setSize(final int size) {
		this.size = size;
		return this;
	}

	/** Renvoie la taille des mots du port série. */
	public int getSize() {
		return this.size;
	}

	/**
	 * Définit le nombre de bits de stop du port série.
	 * 
	 * @param stop Le nombre de bits de stop : 1 (par défaut), 1.5 ou 2.
	 * @return Cet objet permettant la construction
	 *         <tt>new SerialInterface().setStop(..</tt>.
	 */
	public SerialInterface setStop(final double stop) {
		this.stop = stop;
		return this;
	}

	/** Renvoie le nombre de bits de stop du port série. */
	public double getStop() {
		return this.stop;
	}

	/**
	 * Ouvre le port série avec les paramètres actuels.
	 * 
	 * @return Cet objet permettant la construction
	 *         <tt>new SerialInterface().set*(.. ). open()</tt>.
	 */
	public SerialInterface open() {
		this.close();
		this.data.clear();
		try {
			this.open(SerialPort.getCommPort(this.name));
			return this;
		} catch (final Throwable e) {
			this.output = null;
			this.input = null;
			this.port = null;
			System.out.println(I18N.ngettext("Error while opening serial port \n\t{0}\n\t : {1}", this, e));
			return this;
		}
	}

	// Opens on a given port identifier
	private void open(final SerialPort id) throws Exception {
		final int timeout = 5000; // milliseconds
		this.port = id;
		this.port.openPort();
		this.output = this.port.getOutputStream();
		this.input = this.port.getInputStream();

		this.port.setComPortParameters(this.rate, 8,
				this.stop == 2.0 ? SerialPort.TWO_STOP_BITS
						: this.stop == 1.5 ? SerialPort.ONE_POINT_FIVE_STOP_BITS : SerialPort.ONE_STOP_BIT,
				this.parity == 'E' ? SerialPort.EVEN_PARITY
						: this.parity == 'O' ? SerialPort.ODD_PARITY : SerialPort.NO_PARITY,
				true);
		this.port.setComPortTimeouts(SerialPort.TIMEOUT_READ_BLOCKING | SerialPort.TIMEOUT_WRITE_BLOCKING, timeout,
				timeout);
		this.port.addDataListener(this.listener = new SerialPortDataListener() {

			@Override
			public void serialEvent(final SerialPortEvent event) {
				if (event.getEventType() == SerialPort.LISTENING_EVENT_DATA_AVAILABLE) {
					try {
						while (SerialInterface.this.input.available() > 0) {
							SerialInterface.this.reading(SerialInterface.this.input.read());
						}
					} catch (final Exception e) {
						System.out.println(
								I18N.ngettext("Error while reading on serial port \n\t{0}\n\t : {1}", this, e));
					}
				}
			}

			@Override
			public int getListeningEvents() {
				return SerialPort.LISTENING_EVENT_DATA_AVAILABLE;
			}
		});
	}

	/** Ferme le port série. */
	public void close() {
		try {
			if (this.listener != null) {
				this.port.removeDataListener();
				this.listener = null;
			}
			if (this.output != null) {
				this.output.close();
				this.output = null;
			}
			if (this.input != null) {
				this.input.close();
				this.input = null;
			}
			if (this.port != null) {
				this.port.closePort();
				this.port = null;
			}
		} catch (final Exception e) {
			this.output = null;
			this.input = null;
			this.port = null;
			System.out.println(I18N.ngettext("Error while closing serial port\n\t{0}\n\t : {1}", this, e));
		}
	}

	/**
	 * Teste si le port est ouvert.
	 * 
	 * @return Renvoie true si le port a été ouvert sans erreur.
	 */
	public boolean isOpen() {
		return this.port != null;
	}

	/**
	 * Ecrit un octet sur le port série.
	 * 
	 * @param value L'octet à écrire.
	 */
	public void write(final int value) {
		try {
			if (this.writer != null) {
				this.writer.writing(value);
			}
			if (this.output == null) {
				throw new IOException(I18N.gettext("Serial port closed or error"));
			}
			this.output.write(value & 0xff);
			this.output.flush();
		} catch (final Exception e) {
			System.out
					.println(I18N.ngettext("Error while writing '{0}' on comm port \n\t{1}\n\t : {2}", value, this, e));
		}
	}

	/**
	 * Ecrit les octets d'une chaîne de caractère sur le port série.
	 * 
	 * @param value Les octets à écrire. La chaîne ne doit contenir que des
	 *              caractères ASCII.
	 * @return La valeur true, si la chaîne est bien de l'ASCII, et fausse sinon.
	 *         Dans ce cas aucun caratère n'est écrit.
	 */
	public boolean write(final String value) {
		if (SerialInterface.isASCII(value)) {
			for (final char c : value.toCharArray()) {
				this.write(c);
			}
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Teste si une chaîne ne contient que des caractères l'ASCII.
	 * 
	 * @param value La chaîne à tester.
	 * @return La valeur true, si la chaîne est bien de l'ASCII, et fausse sinon.
	 */
	public static boolean isASCII(final String value) {
		boolean ok = true;
		for (final char c : value.toCharArray()) {
			// int v = (int) c;
			if (c < 0 || 255 < c) {
				System.out.println(I18N.ngettext(
						"Cannot write string \"" + value + "\" which contains non-ASCII character '" + c + "'", value,
						c));
				ok = false;
			}
		}
		return ok;
	}

	/** Définit un moniteur des caractères émis. */
	public interface Writer {
		/**
		 * Routine appellée à l'écriture de chaque caractère.
		 * 
		 * @param c Le caractère écrit.
		 */
		void writing(int c);
	}

	/**
	 * Connecte un writer au port série.
	 * 
	 * @param writer Un writer qui est appellé à chaque caratère écrit.
	 * @return Cet objet permettant la construction
	 *         <tt>new SerialInterface().setWriter(..</tt>.
	 */
	public SerialInterface setWriter(final Writer writer) {
		this.writer = writer;
		return this;
	}

	/**
	 * Lit un octet sur le port série.
	 * <p>
	 * L'octet lu est retiré du buffer de lecture, après l'appel de cette fonction.
	 * </p>
	 * 
	 * @return La valeur de l'octet à lire ou -1 si il n'y a pas d'octet à lire.
	 */
	public int read() {
		if (this.data.size() == 0) {
			return -1;
		} else {
			final int value = this.data.get(0);
			this.data.remove(0);
			return value;
		}
	}

	// Lecture de l'octet à son arrivée
	private void reading(final int c) {
		this.data.add(c);
		if (this.reader != null) {
			this.reader.reading(c);
		}
	}

	/**
	 * Renvoie tous les octets actuellement dans le buffer.
	 * 
	 * @return Un tableau avec tous les octets actuellement dans le buffer.
	 */
	int[] getChars() {
		final int chars[] = new int[this.data.size()];
		int i = 0;
		for (final int c : this.data) {
			chars[i++] = c;
		}
		return chars;
	}

	/** Définit un lecteur de caractère. */
	public interface Reader {
		/**
		 * Routine appellée à l'arrivée de chaque caractère.
		 * 
		 * @param c Le caractère reçu.
		 */
		void reading(int c);
	}

	/**
	 * Connecte un reader au port série.
	 * 
	 * @param reader Un reader qui est appellé à l'arrivée de chaque caractère en
	 *               lecture.
	 * @return Cet objet permettant la construction
	 *         <tt>new SerialInterface().setReader(..</tt>.
	 */
	public SerialInterface setReader(final Reader reader) {
		this.reader = reader;
		return this;
	}

	/**
	 * Ajoute des octets entrée pour simuler une lecture.
	 * 
	 * @param string Les octets en entrée.
	 */
	public void simulReading(final String string) {
		if (SerialInterface.isASCII(string)) {
			for (final char c : string.toCharArray()) {
				this.reading(c);
			}
		}
	}

	/** Renvoie une description des paramètres du port série. */
	@Override
	public String toString() {
		return "<port name=\""
				+ this.name
				+ "\" rate=\""
				+ this.rate
				+ "\" parity=\""
				+ this.parity
				+ "\" size=\""
				+ this.size
				+ "\" stop=\""
				+ this.stop
				+ "\" open=\""
				+ this.isOpen()
				+ "\" input-buffer-size =\""
				+ this.data.size()
				+ "\"/>";
	}

	/** Renvoie la liste des noms de ports séries disponibles. */
	public static String[] getPortNames() {
		final SerialPort[] ports = SerialPort.getCommPorts();
		final String[] names = new String[ports.length];
		for (int i = 0; i < ports.length; i++) {
			names[i] = ports[i].getSystemPortName();
		}
		return names;
	}

	// Affiche les paramètres du port.
	public static void show(final SerialPort id) {
		System.out.println(id.getDescriptivePortName() + ": " + (id.isOpen() ? "opened" : "closed"));
	}

	/**
	 * Renvoie la liste des des noms de ports séries disponibles ce qui teste
	 * l'installation des librairies.
	 * 
	 * @param usage <tt>java -cp javascool-proglets.jar org.javascool.proglets.commSerie.SerialInterface</tt>
	 */
	public static void main(final String usage[]) {
		final String names[] = SerialInterface.getPortNames();
		{
			System.out.print(I18N.ngettext("There is {0} comm ports:", names.length));
			for (final String name : names) {
				System.out.print(" " + name);
			}
			System.out.println();
		}
	}
}
