/*******************************************************************************
* Christophe.Beasse@ac-rennes.fr, Copyright (C) 2011.  All rights reserved.    *
*******************************************************************************/

package org.javascool.proglets.detectionForme;

import java.awt.Color;

import org.javascool.macros.Macros;

/**
 * Démonstration de la proglet.
 *
 * @see <a href="Demo.java.html">code source</a>
 */
public class Demo {
	/** Lance la démo de la proglet. */
	public static void start() {

		try {
			Functions.createImage(1, 300, 300);
			Functions.drawFillRect(1, 100, 100, 100, 100, Color.black);
			Functions.rotateImage(1, 45);
			Functions.showImage(1);
			Macros.sleep(500);
			Functions.copyImage(1, 2);
			Functions.cutImage(2, 75, 75, 150, 150);
			Functions.showImage(2);
			Macros.sleep(500);
			Functions.copyImage(2, 3, 30);
			Functions.showImage(3);
			Macros.sleep(500);
			Functions.copyImage(3, 4);
			Functions.rotateImage(4, -45);
			Functions.sideDetection(4);
			Functions.showImage(4);
			Macros.sleep(500);
			Functions.showPipImage();

		} catch (final Exception e) {
			throw new RuntimeException(e + " Fct [Demo]");
		}

	}
}
