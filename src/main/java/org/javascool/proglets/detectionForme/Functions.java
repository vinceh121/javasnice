/*******************************************************************************
* Christophe.Beasse@ac-rennes.fr, Copyright (C) 2011.  All rights reserved.    *
* from proglet codagePixels /  Thierry.Vieville@sophia.inria.fr                *
*******************************************************************************/

package org.javascool.proglets.detectionForme;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.ConvolveOp;
import java.awt.image.Kernel;
import java.awt.image.WritableRaster;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.Random;

import javax.imageio.ImageIO;

import org.javascool.gui.Core;
// Used to define the gui
import org.javascool.macros.Macros;

import com.jhlabs.image.BlockFilter;
import com.jhlabs.image.BoxBlurFilter;
import com.jhlabs.image.ContourFilter;
import com.jhlabs.image.DiffuseFilter;
import com.jhlabs.image.EdgeFilter;

import lol.javasnice.I18N;

/**
 * Définit les fonctions de la proglet qui permettent de manipuler les pixels
 * d'une image.
 *
 * @see <a href="Functions.java.html">code source</a>
 * @serial exclude
 */
public class Functions {
	// img[0] et g2d[0] ne sont pas utilisés
	// Les indices d'images utilisés sont 1,2,3,4
	private static BufferedImage[] img = new BufferedImage[5];
	private static Graphics2D[] g2d = new Graphics2D[5];

	private static DataInputStream fileR = null;
	private static DataOutputStream fileW = null;

	// @factory
	private Functions() {
	}

	/** Renvoie l'instance de la proglet pour accéder à ses éléments. */
	private static Panel getPane() {
		return (Panel) Macros.getProgletPane();
	}

	/**
	 * Création des images de travail On dispose de 4 images de travail (1,2,3 ou 4)
	 * - La taille de l'image ne doit pas être trop importante(pas plus de 500^2).
	 * 
	 * @param width  largeur de l'image.
	 * @param height hauteur de l'image.
	 * @param num    numéro de l'image de travail.*
	 */

	public static void createImage(final int num, final int width, final int height) {
		if (num < 1 || num > 4) {
			throw new RuntimeException(I18N.gettext("Fct [createImage] : illegal image number"));
		}
		try {
			if (Functions.g2d[num] != null) {
				Functions.g2d[num].dispose();
			}
			Functions.img[num] = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
			Functions.g2d[num] = Functions.img[num].createGraphics();
			Functions.clearImage(num);
		} catch (final Exception e) {
			throw new RuntimeException(I18N.ngettext("Fct [createImage] : Création image {0} impossible", num));
		}
	}

	/**
	 * Suppression des images de travail On dispose de 4 images (1,2,3 ou 4) Par
	 * défaut on supprime les 4 images de travail.
	 * 
	 * @param num numéro de l'image de travail (si précisé, sinon toutes les
	 *            images).
	 */
	public static void disposeImage(final int num) {
		if (num < 1 || num > 4) {
			throw new RuntimeException(I18N.gettext("Fct [disposeImage] : Illegal image number"));
		}
		if (Functions.g2d[num] != null) {
			Functions.g2d[num].dispose();
		}
		Functions.img[num] = null;
	}

	public static void disposeImage() {
		for (int i = 1; i <= 4; i++) {
			if (Functions.g2d[i] != null) {
				Functions.g2d[i].dispose();
			}
			Functions.img[i] = null;
		}
	}

	/**
	 * Charge une image locale ou distante. La taille de l'image ne doit pas être
	 * trop importante (pas plus de 500^2).
	 * 
	 * @param num      numéro de l'image de travail dans laquelle charger l'image.
	 *                 On dispose de 4 images (1,2,3 ou 4)
	 * @param location Une URL (Universal Resource Location) de la forme:
	 *                 <div id="load-format">
	 *                 <table>
	 *                 <caption>URL (Universal Resource Location) prises en
	 *                 charge</caption>
	 *                 <tr>
	 *                 <td><tt>http:/<i>path-name</i></tt></td>
	 *                 <td>pour aller chercher le contenu sur un site web</td>
	 *                 </tr>
	 *                 <tr>
	 *                 <td><tt>http:/<i>path-name</i>?param_i=value_i&amp;..</tt></td>
	 *                 <td>pour le récupérer sous forme de requête HTTP</td>
	 *                 </tr>
	 *                 <tr>
	 *                 <td><tt>file:/<i>path-name</i></tt></td>
	 *                 <td>pour le charger du système de fichier local ou en tant
	 *                 que ressource Java dans le CLASSPATH</td>
	 *                 </tr>
	 *                 <tr>
	 *                 <td><tt>jar:/<i>jar-path-name</i>!/<i>jar-entry</i></tt></td>
	 *                 <td>pour le charger d'une archive
	 *                 <div>(exemple:<tt>jar:http://javascool.gforge.inria.fr/javascool.jar!/META-INF/MANIFEST.MF</tt>)</div></td>
	 *                 </tr>
	 *                 </table>
	 *                 </div>
	 *
	 * @throws IllegalArgumentException Si l'URL est mal formée.
	 * @throws RuntimeException         Si une erreur d'entrée-sortie s'est
	 *                                  produite.
	 */
	public static void loadImage(final int num, final String location) {
		if (num < 1 || num > 4) {
			throw new RuntimeException(I18N.gettext("Fct [loadImage] : Illegal image number"));
		}
		Functions.img[num] = null;
		try {
			Functions.img[num] = ImageIO.read(Macros.getResourceURL(location));
		} catch (final IOException e) {
			throw new RuntimeException(e + " when loading: " + location + " : " + Macros.getResourceURL(location));
		}
		if (Functions.img[num] == null) {
			throw new RuntimeException("Unable to load: " + location);
		}
	}

	/**
	 * Sauvegarde d'une image de travail.
	 * 
	 * @param num      numéro de l'image de travail On dispose de 4 images (1,2,3 ou
	 *                 4)
	 * @param location Une URL (Universal Resource Location) de la forme:
	 *                 <div id="save-format">
	 *                 <table>
	 *                 <caption>URL (Universal Resource Location) prises en
	 *                 charge</caption>
	 *                 <tr>
	 *                 <td><tt>ftp:/<i>path-name</i></tt></td>
	 *                 <td>pour sauver sur un site FTP.</td>
	 *                 </tr>
	 *                 <tr>
	 *                 <td><tt>file:/<i>path-name</i></tt></td>
	 *                 <td>pour sauver dans le système de fichier local (le
	 *                 <tt>file:</tt> est optionnel).</td>
	 *                 </tr>
	 *                 <tr>
	 *                 <td><tt>mailto:<i>address</i>?subject=<i>subject</i></tt></td>
	 *                 <td>pour envoyer un courriel avec le texte en contenu.</td>
	 *                 </tr>
	 *                 <tr>
	 *                 <td><tt>stdout:/</tt></td>
	 *                 <td>pour l'imprimer dans la console.</td>
	 *                 </tr>
	 *                 </table>
	 *                 </div>
	 *
	 * @throws IllegalArgumentException Si l'URL est mal formée.
	 * @throws RuntimeException         Si une erreur d'entrée-sortie s'est
	 *                                  produite.
	 */
	public static void saveImage(final int num, String location) {
		if (num < 1 || num > 4) {
			throw new RuntimeException("Fct [saveImage] : Num image incorrect");
		}
		location = Macros.getResourceURL(location).toString();
		try {
			if (location.startsWith("file:")) {
				ImageIO.write(Functions.img[num], "png", new File(location.substring(5)));
			} else {
				final URLConnection connection = new URL(location).openConnection();
				connection.setDoOutput(true);
				final OutputStream writer = connection.getOutputStream();
				ImageIO.write(Functions.img[num], "png", writer);
				writer.close();
			}
		} catch (final IOException e) {
			throw new RuntimeException(e + " when saving: " + location);
		}
	}

	/**
	 * Création d'une image à partir d'un tableau d'entiers On dispose de 4 images
	 * de travail (1,2,3 ou 4)
	 * 
	 * @param num    numéro de l'image de travail.
	 * @param pixMap Tableau d'entiers.
	 * @param width  largeur pixMap.
	 * @param height hauteur pixMap.
	 */

	static public void createImageFromPixMap(final int num, final int pixMap[][], final int width, final int height) {
		if (num < 1 || num > 4) {
			throw new RuntimeException("Fct [createImageFromPixMap] : Num image incorrect");
		}
		try {
			if (Functions.g2d[num] != null) {
				Functions.g2d[num].dispose();
			}
			Functions.img[num] = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
			Functions.g2d[num] = Functions.img[num].createGraphics();

			for (int x = 0; x < width; x++) {
				for (int y = 0; y < height; y++) {
					Functions.img[num].setRGB(x, y, pixMap[x][y]);
				}
			}
		} catch (final Exception e) {
			throw new RuntimeException(e + " Fct [createImageFromPixMap] : creation image impossible");
		}
	}

	/**
	 * Chargement d'une image dans un tableau d'entiers On dispose de 4 images de
	 * travail (1,2,3 ou 4)
	 * 
	 * @param num    numéro de l'image de travail.
	 * @param pixMap Tableau d'entiers.
	 */

	static public void loadImageToPixMap(final int num, final int pixMap[][]) {
		if (num < 1 || num > 4) {
			throw new RuntimeException("Fct [loadImageToPixMap] : Num image incorrect");
		}
		try {

			final int width = Functions.img[num].getWidth();
			final int height = Functions.img[num].getHeight();

			for (int x = 0; x < width; x++) {
				for (int y = 0; y < height; y++) {
					pixMap[x][y] = Functions.img[num].getRGB(x, y);
				}
			}
		} catch (final Exception e) {
			throw new RuntimeException(e + " Fct [loadImageToPixMap] : Impossible de charger le pixmap");
		}
	}

	/**
	 * Affichage des images de travail dans le panel de la proglet On dispose de 4
	 * images de travail (1,2,3 ou 4)
	 * 
	 * @param num numéro de l'image de travail.*
	 */
	public static void showImage(final int num) {
		if (num < 1 || num > 4) {
			throw new RuntimeException("Fct [showImage] : Num image incorrect");
		}
		try {
			Functions.getPane().reset(Functions.img[num], false);
		} catch (final Exception e) {
			throw new RuntimeException(e + " Fct [showImage : Impossible de charger Bufferedimage num " + num);
		}
	}

	/**
	 * Réinitialisation des images de travail (fond blanc) On dispose de 4 images de
	 * travail (1,2,3 ou 4)
	 * 
	 * @param num numéro de l'image de travail.*
	 */

	public static void clearImage(final int num) {
		if (num < 1 || num > 4) {
			throw new RuntimeException("Fct [clearImage] : Num image incorrect");
		}
		Functions.g2d[num].setColor(Color.white);
		Functions.g2d[num].fillRect(0, 0, Functions.img[num].getWidth(), Functions.img[num].getHeight());
	}

	/**
	 * Copie de l'image de travail num1 dans l'image de travail num2 On dispose de 4
	 * images de travail (1,2,3 ou 4)
	 * 
	 * @param num1 numéro de l'image source
	 * @param num2 numéro de l'image destination
	 */

	public static void copyImage(final int num1, final int num2) {
		if (num1 < 1 || num1 > 4) {
			throw new RuntimeException("Fct [copyImage] : Num1 image incorrect");
		}
		if (num2 < 1 || num2 > 4) {
			throw new RuntimeException("Fct [copyImage] : Num2 image incorrect");
		}
		final ColorModel cm = Functions.img[num1].getColorModel();
		final boolean isAlphaPremultiplied = cm.isAlphaPremultiplied();
		final WritableRaster raster
				= Functions.img[num1].getSubimage(0, 0, Functions.img[num1].getWidth(), Functions.img[num1].getHeight())
						.copyData(null);
		Functions.img[num2] = new BufferedImage(cm, raster, isAlphaPremultiplied, null);
		if (Functions.g2d[num2] != null) {
			Functions.g2d[num2].dispose();
		}
		Functions.g2d[num2] = Functions.img[num2].createGraphics();
	}

	/**
	 * Copie d'une partie de l'image de travail num1 dans l'image de travail num2 On
	 * dispose de 4 images de travail (1,2,3 ou 4)
	 * 
	 * @param num1 numéro de l'image source
	 * @param num2 numéro de l'image destination
	 * @param x    abscisse de la zone à récupérer
	 * @param y    ordonnée de la zone à récupérer
	 * @param w    largeur de la zone à récupérer
	 * @param h    hauteur de la zone à récupérer
	 */

	public static void copyImage(final int num1, final int num2, final int x, final int y, final int w, final int h) {
		if (num1 < 1 || num1 > 4) {
			throw new RuntimeException("Fct [copyImage2] : Num1 image incorrect");
		}
		if (num2 < 1 || num2 > 4) {
			throw new RuntimeException("Fct [copyImage2] : Num image incorrect");
		}
		final ColorModel cm = Functions.img[num1].getColorModel();
		final boolean isAlphaPremultiplied = cm.isAlphaPremultiplied();
		final WritableRaster raster = Functions.img[num1].getSubimage(x, y, w, h).copyData(null);
		Functions.img[num2] = new BufferedImage(cm, raster, isAlphaPremultiplied, null);
		if (Functions.g2d[num2] != null) {
			Functions.g2d[num2].dispose();
		}
		Functions.g2d[num2] = Functions.img[num2].createGraphics();
	}

	/**
	 * Copie de l'image de travail num1 dans l'image de travail num2 avec ajout
	 * d'une bordure de largeur borderWidth. On dispose de 4 images de travail
	 * (1,2,3 ou 4)
	 * 
	 * @param num1        numéro de l'image source
	 * @param num2        numéro de l'image destination
	 * @param borderWidth largeur du bord ajouté
	 */
	public static void copyImage(final int num1, final int num2, final int borderWidth) {
		if (num1 < 1 || num1 > 4) {
			throw new RuntimeException("Fct [copyImage3] : Num1 image incorrect");
		}
		if (num2 < 1 || num2 > 4) {
			throw new RuntimeException("Fct [copyImage3] : Num image incorrect");
		}
		final int w = Functions.img[num1].getWidth();
		final int h = Functions.img[num1].getHeight();
		Functions.createImage(num2, w + 2 * borderWidth, h + 2 * borderWidth);
		final Graphics2D g = Functions.img[num2].createGraphics();
		g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
		// g.setColor(Color.white);
		// g.fillRect(0,0,w+2*borderWidth,h+2*borderWidth);
		// g.drawImage(img[num1], 0, 0, newW, newH, 0, 0, w, h, null);
		g.drawImage(Functions.img[num1], borderWidth, borderWidth, w, h, null);
		g.dispose();
	}

	/**
	 * Rogner une image On dispose de 4 images de travail (1,2,3 ou 4)
	 * 
	 * @param num numéro de l'image à rognenr
	 * @param x   abscisse de la zone à garder
	 * @param y   ordonnée de la zone à garder
	 * @param w   largeur de la zone à garder
	 * @param h   hauteur de la zone à garder
	 */

	public static void cutImage(final int num, final int x, final int y, final int w, final int h) {
		if (num < 1 || num > 4) {
			throw new RuntimeException("Fct [clearImage] : Num image incorrect");
		}

		final ColorModel cm = Functions.img[num].getColorModel();
		final boolean isAlphaPremultiplied = cm.isAlphaPremultiplied();
		final WritableRaster raster = Functions.img[num].getSubimage(x, y, w, h).copyData(null);
		Functions.img[num] = new BufferedImage(cm, raster, isAlphaPremultiplied, null);
		if (Functions.g2d[num] != null) {
			Functions.g2d[num].dispose();
		}
		Functions.g2d[num] = Functions.img[num].createGraphics();
	}

	/**
	 * Change la valeur d'un pixel de l'image de travail. On dispose de 4 images de
	 * travail (1,2,3 ou 4)
	 * 
	 * @param num numéro de l'image de travail.*
	 * @param x   Abcisse de l'image.
	 * @param y   Ordonnée de l'image.
	 * @param c   Couleur: "black" (default), "blue", "cyan", "gray", "green",
	 *            "magenta", "orange", "pink", "red", "white", "yellow".
	 */

	public static void setPixel(final int num, final int x, final int y, final int c) {
		if (num < 1 || num > 4) {
			throw new RuntimeException("Fct [setPixel(1)] : Num image incorrect");
		}
		Functions.img[num].setRGB(x, y, c);
	}

	public static void setPixel(final int num, final int x, final int y, final Color c) {
		if (num < 1 || num > 4) {
			throw new RuntimeException("Fct [setPixel(2)] : Num image incorrect");
		}
		Functions.img[num].setRGB(x, y, c.getRGB());
	}

	/**
	 * Retourne la valeur d'un pixel de l'image de travail. On dispose de 4 images
	 * de travail (1,2,3 ou 4)
	 * 
	 * @param num numéro de l'image de travail.*
	 * @param x   Abcisse de l'image.
	 * @param y   Ordonnée de l'image.
	 * @return Renvoie la couleur spécifiée (entier)
	 */

	public static int getPixelColor(final int num, final int x, final int y) {
		if (num < 1 || num > 4) {
			throw new RuntimeException("Fct [getPixelColor] : Num image incorrect");
		}
		return Functions.img[num].getRGB(x, y);
	}

	/**
	 * A pixel is represented by a 4-byte (32 bit) integer, like so:
	 *
	 * 00000000 00000000 00000000 11111111 ^ Alpha ^Red ^Green ^Blue
	 *
	 * Alpha, which denotes "transparency". An alpha value of 0xff (=255) is fully
	 * opaque An alpha value of 0 is fully transparent. int color = color &
	 * 0x00ffffff; use bitwise & to remove alpha component
	 *
	 * pixel = (alpha << 24) | (red << 16) | (green << 8) | blue;
	 */

	/**
	 * Returns the alpha component in the range 0-255 in the default sRGB space.
	 * 
	 * @param color couleur du pixel (int)
	 * @return the alpha component.
	 */
	public static int getAlpha(final int color) {
		return color >> 24 & 0xFF;
	}

	/**
	 * Returns the red component in the range 0-255 in the default sRGB space.
	 * 
	 * @param color couleur du pixel (int)
	 * @return the red component.
	 */
	public static int getRed(final int color) {
		return color >> 16 & 0xFF;
	}

	/**
	 * Returns the green component in the range 0-255 in the default sRGB space.
	 * 
	 * @param color couleur du pixel (int)
	 * @return the green component.
	 */
	public static int getGreen(final int color) {
		return color >> 8 & 0xFF;
	}

	/**
	 * Returns the blue component in the range 0-255 in the default sRGB space.
	 * 
	 * @param color couleur du pixel (int)
	 * @return the blue component.
	 */
	public static int getBlue(final int color) {
		return color >> 0 & 0xFF;
	}

	/**
	 * teste la valeur d'un pixel de l'image de travail. On dispose de 4 images de
	 * travail (1,2,3 ou 4)
	 * 
	 * @param num numéro de l'image de travail.*
	 * @param x   Abcisse de l'image.
	 * @param y   Ordonnée de l'image.
	 * @param c   Couleur: "black" (default), "blue", "cyan", "gray", "green",
	 *            "magenta", "orange", "pink", "red", "white", "yellow".
	 * @return Renvoie true si le pixel est de la couleur spécifiée
	 */

	public static boolean isPixelColor(final int num, final int x, final int y, final Color c) {
		if (num < 1 || num > 4) {
			throw new RuntimeException("Fct [isPixelColor] : Num image incorrect");
		}
		return Functions.img[num].getRGB(x, y) == c.getRGB();
	}

	public static boolean isPixelColor(final int num, final int x, final int y, final int c) {
		if (num < 1 || num > 4) {
			throw new RuntimeException("Fct [isPixelColor] : Num image incorrect");
		}
		return Functions.img[num].getRGB(x, y) == c;
	}

	/**
	 * Renvoie le code coudeur de la couleur spécifiée
	 * 
	 * @param c Couleur: "black" (default), "blue", "cyan", "gray", "green",
	 *          "magenta", "orange", "pink", "red", "white", "yellow".
	 * @return Renvoie la valeur int RGB de la couleur spécifiée
	 */

	public static int codeCouleur(final Color c) {
		return c.getRGB();
	}

	/**
	 * Tracer d'un rectangle dans l'images de travail On dispose de 4 images de
	 * travail (1,2,3 ou 4)
	 * 
	 * @param num numéro de l'image de travail.
	 * @param xc  abscisse coin haut gauche du rectangle.
	 * @param yc  ordonnée coin haut gauche du rectangle.
	 * @param w   largueur côté du rectangle.
	 * @param h   hauteur côté du rectangle.
	 * @param c   couleur du tracé.
	 */

	public static void drawRect(final int num, final int xc, final int yc, final int w, final int h, final Color c) {
		if (num < 1 || num > 4) {
			throw new RuntimeException("Fct [drawFillSquare] : Num image incorrect");
		}
		Functions.g2d[num].setColor(c);
		Functions.g2d[num].drawRect(xc, yc, w, h);
	}

	/**
	 * Tracer d'un rectangle plein dans l'images de travail On dispose de 4 images
	 * de travail (1,2,3 ou 4)
	 * 
	 * @param num numéro de l'image de travail.
	 * @param xc  abscisse coin haut gauche du rectangle.
	 * @param yc  ordonnée coin haut gauche du rectangle.
	 * @param w   largueur côté du rectangle.
	 * @param h   hauteur côté du rectangle.
	 * @param c   couleur du tracé.
	 */

	public static void drawFillRect(final int num, final int xc, final int yc, final int w, final int h,
			final Color c) {
		if (num < 1 || num > 4) {
			throw new RuntimeException("Fct [drawFillSquare] : Num image incorrect");
		}
		Functions.g2d[num].setColor(c);
		Functions.g2d[num].fillRect(xc, yc, w, h);
	}

	/**
	 * Tracer d'un ovale dans l'images de travail On dispose de 4 images de travail
	 * (1,2,3 ou 4)
	 * 
	 * @param num numéro de l'image de travail.
	 * @param xc  abscisse coin haut gauche du ovale.
	 * @param yc  ordonnée coin haut gauche du ovale.
	 * @param w   largueur du carré englobant.
	 * @param h   hauteur du carré englobant.
	 * @param c   couleur du tracé.
	 */
	public static void drawOval(final int num, final int xc, final int yc, final int w, final int h, final Color c) {
		if (num < 1 || num > 4) {
			throw new RuntimeException("Fct [drawFillCircle] : Num image incorrect");
		}
		Functions.g2d[num].setColor(c);
		Functions.g2d[num].drawOval(xc, yc, w, h);
	}

	/**
	 * Tracer d'un ovale plein dans l'images de travail On dispose de 4 images de
	 * travail (1,2,3 ou 4)
	 * 
	 * @param num numéro de l'image de travail.
	 * @param xc  abscisse coin haut gauche du ovale.
	 * @param yc  ordonnée coin haut gauche du ovale.
	 * @param w   largueur du carré englobant.
	 * @param h   hauteur du carré englobant.
	 * @param c   couleur du tracé.
	 */
	public static void drawFillOval(final int num, final int xc, final int yc, final int w, final int h,
			final Color c) {
		if (num < 1 || num > 4) {
			throw new RuntimeException("Fct [drawFillCircle] : Num image incorrect");
		}
		Functions.g2d[num].setColor(c);
		Functions.g2d[num].fillOval(xc, yc, w, h);
	}

	/**
	 * Tracer d'un segment dans l'images de travail On dispose de 4 images de
	 * travail (1,2,3 ou 4)
	 * 
	 * @param num numéro de l'image de travail.
	 * @param x1  abscisse point initial.
	 * @param y1  ordonnée point initial.
	 * @param x2  abscisse point final.
	 * @param y2  ordonnée point final.
	 * @param c   couleur du tracé.*
	 */
	public static void drawSegment(final int num, final int x1, final int y1, final int x2, final int y2,
			final Color c) {
		if (num < 1 || num > 4) {
			throw new RuntimeException("Fct [drawSegment] : Num image incorrect");
		}
		Functions.g2d[num].setColor(c);
		Functions.g2d[num].drawLine(x1, y1, x2, y2);
	}

	/**
	 * Tracer d'une chaîne de caractères dans l'images de travail On dispose de 4
	 * images de travail (1,2,3 ou 4)
	 * 
	 * @param num numéro de l'image de travail.
	 * @param str message à afficher.
	 * @param x   abscisse coin haut gauche du carré.
	 * @param y   ordonnée coin haut gauche du carré.
	 * @param c   couleur du tracé.
	 */

	public static void drawString(final int num, final String str, final int x, final int y, final Color c) {
		if (num < 1 || num > 4) {
			throw new RuntimeException("Fct [drawString] : Num image incorrect");
		}
		Functions.g2d[num].setColor(c);
		Functions.g2d[num].drawString(str, x, y);
	}

	/**
	 * Renvoie la largeur de l'image. On dispose de 4 images de travail (1,2,3 ou 4)
	 * 
	 * @param num numéro de l'image de travail.
	 */
	static public int getImageWidth(final int num) {
		if (num < 1 || num > 4) {
			throw new RuntimeException("Fct [getImageWidth] : Num image incorrect");
		}
		return Functions.img[num].getWidth();
	}

	/**
	 * Renvoie la hauteur de l'image. On dispose de 4 images de travail (1,2,3 ou 4)
	 * 
	 * @param num numéro de l'image de travail.
	 */
	static public int getImageHeight(final int num) {
		if (num < 1 || num > 4) {
			throw new RuntimeException("Fct [getImageHeight] : Num image incorrect");
		}
		return Functions.img[num].getHeight();
	}

	/**
	 * Convolue l'image avec le masque spécifié. On dispose de 4 images de travail
	 * (1,2,3 ou 4)
	 * 
	 * @param num  numéro de l'image de travail.
	 * @param mask masque de convolution qui doit être défini de la manière suivante
	 *             : float[ ] masque = { 0.1f, 0.1f, 0.1f, 0.1f, 0.2f, 0.1f, 0.1f,
	 *             0.1f, 0.1f};
	 */

	public static void convolveImage(final int num, final float[] mask) {
		if (num < 1 || num > 4) {
			throw new RuntimeException("Fct [convolveImage] : Num image incorrect");
		}
		final BufferedImage imgConv = new BufferedImage(Functions.getImageWidth(num), Functions.getImageHeight(num),
				BufferedImage.TYPE_INT_RGB);

		final Kernel masque = new Kernel(3, 3, mask);
		final ConvolveOp operation = new ConvolveOp(masque);
		operation.filter(Functions.img[num], imgConv);
		Functions.img[num] = imgConv;
		if (Functions.g2d[num] != null) {
			Functions.g2d[num].dispose();
		}
		Functions.g2d[num] = Functions.img[num].createGraphics();

	}

	/**
	 * Redimensionne l'image à la nouvelle taille spécifiée On dispose de 4 images
	 * de travail (1,2,3 ou 4)
	 * 
	 * @param num  numéro de l'image de travail.
	 * @param newW nouvelle largeur
	 * @param newH nouvelle hauteur
	 */
	public static void resizeImage(final int num, final int newW, final int newH) {
		if (num < 1 || num > 4) {
			throw new RuntimeException("Fct [resizeImage] : Num image incorrect");
		}
		final BufferedImage imgResized = new BufferedImage(newW, newH, BufferedImage.TYPE_INT_RGB);
		final Graphics2D g = imgResized.createGraphics();
		g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
		g.drawImage(Functions.img[num], 0, 0, newW, newH, null);
		g.dispose();
		Functions.img[num] = imgResized;
		if (Functions.g2d[num] != null) {
			Functions.g2d[num].dispose();
		}
		Functions.g2d[num] = Functions.img[num].createGraphics();

	}

	/**
	 * Redimensionne l'image à la nouvelle taille spécifiée On utilise ici un rendu
	 * supérieur (traitement plus long) On dispose de 4 images de travail (1,2,3 ou
	 * 4)
	 * 
	 * @param num  numéro de l'image de travail.
	 * @param newW nouvelle largeur
	 * @param newH nouvelle hauteur
	 */
	public static void resizeImageWithHint(final int num, final int newW, final int newH) {
		if (num < 1 || num > 4) {
			throw new RuntimeException("Fct [resizeImageWithHint] : Num image incorrect");
		}
		final BufferedImage imgResized = new BufferedImage(newW, newH, BufferedImage.TYPE_INT_RGB);
		final Graphics2D g = imgResized.createGraphics();
		g.setComposite(AlphaComposite.Src);
		g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
		g.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g.drawImage(Functions.img[num], 0, 0, newW, newH, null);
		g.dispose();
		Functions.img[num] = imgResized;
		if (Functions.g2d[num] != null) {
			Functions.g2d[num].dispose();
		}
		Functions.g2d[num] = Functions.img[num].createGraphics();
	}

	/**
	 * Rotation de l'image suivant l'angle spécifié On dispose de 4 images de
	 * travail (1,2,3 ou 4)
	 * 
	 * @param num   numéro de l'image de travail.
	 * @param angle angle de rotation en degrés
	 */

	public static void rotateImage(final int num, final double angle) {
		if (num < 1 || num > 4) {
			throw new RuntimeException("Fct [rotateImage] : Num image incorrect");
		}
		final int w = Functions.img[num].getWidth();
		final int h = Functions.img[num].getHeight();
		final BufferedImage imgRot = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);
		final Graphics2D g = imgRot.createGraphics();
		g.setColor(Color.white);
		g.fillRect(0, 0, w, h);
		g.rotate(Math.toRadians(angle), w / 2, h / 2);
		g.drawImage(Functions.img[num], 0, 0, null);
		g.dispose();
		Functions.img[num] = imgRot;
		if (Functions.g2d[num] != null) {
			Functions.g2d[num].dispose();
		}
		Functions.g2d[num] = Functions.img[num].createGraphics();
		// g2d.translate(170, 0); Translate the center of our coordinates.

	}

	/**
	 * Affiche en simultané les 4 images de travail seules les images existantes
	 * sont affichées PIP : Picture In Picture
	 */
	public static void showPipImage() {

		final int[][] pos = { { 0, 0 }, { 200, 0 }, { 0, 200 }, { 200, 200 } }; // position images
		try {
			final BufferedImage imgPip = new BufferedImage(400, 400, BufferedImage.TYPE_INT_RGB);
			final Graphics2D g = imgPip.createGraphics();
			g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
			g.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
			g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
			g.setColor(Color.lightGray);
			g.fillRect(0, 0, 400, 400);

			int maxWH = 0;
			for (int i = 1; i <= 4; i++) {
				if (Functions.img[i] != null) {
					if (Functions.img[i].getWidth() > maxWH) {
						maxWH = Functions.img[i].getWidth();
					}
					if (Functions.img[i].getHeight() > maxWH) {
						maxWH = Functions.img[i].getHeight();
					}
				}
			}

			double coef = 1;

			if (maxWH > 200) {
				coef = 200.0 / maxWH;
			}

			for (int i = 1; i <= 4; i++) {
				if (Functions.img[i] != null) {
					final int wr = (int) (Functions.img[i].getWidth() * coef) - 1;
					final int hr = (int) (Functions.img[i].getHeight() * coef) - 1;
					final int dx = (200 - wr) / 2;
					final int dy = (200 - hr) / 2;
					g.drawImage(Functions.img[i], pos[i - 1][0] + dx, pos[i - 1][1] + dy, wr, hr, null);
				}
			}

			g.setColor(Color.red);
			g.drawLine(0, 199, 400, 199);
			g.drawLine(0, 200, 400, 200);
			g.drawLine(0, 201, 400, 201);
			g.drawLine(199, 0, 199, 400);
			g.drawLine(200, 0, 200, 400);
			g.drawLine(201, 0, 201, 400);
			g.dispose();

			Functions.getPane().reset(imgPip, false);
		} catch (final Exception e) {
			System.out.println(I18N.gettext("Could not load BufferedImage imgPip"));
			if (Core.DEBUG) {
				System.out.print(e.toString());
			}
			e.printStackTrace();
		}
	}

	// ========================================================
	// == Gestion de fichier
	// ========================================================

	/**
	 * Ouverture du fichier en lecture.
	 * 
	 * @param nomFichier nom du fichier à ouvrir
	 */
	public static void openFileReader(final String nomFichier) {
		try {
			Functions.fileR = new DataInputStream(new BufferedInputStream(new FileInputStream(nomFichier)));
		} catch (final FileNotFoundException e) {}
	}

	/**
	 * Lecture du code suivant dans le fichier.
	 * 
	 * @return valeur suivante lu (type int).
	 */
	public static int readNextCode() {
		int c = -1;

		if (Functions.fileR == null) {
			throw new RuntimeException("Le fichier READER n'est pas ouvert ! ");
		}

		try {
			c = Functions.fileR.readUnsignedByte();
		} catch (final EOFException e) {} catch (final IOException e) {}
		return c;
	}

	/**
	 * Fermeture du fichier ouvert en lecture.
	 */

	public static void closeFileReader() {
		try {
			Functions.fileR.close();
		} catch (final IOException e) {}
	}

	/**
	 * Ouverture du fichier en Ecriture.
	 * 
	 * @param nomFichier nom du fichier à ouvrir
	 */
	public static void openFileWriter(final String nomFichier) {
		try {
			Functions.fileW = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(nomFichier)));
		} catch (final IOException e) {}
	}

	/**
	 * Ecriture du code suivant (octet) dans le fichier ouvert en écriture.
	 * 
	 * @param c code à ecrire (type int) seul l'octet de poid faible est écrit.
	 */
	public static void writeNextCode(final int c) {

		if (Functions.fileW == null) {
			throw new RuntimeException("Le fichier WRITER n'est pas ouvert ! ");
		}

		try {
			Functions.fileW.writeByte(c);
		} catch (final IOException e) {}
	}

	/**
	 * Fermeture du fichier ouvert en Ecriture.
	 */
	public static void closeFileWriter() {
		try {
			Functions.fileW.close();
		} catch (final IOException e) {}
	}

	// ========================================================
	// == Spécifique Activité
	// ========================================================

	/**
	 * Mémorise le type de forme, sa position et ses dimensions.
	 */
	private static String xxshapeType = null;
	private static int xxposx, xxposy;
	private static int xxcote = 0;
	private static double xxangle = 0;

	/**
	 * Trace aléatoirement la forme à détecter dans l'image on travail dans l'image
	 * de travail [1] la fonction définit une image de 400 x 400
	 * 
	 * @param scenario définit le type de scénario proposé : - (1) : détection d'un
	 *                 pixel - (2) : détection d'un carré - (3) : détection d'un
	 *                 cercle - (4) : détection cercle ou carré - (5) : détection
	 *                 d'un carré + rotation - (6) : détection d'un carré + rotation
	 *                 + bruit léger - (7) : détection d'un cercle + bruit léger -
	 *                 (8) : détection carré ou cercle + rotation - (9) : détection
	 *                 carré ou cercle + rotation + bruit - (10) : détection d'un
	 *                 carré + rotation + bruit fort - (11) : détection d'un cercle
	 *                 + bruit fort - (12) : détection carré ou cercle + rotation +
	 *                 bruit fort
	 */
	public static void createRandomShapeImage(final int scenario) {
		if (scenario < 1 || scenario > 11) {
			throw new RuntimeException("Fct [createRandomShape] : scenario incorrect");
		}
		Functions.createImage(1, 400, 400);

		switch (scenario) {
		case 1:
			Functions.xxRandomPixelDrawing();
			break;

		case 2:
			Functions.xxRandomSquareDrawing();
			break;

		case 3:
			Functions.xxRandomCircleDrawing();
			break;

		case 4:
			Functions.xxRandomShapeDrawing();
			break;

		case 5:
			Functions.xxRandomSquareDrawing();
			Functions.xxRandomRotate();
			break;

		case 6:
			Functions.xxRandomSquareDrawing();
			Functions.xxRandomRotate();
			Functions.xxRandomNoiseLight();
			break;

		case 7:
			Functions.xxRandomCircleDrawing();
			Functions.xxRandomNoiseLight();
			break;

		case 8:
			Functions.xxRandomShapeDrawing();
			Functions.xxRandomRotate();
			break;

		case 9:
			Functions.xxRandomShapeDrawing();
			Functions.xxRandomRotate();
			Functions.xxRandomNoiseLight();
			break;

		case 10:
			Functions.xxRandomSquareDrawing();
			Functions.xxRandomRotate();
			Functions.xxRandomNoiseStrong();
			break;

		case 11:
			Functions.xxRandomCircleDrawing();
			Functions.xxRandomNoiseStrong();
			break;

		case 12:
			Functions.xxRandomShapeDrawing();
			Functions.xxRandomRotate();
			Functions.xxRandomNoiseStrong();
			break;

		default:
			System.out.println("Attention ! : drawRandomShape");
			System.out.println("Scénarios possibles:1,2,3,4,5,6,7,8,9,10,11,12");
			break;

		}

	}

	/**
	 * Vérification de la détection
	 * 
	 * @param typeShape     type de forme détectée (square ou circle)
	 * @param posxShape     abscisse du coin haut gauche du carré englobant
	 * @param posyShape     ordonnée du coin haut gauche du carré englobant
	 * @param coteShape     largeur du carré englobant
	 * @param angleRotation angle de la rotation subit par la forme
	 */
	public static void checkRandomShape(final String typeShape, final int posxShape, final int posyShape,
			final int coteShape, final double angleRotation) {

		if (typeShape == Functions.xxshapeType) {
			System.out.println("Detection type de forme [" + typeShape + "]: **CORRECT**");
		} else {
			System.out.println("Detection type de forme [" + typeShape + "]: --NON CORRECT--");
			System.out.println("Il fallait detecter un : " + Functions.xxshapeType);
		}

		System.out.println("Abscisse coin haut gauche detecte : " + posxShape + " attendu [" + Functions.xxposx + "]");
		System.out
				.println("Ordonnee coin haut gauche detectee : " + posyShape + " attendue [" + Functions.xxposy + "]");
		final int err1 = (int) Math.sqrt((posxShape - Functions.xxposx) * (posxShape - Functions.xxposx)
				+ (posyShape - Functions.xxposy) * (posyShape - Functions.xxposy));
		System.out.println("Erreur commise sur la position: " + err1);
		System.out.println("longueur carre englobant detectee : " + coteShape + " attendue [" + Functions.xxcote + "]");
		final int err2 = Math.abs(coteShape - Functions.xxcote);
		System.out.println("Erreur commise sur la taille: " + err2);
		System.out.println("Angle de rotation detecte : " + angleRotation + " attendu [" + Functions.xxangle + "]");
		final int err3 = Math.round((float) Math.abs(Functions.xxangle - angleRotation));
		System.out.println("Erreur commise sur la rotation: " + err3);
		System.out.println("Erreurs totales commises [Score] : " + (err1 + err2 + err3));
		System.out.println("Rappel : cette valeur doit etre la plus faible possible");
	}

	/**
	 * Trace aléatoirement un pixel noir dans l'image on travail dans l'image de
	 * travail [1] la fonction définit une image de 400 x 400
	 */
	public static void xxRandomPixelDrawing() {
		Functions.createImage(1, 400, 400);
		final Random r = new Random();
		Functions.xxshapeType = "point";
		Functions.xxposx = 30 + r.nextInt(340);
		Functions.xxposy = 30 + r.nextInt(340);
		Functions.xxcote = 1;
		Functions.xxangle = 0;
		Functions.setPixel(1, Functions.xxposx, Functions.xxposy, Color.black);
	}

	/**
	 * Trace aléatoirement un carré ou un disque dans l'image de travail 0 on
	 * travail dans l'image de travail par défaut : 0 celle-ci doit faire au minimum
	 * 400 x 400
	 */
	private static void xxRandomShapeDrawing() {

		final Random r = new Random();

		if (r.nextBoolean()) {
			Functions.xxRandomSquareDrawing();
		} else {
			Functions.xxRandomCircleDrawing();
		}

	}

	/**
	 * Trace un disque circulaire de position et de rayon aléatoire on travail dans
	 * l'image de travail par défaut : 0 celle-ci doit faire au minimum 400 x 400
	 */
	private static void xxRandomCircleDrawing() {
		final Random r = new Random();
		Functions.xxshapeType = "circle";
		Functions.xxposx = 50 + r.nextInt(220);
		Functions.xxposy = 50 + r.nextInt(220);
		Functions.xxcote = 30 + r.nextInt(70);
		Functions.xxangle = 0;
		Functions.drawFillOval(1, Functions.xxposx, Functions.xxposy, Functions.xxcote, Functions.xxcote, Color.black);

	}

	/**
	 * Trace un carré de position et de longueur de coté aléatoire on travail dans
	 * l'image de travail par défaut : 0 celle-ci doit faire au minimum 400 x 400
	 */
	private static void xxRandomSquareDrawing() {
		final Random r = new Random();

		Functions.xxshapeType = "square";
		Functions.xxposx = 50 + r.nextInt(220);
		Functions.xxposy = 50 + r.nextInt(220);
		Functions.xxcote = 30 + r.nextInt(70);
		Functions.xxangle = 0;
		Functions.drawFillRect(1, Functions.xxposx, Functions.xxposy, Functions.xxcote, Functions.xxcote, Color.black);
	}

	/**
	 * On fait subir une rotation à l'image on travail dans l'image de travail par
	 * défaut : 0 celle-ci doit faire au minimum 400 x 400
	 */
	private static void xxRandomRotate() {

		final Random r = new Random();
		Functions.xxangle = r.nextInt(45) * 1.0;
		Functions.rotateImage(1, Functions.xxangle);
	}

	/**
	 * Ajoute un bruit léger dans l'image. on travail dans l'image de travail par
	 * défaut : 0 celle-ci doit faire au minimum 400 x 400
	 */
	private static void xxRandomNoiseLight() {
		final Random r = new Random();

		for (int i = 0; i < 100; i++) {
			Functions.setPixel(1, r.nextInt(400), r.nextInt(400), Color.black);
		}

		for (int i = 0; i < 100; i++) {
			Functions.setPixel(1, r.nextInt(400), r.nextInt(400), Color.white);
		}
	}

	/**
	 * Ajoute un bruit important dans l'image. on travail dans l'image de travail
	 * par défaut : 0 celle-ci doit faire au minimum 400 x 400
	 */
	private static void xxRandomNoiseStrong() {
		final Random r = new Random();

		for (int i = 0; i < 300; i++) {
			Functions.setPixel(1, r.nextInt(400), r.nextInt(400), Color.black);
		}

		for (int i = 0; i < 300; i++) {
			Functions.setPixel(1, Functions.xxposx + r.nextInt(Functions.xxcote),
					Functions.xxposy + r.nextInt(Functions.xxcote), Color.white);
		}
	}

	// ========================================================
	// == Spécifique Démo
	// ========================================================

	/** Détection des bords */
	public static void sideDetection(final int num) {
		if (num < 1 || num > 4) {
			throw new RuntimeException("Fct [sideDetection] : Num image incorrect");
		}
		final int w = Functions.img[num].getWidth();
		final int h = Functions.img[num].getHeight();

		for (int x = 0; x < w; x++) {
			for (int y = 0; y < h; y++) {
				if (Functions.isPixelColor(num, x, y, Color.black)) {
					Functions.setPixel(num, x, y, Color.red);
					break;
				}
			}
		}

		for (int y = 0; y < h; y++) {
			for (int x = 0; x < w; x++) {
				if (Functions.isPixelColor(num, x, y, Color.black)) {
					Functions.setPixel(num, x, y, Color.red);
					break;
				}
			}
		}

		for (int x = w - 1; x > 0; x--) {
			for (int y = h - 1; y > 0; y--) {
				if (Functions.isPixelColor(num, x, y, Color.black)) {
					Functions.setPixel(num, x, y, Color.red);
					break;
				}
			}
		}

		for (int y = h - 1; y > 0; y--) {
			for (int x = w - 1; x > 0; x--) {
				if (Functions.isPixelColor(num, x, y, Color.black)) {
					Functions.setPixel(num, x, y, Color.red);
					break;
				}
			}
		}
	}

	// Filter stuff cause we have com.jhlabs.filters so why not use it

	/**
	 * Box blur filter
	 * 
	 * @param num Image number
	 * @author vinceh121
	 */
	public static void blur(final int num) {
		final BoxBlurFilter blur = new BoxBlurFilter();
		blur.filter(Functions.img[num], Functions.img[num]);
	}

	/**
	 * Edge detection filter
	 * 
	 * @param num Image number
	 * @author vinceh121
	 */
	public static void edge(final int num) {
		final EdgeFilter edge = new EdgeFilter();
		edge.filter(Functions.img[num], Functions.img[num]);
	}

	/**
	 * Image diffuser filter
	 * 
	 * @param num Image number
	 * @author vinceh121
	 */
	public static void diffuse(final int num) {
		final DiffuseFilter diffuse = new DiffuseFilter();
		diffuse.filter(Functions.img[num], Functions.img[num]);
	}

	/**
	 * Contour filter
	 * 
	 * @param num Image number
	 * @author vinceh121
	 */
	public static void contour(final int num) {
		final ContourFilter cont = new ContourFilter();
		cont.filter(Functions.img[num], Functions.img[num]);
	}

	/**
	 * Pixelate an image
	 *
	 * @param num  Image number
	 * @param size Size in pixel of each block
	 * @author vinceh121
	 */
	public static void pixelate(final int num, final int size) {
		final BlockFilter block = new BlockFilter();
		block.setBlockSize(size);
		block.filter(Functions.img[num], Functions.img[num]);
	}

	// Ajout de la gestion de la souris

	/**
	 * Définit une portion de code appelée à chaque clic de souris.
	 * 
	 * @param runnable La portion de code à appeler, ou null si il n'y en a pas.
	 */
	public static void setRunnable(final Runnable runnable) {
		Functions.getPane().setRunnable(runnable);
	}

}
