package org.javascool.proglets.ticTacToe;

import java.awt.Color;
import java.net.Socket;

import org.javascool.tools.socket.SocketClient;
import org.javascool.tools.socket.SocketServer;

/**
 * Définit les fonctions de la proglet.
 *
 * @see <a href="Functions.java.html">source code</a>
 * @serial exclude
 * @author Christophe Béasse
 */
public class Functions {
	// @factory
	private Functions() {
	}
	/*
	 * Méthodes liées au jeu de tic-tac-toe
	 */

	/**
	 * Permet de positionner une marque sur la grille du panel de la proglet
	 * 
	 * @param i    Position horizontale entre 1 et 3.
	 * @param j    Position verticale entre 1 et 3.
	 * @param mark Marque du tictactoe soit 'X', soit 'O', sinon la marque est
	 *             effacée.
	 */
	public static void setGrille(final int i, final int j, final char mark) {
		if (0 < i && i < 4 && 0 < j && j < 4) {
			if (mark == 'O') {
				Panel.tictac[i - 1][j - 1].setText("O");
				Panel.tictac[i - 1][j - 1].setForeground(Color.BLUE);
			} else if (mark == 'X') {
				Panel.tictac[i - 1][j - 1].setText("X");
				Panel.tictac[i - 1][j - 1].setForeground(Color.GREEN);
			} else {
				Panel.tictac[i - 1][j - 1].setText(" ");
			}
		}
	}

	/**
	 * Permet de récupérer la marque sur la grille du panel de la proglet .
	 * 
	 * @param i Position horizontale entre 1 et 3.
	 * @param j Position verticale entre 1 et 3.
	 * @return mark La marque du tictactoe soit 'X', soit 'O', soit ' ' si il n'y a
	 *         pas de marque.
	 */
	public static char getGrille(final int i, final int j) {
		return 0 < i && i < 4 && 0 < j && j < 4 ? Panel.tictac[i - 1][j - 1].getText().charAt(0) : ' ';
	}

	/** Remets à zéro le jeu du tic-tac-toe. */
	public static void resetGrille() {
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				Panel.tictac[i][j].setText(" ");
				Panel.tictac[i][j].setForeground(Color.BLACK);
			}
		}
	}

	/*
	 * Méthodes liées à la connection serveur.
	 */
	private static SocketServer server = new SocketServer();

	/**
	 * Ouverture du socket server.
	 * 
	 * @see SocketServer#open(int)
	 */
	public static void openSocketServer(final int numport) {
		Functions.server.open(numport);
	}

	/** Permet de récupérer un message via le socket server. */
	public static String getMessageViaSocketServer() {
		return Functions.server.getMessage();
	}

	/** Permet d'écrire un message sur le socket server. */
	public static void sendMessageViaSocketServer(final String text) {
		Functions.server.sendMessage(text);
	}

	/** Renvoie la socket elle-même pour accéder aux fonctions bas-niveau. */
	public static Socket getSocketServer() {
		return Functions.server.getSocket();
	}

	/** Fermeture du socket server. */
	public static void closeSocketServer() {
		Functions.server.close();
	}

	/*
	 * Méthodes liées à la connection serveur.
	 */
	private static SocketClient client = new SocketClient();

	/**
	 * Ouverture du socket client.
	 * 
	 * @see SocketClient#open(String, int)
	 */
	public static void openSocketClient(final String hostname, final int numport) {
		Functions.client.open(hostname, numport);
	}

	/** Permet de récupérer un message via le socket client. */
	public static String getMessageViaSocketClient() {
		return Functions.client.getMessage();
	}

	/** Permet d'écrire un message sur le socket client. */
	public static void sendMessageViaSocketClient(final String text) {
		Functions.client.sendMessage(text);
	}

	/** Renvoie la socket elle-même pour accéder aux fonctions bas-niveau. */
	public static Socket getSocketClient() {
		return Functions.client.getSocket();
	}

	/** Fermeture du socket client. */
	public static void closeSocketClient() {
		Functions.client.close();
	}
} // class functions
