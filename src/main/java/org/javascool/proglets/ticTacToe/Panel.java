package org.javascool.proglets.ticTacToe;

import java.awt.Font;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JPanel;

/**
 * Définit le panneau graphique de la proglet.
 *
 * @see <a href="Panel.java.html">source code</a>
 * @serial exclude
 * @author Christophe Béasse <oceank2@gmail.com>
 */
public class Panel extends JPanel {
	private static final long serialVersionUID = 1L;

	// Construction de la proglet
	public Panel() {
		final Font f = new Font("Dialog", Font.BOLD, 84);
		this.setLayout(new GridLayout(3, 3));
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				Panel.tictac[i][j] = new JButton(" ");
				Panel.tictac[i][j].setEnabled(false);
				Panel.tictac[i][j].setFont(f);
				this.add(Panel.tictac[i][j]);
			}
		}
	}

	/** Tableau de boutons formant la grille 3x3 du jeu de tic-tac-toe. */
	public static JButton tictac[][] = new JButton[3][3];
}
