package org.javascool.proglets.opengl;

import java.nio.IntBuffer;

import org.javascool.macros.Stdout;
import org.lwjgl.glfw.Callbacks;
import org.lwjgl.glfw.GLFW;
import org.lwjgl.glfw.GLFWErrorCallback;
import org.lwjgl.glfw.GLFWVidMode;
import org.lwjgl.glfw.GLFWWindowSizeCallback;
import org.lwjgl.opengl.GL;
import org.lwjgl.opengl.GL11;
import org.lwjgl.system.MemoryStack;
import org.lwjgl.system.MemoryUtil;

/**
 * Functions for the opengl proglet
 *
 * @author vinceh121
 */
public final class Functions {
	private static long window;

	public static void initLwjgl() {
		// GL.create();
		GLFWErrorCallback.createPrint(System.out).set();
		if (!GLFW.glfwInit()) {
			Stdout.println("Could not init GLFW");
		}
	}

	public static void renderStart() {
		GL11.glClearColor(0.0f, 0.0f, 0.0f, 1.0f); // Set background color to black and opaque
		GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT); // Clear the color buffer (background)
		if (GLFW.glfwWindowShouldClose(Functions.window)) {
			Functions.terminateGlfw();
			throw new IllegalStateException("Window closed by user");
		}
	}

	public static void renderEnd() {
		GL11.glFlush(); // Render now
		GLFW.glfwSwapBuffers(Functions.window); // swap the color buffers
		// Poll for window events. The key callback above will only be
		// invoked during this call.
		GLFW.glfwPollEvents();
	}

	public static long basicWindow() {
		// Configure GLFW
		GLFW.glfwDefaultWindowHints(); // optional, the current window hints are already the default
		GLFW.glfwWindowHint(GLFW.GLFW_VISIBLE, GLFW.GLFW_FALSE); // the window will stay hidden after creation
		GLFW.glfwWindowHint(GLFW.GLFW_RESIZABLE, GLFW.GLFW_TRUE); // the window will be resizable

		// Create the window
		Functions.window = GLFW.glfwCreateWindow(300, 300, "Javasnice OpenGl Window", MemoryUtil.NULL, MemoryUtil.NULL);
		if (Functions.window == MemoryUtil.NULL) {
			throw new RuntimeException("Failed to create the GLFW window");
		}

		// Setup a key callback. It will be called every time a key is pressed, repeated
		// or released.
		GLFW.glfwSetKeyCallback(Functions.window, (window, key, scancode, action, mods) -> {
			if (key == GLFW.GLFW_KEY_ESCAPE && action == GLFW.GLFW_RELEASE) {
				GLFW.glfwSetWindowShouldClose(window, true); // We will detect this in the rendering loop
			}
		});

		// Automatically change the viewport size when the window is resized.
		GLFW.glfwSetWindowSizeCallback(Functions.window, new GLFWWindowSizeCallback() {

			@Override
			public void invoke(final long window, final int width, final int height) {
				GL11.glViewport(0, 0, width, height);
			}
		});

		// Get the thread stack and push a new frame
		try (MemoryStack stack = MemoryStack.stackPush()) {
			final IntBuffer pWidth = stack.mallocInt(1); // int*
			final IntBuffer pHeight = stack.mallocInt(1); // int*

			// Get the window size passed to glfwCreateWindow
			GLFW.glfwGetWindowSize(Functions.window, pWidth, pHeight);

			// Get the resolution of the primary monitor
			final GLFWVidMode vidmode = GLFW.glfwGetVideoMode(GLFW.glfwGetPrimaryMonitor());

			// Center the window
			GLFW.glfwSetWindowPos(Functions.window, (vidmode.width() - pWidth.get(0)) / 2,
					(vidmode.height() - pHeight.get(0)) / 2);
		} // the stack frame is popped automatically

		// Make the OpenGL context current
		GLFW.glfwMakeContextCurrent(Functions.window);
		// Enable v-sync
		GLFW.glfwSwapInterval(1);

		// Make the window visible
		GLFW.glfwShowWindow(Functions.window);

		// This line is critical for LWJGL's interoperation with GLFW's
		// OpenGL context, or any context that is managed externally.
		// LWJGL detects the context that is current in the current thread,
		// creates the GLCapabilities instance and makes the OpenGL
		// bindings available for use.
		GL.createCapabilities();
		return Functions.window;
	}

	public static void discardWindow(final long window) {
		// Free the window callbacks and destroy the window
		Callbacks.glfwFreeCallbacks(window);
		GLFW.glfwDestroyWindow(window);
	}

	public static void terminateGlfw() {
		// Terminate GLFW and free the error callback
		GLFW.glfwTerminate();
		// glfwSetErrorCallback(null).free(); Let's keep the call back in case
	}

	public static void closeLastWindow() {
		GLFW.glfwSetWindowShouldClose(Functions.window, true);
	}

	public static void terminateLwjgl() {
		Functions.terminateGlfw();
	}
}
