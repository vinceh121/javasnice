package org.javascool.proglets.opengl;

/**
 * Adds imports for the opengl proglet
 *
 */
public class Translator extends org.javascool.core.Translator { // Disgusting double names
	@Override
	public String getImports() {
		return "import org.lwjgl.*;\n"
				+ "import org.lwjgl.glfw.*;\n"
				+ "import org.lwjgl.opengl.*;\n"
				+ "import org.lwjgl.system.*;\n"
				+ "import java.nio.*;\n"
				+ "import static org.lwjgl.glfw.Callbacks.*;\n"
				+ "import static org.lwjgl.glfw.GLFW.*;\n"
				+ "import static org.lwjgl.opengl.GL46.*;\n"
				+ "import static org.lwjgl.system.MemoryStack.*;\n"
				+ "import static org.lwjgl.system.MemoryUtil.*;";
	}
}
