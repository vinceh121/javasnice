package org.javascool.proglets.algoDeMaths;

import org.javascool.macros.Macros;

/**
 * Defines the functions of the proglet
 *
 * @see <a href="Functions.java.html">source code</a>
 * @serial exclude
 */
public class Functions {
	// @factory
	private Functions() {
	}

	private static Panel getPane() {
		return (Panel) Macros.getProgletPane();
	}

	/**
	 * Initialize drawing
	 * 
	 * @param Xscale Max horizontal scale, X will be drawn in
	 *               [-Xscale, Xscale], or by default [-1, 1].
	 * @param Yscale Max vertican scale, Y will be drawn in
	 *               [-Yscale, Yscale], or by default [-1, 1].
	 */
	public static void reset(final double Xscale, final double Yscale) {
		Functions.getPane().inputX.setScale(-Xscale, Xscale, 0.001);
		Functions.getPane().inputY.setScale(-Yscale, Yscale, 0.001);
		Functions.getPane().scope.reset(0, 0, Xscale, Yscale);
		Functions.getPane().runnable = null;
		org.javascool.gui.Desktop.getInstance().focusOnProgletPanel();
	}

	/**
	 * Initialise le tracé.
	 * 
	 * @param Xmin Min horizontal scale, X will de drawn in [-Xmin,
	 *             Xmax], or by default [-1, 1].
	 * @param Xmax Max horizontal scale, X will be drawn in [-Xmin,
	 *             Xmax], or by default [-1, 1].
	 * @param Ymin Min vertical scale, Y will be drawn in [-Ymin,
	 *             Ymay], by default [-1, 1].
	 * @param Ymax Max vertical scale, Y will be drawn in [-Ymin,
	 *             Ymax], by default [-1, 1].
	 */
	public static void reset(final double Xmin, final double Xmax, final double Ymin, final double Ymax) {
		Functions.getPane().inputX.setScale(Xmin, Xmax, 0.001);
		Functions.getPane().inputY.setScale(Ymin, Ymax, 0.001);
		Functions.getPane().scope.reset((Xmin + Xmax) / 2, (Ymin + Ymax) / 2, (Xmax - Xmin) / 2, (Ymax - Ymin) / 2);
		Functions.getPane().runnable = null;
		org.javascool.gui.Desktop.getInstance().focusOnProgletPanel();
	}

	/*
	 * @see #reset(double, double, double, double)
	 */
	public static void reset() {
		Functions.reset(1, 1);
	}

	/**
	 * Change la valeur d'un point du tracé.
	 * 
	 * @param x Abcisse de la courbe, dans [-X, X], par défaut [-1, 1].
	 * @param y Ordonnée de la courbe, dans [-Y, Y], par défaut [-1, 1].
	 * @param c Numéro de la courbe: 0 (noir, défaut), 1 (brun), 2 (rouge), 3
	 *          (orange), 4 (jaune), 5 (vert), 6 (bleu), 7 (violet), 8 (gris), 9
	 *          (blanc).
	 * @return L'objet graphique créé, utilisé pour détruire l'objet ensuite.
	 */
	public static Object setPoint(final double x, final double y, final int c) {
		return Functions.getPane().scope.add(x, y, c);
	}

	/*
	 * @see #setPoint(double, double, int)
	 */
	public static Object setPoint(final double x, final double y) {
		return Functions.setPoint(x, y, 0);
	}

	/**
	 * Ajoute une chaîne de caractères au tracé.
	 * 
	 * @param x Abcisse du coin inférieur gauche de la chaîne, dans [-X, X], par
	 *          défaut [-1, 1].
	 * @param y Ordonnée du coin inférieur gauche de la chaîne, dans [-Y, Y], par
	 *          défaut [-1, 1].
	 * @param s Valeur de la chaîne de caractères.
	 * @param c Couleur du point: 0 (noir, défaut), 1 (brun), 2 (rouge), 3 (orange),
	 *          4 (jaune), 5 (vert), 6 (bleu), 7 (violet), 8 (gris), 9 (blanc).
	 * @return L'objet graphique créé, utilisé pour détruire l'objet ensuite.
	 */
	public static Object addString(final double x, final double y, final String s, final int c) {
		return Functions.getPane().scope.add(x, y, s, c);
	}

	/*
	 * @see #addString(double, double, double, String, int)
	 */
	public static Object addString(final double x, final double y, final String s) {
		return Functions.addString(x, y, s, 0);
	}

	/**
	 * Trace un rectangle.
	 * 
	 * @param xmin Abcisse inférieure gauche, dans [-X, X], par défaut [-1, 1].
	 * @param ymin Ordonnée inférieure gauche, dans [-Y, Y], par défaut [-1, 1].
	 * @param xmax Abcisse supérieure droite, dans [-X, X], par défaut [-1, 1].
	 * @param ymax Ordonnée supérieure droite, dans [-Y, Y], par défaut [-1, 1].
	 * @param c    Numéro de la courbe: 0 (noir, défaut), 1 (brun), 2 (rouge), 3
	 *             (orange), 4 (jaune), 5 (vert), 6 (bleu), 7 (violet), 8 (gris), 9
	 *             (blanc).
	 * @return L'objet graphique créé, utilisé pour détruire l'objet ensuite.
	 */
	public static Object addRectangle(final double xmin, final double ymin, final double xmax, final double ymax,
			final int c) {
		return Functions.getPane().scope.addRectangle(xmin, ymin, xmax, ymax, c);
	}

	/*
	 * @see #addRectangle(double, double, double, double, int)
	 */
	public static Object addRectangle(final double xmin, final double ymin, final double xmax, final double ymax) {
		return Functions.addRectangle(xmin, ymin, xmax, ymax, 0);
	}

	/**
	 * Trace un block rectangulaire.
	 * 
	 * @param xmin Abcisse inférieure gauche, dans [-X, X], par défaut [-1, 1].
	 * @param ymin Ordonnée inférieure gauche, dans [-Y, Y], par défaut [-1, 1].
	 * @param xmax Abcisse supérieure droite, dans [-X, X], par défaut [-1, 1].
	 * @param ymax Ordonnée supérieure droite, dans [-Y, Y], par défaut [-1, 1].
	 * @param c    Numéro de la couleur du block: 0 (noir, défaut), 1 (brun), 2
	 *             (rouge), 3 (orange), 4 (jaune), 5 (vert), 6 (bleu), 7 (violet), 8
	 *             (gris), 9 (blanc).
	 * @return L'objet graphique créé, utilisé pour détruire l'objet ensuite.
	 */
	public static Object addBlock(final double xmin, final double ymin, final double xmax, final double ymax,
			final int c) {
		return Functions.getPane().scope.add(xmin, ymin, xmax - xmin, ymax - ymin, c, -1);
	}

	/*
	 * @see #addBlock(double, double, double, double, int)
	 */
	public static Object addBlock(final double xmin, final double ymin, final double xmax, final double ymax) {
		return Functions.addBlock(xmin, ymin, xmax, ymax, 0);
	}

	/**
	 * Trace une ligne.
	 * 
	 * @param x1 Abcisse du 1er point, dans [-X, X], par défaut [-1, 1].
	 * @param y1 Ordonnée du 1er point, dans [-Y, Y], par défaut [-1, 1].
	 * @param x2 Abcisse du 2eme point, dans [-X, X], par défaut [-1, 1].
	 * @param y2 Ordonnée du 2eme point, dans [-Y, Y], par défaut [-1, 1].
	 * @param c  Numéro de la courbe: 0 (noir, défaut), 1 (brun), 2 (rouge), 3
	 *           (orange), 4 (jaune), 5 (vert), 6 (bleu), 7 (violet), 8 (gris), 9
	 *           (blanc).
	 * @return L'objet graphique créé, utilisé pour détruire l'objet ensuite.
	 */
	public static Object addLine(final double x1, final double y1, final double x2, final double y2, final int c) {
		return Functions.getPane().scope.add(x1, y1, x2, y2, c);
	}

	/*
	 * @see #addLine(double, double, double, double, int)
	 */
	public static Object addLine(final double x1, final double y1, final double x2, final double y2) {
		return Functions.addLine(x1, y1, x2, y2, 0);
	}

	/**
	 * Trace une point.
	 * 
	 * @param x1 Abcisse du 1er point, dans [-X, X], par défaut [-1, 1].
	 * @param y1 Ordonnée du 1er point, dans [-Y, Y], par défaut [-1, 1].
	 * @param c  Numéro de la courbe: 0 (noir, défaut), 1 (brun), 2 (rouge), 3
	 *           (orange), 4 (jaune), 5 (vert), 6 (bleu), 7 (violet), 8 (gris), 9
	 *           (blanc).
	 * @return L'objet graphique créé, utilisé pour détruire l'objet ensuite.
	 */
	public static Object addPoint(final double x1, final double y1, final int c) {
		return Functions.getPane().scope.add(x1, y1, x1, y1, c);
	}

	/*
	 * @see #addPoint(double, double, int)
	 */
	public static Object addPoint(final double x1, final double y1) {
		return Functions.addPoint(x1, y1, 0);
	}

	/**
	 * Trace un cercle.
	 * 
	 * @param x Abcisse du centre, dans [-X, X], par défaut [-1, 1].
	 * @param y Ordonnée du centre, dans [-Y, Y], par défaut [-1, 1].
	 * @param r Rayon du cercle.
	 * @param c Numéro de la courbe: 0 (noir, défaut), 1 (brun), 2 (rouge), 3
	 *          (orange), 4 (jaune), 5 (vert), 6 (bleu), 7 (violet), 8 (gris), 9
	 *          (blanc).
	 * @return L'objet graphique créé, utilisé pour détruire l'objet ensuite.
	 */
	public static Object addCircle(final double x, final double y, final double r, final int c) {
		return Functions.getPane().scope.add(x, y, r, c);
	}

	/*
	 * @see #addCircle(double, double, double, int)
	 */
	public static Object addCircle(final double x, final double y, final double r) {
		return Functions.addCircle(x, y, r, 0);
	}

	/**
	 * Détruit l'objet graphique spécifié.
	 * 
	 * @param object L'objet à détruire.
	 * @return La valeur true si l'objet existait, false sinon.
	 */
	public boolean remove(final Object object) {
		return Functions.getPane().scope.remove(object);
	}

	/** Renvoie la valeur horizontale du réticule. */
	public static double getX() {
		return Functions.getPane().inputX.getValue();
	}

	/** Renvoie la valeur verticale du réticule. */
	public static double getY() {
		return Functions.getPane().inputY.getValue();
	}

	/**
	 * Définit la position du réticule.
	 * 
	 * @param x Abscisse du réticule, dans [-X, X], par défaut [-1, 1].
	 * @param y Reticule ordinate, dans [-Y, Y], par défaut [-1, 1].
	 */
	public static void setReticule(final double x, final double y) {
		Functions.getPane().scope.setReticule(x, y);
	}

	/**
	 * Définit une portion de code appelée à chaque modification du réticule.
	 * 
	 * @param runnable La portion de code à appeler, ou null si il n'y en a pas.
	 */
	public static void setRunnable(final Runnable runnable) {
		Functions.getPane().runnable = runnable;
	}
}
