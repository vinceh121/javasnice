package org.javascool.proglets.algoDeMaths;

import java.awt.BorderLayout;

import javax.swing.JPanel;

import org.javascool.widgets.CurveOutput;
import org.javascool.widgets.NumberInput;

/**
 * Définit le panneau graphique de la proglet.
 *
 * @see <a href="Panel.java.html">source code</a>
 * @serial exclude
 */
public class Panel extends JPanel {
	private static final long serialVersionUID = 1L;

	/** Tracé de courbes. */
	public CurveOutput scope;
	/** Entrées de valeurs numériques. */
	public NumberInput inputX, inputY;
	/** Runnable à appeler. */
	public Runnable runnable = null;

	// @bean
	public Panel() {
		super(new BorderLayout());
		this.add(this.scope = new CurveOutput().reset(0, 0, 1, 1), BorderLayout.CENTER);
		final JPanel input = new JPanel(new BorderLayout());
		input.add(this.inputX = new NumberInput().setText("X").setScale(-1, 1, 0.001), BorderLayout.NORTH);
		input.add(this.inputY = new NumberInput().setText("Y").setScale(-1, 1, 0.001), BorderLayout.SOUTH);
		final Runnable run1 = () -> {
			Panel.this.inputX.setValue(Panel.this.scope.getReticuleX());
			Panel.this.inputY.setValue(Panel.this.scope.getReticuleY());
			if (Panel.this.runnable != null) {
				Panel.this.runnable.run();
			}
		};
		final Runnable run2
				= () -> Panel.this.scope.setReticule(Panel.this.inputX.getValue(), Panel.this.inputY.getValue());
		this.scope.setRunnable(run1);
		this.inputX.setRunnable(run2);
		this.inputY.setRunnable(run2);
		this.add(input, BorderLayout.SOUTH);
	}

}
