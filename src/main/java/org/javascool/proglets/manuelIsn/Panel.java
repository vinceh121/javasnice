//  Ces programmes sont sous licence CeCILL-B V1.
// ----------------------------------------------------------------------
// Isn.java
// ----------------------------------------------------------------------
// Julien Cervelle et Gilles Dowek, version javascool.

package org.javascool.proglets.manuelIsn;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Shape;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

import javax.swing.JComponent;

import org.javascool.gui.Core;

/**
 * Définit une proglet qui permet de manipuler les pixels d'une image.
 *
 * @see <a href="Panel.java.html">code source</a>
 * @serial exclude
 */
public class Panel extends JComponent {
	private static final long serialVersionUID = 1L;

	// @bean
	public Panel() {
	}

	// Définit une forme géométrique colorée
	private class ColorShape {
		Shape shape;
		Color foreground, background;
	}

	// Liste des formes géométriques
	private final ArrayList<ColorShape> shapes = new ArrayList<>();

	// Définit une icone à afficher
	private class TranslatedIcon {
		BufferedImage img;
		Point p;
	}

	// Liste des formes géométriques
	private final ArrayList<TranslatedIcon> icons = new ArrayList<>();
	// Image à afficher en fond
	BufferedImage background = null;

	/** Initialise le graphique avec un tracé de tailles (w, h). */
	public void reset(final int w, final int h) {
		this.background = null;
		this.setPreferredSize(new Dimension(w, h));
		this.icons.clear();
		this.shapes.clear();
		this.repaint();
	}

	/**
	 * Initialise le graphique avec une image stockée dans le fichier à la location
	 * donnée.
	 */
	public void reset(final String location) {
		try {
			this.background = org.javascool.tools.image.ImageUtils.loadImage(location);
			this.setPreferredSize(new Dimension(this.background.getWidth(), this.background.getHeight()));
			this.icons.clear();
			this.shapes.clear();
			this.repaint();
		} catch (final Exception e) {
			System.out.println("Impossible d'afficher '" + location + "'");
			if (Core.DEBUG) {
				System.out.println(e.toString());
			}
			e.printStackTrace();
		}
	}

	/** Ajoute une forme géométrique au graphique. */
	public void add(final Shape shape, final Color foreground, final Color background) {
		final ColorShape s = new ColorShape();
		s.shape = shape;
		s.foreground = foreground;
		s.background = background;
		this.shapes.add(s);
		this.repaint();
	}

	/** Ajoute une icone au graphique au graphique. */
	public Point add(final String image, final int x, final int y) {
		final TranslatedIcon i = new TranslatedIcon();
		i.img = org.javascool.tools.image.ImageUtils.loadImage(image);
		i.p = new Point();
		i.p.x = x;
		i.p.y = y;
		this.icons.add(i);
		this.repaint();
		return i.p;
	}

	/** Routine interne de tracé, ne pas utiliser. */
	@Override
	public void paint(final Graphics g) {
		g.setColor(Color.WHITE);
		g.fillRect(0, 0, this.getWidth(), this.getHeight());
		super.paint(g);
		if (this.background != null) {
			g.drawImage(this.background, 0, 0, this);
		}
		try {
			for (final TranslatedIcon i : this.icons) {
				g.drawImage(i.img, i.p.x, i.p.y, null);
			}
		} catch (final Exception e) {
			e.printStackTrace();
		}
		try {
			for (final ColorShape s : this.shapes) {
				if (s.background != null) {
					g.setColor(s.background);
					((Graphics2D) g).fill(s.shape);
				}
				if (s.foreground != null) {
					g.setColor(s.foreground);
					((Graphics2D) g).draw(s.shape);
				}
			}
		} catch (final Exception e) {}
	}
}
