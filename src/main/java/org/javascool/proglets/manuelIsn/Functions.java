//  Ces programmes sont sous licence CeCILL-B V1.
// ----------------------------------------------------------------------
// Isn.java
// ----------------------------------------------------------------------
// Julien Cervelle et Gilles Dowek, version javascool.

package org.javascool.proglets.manuelIsn;

// graphisme
import java.awt.Color;
import java.awt.Font;
import java.awt.Point;
import java.awt.Shape;
// tracé de texte
import java.awt.font.FontRenderContext;
import java.awt.font.GlyphVector;
import java.awt.geom.AffineTransform;
import java.awt.geom.Area;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D;

import org.javascool.macros.Macros;

/**
 * Définit les fonctions de la proglet qui permettent de faire les exercices
 * Isn.
 *
 * @see <a href="Functions.java.html">code source</a>
 * @serial exclude
 */
public class Functions {
	// @factory
	private Functions() {
	}

	/** Renvoie l'instance de la proglet pour accéder à ses éléments. */
	private static Panel getPane() {
		return (Panel) Macros.getProgletPane();
	}

	// ----------------------------------------------------------------------
	// graphisme
	// ----------------------------------------------------------------------

	/**
	 * Initialise le graphique avec un tracé de tailles (w, h). - Dans ce contexte
	 * le titre "s" et la position (x, y) sont sans importance, on pourra donc
	 * utiliser : <tt>initDrawing("", 0, 0, w, h);</tt>
	 */
	public static void initDrawing(final String s, final int x, final int y, final int w, final int h) {
		Functions.initDrawing(w, h);
	}

	/**
	 * @see #initDrawing(String, int, int, int, int)
	 */
	public static void initDrawing(final int w, final int h) {
		Functions.getPane().reset(w, h);
		org.javascool.gui.Desktop.getInstance().focusOnProgletPanel();
	}

	/** Peint un pixel en (x, y) de couleur RGB = (c1, c2, c3). */
	public static void drawPixel(final double x, final double y, final int c1, final int c2, final int c3) {
		Functions.getPane().add(new Rectangle2D.Double(x, y, 0, 0), new Color(c1, c2, c3), Color.WHITE);
	}

	/**
	 * Trace un rectangle de coin supérieur gauche (x, y), de tailles (a, b) et de
	 * couleur RGB = (c1, c2, c3).
	 */
	public static void drawRect(final double x, final double y, final double a, final double b, final int c1,
			final int c2, final int c3) {
		Functions.getPane().add(new Rectangle2D.Double(x, y, a, b), new Color(c1, c2, c3), Color.WHITE);
	}

	/**
	 * Remplit un rectangle de coin supérieur gauche (x, y), de tailles (a, b) avec
	 * la couleur RGB = (c1, c2, c3).
	 */
	public static void paintRect(final double x, final double y, final double a, final double b, final int c1,
			final int c2, final int c3) {
		Functions.getPane().add(new Rectangle2D.Double(x, y, a, b), new Color(c1, c2, c3), new Color(c1, c2, c3));
	}

	/**
	 * Trace un segment de droite de (x1, y1) à (x2, y2) et de couleur RGB = (c1,
	 * c2, c3).
	 */
	public static void drawLine(final double x1, final double y1, final double x2, final double y2, final int c1,
			final int c2, final int c3) {
		Functions.getPane().add(new Line2D.Double(x1, y1, x2, y2), new Color(c1, c2, c3), Color.WHITE);
	}

	/**
	 * Trace un cercle de centre (cx, cy) de rayon r et de couleur RGB = (c1, c2,
	 * c3).
	 */
	public static void drawCircle(final double cx, final double cy, final double r, final int c1, final int c2,
			final int c3) {
		Functions.getPane().add(new Ellipse2D.Double(cx - r, cy - r, 2 * r, 2 * r), new Color(c1, c2, c3), null);
	}

	/**
	 * Remplit un cercle de centre (cx, cy) de rayon r avec la couleur RGB = (c1,
	 * c2, c3).
	 */
	public static void paintCircle(final double cx, final double cy, final double r, final int c1, final int c2,
			final int c3) {
		Functions.getPane()
				.add(new Ellipse2D.Double(cx - r, cy - r, 2 * r, 2 * r), new Color(c1, c2, c3), new Color(c1, c2, c3));
	}

	/**
	 * Affiche une imagette à un endroit donné et renvoie sa position pour permettre
	 * de la déplacer.
	 */
	public static Point drawImage(final String location, final int x, final int y) {
		return Functions.getPane().add(location, x, y);
	}

	/**
	 * Écrit un texte au point (x, y) avec une fonte de taille size et la couleur
	 * RGB = (c1, c2, c3).
	 */
	public static Rectangle2D drawText(final String text, final double x, final double y, final int size, final int c1,
			final int c2, final int c3) {
		final FontRenderContext ctx = new FontRenderContext(null, true, true);
		final Font font = new Font(Font.SANS_SERIF, Font.PLAIN, size);
		final GlyphVector vector = font.createGlyphVector(ctx, text);
		final Color color = new Color(c1, c2, c3);
		final Shape shape = vector.getOutline();
		Area area = new Area(shape);
		area = area.createTransformedArea(AffineTransform.getTranslateInstance(x, y));
		Functions.getPane().add(area, color, color);
		return area.getBounds2D();
	}

	/** Affiche une image stockée dans le fichier à la location donnée. */
	public static void showImage(final String location) {
		Functions.getPane().reset(location);
	}
}
