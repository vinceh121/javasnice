/*******************************************************************************
* Thierry.Vieville@sophia.inria.fr, Copyright (C) 2009.  All rights reserved. *
*******************************************************************************/

package org.javascool.proglets.syntheSons;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.io.IOException;

// Used to define an audio stream
import javax.sound.sampled.AudioInputStream;
// Used to show a curve
import javax.swing.JPanel;

import org.javascool.tools.sound.SoundBit;

/**
 * Defines the drawing of a SoundBit spectrum and trace's start. <div>Opens a
 * frame, drawing:
 * <ul>
 * <li>drawing the frequencies amplitudes (in red, in normalized dB (log
 * coordinates)), frequencies being drawn between A0 (27.5Hz) and A9 (6400Hz)
 * around A3 (440Hz);</li>
 * <li>drawing the 1st samples of the signal (in yellow, the 1st
 * <tt>11</tt>ms).</li>
 * </ul>
 * while the sound name main frequency and spectral magnitude is printed.
 *
 * @see <a href="SoundBitPanel.java.html">code source</a>
 * @serial exclude
 */
public class SoundBitPanel extends JPanel {
	private static final long serialVersionUID = 1L;

	int hsize = 16 * 31, wsize = 3, vsize = 160, b = 0, m = 20, width = 2 * this.m + this.hsize,
			height = this.b + 4 * this.m + 3 * this.vsize, height2 = this.b + 2 * this.m + 2 * this.vsize;
	double f0 = 440.0 / 16, f1 = 440.0 * 16;

	private double data[];
	private double mag[];
	private double mag_max;
	// private String label;

	/**
	 * Defines the spectrum and trace's start of the sound.
	 * 
	 * @param sound   The sound to visualize.
	 * @param channel Left 'l' or right 'r' channel.
	 */
	public void reset(final SoundBit sound, final char channel) { // XXX: Some code was commented because it seems
																	// useless
		// Calculates the spectrum magnitude and phase
		this.data = SoundBitPanel.getData(sound, channel);
		// Gets the smoothed spectrum magnitude and calculates the maximum
		// double f_max = 0;
		this.mag_max = 0;
		this.mag = SoundBitPanel.getFFTMagnitude(this.data, this.f0, this.f1, this.hsize, this.wsize);
		for (int i = 0; i <= this.hsize; i++) {
			// double f = f0 * Math.pow(f1 / f0, i / (double) hsize);
			if (this.mag_max < this.mag[i]) {
				// f_max = f;
				this.mag_max = this.mag[i];
			}
		}
		// label = sound.getName() + " (|fft| < " + ((int) mag_max) + ", f_max = " +
		// ((int) f_max) + "Hz) ";
		this.repaint();
	}

	/**
	 * Routine interne de tracé, ne pas utiliser.
	 *
	 */
	@Override
	public void paint(final Graphics g) {
		super.paint(g);
		// Backgrounds and axis
		g.setColor(Color.GRAY);
		g.fillRect(this.m, this.b + this.m, this.hsize, 2 * this.vsize);
		g.fillRect(this.m, this.height2 + this.m, this.hsize, this.vsize);
		g.setColor(Color.BLACK);
		g.drawLine(this.m, this.height2, this.width - this.m, this.height2);
		g.drawLine(this.m / 2, this.height2 + this.m + this.vsize / 2, this.m, this.height2 + this.m + this.vsize / 2);
		g.drawLine(this.width - this.m / 2, this.height2 + this.m + this.vsize / 2, this.width - this.m,
				this.height2 + this.m + this.vsize / 2);
		g.setFont(new Font("Times", 0, 10));
		for (int i = this.m; i <= this.width - this.m; i += this.hsize / 16) {
			g.drawLine(i, this.height2 - (i == this.width / 2 ? this.m : this.m / 2), i,
					this.height2 + (i == this.width / 2 ? this.m - 2 : this.m / 2));
			final double f = this.f0 * Math.pow(this.f1 / this.f0, (i - this.m) / (double) this.hsize);
			if (i < this.width - this.m) {
				g.drawString(Integer.toString((int) f), i + 1, this.height2 - 1);
			}
		}
		// Amplitude and phase curves
		for (int i = this.m, a1 = 0, d1 = 0; i <= this.width - this.m; i++) {
			final double a = Math.log(1 + 9 * this.mag[i - this.m] / this.mag_max) / Math.log(10);
			int a0 = this.height2 - this.m - (int) Math.rint(2 * this.vsize * a);
			if (a0 < this.b + this.m) {
				a0 = this.b + this.m;
			}
			g.setColor(Color.RED);
			if (i > this.m) {
				g.drawLine(i - 1, a1, i, a0);
			}
			a1 = a0;
			// Drawing the 1st data samples
			final int d0 = this.height - this.m - (int) Math
					.rint(this.vsize * (0.5 * (1 + (i - this.m < this.data.length ? this.data[i - this.m] : 0))));
			g.setColor(Color.YELLOW);
			if (i > this.m) {
				g.drawLine(i - 1, d1, i, d0);
			}
			d1 = d0;
		}
		g.setColor(Color.BLACK);
		g.setFont(new Font("Times", Font.BOLD, 16));
		g.drawString("Spectrum amplitude", this.m + this.m / 2, this.b + 2 * this.m);
		g.drawString("Begining of signal", this.m + this.m / 2, this.height2 + 2 * this.m);
	}

	/**
	 * Converts a mono/stereo 16bit stream to a data buffer.
	 * 
	 * @param sound   The audio stream to convert.
	 * @param channel Left 'l' or right 'r' channel.
	 */
	public static double[] getData(final SoundBit sound, final char channel) {
		final AudioInputStream stream = sound.getStream();
		SoundBit.checkFormat(stream);
		if (stream.getFrameLength() > Integer.MAX_VALUE) {
			throw new IllegalArgumentException("Cannot convert huge audio stream to buffer");
		}
		final int length = (int) stream.getFrameLength();
		final double data[] = new double[length];
		final int n = stream.getFormat().getChannels() * 2,
				o = stream.getFormat().getChannels() == 1 || channel == 'l' ? 0 : 2;
		final byte read[] = new byte[n];
		try {
			for (int i = 0; i < length; i++) {
				stream.read(read);
				final int h = read[o + 1], l = read[o], v = 128 + h << 8 | 128 + l;
				data[i] = 1 * (v / 32767.0 - 1);
			}
		} catch (final IOException e) {
			throw new RuntimeException(e + " when reading the audio stream " + sound.getName());
		}
		return data;
	}

	/**
	 * Renvoie le spectre de fréquence d'un signal.
	 * 
	 * @param data  Les données en entrées.
	 * @param f0    La fréquence minimale.
	 * @param f1    La frequence maximale.
	 * @param hsize Le nombre fréquence.
	 * @param wsize La taille de la fenêtre de lissage, 0 pour éviter le lissage.
	 * @return Un tableau de taille <tt>hsize + 1</tt> des amplitudes à une
	 *         fréquence donnée, en coordonnées logarithmique.
	 *         <p>
	 *         La valeur d'indice <tt>i</tt> correspond à la fréquence
	 *         <tt>f0 * Math.pow(f1 / f0, i / (double) hsize)</tt>.
	 *         </p>
	 */
	public static double[] getFFTMagnitude(final double data[], final double f0, final double f1, final int hsize,
			int wsize) {
		final Complex fft[] = SoundBitPanel.getFFT(data);
		final double mag[] = new double[hsize + 1];
		for (int i = 0; i <= hsize; i++) {
			final double f = f0 * Math.pow(f1 / f0, i / (double) hsize);
			final int k = (int) Math.rint(fft.length * f / SoundBit.SAMPLING);
			mag[i] = Math.sqrt(fft[k].x * fft[k].x + fft[k].y * fft[k].y);
		}
		// Smooths the spectrum magnitude and calculates the maximum
		final double smag[] = new double[hsize + 1];
		wsize = Math.max(0, wsize);
		for (int i = 0, w = wsize; i <= hsize; i++) {
			double n = 0, v = 0;
			for (int j = Math.max(0, i - w); j <= Math.min(hsize, i + w); j++) {
				n++;
				v += mag[j];
			}
			smag[i] = v / n;
		}
		return smag;
	}

	// Computes the FFT of a stream after
	// http://www.cs.princeton.edu/introcs/97data/FFT.java.html
	private static Complex[] getFFT(final double data[]) {
		// Calculates the largest power of two not greater than data length
		int length = (int) Math.pow(2, Math.ceil(Math.log(data.length) / Math.log(2)));
		if (length == 0) {
			length = 1;
		}
		// Builds the complex buffer and computes fft, appkying a Hann's windowing
		final Complex cdata[] = new Complex[length];
		for (int i = 0; i < length; i++) {
			cdata[i] = new Complex(
					i < data.length ? 0.5 * (1 - Math.cos(2.0 * Math.PI * i / data.length)) * data[i] : 0, 0);
		}
		return SoundBitPanel.fft(cdata);
	}

	// Implements the Cooley-Tukey FFT algorithm assuming x.length is a power of two
	private static Complex[] fft(final Complex[] x) {
		final int n = x.length;
		if (n == 1) {
			return new Complex[] { x[0] };
		}
		// Applies FFT on event and odd parts of the signal
		Complex e[] = new Complex[n / 2], o[] = new Complex[n / 2];
		for (int i = 0; i < n / 2; i++) {
			e[i] = x[2 * i];
			o[i] = x[2 * i + 1];
		}
		e = SoundBitPanel.fft(e);
		o = SoundBitPanel.fft(o);
		// Combines the result
		final Complex s[] = new Complex[n];
		for (int i = 0; i < n / 2; i++) {
			final double k = -2 * i * Math.PI / n;
			Complex w = new Complex(Math.cos(k), Math.sin(k));
			w = w.mul(o[i]);
			s[i] = new Complex(e[i].x + w.x, e[i].y + w.y);
			s[i + n / 2] = new Complex(e[i].x - w.x, e[i].y - w.y);
		}
		return s;
	}

	// Defines a complex number and its multiplication
	private static class Complex {
		Complex(final double x, final double y) {
			this.x = x;
			this.y = y;
		}

		double x, y;

		Complex mul(final Complex w) {
			return new Complex(this.x * w.x - this.y * w.y, this.x * w.y + this.y * w.x);
		}
	}

	// Returns the inverse FFT of spectrum which length is a power of two
	// private double[] getInvFFT(complex[] spectrum) {
	// for (int i = 0; i < spectrum.length; i++)
	// spectrum[i].y = -spectrum[i].y;
	// complex[] sp = fft(spectrum);
	// double s[] = new double[spectrum.length];
	// for (int i = 0; i < spectrum.length; i++) {
	// s[i] = sp[i].x = sp[i].x / spectrum.length;
	// sp[i].y = -sp[i].y / spectrum.length;
	// spectrum[i].y = -spectrum[i].y;
	// }
	// return s;
	// }
}
