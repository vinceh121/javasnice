package org.javascool.proglets.visages;

import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import javax.imageio.ImageIO;

public class Functions {
	// @factory
	private Functions() {
	}

	/**
	 * Donne une matrice égale au produit de deux matrices ayant au moins deux
	 * colonnes, codées en float.
	 *
	 */
	public static float[][] product(final float[][] matrice1, final float[][] matrice2) {

		final int hauteur1 = matrice1.length;
		final int largeur1 = matrice1[0].length;
		final int hauteur2 = matrice2.length;
		final int largeur2 = matrice2[0].length;

		final float[][] matrice_produit = new float[hauteur1][largeur2];
		if (largeur1 == hauteur2 && hauteur1 == largeur2) {

			for (int n = 0; n < hauteur1; n++) {
				// System.out.println();
				// System.out.print(n + ": ");
				for (int p = 0; p < largeur2; p++) {

					float s = 0;
					for (int l = 0; l < largeur1; l++) {
						s = s + matrice1[n][l] * matrice2[l][p];
					}
					matrice_produit[n][p] = s;
					// System.out.print(s+" ; ");
				}

			}
		}
		return matrice_produit;
	}

	/**
	 * Donne une matrice égale à la différence de deux matrices ayant au moins deux
	 * colonnes, codées en float.
	 *
	 */
	public static float[][] differrence(final float[][] matrice1, final float[][] matrice2) {
		final int hauteur1 = matrice1.length;
		final int largeur1 = matrice1[0].length;
		final int hauteur2 = matrice1.length;
		final int largeur2 = matrice2[0].length;
		final float[][] matrice_difference = new float[hauteur1][largeur1];
		if (largeur1 == largeur2 && hauteur1 == hauteur2) {

			for (int n = 0; n < hauteur1; n++) {
				for (int p = 0; p < largeur1; p++) {
					matrice_difference[n][p] = matrice1[n][p] - matrice2[n][p];
				}
			}

		}
		return matrice_difference;
	}

	/**
	 * Donne une matrice égale à la somme de deux matrices ayant au moins deux
	 * colonnes, codées en float.
	 *
	 */
	public static float[][] sum(final float[][] matrice1, final float[][] matrice2) {

		final int hauteur1 = matrice1.length;
		final int largeur1 = matrice1[0].length;
		final int hauteur2 = matrice1.length;
		final int largeur2 = matrice2[0].length;
		final float[][] matrice_somme = new float[hauteur1][largeur1];
		if (largeur1 == largeur2 && hauteur1 == hauteur2) {

			for (int n = 0; n < hauteur1; n++) {
				for (int p = 0; p < largeur1; p++) {
					matrice_somme[n][p] = matrice1[n][p] + matrice2[n][p];
				}
			}

		}
		return matrice_somme;
	}

	/**
	 * Donne une matrice égale à la transposée d'une matrice ayant au moins deux
	 * colonnes, codée en float.
	 *
	 */
	public static float[][] transposee(final float[][] matrice1) {

		final int hauteur1 = matrice1.length;
		final int largeur1 = matrice1[0].length;
		final float[][] transposee = new float[largeur1][hauteur1];
		for (int n = 0; n < hauteur1; n++) {

			for (int p = 0; p < largeur1; p++) {
				transposee[p][n] = matrice1[n][p];

			}
		}
		return transposee;

	}

	/**
	 * Donne une matrice égale au produit d'une matrice ayant au moins deux colonnes
	 * par une constante codées en float.
	 *
	 */
	public static float[][] proportional(final float[][] matrice1, final float coef) {

		final int hauteur1 = matrice1.length;
		final int largeur1 = matrice1[0].length;
		final float[][] matrice_proportionnelle = new float[hauteur1][largeur1];
		for (int n = 0; n < hauteur1; n++) {
			for (int p = 0; p < largeur1; p++) {
				matrice_proportionnelle[n][p] = coef * matrice1[n][p];

			}
		}
		return matrice_proportionnelle;

	}

	/**
	 * Donne une matrice égale à la somme de chaque élements d'une matrice ayant au
	 * moins deux colonnes et d'une même constante , codées en float.
	 *
	 */
	public static float[][] sumConstant(final float[][] matrice1, final float coef) {

		final int hauteur1 = matrice1.length;
		final int largeur1 = matrice1[0].length;
		final float[][] matrice_somme = new float[hauteur1][largeur1];
		for (int n = 0; n < hauteur1; n++) {
			for (int p = 0; p < largeur1; p++) {
				matrice_somme[n][p] = coef + matrice1[n][p];

			}
		}
		return matrice_somme;

	}

	/**
	 * Donne une matrice égale à la valeur absolue d'une matrice ayant au moins deux
	 * colonnes, codée en float.
	 *
	 */
	public static float[][] abs(final float[][] matrice1) {

		final int hauteur1 = matrice1.length;
		final int largeur1 = matrice1[0].length;
		final float[][] matrice_val_abs = new float[hauteur1][largeur1];
		for (int n = 0; n < hauteur1; n++) {
			for (int p = 0; p < largeur1; p++) {
				matrice_val_abs[n][p] = Math.abs(matrice1[n][p]);

			}
		}
		return matrice_val_abs;

	}

	/**
	 * Donne une matrice égale à la racine carrée d'une matrice ayant au moins deux
	 * colonnes, codées en float.
	 *
	 */
	public static float[][] sqrt(final float[][] matrice1) {

		final int hauteur1 = matrice1.length;
		final int largeur1 = matrice1[0].length;
		final float[][] matrice_racine = new float[hauteur1][largeur1];
		for (int n = 0; n < hauteur1; n++) {
			for (int p = 0; p < largeur1; p++) {
				if (matrice1[n][p] >= 0) {
					matrice_racine[n][p] = (float) Math.sqrt(matrice1[n][p]);
				} else {
					matrice_racine[n][p] = 0;
				}

			}
		}
		return matrice_racine;

	}

	/**
	 * Calcul des nbvecteurpropres premiers vecteurs propres et valeurs propres
	 * d'une matrices ayant au moins deux colonnes, codées en float. Le classement
	 * est obtenu dans l'ordre décroissant des valeurs propres. Les valeurs propres
	 * sont calculées par la méthode des puissances ittérées. La fonction enregistre
	 * deux fichiers "texte": - un fichier "valeurs_propres.txt" contenant les
	 * valeurs propres classées par ordre décroissant, avec une valeur par ligne -
	 * un fichier "vecteurs_propres.txt" contenant les composantes de chaque vecteur
	 * popre : chaque colonne est un vecteur propre, les composantes de rang i sont
	 * sur la même ligne de numéro i séparées par un point virgule
	 */
	public static void properVectors(final float[][] covariance, final int nbvecteurspropres) {

		final int nombre = nbvecteurspropres;// nombre de vecteurs propres choisi
		final int dimension = covariance.length;
		final float[] image0 = new float[dimension];
		final float[][] image_propre = new float[dimension][nombre];
		final float[] b = new float[dimension];
		float norme_image;
		final float[] val_propre0 = new float[nombre], val_propre = new float[nombre];
		final float[][] Bcov = new float[dimension][dimension];
		final float[] valeurs_propres = new float[nombre];

		for (int lin = 0; lin < dimension; lin++) {

			for (int col = 0; col < dimension; col++) {

				Bcov[lin][col] = covariance[lin][col];

			}
		}

		int num = 0;
		while (num < nombre) {
			// Calcul de la matrice B = A - lambda(num-1)*u(num-1)*t(vnum-1)
			if (num > 0) {
				double norme_carree = 0;
				for (int lin = 0; lin < dimension; lin++) {

					norme_carree = norme_carree + image_propre[lin][num - 1] * image_propre[lin][num - 1];

				}

				for (int lin = 0; lin < dimension; lin++) {
					for (int col = 0; col < dimension; col++) {

						Bcov[lin][col] = (float) (Bcov[lin][col] - val_propre[num - 1] * image_propre[lin][num - 1]
								* image_propre[col][num - 1] / norme_carree);

					}
				}

			}
			val_propre0[num] = 1;
			val_propre[num] = 0;
			int l = 0;
			while (l < dimension) {
				image0[l] = 100;
				l = l + 1;
			}

			while (Math.abs(val_propre0[num] - val_propre[num]) > 0.3) {// calcul de la valeur propre lambda_num et du
																		// vecteur propre
				l = 0;
				norme_image = 0;
				val_propre0[num] = val_propre[num];
				while (l < dimension) {
					norme_image = norme_image + image0[l] * image0[l];
					l = l + 1;
				}
				norme_image = (float) Math.sqrt(norme_image);
				l = 0;
				while (l < dimension) {
					b[l] = image0[l] / norme_image;
					l = l + 1;
				}
				int lin = 0, col = 0;
				val_propre[num] = 0;
				for (lin = 0; lin < dimension; lin++) {

					float s = 0;
					for (col = 0; col < dimension; col++) {
						s = s + Bcov[lin][col] * b[col];
					}
					image0[lin] = s;
					val_propre[num] = val_propre[num] + b[lin] * image0[lin];
				}
				// System.out.println(val_propre[num]);
			} // fin du calcul de lamda_num
			valeurs_propres[num] = (int) val_propre[num];

			// Chargement et sauvegarde de l'image _propre num
			l = 0;
			norme_image = 0;
			while (l < dimension) {
				norme_image = norme_image + image0[l] * image0[l];
				l = l + 1;
			}
			norme_image = (float) Math.sqrt(norme_image);

			l = 0;
			while (l < dimension) {
				image_propre[l][num] = image0[l] / norme_image;
				l = l + 1;
			}

			num++;
		}

		Functions.saveText(image_propre, valeurs_propres);// sauvegarde des vecteurs propres et valeurs propres.

	}

	/**
	 * Transforme une image de dimension largeurxhauteur en une matrice unicolonne:
	 * pixels sont rangées dans l'ordre des lignes : les n pixels d'une lignes se
	 * trouvent les uns aux dessus des autres dans l'odre de la lecteur de la ligne.
	 *
	 */
	public static float[] imageToVector(final String nom, final int largeur, final int hauteur) {

		int[][] pixel;

		BufferedImage images;
		final float[] vecteur_image = new float[largeur * hauteur];
		pixel = new int[largeur][hauteur];
		final ImageOpener ouverture_image = new ImageOpener();
		images = ouverture_image.openImage(nom);
		// BufferedImage imageint=images;
		System.out.println(nom);
		final int width1 = images.getWidth();
		final int height1 = images.getHeight();

		int m = 0;
		for (int i = 0; i < height1; i++) {

			for (int j = 0; j < width1; j++) {

				int luma;// = images[n].getRGB(i, j)& 0xFFFFFF;;
				pixel[i][j] = images.getRGB(i, j);
				// short alpha = (short) ((pixel[i][j] >> 24) & 0x000000FF);
				final short rouge = (short) (pixel[i][j] >> 16 & 0x000000FF);
				final short vert = (short) (pixel[i][j] >> 8 & 0x000000FF);
				final short bleu = (short) (pixel[i][j] & 0x000000FF);

				// Test image couleur ou non
				// short luma;

				if (rouge == bleu && bleu == vert) {
					luma = rouge;
				}

				else {
					luma = (short) ((77 * rouge + 151 * vert + 28 * bleu) / 256);
				} // ((bleu + rouge +vert)/3);}*/

				// if (luma>255){System.out.println("Valeur erronéée de luma");
				// System.out.println(m);
				vecteur_image[m] = luma;
				// System.out.println("image "+ p + " pixel "+ m +" ; alpha = : "+alpha);
				/*
				 * if (pixel[n][i][j]==0){ System.out.println("image "+n+" pixel "+i+j+" nul");
				 * }
				 */

				m = m + 1;
			}
		}
		// System.out.println(nom_image+"; largeur = "+ width1 + "; hauteur = "
		// +height1+ ";dim = "+ m);

		return vecteur_image;

	}

	/**
	 * Sauvegarde des vecteurs propres et valeurs propres dans deux fichiers textes
	 * distincts.
	 *
	 */
	public static void saveText(final float[][] image_propre, final float[] valeurs_propres) {

		final String nom_fichier3 = "vecteurs_propres.txt";
		final String nom_fichier4 = "valeurs_propres.txt";
		final int dimension = image_propre.length;
		final int nombre = image_propre[0].length;
		try {
			final PrintWriter f3 = new PrintWriter(new FileWriter(nom_fichier3));

			for (int m = 0; m < dimension; m++) {

				for (int n = 0; n < nombre; n++) {
					final float s = image_propre[m][n];
					f3.print(s + ";");

				}
				f3.println();
			}
			f3.close();
			final PrintWriter f4 = new PrintWriter(new FileWriter(nom_fichier4));
			for (int n = 0; n < nombre; n++) {
				f4.println(valeurs_propres[n]);

			}
			f4.close();

		} catch (final FileNotFoundException e) {
			e.printStackTrace();
		} catch (final IOException e) {}

	}

	/**
	 * Donne une matrice unicolonne dont chaque composante est égale à moyenne des
	 * composantes d'une ligne d'une matrice codée en float.
	 *
	 */
	public static float[] average(final float[][] vecteurs_images, final int dimension, final int nombre_image) {

		final float[] image_moyenne = new float[dimension];
		for (int n = 0; n < dimension; n++) {
			for (int p = 0; p < nombre_image; p++) {
				image_moyenne[n] = image_moyenne[n] + vecteurs_images[n][p];
			}
			image_moyenne[n] = image_moyenne[n] / nombre_image;
		}

		return image_moyenne;
	}

	/**
	 * Sauvegarde d'un vecteur image unicolonne en image jpg.
	 *
	 */
	public static void savejpg(final float[] vecteur_image, final String nom, final int largeur, final int hauteur) {

		int p = 0, i = 0, j = 0;
		final int dimension = vecteur_image.length;
		int val_pixel;
		final File f = new File(nom);
		final BufferedImage image = new BufferedImage(largeur, hauteur, BufferedImage.TYPE_BYTE_GRAY);
		while (p < dimension) {

			val_pixel = (int) vecteur_image[p];

			final int intens = (255 << 24) + (val_pixel << 16) + (val_pixel << 8) + val_pixel;// décalage de 50 et
																								// valeur
																								// absolue de la valeur
																								// pour
																								// une représentaion
																								// "humaine

			if (p % largeur == 0 && p != 0) {
				i = i + 1;
				j = 0;

			}

			image.setRGB(i, j, intens);

			j = j + 1;
			p = p + 1;
		}

		try {
			ImageIO.write(image, "JPG", f);
			System.out.println(nom);
		} catch (final IOException e) {

			e.printStackTrace();
		}

	}

	/**
	 * Lit un fichier texte contenant un tableau de n lignes, p colonnes codé en
	 * float, Chaque composante d'une ligne doit être séparée de l'autre par un
	 * point virgule dans le fichier texte..
	 */
	public static float[][] readTable(final int lignes, final int colonnes, final String nom_fichier)
			throws IOException {

		final float[][] vecteurs_propres = new float[lignes][colonnes];

		String ligne = "";

		BufferedReader entree;

		entree = new BufferedReader(new FileReader(nom_fichier));

		int p = 0, num = 0;
		do {
			ligne = entree.readLine();
			final int longueur_ligne = ligne.length();
			int n1 = 0, n2 = 0;
			final char c = ';';

			for (int n = 0; n < longueur_ligne; n++) {
				if (ligne.charAt(n) == c) {
					n2 = n;
					vecteurs_propres[p][num] = Float.parseFloat(ligne.substring(n1, n2));

					n1 = n + 1;
					num = num + 1;
				}

			}

			num = 0;
			p++;
		}

		while (p != lignes);
		entree.close();

		return vecteurs_propres;
	}

	/**
	 * Donne le produit scalaire de deux vecteurs codés en float.
	 *
	 */
	public static float scalar(final float[] vecteur1, final float[] vecteur2) {

		float produit_scalaire = 0;
		final int dim1 = vecteur1.length;
		final int dim2 = vecteur2.length;
		if (dim1 == dim2) {
			float s = 0;
			for (int n = 0; n < dim1; n++) {
				s = s + vecteur1[n] * vecteur2[n];
			}
			produit_scalaire = s;
		} else {
			System.out.println("Attention vecteur de dimensions différentes : calcul imposible ! Retourne 0");
		}

		return produit_scalaire;
	}

	/**
	 * Sauvegarde d'un tableau de float ayant n colonnes et p lignes, dans un
	 * fichier texte ayant n colonnes, p lignes . Les composantes sont séparées par
	 * un point virgule.
	 */
	public void save(final float[][] tableau, final String nom_fichier) {

		final int nombre_ligne = tableau.length;
		final int nombre_colonnes = tableau[0].length;
		try {
			final PrintWriter f3 = new PrintWriter(new FileWriter(nom_fichier));

			for (int m = 0; m < nombre_ligne; m++) {

				for (int n = 0; n < nombre_colonnes; n++) {
					f3.print(tableau[m][n] + ";");

				}
				f3.println();
			}
			f3.close();
		} catch (final FileNotFoundException e) {
			e.printStackTrace();
		} catch (final IOException e) {}

	}

}// fin classe Function

class ImageOpener {
	/**
	 * Ouvre une image et la stocke dans une image mémoire "BufferedImage" .
	 *
	 */
	public BufferedImage openImage(final String nom) {
		BufferedImage image;

		try {

			image = ImageIO.read(new File(nom));
			return image;
		} catch (final IOException e) {
			e.printStackTrace();
			System.out.println("Error while reading image file");
		}
		return null;

	}
}

class ImageText {
	/**
	 * Donne une matrice unicolonne à partir d'une matrice ayant au moins deux
	 * colonnes, codées en float. La matrice est lue ligne par ligne et les
	 * composantes d'une ligne sont classées dans l'ordre les unes à la suite des
	 * autres.
	 */
	public float[][] imageTable(final float[] image) {
		final int dimension = image.length;
		int p = 0, q = 0;
		final float[][] tableau = new float[50][50];
		for (int n = 0; n < dimension; n++) {

			if (n % 50 == 0 && p != 0) {
				p = 0;
				q = q + 1;
			}
			tableau[q][p] = image[n];

			p++;
		}

		return tableau;

	}

}

class JPGSaver {
	/**
	 * Sauvegarde d'un vecteur unicolonne en une image jpg de dimension
	 * largeurxhauteur. La dimension du vecteur doit être égale à largeurxhauteur.
	 */
	public void saveJpg(final float[] vecteur_image, final String nom, final int largeur, final int hauteur) {

		int p = 0, i = 0, j = 0;
		final int dimension = vecteur_image.length;
		int val_pixel;
		final File f = new File(nom);
		final BufferedImage image = new BufferedImage(largeur, hauteur, BufferedImage.TYPE_BYTE_GRAY);
		while (p < dimension) {

			val_pixel = (int) vecteur_image[p];

			final int intens = (255 << 24) + (val_pixel << 16) + (val_pixel << 8) + val_pixel;// décalage de 50 et
																								// valeur
																								// absolue de la valeur
																								// pour
																								// une représentaion
																								// "humaine

			if (p % largeur == 0 && p != 0) {
				i = i + 1;
				j = 0;

			}

			image.setRGB(i, j, intens);

			j = j + 1;
			p = p + 1;
		}

		try {
			ImageIO.write(image, "JPG", f);
			System.out.println(nom);
		} catch (final IOException e) {

			e.printStackTrace();
		}

	}
}
