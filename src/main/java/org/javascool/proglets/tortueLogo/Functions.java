package org.javascool.proglets.tortueLogo;

import java.awt.Color;

import org.javascool.macros.Macros;

/**
 * Définit les fonctions de la proglet qui permet de simuler la tortue logo.
 *
 * @see <a href=
 *      "http://fr.wikipedia.org/wiki/Logo_(langage)#Primitives_graphiques">La
 *      référence du langage logo</a>
 * @see <a href="Functions.java.html">code source</a>
 * @serial exclude
 */
public class Functions {
	// @factory
	private Functions() {
	}

	/** Renvoie l'instance de la proglet pour accéder à ses éléments. */
	private static Panel getPane() {
		return (Panel) Macros.getProgletPane();
	}

	// Updates the turtle position and draw if required
	private static void update(double x, double y) {
		if (x < 0) {
			x = 0;
		}
		if (x > 511) {
			x = 511;
		}
		if (y < 0) {
			y = 0;
		}
		if (y > 511) {
			y = 511;
		}
		if (Functions.pen) {
			Functions.draw((int) Functions.x, (int) x, (int) Functions.y, (int) y);
		}
		Functions.x = x;
		Functions.y = y;
		if (Functions.turtle_shown) {
			Functions.getPane().show((int) x, (int) y, Functions.turtle_shown);
			Macros.sleep(1);
		}
	}

	private static void draw(final int x1, final int x2, final int y1, final int y2) {
		if (Math.abs(x1 - x2) > Math.abs(y1 - y2)) {
			if (x1 < x2) {
				Functions.draw_x(x1, x2, y1, y2);
			} else if (x1 > x2) {
				Functions.draw_x(x2, x1, y2, y1);
			}
		} else {
			if (y1 < y2) {
				Functions.draw_y(x1, x2, y1, y2);
			} else if (y1 > y2) {
				Functions.draw_y(x2, x1, y2, y1);
			}
		}
	}

	private static void draw_x(final int x1, final int x2, final int y1, final int y2) {
		for (int x = x1; x <= x2; x++) {
			Functions.getPane().add(x, y1 + (y2 - y1) * (x - x1) / (x2 - x1), Functions.pen_color);
		}
	}

	private static void draw_y(final int x1, final int x2, final int y1, final int y2) {
		for (int y = y1; y <= y2; y++) {
			Functions.getPane().add(x1 + (x2 - x1) * (y - y1) / (y2 - y1), y, Functions.pen_color);
		}
	}

	private static double x = 0, y = 0;
	private static double a = 0;
	private static Color pen_color = Color.BLACK;
	private static boolean pen = true;
	private static boolean turtle_shown = true;

	/** Efface toutes traces du carré de salade de taille (512, 512). */
	public static void clear_all() {
		Functions.getPane().clear();
	}

	/** Retour au milieu du carré de salade, au point (256, 256). */
	public static void home() {
		Functions.update(Panel.width / 2, Panel.height / 2);
	}

	/** La tortue avance de n pas. */
	public static void forward(final double n) {
		Functions.set_position(Functions.x + n * Math.cos(Functions.a), Functions.y + n * Math.sin(Functions.a));
	}

	/** La tortue recule de n pas. */
	public static void backward(final double n) {
		Functions.forward(-n);
	}

	/** La tortue tourne de n degrés d'angle vers la gauche. */
	public static void leftward(final double n) {
		Functions.a -= Math.PI / 180.0 * n;
	}

	/** La tortue tourne de n degrés d'angle vers la droite. */
	public static void rightward(final double n) {
		Functions.leftward(-n);
	}

	/** Fixe la position absolue de la tortue dans le carré de salade. */
	public static void set_position(final double x, final double y) {
		Functions.update(x, y);
	}

	/** Fixe le cap de la tortue de maniere absolue, selon l'angle de a degrés. */
	public static void set_heading(final double a) {
		Functions.a = Math.PI / 180.0 * a;
	}

	/** La tortue ne laisse pas de trace. */
	public static void pen_up() {
		Functions.pen = false;
	}

	/** La tortue laisse sa trace (par défaut). */
	public static void pen_down() {
		Functions.pen = true;
	}

	/**
	 * Change la couleur du fond, n est un entier positif entre 0 et 9.
	 * 
	 * @param n : 0 (noir), 1 (brun), 2 (rouge), 3 (orange), 4 (jaune), 5 (vert), 6
	 *          (bleu), 7 (violet), 8 (gris), 9 (blanc).
	 */
	public static void set_background(final int n) {
		Functions.getPane().setBackground(Functions.colors[n < 0 || n > 9 ? 0 : n]);
	}

	/**
	 * Change la couleur du crayon, n est un entier positif entre 0 et 9.
	 * 
	 * @param n : 0 (noir), 1 (brun), 2 (rouge), 3 (orange), 4 (jaune), 5 (vert), 6
	 *          (bleu), 7 (violet), 8 (gris), 9 (blanc).
	 */
	public static void set_color(final int n) {
		Functions.pen_color = Functions.colors[n < 0 || n > 9 ? 0 : n];
	}

	private static Color colors[] = { Color.BLACK, new Color(150, 75, 0), Color.RED, Color.ORANGE, Color.YELLOW,
			Color.GREEN, Color.BLUE, Color.MAGENTA, Color.GRAY, Color.WHITE };

	public static void show_turtle() {
		Functions.turtle_shown = true;
		Functions.getPane().show((int) Functions.x, (int) Functions.y, Functions.turtle_shown);
	}

	public static void hide_turtle() {
		Functions.turtle_shown = false;
		Functions.getPane().show((int) Functions.x, (int) Functions.y, Functions.turtle_shown);
	}

}
