package org.javascool.proglets.tortueLogo;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;

import javax.swing.ImageIcon;
import javax.swing.JPanel;

import org.javascool.macros.Macros;

/**
 * Définit le panneau graphique de la proglet qui permet de simuler la tortue
 * logo.
 *
 * @see <a href=
 *      "http://fr.wikipedia.org/wiki/Logo_(langage)#Primitives_graphiques">La
 *      référence du langage logo</a>
 * @see <a href="Panel.java.html">code source</a>
 * @serial exclude
 */
public class Panel extends JPanel {
	private static final long serialVersionUID = 1L;

	// @bean
	public Panel() {
		this.setPreferredSize(new Dimension(Panel.width, Panel.height));
		this.setBackground(new Color(10, 100, 10));
		// Adds the garden
		this.clear();
		// Adds the turtle
		this.turtle = Macros.getIcon("org/javascool/proglets/tortueLogo/turtle.gif");
	}

	/**
	 * Routine interne de tracé, ne pas utiliser.
	 *
	 */
	@Override
	public void paint(final Graphics g) {
		super.paint(g);
		for (int j = 0; j < Panel.height; j++) {
			for (int i = 0; i < Panel.width; i++) {
				if (this.garden[i + j * Panel.width] != null) {
					g.setColor(this.garden[i + j * Panel.width]);
					g.fillRect(i, j, 1, 1);
				}
			}
		}
		if (this.turtle_shown) {
			g.drawImage(this.turtle.getImage(), this.turtle_x, this.turtle_y, this.getBackground(),
					this.turtle.getImageObserver());
		}
	}

	static final int width = 512, height = 512;
	private Color garden[];
	private final ImageIcon turtle;
	private int turtle_x = Panel.width / 2, turtle_y = Panel.height / 2;
	private boolean turtle_shown = true;

	/** Clears the garden. */
	public final void clear() {
		this.garden = new Color[Panel.width * Panel.height];
		this.repaint(0, 0, 0, this.getWidth(), this.getHeight());
	}

	/**
	 * Shows the turtle at a given location.
	 * 
	 * @param x Turtle horizontal position, not shown if &lt; 0.
	 * @param y Turtle vertical position, not shown if &lt; 0.
	 */
	public void show(final int x, final int y, final boolean turtle_visibility) {
		this.repaint(this.turtle_x - 1, this.turtle_y - 1, this.turtle.getIconWidth() + 3,
				this.turtle.getIconHeight() + 3);
		if (x < 0 || y < 0) {
			this.turtle_shown = false;
		} else {
			this.turtle_x = x;
			this.turtle_y = y;
			this.turtle_shown = turtle_visibility;
		}
		this.repaint(this.turtle_x - 1, this.turtle_y - 1, this.turtle.getIconWidth() + 3,
				this.turtle.getIconHeight() + 3);
	}

	/**
	 * Adds a trace value.
	 * 
	 * @param x Pixel abscissa, in [-1..1].
	 * @param y Pixel Ordinate, in [-1..1].
	 * @param c Color in {0, 9}.
	 */
	public void add(int x, int y, final Color c) {
		if (x < 0) {
			x = 0;
		}
		if (x > Panel.width - 1) {
			x = Panel.width - 1;
		}
		if (y < 0) {
			y = 0;
		}
		if (y > Panel.height - 1) {
			y = Panel.height - 1;
		}
		this.garden[x + y * Panel.width] = c;
		this.repaint(x - 1, y - 1, 3, 3);
	}
}
