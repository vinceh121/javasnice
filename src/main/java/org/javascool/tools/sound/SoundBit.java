package org.javascool.tools.sound;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;

import javax.sound.sampled.AudioFileFormat.Type;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioFormat.Encoding;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine.Info;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.SourceDataLine;

import org.javascool.macros.Macros;

public class SoundBit {
	protected String name = null;
	protected double length = 0.0D;
	public static final float SAMPLING = 44100.0F;
	// private static final int FRAME_SIZE = 4;

	public double get(final char channel, final long index) {
		return this.get(channel, index / 44100.0F);
	}

	public double get(final char channel, final double time) {
		return 0.0D;
	}

	public SoundBit reset(final String definition) {
		return this;
	}

	public String getName() {
		return this.name == null ? this.getClass().getName() : this.name;
	}

	public void setLength(final double length) {
		this.length = length;
	}

	public double getLength() {
		return this.length >= 0.0D ? this.length : 5.2286688591872E13D;
	}

	public AudioInputStream getStream() {
		return new SoundBit.Stream(this.length);
	}

	@Override
	public String toString() {
		return "SoundBit \"" + this.getName() + "\" : " + this.getStream();
	}

	public void save(final String path) throws IOException {
		AudioSystem.write(this.getStream(), Type.WAVE, new File(path.replaceFirst("\\.wav$", "") + ".wav"));
	}

	public void play(final double period) {
		final AudioInputStream stream = this.getStream();

		try {
			final SourceDataLine line
					= (SourceDataLine) AudioSystem.getLine(new Info(SourceDataLine.class, stream.getFormat()));
			line.open(stream.getFormat());
			line.start();
			final int size = (int) (4.0D * (period <= 0.0D ? 1.0D : period) * 44100.0D);
			int n = 0;
			final byte[] data = new byte[size];

			for (long t = 0L; t < 4L * stream.getFrameLength(); ++n) {
				if (period > 0.0D) {
					this.sample(n);
				}

				final int s = stream.read(data, 0, size);
				if (s > 0) {
					line.write(data, 0, s);
				}

				Macros.sleep(0);
				t += size;
			}

			line.close();
		} catch (final IOException var11) {
			throw new RuntimeException(var11.toString());
		} catch (final LineUnavailableException var12) {
			throw new RuntimeException(var12.toString());
		}
	}

	public void play() {
		this.play(0.0D);
	}

	public void sample(final int n) {
	}

	public static void checkFormat(final AudioInputStream stream) {
		if (stream.getFormat().getChannels() != 1 && stream.getFormat().getChannels() != 2
				|| stream.getFormat().getSampleSizeInBits() != 16
				|| stream.getFormat().getEncoding() != Encoding.PCM_SIGNED || stream.getFormat().isBigEndian()) {
			throw new IllegalArgumentException("Bad stream format: " + stream.getFormat().toString());
		}
	}

	public double[] events(final double frequence, final double period, final double cut) {
		final int size = 2 + (int) (this.getLength() / period);
		final double[] events = new double[size];
		double c = 0.0D;
		double s = 0.0D;
		double a = 0.0D;
		final double g = 1.0D - 1.0D / (44100.0D * period);
		double max = 0.0D;
		double moy = 0.0D;
		double var = 0.0D;
		int n = 0;
		double thres = 0.0D;
		double nthres = period;
		final double dt = 2.2675736961451248E-5D;

		for (final double T = this.getLength(); thres < T; thres += dt) {
			final double v = this.get('l', thres) + this.get('r', thres);
			c += g * (Math.cos(6.283185307179586D * frequence * thres) * v - c);
			s += g * (Math.sin(6.283185307179586D * frequence * thres) * v - s);
			a += g * (c * c + s * s - a);
			if (nthres <= thres && n < size) {
				events[n++] = a;
				nthres += period;
				if (a > max) {
					max = a;
				}

				moy += a;
				var += a * a;
			}
		}

		if (n > 0) {
			moy /= n;
			var = Math.sqrt(var / n - moy * moy);
		}

		thres = moy + cut * var;
		nthres = 0.0D;

		for (int k = 0; k < size; ++k) {
			if (events[k] < thres) {
				events[k] = 0.0D;
			} else {
				++nthres;
			}
		}

		System.out.println("events("
				+ frequence
				+ ", "
				+ period
				+ ", "
				+ cut
				+ ") = "
				+ moy
				+ " +- "
				+ var
				+ " < "
				+ max
				+ ", "
				+ nthres
				+ "/"
				+ size
				+ " = "
				+ 100.0D * nthres / size
				+ "% < "
				+ thres);
		return events;
	}

	private class Stream extends AudioInputStream {
		private long frameMark = 0L;
		private long frameLimit = 2305843009213693951L;

		public Stream(final double length) {
			super(new ByteArrayInputStream(new byte[0]),
					new AudioFormat(Encoding.PCM_SIGNED, 44100.0F, 16, 2, 4, 44100.0F, false),
					length >= 0.0D ? (long) (length * 44100.0D) : 2305843009213693951L);
		}

		@Override
		public int read(final byte[] data, final int offset, int byteLength) {
			byteLength = offset + byteLength > data.length ? data.length - offset : byteLength;
			long nFrame = byteLength / this.frameSize;
			if (nFrame > this.frameLength - this.framePos) {
				nFrame = this.frameLength - this.framePos;
			}

			byteLength = (int) nFrame * this.frameSize;
			int n = 0;

			for (int i = offset; n < nFrame; i += this.frameSize) {
				long l = Math.round(32767.0D * (1.0D + SoundBit.this.get('l', this.framePos)));
				long r = Math.round(32767.0D * (1.0D + SoundBit.this.get('r', this.framePos)));
				++this.framePos;
				if (l < 0L) {
					l = 0L;
				}

				if (l > 65535L) {
					l = 65535L;
				}

				if (r < 0L) {
					r = 0L;
				}

				if (r > 65535L) {
					r = 65535L;
				}

				data[i + 0] = (byte) (int) ((l & 255L) - 128L);
				data[i + 1] = (byte) (int) ((l >>> 8 & 255L) - 128L);
				data[i + 2] = (byte) (int) ((r & 255L) - 128L);
				data[i + 3] = (byte) (int) ((r >>> 8 & 255L) - 128L);
				++n;
			}

			return byteLength <= 0 ? -1 : byteLength;
		}

		@Override
		public int read(final byte[] data) {
			return this.read(data, 0, data.length);
		}

		@Override
		public int available() {
			return (int) (this.frameSize * (this.frameLength - this.framePos));
		}

		@Override
		public void reset() {
			if (this.framePos > this.frameLimit) {
				throw new IllegalStateException("Mark limit exceeded");
			} else {
				this.framePos = this.frameMark;
			}
		}

		@Override
		public void mark(final int byteLimit) {
			this.frameMark = this.framePos;
			this.frameLimit = byteLimit > 0 ? this.framePos + byteLimit / this.frameSize : this.frameLength;
		}

		@Override
		public boolean markSupported() {
			return true;
		}

		@Override
		public int read() {
			throw new IllegalStateException("SoundBit has no one byte frame size, operation forbidden");
		}

		@Override
		public String toString() {
			return "["
					+ super.toString()
					+ " length = "
					+ this.frameLength / 44100.0F
					+ "s pos = "
					+ this.framePos
					+ " < "
					+ this.frameLength
					+ " mark = "
					+ this.frameMark
					+ " < "
					+ this.frameLimit
					+ "]";
		}

		@Override
		public void close() {
		}
	}
}
