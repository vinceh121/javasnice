package org.javascool.tools.sound;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioFormat.Encoding;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine.Info;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.TargetDataLine;

import org.javascool.gui.Core;

public class InputSoundBit extends SoundBit {
	private TargetDataLine line = null;
	private boolean recording = false;
	private byte[] buffer = null;

	public SoundBit reset(final double length) {
		this.buffer = new byte[2 * (int) (44100.0D * (this.length = length))];
		final AudioFormat format = new AudioFormat(Encoding.PCM_SIGNED, 44100.0F, 16, 1, 2, 44100.0F, false);
		final Info info = new Info(TargetDataLine.class, format);
		try {
			this.line = (TargetDataLine) AudioSystem.getLine(info);
			this.line.open(format);
			new Thread(() -> {
				int offset = 0;
				final int length1 = InputSoundBit.this.line.getBufferSize() / 5;
				InputSoundBit.this.line.start();
				InputSoundBit.this.recording = true;

				while (offset < InputSoundBit.this.buffer.length && InputSoundBit.this.recording) {
					InputSoundBit.this.line.read(InputSoundBit.this.buffer, offset,
							offset + length1 < InputSoundBit.this.buffer.length ? length1
									: InputSoundBit.this.buffer.length - offset);
					offset += length1;
				}

				InputSoundBit.this.length = (int) (0.5D * offset / 44100.0D);
				if (Core.DEBUG) {
					System.out.println("sound input : " + offset + " bytes, " + InputSoundBit.this.length + " sec");
				}
				InputSoundBit.this.line.stop();
				InputSoundBit.this.line.close();
			}).start();
			return this;
		} catch (final LineUnavailableException var6) {
			throw new RuntimeException("No microphone available for this audio system (" + var6 + ")");
		}
	}

	public void stop() {
		this.recording = false;
	}

	public boolean isStopped() {
		return this.recording;
	}

	@Override
	public double get(final char channel, final long index) {
		final int i = (int) index * 2;
		if (this.buffer != null && i >= 0 && i < this.buffer.length) {
			final int h = this.buffer[i + 1];
			final int l = this.buffer[i];
			final int v = 128 + h << 8 | 128 + l;
			return 1.0D * (v / 32767.0D - 1.0D);
		} else {
			return 0.0D;
		}
	}

	@Override
	public double get(final char channel, final double time) {
		return this.get(channel, (long) (44100.0D * time));
	}

	@Override
	public void setLength(final double length) {
		throw new IllegalStateException("Cannot adjust length of buffered sound-bit of name " + this.getName());
	}
}
