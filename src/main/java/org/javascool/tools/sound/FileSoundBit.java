package org.javascool.tools.sound;

import java.io.IOException;
import java.util.HashMap;

import javax.sound.midi.MidiSystem;
import javax.sound.midi.MidiUnavailableException;
import javax.sound.midi.Soundbank;
import javax.sound.midi.SoundbankResource;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.UnsupportedAudioFileException;

import org.javascool.macros.Macros;

public class FileSoundBit extends SoundBit {
	private int c;
	private int s;
	private byte[] buffer;
	private static HashMap<String, AudioInputStream> midis = null;
	private static String play_location;

	@Override
	public SoundBit reset(final String location) {
		AudioInputStream stream;
		if (location.startsWith("midi:")) {
			FileSoundBit.getMidiNames();
			final String midiName = location.substring(5);
			if (!FileSoundBit.midis.containsKey(midiName)) {
				throw new RuntimeException("undefined midi sound " + midiName);
			}

			stream = FileSoundBit.midis.get(midiName);
		} else {
			try {
				stream = AudioSystem.getAudioInputStream(Macros.getResourceURL(location));
			} catch (final UnsupportedAudioFileException var5) {
				throw new RuntimeException(var5);
			} catch (final IOException var6) {
				throw new RuntimeException(var6);
			}
		}

		SoundBit.checkFormat(stream);
		this.c = stream.getFormat().getChannels();
		this.s = stream.getFormat().getFrameSize();
		this.buffer = new byte[(int) stream.getFrameLength() * stream.getFormat().getFrameSize()];

		try {
			int offset = 0;

			while (true) {
				int size;
				if ((size = stream.read(this.buffer, offset, this.buffer.length - offset)) == -1) {
					stream.close();
					break;
				}

				offset += size;
			}
		} catch (final IOException var7) {
			throw new RuntimeException(var7);
		}

		this.name = location;
		this.length = stream.getFrameLength() / 44100.0F;
		return this;
	}

	@Override
	public double get(final char channel, final long index) {
		final int i = (int) index * this.s + (this.c != 1 && channel != 'l' ? 2 : 0);
		if (this.buffer != null && i >= 0 && i < this.buffer.length) {
			final int h = this.buffer[i + 1];
			final int l = this.buffer[i];
			final int v = 128 + h << 8 | 128 + l;
			return 1.0D * (v / 32767.0D - 1.0D);
		} else {
			return 0.0D;
		}
	}

	@Override
	public double get(final char channel, final double time) {
		return this.get(channel, (long) (44100.0D * time));
	}

	@Override
	public void setLength(final double length) {
		throw new IllegalStateException("Cannot adjust length of buffered sound-bit of name " + this.getName());
	}

	public static String[] getMidiNames() {
		if (FileSoundBit.midis == null) {
			FileSoundBit.midis = new HashMap<>();

			try {
				final Soundbank s = MidiSystem.getSynthesizer().getDefaultSoundbank();

				for (int i = 0; i < s.getResources().length; ++i) {
					final SoundbankResource r = s.getResources()[i];
					if (r.getDataClass() != null
							&& r.getDataClass().getName().equals("javax.sound.sampled.AudioInputStream")) {
						final String name = r.getName().toLowerCase().replaceAll(" ", "_");
						final AudioInputStream stream = (AudioInputStream) r.getData();

						try {
							SoundBit.checkFormat(stream);
							FileSoundBit.midis.put(name, stream);
						} catch (final IllegalArgumentException var6) {}
					}
				}
			} catch (final MidiUnavailableException var7) {
				throw new RuntimeException(var7.toString());
			}
		}

		return FileSoundBit.midis.keySet().toArray(new String[FileSoundBit.midis.size()]);
	}

	public static void play(final String location) {
		FileSoundBit.play_location = location;
		new Thread(() -> {
			try {
				final Clip clip = AudioSystem.getClip();
				final AudioInputStream inputStream
						= AudioSystem.getAudioInputStream(Macros.getResourceURL(FileSoundBit.play_location));
				clip.open(inputStream);
				clip.start();
			} catch (final Exception var3) {
				throw new RuntimeException(var3.toString());
			}
		}).start();
	}
}
