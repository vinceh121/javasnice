package org.javascool.tools;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import javax.swing.JOptionPane;

import org.apache.commons.configuration2.INIConfiguration;
import org.apache.commons.configuration2.event.ConfigurationEvent;
import org.apache.commons.configuration2.ex.ConfigurationException;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.SystemUtils;
import org.javascool.gui.Core;

public class UserConfig {
	private static UserConfig userConfig = new UserConfig();
	private final INIConfiguration config;

	private UserConfig() {
		this.config = new INIConfiguration();
		try {
			final FileReader read = new FileReader(this.getApplicationFolder() + "javasnice.ini");
			this.config.read(read);
			read.close();
		} catch (final FileNotFoundException e) {
			System.out.println("Config not found");
			// TODO: write default
		} catch (final IOException e) {
			e.printStackTrace();
		} catch (final ConfigurationException e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(null, "Error while reading configuration file");
		}
		this.config.addEventListener(ConfigurationEvent.SET_PROPERTY, event -> UserConfig.this.save());

	}

	public void save() {
		try {
			final File f = new File(this.getApplicationFolder() + "javasnice.ini");
			FileUtils.touch(f);
			this.config.write(new FileWriter(f));
		} catch (ConfigurationException | IOException e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(null, "Failed to save settings");
		}
	}

	public String getApplicationFolder() {
		if (SystemUtils.IS_OS_WINDOWS) {
			return System.getenv("APPDATA") + "\\javasnice\\";
		} else if (SystemUtils.IS_OS_MAC) {
			return System.getProperty("user.home") + "/Library/Application Support/javasnice/";
		} else if (SystemUtils.IS_OS_LINUX) {
			return System.getProperty("user.home") + "/.config/javasnice/";
		} else {
			if (Core.DEBUG) {
				System.out.println("Could not create folder for config on OS «" + SystemUtils.OS_NAME + "»");
			}
			return UserConfig.createTempDir("javasnice").getAbsolutePath();
		}

	}

	public INIConfiguration getConfig() {
		return this.config;
	}

	public static INIConfiguration getConf() {
		return UserConfig.userConfig.config;
	}

	public static UserConfig getInstance() {
		return UserConfig.userConfig;
	}

	public static File createTempDir(final String prefix) {
		try {
			final File d = File.createTempFile(prefix, "");
			d.delete();
			d.mkdirs();
			return d;
		} catch (final IOException var2) {
			throw new RuntimeException(var2 + " when creating temporary directory");
		}
	}
}
