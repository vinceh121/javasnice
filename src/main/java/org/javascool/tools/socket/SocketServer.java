package org.javascool.tools.socket;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;

public class SocketServer {
	private ServerSocket server;
	private Socket socket;
	private BufferedReader plec;
	private PrintWriter pred;

	public SocketServer open(final int port) {
		try {
			this.server = new ServerSocket(port);
			if (this.server == null) {
				throw new RuntimeException("Could not open server on port " + port);
			} else {
				this.socket = this.server.accept();
				if (this.socket == null) {
					throw new RuntimeException("Could not open serversocket on port " + port);
				} else {
					this.plec = new BufferedReader(new InputStreamReader(this.socket.getInputStream()));
					this.pred = new PrintWriter(
							new BufferedWriter(new OutputStreamWriter(this.socket.getOutputStream())), true);
					return this;
				}
			}
		} catch (final UnknownHostException var3) {
			throw new IllegalArgumentException("Your hostname is crap (" + var3 + ")");
		} catch (final SecurityException var4) {
			throw new RuntimeException("Security settings being a bitch (" + var4 + ")");
		} catch (final IOException var5) {
			throw new RuntimeException("IO error, could be fucking anything (" + var5 + ")");
		}
	}

	public void close() {
		try {
			if (this.plec != null) {
				this.plec.close();
			}

			this.plec = null;
		} catch (final IOException var4) {}

		if (this.pred != null) {
			this.pred.close();
		}

		this.pred = null;

		try {
			if (this.socket != null) {
				this.socket.close();
			}

			this.socket = null;
		} catch (final IOException var3) {}

		try {
			if (this.server != null) {
				this.server.close();
			}

			this.server = null;
		} catch (final IOException var2) {}

	}

	public Socket getSocket() {
		return this.socket;
	}

	public void sendMessage(final String text) {
		if (this.pred != null) {
			this.pred.println(text);
		}

	}

	public String getMessage() {
		try {
			return this.plec == null ? null : this.plec.readLine();
		} catch (final IOException var2) {
			throw new RuntimeException("IO error while reading messsage on client socket (" + var2 + ")");
		}
	}
}
