package org.javascool.tools.socket;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

public class SocketClient {
	private Socket socket;
	private BufferedReader plec;
	private PrintWriter pred;

	public SocketClient open(final String host, final int port) {
		try {
			this.socket = new Socket(host, port);
			if (this.socket == null) {
				throw new RuntimeException("Could not connect to server on port " + port + " on host " + host);
			} else {
				this.plec = new BufferedReader(new InputStreamReader(this.socket.getInputStream()));
				this.pred = new PrintWriter(new BufferedWriter(new OutputStreamWriter(this.socket.getOutputStream())),
						true);
				return this;
			}
		} catch (final UnknownHostException var4) {
			throw new IllegalArgumentException("Could not connect to host (" + var4 + ")");
		} catch (final SecurityException var5) {
			throw new RuntimeException("Security settings being a bitch (" + var5 + ")");
		} catch (final IOException var6) {
			throw new RuntimeException("IO Error, could be fucking anything (" + var6 + ")");
		}
	}

	public void close() {
		try {
			if (this.plec != null) {
				this.plec.close();
			}

			this.plec = null;
		} catch (final IOException var3) {}

		if (this.pred != null) {
			this.pred.close();
		}

		this.pred = null;

		try {
			if (this.socket != null) {
				this.socket.close();
			}

			this.socket = null;
		} catch (final IOException var2) {}

	}

	public Socket getSocket() {
		return this.socket;
	}

	public void sendMessage(final String text) {
		if (this.pred != null) {
			this.pred.println(text);
		}

	}

	public String getMessage() {
		try {
			return this.plec == null ? null : this.plec.readLine();
		} catch (final IOException var2) {
			throw new RuntimeException("Error while reading message on socket (" + var2 + ")");
		}
	}
}
