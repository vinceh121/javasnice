package org.javascool.tools;

import java.net.InetAddress;
import java.net.URLEncoder;

import javax.swing.JOptionPane;

import org.javascool.core.Jvs2Java;
import org.javascool.core.Utils;
import org.javascool.macros.Macros;

public class ErrorCatcher {
	private static String uncaughtExceptionAlertHeader;
	private static int uncaughtExceptionAlertOnce = 0;
	private static String uncaughtExceptionKeyword = null;

	private ErrorCatcher() {
	}

	public static void setUncaughtExceptionAlert(final String header, final String revision, final String keyword) {
		ErrorCatcher.uncaughtExceptionAlertHeader = header;
		ErrorCatcher.uncaughtExceptionKeyword = keyword;
		System.setProperty("application.revision", "" + revision);
		Thread.setDefaultUncaughtExceptionHandler((t, e) -> {
			String s = "";
			String r = "";
			if (ErrorCatcher.uncaughtExceptionAlertOnce <= 1) {
				s = s + ErrorCatcher.uncaughtExceptionAlertHeader + "\n<hr><pre>";
				final String[] var5 = new String[] { "application.revision", "java.version", "java.home",
						"java.class.path", "os.name", "os.arch", "os.version", "user.name", "user.home", "user.dir" };
				final int var6 = var5.length;

				for (int var7 = 0; var7 < var6; ++var7) {
					final String p = var5[var7];
					s = s + "> " + p + " = " + System.getProperty(p) + "\n";
				}
			}

			try {
				s = s + "> localhost = " + InetAddress.getLocalHost() + "\n";
			} catch (final Exception var10) {}

			s = s + "> file.enc = " + Utils.javascoolJarEnc() + "\n";
			s = s + "> thread.name = " + t.getName() + "\n";
			s = s + "> throwable = " + e + "\n";
			if (0 < ErrorCatcher.uncaughtExceptionAlertOnce) {
				s = s + "> count = " + ErrorCatcher.uncaughtExceptionAlertOnce + "\n";
			}

			s = s + "> stack-trace = «\n";

			for (int i = 0; i < t.getStackTrace().length; ++i) {
				r = r + e.getStackTrace()[i] + (i < t.getStackTrace().length - 1 ? "\n" : "»");
			}

			final boolean alert = (ErrorCatcher.uncaughtExceptionKeyword == null
					|| r.indexOf(ErrorCatcher.uncaughtExceptionKeyword) != -1)
					&& e.toString().indexOf("java.util.ConcurrentModificationException") == -1
					&& e.toString()
							.indexOf(
									"com.sun.java.swing.plaf.nimbus.DerivedColor$UIResource cannot be cast to com.sun.java.swing.Painter") == -1;
			s = s + r + "</pre><hr>";
			System.err.println(s);
			if (r.toString().indexOf("JvsToJavaTranslated") != -1) {
				Jvs2Java.report(e);
			} else {
				try {
					if (ErrorCatcher.uncaughtExceptionAlertOnce < 3 && alert) {
						final int option = JOptionPane.showConfirmDialog(null,
								"an eviw ewwow showed up and i'm nyow scawed >~< do u want to wepowt ffat wude ewwow so we can fix it (・`ω´・) ?",
								"fucky wucky", JOptionPane.YES_NO_OPTION, JOptionPane.ERROR_MESSAGE,
								Macros.getIcon("icons/owoDisawed.png"));
						if (option == JOptionPane.OK_OPTION) {
							FileManager.load("http://gitlab.com/vinceh121/javasnice/-/issues/new?issue[name]="
									+ URLEncoder.encode("[Autowated fucky wucky report]", "UTF-8")
									+ "&issue[description]="
									+ URLEncoder.encode(s, "UTF-8"));
						}
					}
				} catch (final Exception var9) {
					System.out.println("Could not show the alert from the web (" + var9 + ")");
				}

				if (ErrorCatcher.uncaughtExceptionAlertOnce == 0 && alert) {
					Macros.message(s, true);
				}

				ErrorCatcher.uncaughtExceptionAlertOnce++;
			}

		});
	}

	public static void setUncaughtExceptionAlert(final String header, final String revision) {
		ErrorCatcher.setUncaughtExceptionAlert(header, revision, "org.javascool");
	}
}
