package org.javascool.tools;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Properties;
import java.util.regex.Pattern;

import org.json.JSONArray;
import org.json.JSONObject;

public class Pml {
	private HashMap<String, Object> data = new HashMap<>();
	private static String xml2pml
			= "<?xml version='1.0' encoding='utf-8'?>\n<xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform' version='1.0'>\n  <xsl:output method='text' encoding='utf-8' omit-xml-declaration='yes'/>\n  <xsl:template match='*'>\n  {<xsl:value-of select='name(.)'/><xsl:text> </xsl:text>\n    <xsl:for-each select='@*'><xsl:value-of select='name(.)'/>=\"<xsl:value-of select=\"translate(., '&quot;','¨')\"/>\"<xsl:text> </xsl:text></xsl:for-each>\n    <xsl:if test='count(*) = 0'>\n      <xsl:apply-templates select='*'/>\n      <xsl:value-of select=\"concat('&quot;', translate(translate(text(), '&quot;','¨'), '{', '«'), '&quot;')\"/>\n    </xsl:if>\n    <xsl:if test='count(*) > 0'><xsl:apply-templates/></xsl:if>\n  }</xsl:template>\n</xsl:stylesheet>";
	private String tag = this.getClass().getName();
	private Pml parent = null;
	private int count = -1;
	static Pattern index = Pattern.compile("[0-9]+");

	public Pml reset(final String value) {
		this.data = new HashMap<>();
		this.tag = "";
		this.parent = null;
		this.count = -1;
		new Pml.PmlReader().read(value, this);
		return this;
	}

	public Pml reset(final String value, String format) {
		format = format.toLowerCase();
		if ("xml".equals(format)) {
			return this.reset(Xml2Xml.run(value, Pml.xml2pml).replaceAll("¨", "\\\\\"").replaceAll("«", "\\{"), "pml");
		} else if (!"htm".equals(format) && !"html".equals(format)) {
			return "json".equals(format) ? this.resetJSON(value) : this.reset(value);
		} else {
			return this.reset(Xml2Xml.run(Xml2Xml.html2xhtml(value), Pml.xml2pml), "pml");
		}
	}

	public Pml reset(final Pml pml) {
		this.data = new HashMap<>();
		this.tag = "";
		this.parent = null;
		this.count = -1;
		if (pml != null) {
			this.setTag(pml.getTag());
			final Iterator<?> var2 = pml.attributes().iterator();

			while (var2.hasNext()) {
				final String name = (String) var2.next();
				this.set(name, pml.getChild(name));
			}

			for (int i = 0; i < pml.getCount(); ++i) {
				this.set(i, pml.getChild(i));
			}
		}

		return this;
	}

	public Pml reset(final String[] usage) {
		this.reset("{usage}");

		for (int i = 0; i < usage.length; ++i) {
			if (!"-".equals(usage[i])) {
				if (!usage[i].startsWith("-") || i != 0 && "-".equals(usage[i - 1])) {
					this.add(usage[i]);
				} else {
					final String name = usage[i].replaceFirst("-+", "");
					if (i != usage.length - 1 && !usage[i + 1].startsWith("-")) {
						++i;
						this.set(name, usage[i]);
					} else {
						this.set(name, true);
					}
				}
			}
		}

		return this;
	}

	public String toString(String format) {
		format = format.toLowerCase();
		return "xml".equals(format) ? new Pml.XmlWriter().toString(this)
				: "raw".equals(format) ? new Pml.PlainWriter().toString(this, 0)
						: "php".equals(format) ? new Pml.PhpWriter().toString(this)
								: "json".equals(format) ? new Pml.JsonWriter().toString(this)
										: "jmf".equals(format) ? new Pml.JmfWriter().toString(this)
												: new Pml.PlainWriter().toString(this, 180);
	}

	@Override
	public final String toString() {
		return this.toString("raw");
	}

	public final Pml load(final String location, String format, final boolean utf8) {
		if (format == null) {
			format = location.replaceAll("^.*\\.([A-Za-z]+)$", "$1");
		}

		return this.reset(FileManager.load(location, utf8), format);
	}

	public final Pml load(final String location, final String format) {
		return this.load(location, format, false);
	}

	public final Pml load(final String location, final boolean utf8) {
		return this.load(location, (String) null, utf8);
	}

	public final Pml load(final String location) {
		return this.load(location, (String) null, false);
	}

	public final Pml save(final String location, final String format) {
		FileManager.save(location, this.toString(format) + "\n");
		return this;
	}

	public final Pml save(final String location) {
		return this.save(location, location.replaceAll("^.*\\.([A-Za-z]+)$", "$1"));
	}

	private Pml resetJSON(String value) {
		value = value.trim();

		try {
			if (value.trim().startsWith("{")) {
				Pml.resetJSONobject(this, new JSONObject(value));
			} else if (value.trim().startsWith("[")) {
				Pml.resetJSONarray(this, new JSONArray(value));
			}
		} catch (final Exception var3) {
			this.set("tag", "json");
			this.set("body", value);
			this.set("error", var3.toString());
		}

		return this;
	}

	private static Pml resetJSONobject(final Pml pml, final JSONObject object) {
		final Iterator<?> k = object.keys();

		while (k.hasNext()) {
			final String key = k.next().toString();
			if (object.optJSONObject(key) != null) {
				pml.set(key, Pml.resetJSONobject(new Pml(), object.optJSONObject(key)));
			} else if (object.optJSONArray(key) != null) {
				pml.set(key, Pml.resetJSONarray(new Pml(), object.optJSONArray(key)));
			} else {
				pml.set(key, object.optString(key));
			}
		}

		return pml;
	}

	private static Pml resetJSONarray(final Pml pml, final JSONArray array) {
		for (int i = 0; i < array.length(); ++i) {
			if (array.optJSONObject(i) != null) {
				pml.set(i, Pml.resetJSONobject(new Pml(), array.optJSONObject(i)));
			} else if (array.optJSONArray(i) != null) {
				pml.set(i, Pml.resetJSONarray(new Pml(), array.optJSONArray(i)));
			} else {
				pml.set(i, array.optString(i));
			}
		}

		return pml;
	}

	private static String toName(final String string) {
		if (string.length() <= 0) {
			return string;
		} else {
			final String c_0 = string.substring(0, 1);
			String name = !c_0.matches("[_-]") && !Character.isLetter(c_0.charAt(0)) ? "_" : "";

			for (int i = 0; i < string.length(); ++i) {
				final String c_i = string.substring(i, i + 1);
				name = name + (!c_i.matches("_-") && !Character.isLetterOrDigit(c_i.charAt(0)) ? "_" : c_i);
			}

			return name;
		}
	}

	public final String getTag() {
		return this.tag;
	}

	protected final Pml setTag(final String value) {
		this.tag = value;
		return this;
	}

	public final Pml getParent() {
		return this.parent;
	}

	private void setParent(final Pml value) {
		if (this.parent == null) {
			this.parent = value;
		}

	}

	public final boolean isDefined(final String name) {
		return this.data.containsKey(name);
	}

	public final boolean isDefined(final int index) {
		return this.isDefined(Integer.toString(index));
	}

	public Pml getChild(final String name) {
		final Object o = this.data.get(name);
		return o == null ? null : o instanceof Pml ? (Pml) o : new Pml().reset("{\"" + o.toString() + "\"}");
	}

	public final Pml getChild(final int index) {
		return this.getChild(Integer.toString(index));
	}

	public Object getObject(final String name) {
		return this.data.get(name);
	}

	public final Object getObject(final int index) {
		return this.getObject(Integer.toString(index));
	}

	public final String getString(final String name, final String value) {
		if (this.data.get(name) == null) {
			return "";
		} else {
			String v = this.data.get(name).toString();
			if (v.matches("[{]\".*\"[}]")) {
				v = v.substring(2, v.length() - 2);
			}

			if (v.matches("[{].*[}]")) {
				v = v.substring(1, v.length() - 1);
			}

			return v != null ? v : value != null ? value : "";
		}
	}

	public final String getString(final int index, final String value) {
		return this.getString(Integer.toString(index), value);
	}

	public final String getString(final String name) {
		return this.getString(name, (String) null);
	}

	public final String getString(final int index) {
		return this.getString(index, (String) null);
	}

	public final double getDecimal(final String name, final double value) {
		try {
			return Double.parseDouble(this.getString(name));
		} catch (final NumberFormatException var5) {
			return value;
		}
	}

	public final double getDecimal(final int index, final double value) {
		return this.getDecimal(Integer.toString(index), value);
	}

	public final double getDecimal(final String name) {
		return this.getDecimal(name, 0.0D);
	}

	public final double getDecimal(final int index) {
		return this.getDecimal(index, 0.0D);
	}

	public final int getInteger(final String name, final int value) {
		try {
			return Integer.parseInt(this.getString(name));
		} catch (final NumberFormatException var4) {
			return value;
		}
	}

	public final int getInteger(final int index, final int value) {
		return this.getInteger(Integer.toString(index), value);
	}

	public final int getInteger(final String name) {
		return this.getInteger(name, 0);
	}

	public final int getInteger(final int index) {
		return this.getInteger(index, 0);
	}

	public final boolean getBoolean(final String name, final boolean value) {
		final String v = this.getString(name);
		if (v != null) {
			if ("true".equals(v.toLowerCase())) {
				return true;
			}

			if ("false".equals(v.toLowerCase())) {
				return false;
			}
		}

		return value;
	}

	public final boolean getBoolean(final int index, final boolean value) {
		return this.getBoolean(Integer.toString(index), value);
	}

	public final boolean getBoolean(final String name) {
		return this.getBoolean(name, false);
	}

	public final boolean getBoolean(final int index) {
		return this.getBoolean(index, false);
	}

	public Pml set(final String name, final Object value) {
		if (value == null) {
			try {
				final int i = Integer.parseInt(name);
				final int l = this.getCount() - 1;
				if (0 <= i && i <= l) {
					for (int j = i; j < l; ++j) {
						this.data.put(Integer.toString(j), this.data.get(Integer.toString(j + 1)));
					}

					this.data.remove(Integer.toString(l));
				} else {
					this.data.remove(name);
				}
			} catch (final NumberFormatException var6) {
				this.data.remove(name);
			}
		} else {
			this.data.put(name, value);
			if (value instanceof Pml) {
				((Pml) value).setParent(this);
			}
		}

		this.count = -1;
		return this;
	}

	public final Pml set(final int index, final Object value) {
		return this.set(Integer.toString(index), value);
	}

	public final Pml set(final String name, final String value) {
		if (value == null) {
			return this.set(name, (Object) null);
		} else {
			final Pml v = new Pml();
			v.reset("\"" + value.replaceAll("\"", "\\\"") + "\"");
			return this.set(name, v);
		}
	}

	public final Pml set(final int index, final String value) {
		return this.set(Integer.toString(index), value);
	}

	public final Pml set(final String name, final double value) {
		return this.set(name, Double.toString(value));
	}

	public final Pml set(final int index, final double value) {
		return this.set(Integer.toString(index), value);
	}

	public final Pml set(final String name, final int value) {
		return this.set(name, Integer.toString(value));
	}

	public final Pml set(final int index, final int value) {
		return this.set(Integer.toString(index), value);
	}

	public final Pml set(final String name, final boolean value) {
		return this.set(name, value ? "true" : "false");
	}

	public final Pml set(final int index, final boolean value) {
		return this.set(Integer.toString(index), value);
	}

	public Pml del(final String name) {
		return this.set(name, null);
	}

	public final Pml del(final int index) {
		return this.set(Integer.toString(index), null);
	}

	public final Pml add(final Pml value) {
		int c = this.getCount();
		this.set(c, value);
		++c;
		this.count = c;
		return this;
	}

	public final Pml add(final String value) {
		final Pml v = new Pml();
		v.reset(value);
		return this.add(v);
	}

	public final Pml add(final double value) {
		return this.add(Double.toString(value));
	}

	public final Pml add(final int value) {
		return this.add(Integer.toString(value));
	}

	public final Pml set(final Pml pml) {
		final Iterator<?> var2 = pml.attributes().iterator();

		while (var2.hasNext()) {
			final String name = (String) var2.next();
			this.set(name, pml.getObject(name));
		}

		for (int n = 0; n < pml.getCount(); ++n) {
			this.set(n, pml.getObject(n));
		}

		return this;
	}

	public final Pml set(final Properties pml) {
		final Iterator<?> var2 = pml.stringPropertyNames().iterator();

		while (var2.hasNext()) {
			final String name = (String) var2.next();
			this.set(name, pml.getProperty(name));
		}

		return this;
	}

	public int getCount() {
		if (this.count < 0) {
			this.count = 0;
			final Iterator<String> var1 = this.data.keySet().iterator();

			while (var1.hasNext()) {
				final String key = var1.next();
				if (Pml.isIndex(key)) {
					this.count = Math.max(Integer.parseInt(key) + 1, this.count);
				}
			}
		}

		return this.count;
	}

	public int getSize() {
		return this.data.size();
	}

	public final Iterable<?> attributes() {
		return () -> new Iterator<Object>() {
			Iterator<String> keys;
			String key;

			{
				this.keys = Pml.this.data.keySet().iterator();
				this.nextKey();
			}

			private void nextKey() {
				for (this.key = null; this.keys.hasNext() && Pml.isIndex(this.key = this.keys.next()); this.key
						= null) {}

			}

			@Override
			public String next() {
				final String value = this.key;
				this.nextKey();
				return value;
			}

			@Override
			public boolean hasNext() {
				return this.key != null;
			}

			@Override
			public void remove() {
				throw new UnsupportedOperationException();
			}
		};
	}

	private static boolean isIndex(final String name) {
		return Pml.index.matcher(name).matches();
	}

	public final Properties toProperties() {
		final Properties properties = new Properties();
		final Iterator<?> var2 = this.attributes().iterator();

		while (var2.hasNext()) {
			final String name = (String) var2.next();
			properties.setProperty(name, this.getObject(name).toString());
		}

		for (int n = 0; n < this.getCount(); ++n) {
			properties.setProperty("" + n, this.getObject(n).toString());
		}

		return properties;
	}

	public static void main(final String[] usage) {
		if (usage.length > 0) {
			new Pml().load(usage[0])
					.save(usage.length > 1 ? usage[1] : "stdout:",
							usage.length > 1 && usage[1].matches(".*\\.(pml|php|xml|json|jmf)")
									? usage[1].replaceFirst(".*\\.", "")
									: "pml");
		}

	}

	private static class JmfWriter {
		private StringBuffer string;

		private JmfWriter() {
		}

		public String toString(final Pml pml) {
			this.string = new StringBuffer();
			if (pml == null) {
				return "";
			} else {
				final Iterator<?> var2 = pml.attributes().iterator();

				while (var2.hasNext()) {
					final String name = (String) var2.next();
					this.string.append(name).append(": ").append(JmfWriter.quote(pml.getChild(name))).append("\n");
				}

				return this.string.toString();
			}
		}

		private static String quote(final String string) {
			return string.replaceAll("\n", " ");
		}

		private static String quote(final Pml pml) {
			return JmfWriter.quote(pml.getSize() == 0 ? pml.getTag() : pml.toString());
		}

	}

	private static class JsonWriter {

		private JsonWriter() {
		}

		public String toString(final Pml pml) {
			if (pml == null) {
				return " {} ";
			} else if (pml.getSize() == 0) {
				return JsonWriter.quote(pml.getTag());
			} else {
				String s;
				if (pml.getSize() == pml.getCount()) {
					s = "[\n";

					for (int n = 0; n < pml.getCount(); ++n) {
						s = s + (n == 0 ? "" : " , ") + this.toString(pml.getChild(n));
					}

					return s + "]\n";
				} else {
					s = "{\n";
					boolean once = true;
					if (pml.getTag().length() > 0 && !pml.getTag().equals("org.javascool.tools.Pml")
							&& !pml.isDefined("tag")) {
						s = s + " \"tag\" : " + JsonWriter.quote(pml.getTag()) + "\n";
						once = false;
					}

					for (final Iterator<?> var4 = pml.attributes().iterator(); var4.hasNext(); once = false) {
						final String name = (String) var4.next();
						s = s
								+ (once ? "  " : ", ")
								+ JsonWriter.quote(name)
								+ " : "
								+ this.toString(pml.getChild(name))
								+ "\n";
					}

					for (int n = 0; n < pml.getCount(); ++n) {
						if (pml.getChild(n).getSize() == pml.getChild(n).getCount()
								&& pml.getChild(n).getCount() == 1) {
							s = s
									+ (once ? "  " : ", ")
									+ JsonWriter.quote(pml.getChild(n).getTag())
									+ " : "
									+ this.toString(pml.getChild(n).getChild(0))
									+ "\n";
						} else {
							s = s
									+ (once ? "  " : ", ")
									+ JsonWriter.quote("" + n)
									+ " : "
									+ this.toString(pml.getChild(n))
									+ "\n";
						}

						once = false;
					}

					return s + "}\n";
				}
			}
		}

		private static String quote(final String string) {
			return "\""
					+ string.replaceAll("\n", "\t").replaceAll("\\\\", "\\\\\\\\").replaceAll("\"", "\\\\\"")
					+ "\"";
		}
	}

	private static class PhpWriter {
		private StringBuffer string;

		private PhpWriter() {
		}

		public String toString(final Pml pml) {
			this.string = new StringBuffer();
			if (pml == null) {
				return "<?php $pml = array(); ?>";
			} else {
				String tag = Pml.toName(pml.getTag());
				if (tag.length() == 0) {
					tag = "pml";
				}

				this.string.append("<?php $")
						.append(tag)
						.append(" = array(\"_tag\" => ")
						.append(PhpWriter.quote(pml.getTag()));
				final Iterator<?> var3 = pml.attributes().iterator();

				while (var3.hasNext()) {
					final String name = (String) var3.next();
					this.string.append(", ")
							.append(PhpWriter.quote(name))
							.append(" => ")
							.append(PhpWriter.quote(pml.getChild(name)));
				}

				for (int n = 0; n < pml.getCount(); ++n) {
					this.string.append(", ").append(PhpWriter.quote(pml.getChild(n)));
				}

				this.string.append("); ?>");
				return this.string.toString();
			}
		}

		private static String quote(final String string) {
			return "\"" + string.replaceAll("\\\\", "\\\\\\\\").replaceAll("\"", "\\\\\"") + "\"";
		}

		private static String quote(final Pml pml) {
			return PhpWriter.quote(pml.getSize() == 0 ? pml.getTag() : pml.toString());
		}

	}

	private static class XmlWriter {
		private StringBuffer string;

		private XmlWriter() {
		}

		public String toString(final Pml pml) {
			this.string = new StringBuffer();
			if (pml == null) {
				return "<null/>";
			} else {
				this.write(pml);
				return this.string.toString();
			}
		}

		private void write(final Pml pml) {
			if (pml.getSize() == 0) {
				this.string.append(" ")
						.append(pml.getTag()
								.replaceFirst("^\"([{}])\"$", "$1")
								.replaceAll("&", "&amp;")
								.replaceAll("<", "&lt;"));
			} else {
				this.string.append(" <").append(Pml.toName(pml.getTag()));
				final Iterator<?> var2 = pml.attributes().iterator();

				while (var2.hasNext()) {
					final String name = (String) var2.next();
					this.string.append(" ")
							.append(Pml.toName(name))
							.append("=\"")
							.append(pml.getChild(name)
									.toString()
									.replaceFirst("^\\{(.*)\\}$", "$1")
									.replaceFirst("^\"(.*)\"$", "$1")
									.replaceAll("&", "&amp;")
									.replaceAll("<", "&lt;")
									.replaceAll("\"", "&quot;"))
							.append("\"");
				}

				if (pml.getCount() > 0) {
					this.string.append(">");

					for (int n = 0; n < pml.getCount(); ++n) {
						this.write(pml.getChild(n));
					}

					this.string.append("</").append(Pml.toName(pml.getTag())).append(">");
				} else {
					this.string.append("/>");
				}
			}

		}

	}

	private static class PlainWriter {
		private StringBuffer string;
		int width;
		int l;

		private PlainWriter() {
		}

		public String toString(final Pml pml, final int width) {
			if (pml == null) {
				return "null";
			} else {
				this.string = new StringBuffer();
				if (width == 0) {
					this.write1d(pml);
				} else {
					this.width = width;
					this.l = 0;
					this.write2d(pml, 0, 0);
				}

				return this.string.toString();
			}
		}

		private void write1d(final Pml pml) {
			if (pml == null) {
				this.string.append(" {null} ");
			} else {
				this.string.append("{").append(PlainWriter.quote(pml.getTag()));
				final Iterator<?> var2 = pml.attributes().iterator();

				while (var2.hasNext()) {
					final String name = (String) var2.next();
					this.string.append(" ").append(PlainWriter.quote(name)).append("=");
					this.write1d(pml.getChild(name));
				}

				for (int n = 0; n < pml.getCount(); ++n) {
					this.string.append(" ");
					this.write1d(pml.getChild(n));
				}

				this.string.append("}");
			}
		}

		private boolean write2d(final Pml pml, final int n, final int tab) {
			if (pml == null) {
				this.string.append(" {null} ");
				return false;
			} else {
				boolean ln;
				if (pml.getSize() == 0) {
					ln = n >= 0 && (n == 0 || pml.getParent() != null && pml.getParent().getChild(n - 1) != null
							&& pml.getParent().getChild(n - 1).getSize() > 0);
					this.writeln(ln, tab);
					this.write(PlainWriter.quote(pml.getTag()), tab);
					return ln;
				} else {
					ln = pml.getTag().length() > 1 || "p".equals(pml.getTag());
					this.writeln(n >= 0 && ln, tab);
					this.write("{" + PlainWriter.quote(pml.getTag()), tab);
					ln = false;

					String name;
					for (final Iterator<?> var5 = pml.attributes().iterator(); var5.hasNext(); ln
							|= this.write2d(pml.getChild(name), -1, tab + 1)) {
						name = (String) var5.next();
						this.write(" " + PlainWriter.quote(name) + " =", tab);
					}

					for (int i = 0; i < pml.getCount(); ++i) {
						ln |= this.write2d(pml.getChild(i), i, tab + 2);
					}

					this.writeln(ln, tab);
					this.write("}", tab);
					return ln;
				}
			}
		}

		private void writeln(final boolean ln, final int tab) {
			if (ln) {
				this.string.append("\n");

				for (int t = 0; t < tab; ++t) {
					this.string.append(" ");
				}

				this.l = tab;
			} else {
				this.string.append(" ");
			}

		}

		private void write(final String word, final int tab) {
			if (this.l + word.length() > this.width) {
				this.writeln(false, tab + 1);
			}

			this.string.append(word);
			this.l += word.length();
		}

		private static String quote(final String string) {
			return string == null ? "null"
					: !string.matches("[a-zA-Z_][a-zA-Z0-9_]*") && !"\"{\"".equals(string) && !"\"}\"".equals(string)
							? "\"" + string.replaceAll("\\\\", "\\\\\\\\").replaceAll("\"", "\\\\\"") + "\""
							: string;
		}

	}

	private static class PmlReader extends Pml.TokenReader {
		private PmlReader() {
		}

		public void read(final String string, final Pml pml) {
			this.reset(string);
			this.parse(pml);
			final String trailer = this.trailer();
			if (trailer.length() > 0) {
				final Pml p = new Pml();
				p.setTag(trailer);
				pml.set("string_trailer", p);
			}

		}

		private Pml parse(final Pml pml) {
			final String b = this.getToken(0);
			if ("{".equals(b)) {
				this.next(1);
				boolean start = true;

				while (true) {
					final String t = this.getToken(0);
					if ("}".equals(t)) {
						this.next(1);
						break;
					}

					Pml p;
					if ("{".equals(t)) {
						p = new Pml();
						this.parse(p);
						pml.add(p);
					} else if ("=".equals(this.getToken(1))) {
						this.next(2);
						if ("}".equals(this.getToken(0))) {
							pml.set(t, "true");
						} else {
							p = new Pml();
							this.parse(p);
							pml.set(t, p);
						}
					} else if (start) {
						this.next(1);
						pml.setTag(t);
					} else {
						p = new Pml();
						this.parse(p);
						pml.add(p);
					}

					start = false;
				}
			} else if ("=".equals(this.getToken(1))) {
				while ("=".equals(this.getToken(1))) {
					final String t = this.getToken(0);
					this.next(2);
					if ("}".equals(this.getToken(0))) {
						pml.set(t, "true");
					} else {
						final Pml p = new Pml();
						this.parse(p);
						pml.set(t, p);
					}
				}
			} else {
				pml.setTag(b);
				this.next(1);
			}

			return pml;
		}
	}

	protected static class TokenReader {
		ArrayList<token> tokens;
		int itoken;

		public Pml.TokenReader reset(final String string) {
			this.tokens = new ArrayList<>();
			this.itoken = 0;
			final char[] chars = string.toCharArray();
			int ichar = 0;
			int ln = 0;

			while (true) {
				while (true) {
					do {
						if (ichar >= chars.length) {
							this.itoken = 0;
							return this;
						}

						for (; ichar < chars.length && Character.isWhitespace(chars[ichar]); ++ichar) {
							if (chars[ichar] == '\n') {
								++ln;
							}
						}
					} while (ichar >= chars.length);

					int ichar0 = ichar;
					if (chars[ichar] == '"') {
						for (; ichar < chars.length
								&& (ichar == ichar0 || chars[ichar] != '"' || chars[ichar - 1] == '\\'); ++ichar) {
							if (chars[ichar] == '\n') {
								++ln;
							}
						}

						++ichar;
						int ichar1;
						if (ichar != ichar0 + 3 || chars[ichar0 + 1] != '{' && chars[ichar0 + 1] != '}') {
							++ichar0;
							ichar1 = ichar - 1;
						} else {
							ichar1 = ichar;
						}

						this.tokens.add(new Pml.TokenReader.token(string, ichar0, ichar1, ln));
					} else if (!Character.isLetter(chars[ichar]) && chars[ichar] != '_') {
						if (!Character.isDigit(chars[ichar]) && chars[ichar] != '.') {
							if (!TokenReader.isOperator(chars[ichar])) {
								this.tokens.add(new Pml.TokenReader.token(string, ichar++, ichar, ln));
							} else {
								while (ichar < chars.length && TokenReader.isOperator(chars[ichar])) {
									++ichar;
								}

								this.tokens.add(new Pml.TokenReader.token(string, ichar0, ichar, ln));
							}
						} else {
							while (ichar < chars.length && Character.isDigit(chars[ichar])) {
								++ichar;
							}

							if (ichar < chars.length && chars[ichar] == '.') {
								++ichar;

								while (ichar < chars.length && Character.isDigit(chars[ichar])) {
									++ichar;
								}
							}

							if (ichar < chars.length && (chars[ichar] == 'E' || chars[ichar] == 'e')) {
								++ichar;
								if (ichar < chars.length && (chars[ichar] == '+' || chars[ichar] == '-')) {
									++ichar;
								}

								while (ichar < chars.length && Character.isDigit(chars[ichar])) {
									++ichar;
								}
							}

							this.tokens.add(new Pml.TokenReader.token(string, ichar0, ichar, ln));
						}
					} else {
						while (ichar < chars.length
								&& (Character.isLetterOrDigit(chars[ichar]) || chars[ichar] == '_')) {
							++ichar;
						}

						this.tokens.add(new Pml.TokenReader.token(string, ichar0, ichar, ln));
					}
				}
			}
		}

		private static boolean isOperator(final char c) {
			switch (c) {
			case '!':
			case '%':
			case '&':
			case '*':
			case '+':
			case '-':
			case '.':
			case '/':
			case ':':
			case '<':
			case '=':
			case '>':
			case '^':
			case '|':
				return true;
			default:
				return false;
			}
		}

		public String getToken(final int next) {
			final String current
					= this.itoken + next < this.tokens.size() ? this.tokens.get(this.itoken + next).string : "}";
			return current;
		}

		public boolean isNext() {
			return this.itoken < this.tokens.size();
		}

		public void next(final int next) {
			this.itoken += next;
		}

		public String trailer() {
			String t;
			for (t = ""; this.itoken < this.tokens.size(); t = t + " " + this.tokens.get(this.itoken++).string) {}

			return t.trim();
		}

		public void check(final boolean ok, final String message) {
			if (!ok) {
				System.out.println("Syntax error \""
						+ message
						+ "\", line "
						+ (this.itoken < this.tokens.size()
								? "" + this.tokens.get(this.itoken).line + " to \"" + this.getToken(0) + "\""
								: "final"));
			}

		}

		@Override
		public String toString() {
			String s = "[";

			for (int i = 0; i < this.tokens.size(); ++i) {
				s = s
						+ (i == this.itoken ? " ! " : " ")
						+ "\""
						+ this.tokens.get(i).string
						+ "\"#"
						+ this.tokens.get(i).line;
			}

			return s + " ]";
		}

		private static class token {
			String string;
			int line;

			token(final String s, final int i0, final int i1, final int l) {
				this.string = s.substring(i0, i1);
				this.line = l;
			}

			@Override
			public String toString() {
				return "#" + this.line + " \"" + this.string + "\" ";
			}
		}
	}
}
