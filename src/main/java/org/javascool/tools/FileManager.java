package org.javascool.tools;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

public class FileManager {

	private FileManager() {
	}

	public static String load(final String location, final boolean utf8) {
		try {
			final BufferedReader reader
					= new BufferedReader(new InputStreamReader(FileManager.getResourceURL(location, true).openStream(),
							utf8 ? Charset.forName("utf-8") : Charset.defaultCharset()), 10240);
			final StringBuilder buffer = new StringBuilder();
			final char[] chars = new char[10240];

			while (true) {
				final int l = reader.read(chars);
				if (l == -1) {
					return buffer.toString();
				}

				buffer.append(chars, 0, l);
			}
		} catch (final IOException var6) {
			throw new RuntimeException(var6 + " when loading: " + location);
		}
	}

	public static String load(final String location) {
		return FileManager.load(location, false);
	}

	public static void save(String location, final String string, final boolean backup, final boolean utf8) {
		if (location.startsWith("stdout:")) {
			System.out.println("\n" + location + " " + string);
		} else {
			location = FileManager.getResourceURL(location, false).toString();

			try {
				if (location.startsWith("file:") && new File(location.substring(5)).getParentFile() != null) {
					new File(location.substring(5)).getParentFile().mkdirs();
				}

				if (backup && !location.startsWith("file:")) {
					throw new IllegalArgumentException("Could not load backup URL «" + location + "»");
				} else {
					final OutputStreamWriter writer = location.startsWith("file:")
							? FileManager.getFileWriter(location.substring(5), backup, utf8)
							: FileManager.getUrlWriter(location, utf8);

					for (int i = 0; i < string.length(); ++i) {
						writer.write(string.charAt(i));
					}

					writer.close();
				}
			} catch (final IOException var6) {
				throw new RuntimeException(var6 + " when saving: " + location);
			}
		}
	}

	public static void save(final String location, final String string, final boolean backup) {
		FileManager.save(location, string, backup, false);
	}

	public static void save(final String location, final String string) {
		FileManager.save(location, string, false, false);
	}

	private static OutputStreamWriter getUrlWriter(final String location, final boolean utf8) throws IOException {
		final URL url = new URL(location);
		final URLConnection connection = url.openConnection();
		connection.setDoOutput(true);
		final OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream(),
				utf8 ? Charset.forName("utf-8") : Charset.defaultCharset());
		if (url.getProtocol().equals("mailto")) {
			final int i = url.toString().indexOf("?subject=");
			if (i != -1) {
				writer.write("Subject: " + url.toString().substring(i + 9) + "\n");
			}
		}

		return writer;
	}

	private static OutputStreamWriter getFileWriter(final String location, final boolean backup, final boolean utf8)
			throws IOException {
		final File file = new File(location);
		final File parent = file.getParentFile();
		if (parent != null && !parent.isDirectory()) {
			parent.mkdirs();
		}

		if (backup && file.exists()) {
			FileManager.backup(file);
		}

		return new OutputStreamWriter(new FileOutputStream(location),
				utf8 ? Charset.forName("utf-8") : Charset.defaultCharset());
	}

	private static void backup(final File file) {
		final File backup = new File(file.getAbsolutePath() + "~");
		if (backup.exists()) {
			FileManager.backup(backup);
		}

		file.renameTo(backup);
	}

	public static boolean exists(String location) {
		if (location.matches("(ftp|http|https|jar):.*")) {
			try {
				return FileManager.exists(new URL(location));
			} catch (final IOException var2) {
				return false;
			}
		} else {
			if (location.matches("file:.*")) {
				location = location.substring(5);
			}

			return new File(location).canRead();
		}
	}

	public static boolean exists(final URL location) {
		try {
			location.openStream().close();
			return true;
		} catch (final IOException var2) {
			return false;
		}
	}

	public static long getLastModified(String location) {
		location = FileManager.getResourceURL(location).toString();
		if (location.matches("(ftp|http|https|jar):.*")) {
			try {
				return new URL(location).openConnection().getLastModified();
			} catch (final IOException var2) {
				return 0L;
			}
		} else {
			if (location.matches("file:.*")) {
				location = location.substring(5);
			}

			return new File(location).lastModified();
		}
	}

	public static String[] list(String folder, final String pattern, final int depth) {
		if (folder.matches("(ftp|http|https|jar):.*")) {
			throw new IllegalArgumentException("Cannot read URL content of this type: " + folder);
		} else {
			if (folder.matches("file:.*")) {
				folder = folder.substring(5);
			}

			final ArrayList<String> files = new ArrayList<>();
			if (folder.matches(".*\\.jar")) {
				try {
					final JarFile jarFile = new JarFile(folder);
					final Enumeration<JarEntry> e = jarFile.entries();
					jarFile.close();

					while (true) {
						String file;
						do {
							if (!e.hasMoreElements()) {
								return files.toArray(new String[files.size()]);
							}

							file = e.nextElement().getName();
						} while (pattern != null && !file.matches(pattern));

						files.add("jar:" + folder + "!" + file);
					}
				} catch (final IOException var9) {
					throw new IllegalArgumentException(var9);
				}
			} else if (new File(folder).isDirectory() && depth >= 0) {
				try {
					File[] var10 = new File(folder).listFiles();
					int var11 = var10.length;

					int var6;
					File file;
					for (var6 = 0; var6 < var11; ++var6) {
						file = var10[var6];
						if (pattern == null || file.getName().matches(pattern)) {
							files.add(file.getCanonicalPath());
						}
					}

					if (depth > 0) {
						var10 = new File(folder).listFiles();
						var11 = var10.length;

						for (var6 = 0; var6 < var11; ++var6) {
							file = var10[var6];
							if (file.isDirectory()) {
								files.addAll(
										Arrays.asList(FileManager.list(file.getCanonicalPath(), pattern, depth - 1)));
							}
						}
					}
				} catch (final IOException var8) {
					throw new IllegalArgumentException(var8);
				}
			}

			return files.toArray(new String[files.size()]);
		}
	}

	public static String[] list(final String folder, final int depth) {
		return FileManager.list(folder, (String) null, depth);
	}

	public static String[] list(final String folder, final String pattern) {
		return FileManager.list(folder, pattern, 0);
	}

	public static String[] list(final String folder) {
		return FileManager.list(folder, (String) null, 0);
	}

	public static URL getResourceURL(String location, final String base, final boolean reading) {
		if (base != null) {
			location = base + "/" + location;
		}

		try {
			URL url;
			if (location.matches("jar:[^!]*!.*")) {
				final String res = location.replaceFirst("[^!]*!/", "");
				url = Thread.currentThread().getContextClassLoader().getResource(res);
				if (url != null) {
					return url;
				} else {
					throw new IllegalArgumentException(
							"Unable to find " + res + " from " + location + " as a classpath resource");
				}
			} else if (location.matches("(ftp|http|https|jar|mailto|stdout):.*")) {
				return new URL(location).toURI().normalize().toURL();
			} else {
				if (location.startsWith("file:")) {
					location = location.substring(5);
				}

				if (reading) {
					File file;
					try {
						file = new File(location);
						if (file.exists()) {
							return new URL("file:" + file.getCanonicalPath());
						}
					} catch (final Throwable var8) {}

					try {
						file = new File(System.getProperty("user.dir"), location);
						if (file.exists()) {
							return new URL("file:" + file.getCanonicalPath());
						}
					} catch (final Throwable var7) {}

					try {
						file = new File(System.getProperty("user.home"), location);
						if (file.exists()) {
							return new URL("file:" + file.getCanonicalPath());
						}
					} catch (final Throwable var6) {}

					try {
						url = Thread.currentThread().getContextClassLoader().getResource(location);
						if (url != null) {
							return url;
						}
					} catch (final Throwable var5) {}
				}

				return new URL("file:" + location);
			}
		} catch (final IOException var9) {
			throw new IllegalArgumentException(var9 + " : " + location + " is a malformed URL");
		} catch (final URISyntaxException var10) {
			throw new IllegalArgumentException(var10 + " : " + location + " is a malformed URL");
		}
	}

	public static URL getResourceURL(final String location, final String base) {
		return FileManager.getResourceURL(location, base, true);
	}

	public static URL getResourceURL(final String location, final boolean reading) {
		return FileManager.getResourceURL(location, (String) null, reading);
	}

	public static URL getResourceURL(final String location) {
		return FileManager.getResourceURL(location, (String) null, true);
	}
}
