package org.javascool.tools;

import java.io.StringReader;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Properties;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

public class Xml2Xml {
	private static TransformerFactory tfactory;
	private static HashMap<String, Transformer> tranformers = new HashMap<>();

	private Xml2Xml() {
	}

	public static String run(String xml, final String xsl, final Properties params) {
		try {
			if (!Xml2Xml.tranformers.containsKey(xsl)) {
				StreamSource xslSource;
				if (xsl.trim().startsWith("<")) {
					xslSource = new StreamSource(new StringReader(xsl));
				} else {
					xslSource = new StreamSource(xsl);
					xslSource.setSystemId(xsl);
				}

				Xml2Xml.tranformers.put(xsl, Xml2Xml.tfactory.newTransformer(xslSource));
			}
		} catch (final TransformerConfigurationException var8) {
			throw new RuntimeException(var8 + " when compiling: " + xsl);
		}

		try {
			if (xml == null) {
				xml = "<null/>";
			}

			final StringWriter writer = new StringWriter();
			final Transformer transformer = Xml2Xml.tranformers.get(xsl);
			transformer.clearParameters();
			if (params != null) {
				final Iterator<?> var5 = params.stringPropertyNames().iterator();

				while (var5.hasNext()) {
					final String name = (String) var5.next();
					transformer.setParameter(name, params.getProperty(name));
				}
			}

			final StreamSource in
					= xml.trim().startsWith("<") ? new StreamSource(new StringReader(xml)) : new StreamSource(xml);
			transformer.transform(in, new StreamResult(writer));
			return writer.toString();
		} catch (final TransformerException var7) {
			throw new IllegalArgumentException(var7.getMessageAndLocation());
		}
	}

	public static String run(final String xml, final String xsl) {
		return Xml2Xml.run(xml, xsl, (Properties) null);
	}

	public static String run(final String xml, final String xsl, final String paramName, final String paramValue) {
		final Properties params = new Properties();
		params.setProperty(paramName, paramValue);
		return Xml2Xml.run(xml, xsl, params);
	}

	public static String html2xhtml(final String htm) {
		return htm.replaceAll("&agrave;", "à")
				.replaceAll("&acirc;", "â")
				.replaceAll("&eacute;", "é")
				.replaceAll("&egrave;", "è")
				.replaceAll("&euml;", "ë")
				.replaceAll("&ecirc;", "ê")
				.replaceAll("&iuml;", "ï")
				.replaceAll("&icirc;", "î")
				.replaceAll("&ouml;", "ö")
				.replaceAll("&ocirc;", "ô")
				.replaceAll("&ldquo;", "&#8220;")
				.replaceAll("&rdquo;", "&#8221;")
				.replaceAll("&laquo;", "&#171;")
				.replaceAll("&raquo;", "&#172;;")
				.replaceAll("&ugrave;", "ù")
				.replaceAll("&ccedil;", "ç")
				.replaceAll("<[!?][^>]*>", "")
				.replaceAll("&nbsp;", "&#160;")
				.replaceAll("(<(meta|img|hr|br|link)[^>]*[^>/]?)>", "$1/>");
	}

	public static void main(final String[] usage) {
		if (usage.length == 5) {
			FileManager.save(usage[2], Xml2Xml.run(FileManager.load(usage[0]), usage[1], usage[3], usage[4]));
		} else if (usage.length > 1) {
			FileManager.save(usage.length > 2 ? usage[2] : "stdout:",
					Xml2Xml.run(FileManager.load(usage[0]), usage[1]));
		}

	}

	static {
		try {
			System.setProperty("javax.xml.parsers.SAXParserFactory", "com.icl.saxon.aelfred.SAXParserFactoryImpl");
			System.setProperty("javax.xml.transform.TransformerFactory", "com.icl.saxon.TransformerFactoryImpl");
			System.setProperty("javax.xml.parsers.DocumentBuilderFactory",
					"com.icl.saxon.om.DocumentBuilderFactoryImpl");
			Xml2Xml.tfactory = TransformerFactory.newInstance();
		} catch (final Throwable var1) {
			var1.printStackTrace();
		}

	}
}
