package org.javascool.tools.image;

import java.awt.image.BufferedImage;
import java.io.EOFException;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.Reader;
import java.io.StringReader;
import java.net.URL;
import java.net.URLConnection;

import javax.imageio.ImageIO;

import org.javascool.macros.Macros;
import org.javascool.tools.FileManager;

public class ImageUtils {
	private ImageUtils() {
	}

	public static BufferedImage loadImage(final String location) {
		BufferedImage image = null;

		try {
			final String format = location.replaceFirst(".*\\.([a-z]+)", "$1").toLowerCase();
			if (!"pbm".equals(format) && !"pgm".equals(format) && !"ppm".equals(format)) {
				image = ImageIO.read(Macros.getResourceURL(location));
			} else {
				image = ImageUtils.loadImagePxM(location);
			}
		} catch (final IOException var3) {
			throw new RuntimeException(var3 + " when loading: " + location + " : " + Macros.getResourceURL(location));
		}

		if (image == null) {
			throw new RuntimeException("Unable to load: " + location);
		} else {
			return image;
		}
	}

	public static void saveImage(String location, final BufferedImage image) {
		final String format = location.replaceFirst(".*\\.([a-z]+)", "$1").toLowerCase();
		if (!"pbm".equals(format) && !"pgm".equals(format) && !"ppm".equals(format)) {
			location = Macros.getResourceURL(location).toString();

			try {
				if (location.startsWith("file:")) {
					ImageIO.write(image, "png", new File(location.substring(5)));
				} else {
					final URLConnection connection = new URL(location).openConnection();
					connection.setDoOutput(true);
					final OutputStream writer = connection.getOutputStream();
					ImageIO.write(image, "png", writer);
					writer.close();
				}
			} catch (final IOException var5) {
				throw new RuntimeException(var5 + " when saving: " + location);
			}
		} else {
			ImageUtils.saveImagePxM(location, image, format);
		}

	}

	private static void saveImagePxM(final String location, final BufferedImage image, String format) {
		format = format.toLowerCase();
		final StringBuffer s
				= new StringBuffer((format.equals("pbm") ? "P1" : format.equals("pgm") ? "P2" : "P3") + "\n");
		s.append(image.getWidth()
				+ " "
				+ image.getHeight()
				+ (format.equals("pbm") ? "" : format.equals("pgm") ? " 765" : " 255")
				+ "\n");
		final String c = image.getWidth() < (format.equals("pbm") ? 35 : format.equals("pgm") ? 17 : 5) ? " " : "\n";

		for (int j = 0; j < image.getHeight(); ++j) {
			for (int i = 0; i < image.getWidth(); ++i) {
				final int rgb = image.getRGB(i, j);
				final int r = rgb >> 16 & 255;
				final int g = rgb >> 8 & 255;
				final int b = rgb & 255;
				final int v = r + g + b;
				// int o = v >= 383;
				// int o = v < 383 ? 0 : 1;
				s.append((format.equals("pbm") ? "" + (b > 0 ? 1 : 0)
						: format.equals("pgm") ? "" + v : "" + r + " " + g + " " + b)
						+ (i < image.getWidth() - 1 ? c : "\n"));
			}
		}

		FileManager.save(location, s.toString());
	}

	private static BufferedImage loadImagePxM(final String location) {
		try {
			final ImageUtils.PxMReader reader = new ImageUtils.PxMReader(new StringReader(FileManager.load(location)));
			final String header = reader.readString(false);
			if (!"P1".equals(header) && !"P2".equals(header) && !"P3".equals(header)) {
				return null;
			} else {
				final int width = reader.readInteger();
				final int height = reader.readInteger();
				final double scale = 255.0D / ("P1".equals(header) ? 1 : reader.readInteger());
				final BufferedImage img = new BufferedImage(width, height, 1);

				for (int j = 0; j < img.getHeight(); ++j) {
					for (int i = 0; i < img.getWidth(); ++i) {
						int r;
						int g;
						int b;
						if ("P3".equals(header)) {
							r = reader.readPixel(scale, false);
							g = reader.readPixel(scale, false);
							b = reader.readPixel(scale, false);
						} else if ("P1".equals(header)) {
							r = g = b = 255 - reader.readPixel(scale, true);
						} else {
							r = g = b = reader.readPixel(scale, false);
						}

						img.setRGB(i, j, r << 16 | g << 8 | b);
					}
				}

				return img;
			}
		} catch (final Exception var13) {
			System.out.println("Error while reading: " + location + " : " + var13);
			return null;
		}
	}

	private static class PxMReader {
		private final Reader r;
		int c = -2;

		public PxMReader(final Reader r) {
			this.r = r;
		}

		public String readString(final boolean binary) throws IOException {
			if (this.c == -2) {
				this.c = this.r.read();
			}

			while (Character.isWhitespace(this.c)) {
				this.c = this.r.read();
			}

			while (this.c == 35) {
				do {
					this.c = this.r.read();
				} while (this.c != -1 && this.c != 10);

				while (Character.isWhitespace(this.c)) {
					this.c = this.r.read();
				}
			}

			if (this.c == -1) {
				throw new EOFException();
			} else {
				final StringBuffer s = new StringBuffer();

				do {
					s.append((char) this.c);
					this.c = this.r.read();
				} while (this.c != -1 && !binary && !Character.isWhitespace(this.c));

				return s.toString();
			}
		}

		public int readInteger() throws IOException {
			return Integer.parseInt(this.readString(false));
		}

		public int readPixel(final double scale, final boolean binary) throws IOException {
			final int v = (int) Math.rint(scale * Double.parseDouble(this.readString(binary)));
			return v < 0 ? 0 : 255 < v ? 255 : v;
		}
	}
}
