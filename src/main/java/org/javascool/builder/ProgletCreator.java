package org.javascool.builder;

import java.io.File;

import org.javascool.tools.FileManager;

public class ProgletCreator {
    private static final String progletPattern = "title=\"«proglet» example\"\nauthor=\"FirstName LastName<email@server.com>\"\nicon=\"sample.png\"\n";
    private static final String helpPattern = "<div title=\"«proglet» @name\">\n  <div class=\"objectif\">\n  </div>\n  <div class=\"intros\">\n    <div title=\"item 1\">\n    </div>\n  etc..\n  </div>\n  <div class=\"works\">\n    <div title=\"item 1\">\n    </div>\n  etc..\n  </div>\n  <div class=\"notes\">\n   <!-- referenced by tags <l class=\"note\" link=\"1\"/> -->\n    <div title=\"item 1\">\n    </div>\n  etc..\n  </div>\n</div>\n";
    private static final String panelPattern = "package org.javascool.proglets.@name;\nimport static org.javascool.macros.Macros.*;\nimport static org.javascool.proglets.@name.Functions.*;\nimport javax.swing.JPanel;\n\n/** Defines the proglet panel «@name» (DISPOSE IF NOT USED).\n *\n * @see <a href=\"Panel.java.html\">source code</a>\n * @serial exclude\n */\npublic class Panel extends JPanel /* or any other Component */ {\n\n  // @bean\n public Panel() {  // @todo Add here the definition of the graphical object\n  }\n\n  /** Proglet demo */\n  public void run() {\n  // @todo Define here the proglet example code.\n  }\n\n}\n";
    private static final String functionsPattern = "package org.javascool.proglets.@name;\nimport static org.javascool.macros.Macros.*;\n\n/** Defines methods to use proglet «@name» (TO DISPOSE IF NOT USED).\n *\n * @see <a href=\"Functions.java.html\">source code</a>\n * @serial exclude\n */\npublic class Functions {\n  private static final long serialVersionUID = 1L;\n  // @factory\n  private Functions() {}\n  /** Returns the proglet instance.\n   * <p> Used for <tt>getPane().method(..)</tt>.</p>\n   */\n  private static Panel getPane() {\n     return (Panel) getProgletPane();\n  }\n\n  //@todo Here, define the methods <tt>public static</tt>\n\n}\n";
    private static final String completionPattern = "<keywords>\n  <keyword \n    name=\"name of completion\" \n    title=\"line description\">\n    <code>completion source code</code>\n    <doc>Method documentation</doc>\n  </keyword>\n  <!-- other keyword -->\n</keywords>\n";
    private static final String translatorPattern = "package org.javascool.proglets.@name;\n\n/** Defines the translation from Jvs to Java to use the proglet «@name» (TO DISPOSE IF NOT USED).\n *\n * @see <a href=\"Translator.java.html\">course code</a>\n * @serial exclude\n */\npublic class Translator extends org.javascool.core.Translator {\n    @Override\n     public String getImports() {\n    return \"\";\n  }\n    @Override\n  public String translate(String code) {\n    return code;\n  }\n}\n";

    private ProgletCreator() {
    }

    public static boolean mkdirProglet(String location) {
        String name = (new File(location)).getName();
        if (!name.matches("[a-z][a-zA-Z][a-zA-Z][a-zA-Z]+")) {
            System.out.println("Proglet name «" + name + "» is wierd: \n respect the bloody naming standars");
            System.out.println("you fucking cunt");
            return false;
        } else {
            if ((new File(location)).isDirectory()) {
                System.out.println("Folder «" + location + "» already exists, files will be renamed");
            }

            if (!(new File(location)).isDirectory() && !(new File(location)).mkdirs()) {
                String tail = (new File(location)).exists() ? "a file exists at this path"
                        : "it should be forbidden to create a folder at this path";
                System.out.println("Could not create folder «" + location + "» for proglet, " + tail);
            }

            FileManager.save(location + File.separator + "proglet.pml", progletPattern.replaceAll("@name", name), true,
                    true);
            FileManager.save(location + File.separator + "help.xml", helpPattern.replaceAll("@name", name), true, true);
            FileManager.save(location + File.separator + "Panel.java", panelPattern.replaceAll("@name", name), true,
                    true);
            FileManager.save(location + File.separator + "Functions.java", functionsPattern.replaceAll("@name", name),
                    true, true);
            FileManager.save(location + File.separator + "completion.xml", completionPattern.replaceAll("@name", name),
                    true, true);
            FileManager.save(location + File.separator + "Translator.java", translatorPattern.replaceAll("@name", name),
                    true, true);
            System.out.println("Proglet «" + name + "» has been created at " + location);
            return true;
        }
    }
}
