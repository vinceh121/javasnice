package org.javascool.builder;

import java.awt.BorderLayout;
import java.awt.Color;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JToolBar;

import org.javascool.tools.FileManager;
import org.javascool.tools.Xml2Xml;

public class Htm2Hml extends JPanel {
    /**
     * 
     */
    private static final long serialVersionUID = -2365819987269409474L;
    private JTextArea htm;
    private JTextArea hml;

    public Htm2Hml() {
        this.setLayout(new BorderLayout());
        JToolBar b = new JToolBar();
        b.setFloatable(false);
        b.add(new JLabel("Translating [X]HTML in HML (Paste HTML on the left and copy the HTM on the right) -> "));
        b.add(new JButton("[Translate]") {
            /**
             * 
             */
            private static final long serialVersionUID = 8987820480973437480L;

            {
                this.addActionListener(e -> Htm2Hml.this.hml.setText(Htm2Hml.translate(Htm2Hml.this.htm.getText())));
            }
        });
        b.add(Box.createHorizontalGlue());
        this.add(b, "North");
        JPanel c = new JPanel();
        c.add(new JScrollPane(this.htm = new JTextArea(40, 64)));
        c.add(new JScrollPane(this.hml = new JTextArea(40, 64) {
            private static final long serialVersionUID = 1L;

            {
                this.setBackground(new Color(200, 200, 200));
                this.setEditable(false);
            }
        }));
        this.add(c, "Center");
    }

    private static String translate(String htm) {
        String hml = Xml2Xml.html2xhtml(htm);

        try {
            hml = Xml2Xml.run(hml, FileManager.load("builder/htm2hml.xslt"));
        } catch (Exception var3) {
            System.out.println("Could not translate HTML in HTM: " + var3);
        }

        return hml;
    }

    public static void main(String[] usage) {
        if (usage.length > 0) {
            FileManager.save(usage.length > 1 ? usage[1] : "stdout:", translate(FileManager.load(usage[0])));
        }
    }
}
