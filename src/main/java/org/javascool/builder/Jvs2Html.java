package org.javascool.builder;

import de.java2html.converter.JavaSource2HTMLConverter;
import de.java2html.javasource.JavaSource;
import de.java2html.javasource.JavaSourceParser;
import de.java2html.javasource.JavaSourceType;
import de.java2html.options.JavaSourceConversionOptions;
import de.java2html.options.JavaSourceStyleEntry;
import de.java2html.util.RGB;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;

import org.javascool.tools.FileManager;

public class Jvs2Html {
    private Jvs2Html() {
    }

    public static String run(String code) {
        try {
            StringReader stringReader = new StringReader(code);
            JavaSource source = (new JavaSourceParser()).parse(stringReader);
            JavaSource2HTMLConverter converter = new JavaSource2HTMLConverter();
            JavaSourceConversionOptions options = JavaSourceConversionOptions.getDefault();
            options.getStyleTable().put(JavaSourceType.KEYWORD, new JavaSourceStyleEntry(RGB.ORANGE, true, false));
            StringWriter writer = new StringWriter();
            converter.convert(source, options, writer);
            return "<pre>" + writer.toString().replace("\n", "").replace("<br/>", "\n")
                    .replace("&#160;&#160;&#160;&#160;", "\t") + "</pre>";
        } catch (IOException var6) {
            throw new RuntimeException(var6 + " when converting: «" + code + "»");
        }
    }

    public static boolean runDirectory(String srcDir, String destDir) {
        try {
            new File(srcDir);
            File dest = new File(destDir);
            String[] fileList = FileManager.list(srcDir);

            for (String file : fileList) {
                if (file.endsWith(".java") || file.endsWith(".jvs")) {
                    FileManager.save(dest.getCanonicalPath() + File.separator + (new File(file)).getName() + ".html",
                            run(FileManager.load(file)));
                }
            }

            return true;
        } catch (Exception e) {
            throw new RuntimeException(e + " when converting: «" + srcDir + "»");
        }
    }

    public static void main(String[] usage) {
        if (usage.length > 0) {
            FileManager.save(usage.length > 1 ? usage[1] : "stdout:", run(FileManager.load(usage[0])));
        }
    }
}
