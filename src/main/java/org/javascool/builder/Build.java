package org.javascool.builder;

import java.io.File;
import java.io.IOException;

import org.javascool.tools.Pml;

public class Build {
    public static final String logo = "icons/logo-builder.png";

    public static void main(String[] usage) {
        Pml args = (new Pml()).reset(usage);
        ProgletsBuilder.setVerbose(args.getBoolean("v") || args.getBoolean("verbose"));
        if (!args.getBoolean("h") && !args.getBoolean("help")) {
            if (!args.getBoolean("q") && !args.getBoolean("w")) {
                DialogFrame.startFrame();
            } else {
                try {
                    if (args.isDefined("target") && (new File(".")).getCanonicalPath()
                            .equals((new File(args.getString("target"))).getCanonicalPath())) {
                        throw new IllegalArgumentException("Sketchbook and build folder cannot be the same.");
                    }
                } catch (IOException ignored) {
                    throw new IllegalArgumentException("Cannot use build folder");
                }

                ProgletsBuilder.setVerbose(args.getBoolean("v") || args.getBoolean("verbose"));
                String target = args.isDefined("target") ? args.getString("target") : null;
                String[] names = args.isDefined("proglets") ? args.getString("proglets").trim().split("[, \t]+") : null;
                System.exit(ProgletsBuilder.build(ProgletsBuilder.getProglets(names), target, args.getBoolean("w")) ? 0
                        : -1);
            }
        } else {
            System.out.println("Java's Nice Builder - Build a jar with the specified proglets");
            System.out.println(
                    "Usage : java -jar javascool-builder.jar [-q] [-w] [-v] [-target target-dir] [-proglets proglet-list]");
            System.out.println("Options : ");
            System.out.println("\t-q\tBuild all available proglets");
            System.out.println("\t-w\tBuild proglets javadoc and jars");
            System.out.println("\t-v\tVerbose mode");
            System.out.println("\t-target target-dir\tBuild directoru (.build by default)");
            System.out.println("\t-proglets proglet-dir\tProglets to use (all by default)");
        }
    }
}
