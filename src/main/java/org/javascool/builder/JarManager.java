package org.javascool.builder;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.jar.JarEntry;
import java.util.jar.JarInputStream;
import java.util.jar.JarOutputStream;
import java.util.jar.Manifest;
import java.util.jar.Attributes.Name;

import org.javascool.tools.FileManager;

public class JarManager {
    private JarManager() {
    }

    public static void jarExtract(String jarFile, String destDir, String jarEntry) {
        try {
            ProgletsBuilder.log("Extract files from " + jarFile + " to " + destDir
                    + (!jarEntry.isEmpty() ? " which start with " + jarEntry : ""), true);
            JarInputStream jip = new JarInputStream(new FileInputStream(jarFile));

            while (true) {
                JarEntry je;
                do {
                    if ((je = jip.getNextJarEntry()) == null) {
                        jip.close();
                        return;
                    }
                } while (!jarEntry.isEmpty() && !je.getName().startsWith(jarEntry));

                if (!je.isDirectory() && !je.getName().contains("META-INF")) {
                    File dest = new File(destDir + File.separator + je.getName());
                    dest.getParentFile().mkdirs();
                    copyStream(jip, new FileOutputStream(dest));
                }
            }
        } catch (Exception e) {
            throw new IllegalStateException(e);
        }
    }

    public static void jarExtract(String jarFile, String destDir) {
        jarExtract(jarFile, destDir, "");
    }

    public static void jarCreate(String jarFile, String mfFile, String srcDir, String[] jarEntries) {
        try {
            ProgletsBuilder.log("Creating jar " + jarFile, true);
            File parent = (new File(jarFile)).getParentFile();
            if (parent != null) {
                parent.mkdirs();
            }

            (new File(jarFile)).delete();
            srcDir = (new File(srcDir)).getCanonicalPath();
            Manifest manifest = new Manifest(new FileInputStream(mfFile));
            manifest.getMainAttributes().put(Name.MANIFEST_VERSION, "1.0");
            JarOutputStream target = new JarOutputStream(new FileOutputStream(jarFile), manifest);
            copyFileToJar(new File(srcDir), target, new File(srcDir), jarEntries);
            target.close();
        } catch (Exception var7) {
            var7.printStackTrace();
            throw new RuntimeException(var7);
        }
    }

    public static void jarCreate(String jarFile, String mfFile, String srcDir) {
        jarCreate(jarFile, mfFile, srcDir, null);
    }

    public static void copyFiles(String srcDir, String dstDir, boolean recurse) throws IOException {
        if ((new File(srcDir)).isDirectory()) {
            if (!(new File(srcDir)).getName().equals(".svn")) {
                String[] var3 = FileManager.list(srcDir);

                for (String s : var3) {
                    String d = dstDir + File.separator + (new File(s)).getAbsoluteFile().getName();
                    if (recurse) {
                        copyFiles(s, d, true);
                    } else if (!(new File(s)).isDirectory()) {
                        copyFile(s, d);
                    }
                }
            }
        } else {
            copyFile(srcDir, dstDir);
        }

    }

    public static void copyFiles(String srcDir, String dstDir) throws IOException {
        copyFiles(srcDir, dstDir, true);
    }

    private static void copyFile(String srcFile, String dstDir) throws IOException {
        (new File(dstDir)).getParentFile().mkdirs();
        copyStream(new FileInputStream(srcFile), new FileOutputStream(dstDir));
    }

    private static void copyFileToJar(File source, JarOutputStream target, File root, String[] jarEntries)
            throws IOException {
        if (jarEntries != null) {
            boolean skip = true;
            for (String jarEntry : jarEntries) {
                String entry = root.toString() + File.separator + jarEntry;
                skip &= !(entry.startsWith(source.toString()) | source.toString().startsWith(entry));
            }

            if (skip)
                return;
        }

        try {
            if (source.isDirectory()) {
                String name = source.getPath().replace(root.getAbsolutePath() + File.separator, "")
                        .replace(File.separator, "/");
                if (!name.isEmpty() && !source.equals(root)) {
                    if (!name.endsWith("/")) {
                        name = name + "/";
                    }

                    JarEntry entry = new JarEntry(name);
                    entry.setTime(source.lastModified());
                    target.putNextEntry(entry);
                    target.closeEntry();
                }

                for (File nestedFile : source.listFiles()) {
                    copyFileToJar(nestedFile, target, root, jarEntries);
                }
            } else {
                JarEntry entry = new JarEntry(source.getPath().replace(root.getAbsolutePath() + File.separator, "")
                        .replace(File.separator, "/"));
                entry.setTime(source.lastModified());
                target.putNextEntry(entry);
                copyStream(new BufferedInputStream(new FileInputStream(source)), target);
            }
        } catch (Throwable t) {
            t.printStackTrace(System.out);
            throw new IllegalStateException(t);
        }
    }

    private static void copyStream(InputStream in, OutputStream out) throws IOException {
        InputStream i = in instanceof JarInputStream ? in : new BufferedInputStream(in, 2048);
        OutputStream o = out instanceof JarOutputStream ? out : new BufferedOutputStream(out, 2048);
        byte[] data = new byte[2048];

        int c;
        while ((c = i.read(data, 0, 2048)) != -1) {
            o.write(data, 0, c);
        }

        if (o instanceof JarOutputStream) {
            ((JarOutputStream) o).closeEntry();
        } else {
            o.close();
        }

        if (i instanceof JarInputStream) {
            ((JarInputStream) i).closeEntry();
        } else {
            i.close();
        }

    }

    public static void rmDir(File dir) {
        if (dir.isDirectory()) {
            for (File f : dir.listFiles()) {
                rmDir(f);
            }
        }

        dir.delete();
    }
}
