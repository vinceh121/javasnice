package org.javascool.builder;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;

import org.javascool.core.Utils;
import org.javascool.tools.FileManager;
import org.javascool.tools.Pml;
import org.javascool.tools.Xml2Xml;

public class ProgletsBuilder {
    private static final String fileRegexSeparator;
    static boolean verbose;

    private ProgletsBuilder() {
    }

    public static void setVerbose(boolean v) {
        verbose = v;
    }

    public static boolean canBuildProglets() {
        try {
            Class.forName("com.icl.saxon.TransformerFactoryImpl");
            return true;
        } catch (Throwable ignored) {
            return false;
        }
    }

    public static String[] getProglets(String[] names) {
        ArrayList<String> proglets = new ArrayList<>();
        String[] var2 = FileManager.list(System.getProperty("user.dir"));

        for (String dir : var2) {
            if (FileManager.exists(dir + File.separator + "proglet.pml")) {
                boolean found = names == null;
                if (names != null) {
                    for (String name : names) {
                        found |= dir.endsWith(name);
                    }
                }

                if (found) {
                    proglets.add(dir);
                }
            }
        }

        return proglets.toArray(new String[proglets.size()]);
    }

    public static String[] getProglets() {
        return getProglets(null);
    }

    public static boolean build(String[] proglets, String targetDir, boolean webdoc) {
        if (!canBuildProglets()) {
            throw new IllegalArgumentException("Badly configured builder, you need to use the right jar!");
        } else {
            try {
                if (proglets.length == 0) {
                    throw new IllegalArgumentException("No proglet to use");
                } else {
                    String targetJar = System.getProperty("user.dir") + File.separator + "javascool-proglets.jar";
                    (new File(targetJar)).delete();
                    log("Scanning for proglets in the folder: " + System.getProperty("user.dir"));
                    File buildDir;
                    if (targetDir == null) {
                        buildDir = new File(".build");
                    } else {
                        buildDir = new File(targetDir);
                        if ((new File(".")).equals(buildDir.getCanonicalFile())) {
                            throw new IllegalArgumentException("Proglet and build folder cannot be the same");
                        }
                    }

                    JarManager.rmDir(buildDir);
                    buildDir.mkdirs();
                    String jarDir = buildDir + File.separator + "jar";
                    String progletsDir = jarDir + File.separator + "org" + File.separator + "javascool" + File.separator
                            + "proglets";
                    (new File(progletsDir)).mkdirs();
                    DialogFrame.setUpdate("Install 1/2", 10);
                    log("Extracting jars from sketchbook", true);
                    String[] fmList = FileManager.list(System.getProperty("user.dir"), ".*\\.jar");
                    for (String proglet : fmList) {
                        if (!proglet.matches(".*" + fileRegexSeparator + "javascool-(builder|proglets).jar")) {
                            JarManager.jarExtract(proglet, jarDir);
                        }
                    }
                    
                    // String proglet;
                    for (String proglet : proglets) {
                        String[] list = FileManager.list(proglet, ".*\\.jar", 2);
                        for (int i = 0; i < list.length; ++i) {
                            proglet = list[i];
                            JarManager.jarExtract(proglet, jarDir);
                        }
                    }

                    String javascoolJar = Utils.javascoolJar();
                    log("Extracting Javasnice", true);
                    String[] libs = new String[] { "org/javascool", "org/fife", "sun/tools/java", "com/sun/tools/javac",
                            "com/sun/source/tree", "com/sun/source/util", "com/sun/tools/doclint" };

                    for (String proglet : libs) {
                        JarManager.jarExtract(javascoolJar, jarDir, proglet);
                    }

                    DialogFrame.setUpdate("Install 2/2", 20);
                    int level = 20;
                    int up = 10 / proglets.length == 0 ? 1 : 10 / proglets.length;

                    for (String proglet : proglets) {
                        ProgletsBuilder.ProgletBuild build = new ProgletsBuilder.ProgletBuild(proglet,
                                (new File(proglet)).getAbsolutePath(), jarDir);
                        proglet = (new File(proglet)).getName();
                        log("Compiling " + proglet + " ...");
                        DialogFrame.setUpdate("Building " + proglet + " 1/4", level = level + up);
                        build.copyFiles();
                        DialogFrame.setUpdate("Building " + proglet + " 2/4", level = level + up);
                        build.checkProglet();
                        DialogFrame.setUpdate("Building " + proglet + " 3/4", level = level + up);
                        build.convertHdocs(false);
                        if (!build.isprocessing) {
                            DialogFrame.setUpdate("Building " + proglet + " 4/4", level = level + up);
                            build.createHtmlApplet();
                            if (webdoc) {
                                build.javadoc(jarDir);
                            }
                        }
                    }

                    log("Compilation des fichiers java");
                    String[] javaFiles = FileManager.list(progletsDir, ".*\\.java", 2);
                    if (javaFiles.length > 0) {
                        javac(jarDir, javaFiles);
                    }

                    DialogFrame.setUpdate("Finalisation 1/2", 90);
                    // System.out.println("Compilation des jarres .. "); // For reference -vinceh
                    System.out.println("Compilation des jars .. ");
//                    String version = "Javasnice on \"" + new Date() + "\" Revision #" + "4.0.1313";
//                    Pml manifest = (new Pml()).set("Main-Class", "org.javascool.gui.Core")
//                            .set("Manifest-version", version)
//                            .set("Created-By",
//                                    "inria.fr (javascool.gforge.inria.fr) ©INRIA: CeCILL V2 + CreativeCommons BY-NC-ND V2")
//                            .set("Implementation-URL", "http://javascool.gforge.inria.fr")
//                            .set("Implementation-Vendor",
//                                    "javascool@googlegroups.com, ou=javascool.gforge.inria.fr, o=inria.fr, c=fr")
//                            .set("Implementation-Version", version).save(buildDir + "/manifest.jmf");
                    String name;
                    String tmpJar;
                    String keystore;
                    if (webdoc) {
                        for (String proglet : proglets) {
                            name = (new File(proglet)).getName();
                            tmpJar = "org" + File.separator + "javascool" + File.separator;
                            String[] jarEntries = new String[] { "org" + File.separator + "dnsalias",
                                    "org" + File.separator + "fife", "com" + File.separator + "sun",
                                    "sun" + File.separator + "tools", tmpJar + "builder", tmpJar + "core",
                                    tmpJar + "gui", tmpJar + "macros", tmpJar + "tools", tmpJar + "widgets",
                                    tmpJar + "proglets" + File.separator + name };
                            keystore = buildDir + File.separator + "javascool-proglet-" + name + ".jar";
                            JarManager.jarCreate(keystore, buildDir + "/manifest.jmf", jarDir, jarEntries);
                            (new ProgletsBuilder.ProgletBuild(proglet, (new File(proglet)).getAbsolutePath(), jarDir))
                                    .convertHdocs(true);
                        }
                    }

                    JarManager.jarCreate(targetJar, buildDir + "/manifest.jmf", jarDir);
                    if (webdoc) {
                        // System.out.print("Signature des jarres: "); // For reference -vinceh
                        System.out.print("Signature des jars: ");
                        System.out.flush();

                        for (String proglet : proglets) {
                            name = (new File(proglet)).getName();
                            tmpJar = buildDir + File.separator + "javascool-proglet-" + name + ".jar";
                            String signedJar = progletsDir + File.separator + name + File.separator
                                    + "javascool-proglet-" + name + ".jar";
                            if ((new File(signedJar)).getParentFile().exists()) {
                                System.out.print(name + " .. ");
                                keystore = jarDir + File.separator + "org" + File.separator + "javascool"
                                        + File.separator + "builder" + File.separator + "javascool.key";
//                                String args = "-storepass\tjavascool\t-keypass\tmer,d,azof\t-keystore\t" + keystore
//                                        + "\t-signedjar\t" + signedJar + "\t" + tmpJar + "\tjavascool";
                                 //JarSigner.main(args.split("\t"));
                            }
                        }

                        System.out.println("ok.");
                    }

                    DialogFrame.setUpdate("Finalisation 2/2", 100);
                    if (targetDir == null) {
                        JarManager.rmDir(buildDir);
                    }

                    System.out.println("Construction achevée avec succès: «" + targetJar + "» a été créé");
                    System.out.println("\tIl faut lancer «" + targetJar + "» pour tester/utiliser les proglets.");
                    return true;
                }
            } catch (Exception var20) {
                var20.printStackTrace(System.err);
                System.out.println("Erreur inopinée lors de la construction (" + var20.getMessage()
                        + "): corriger l'erreur et relancer la construction");
                return false;
            }
        }
    }

    public static boolean build(String[] proglets, String targetDir) {
        return build(proglets, targetDir, false);
    }

    public static boolean build(String[] proglets, boolean webdoc) {
        return build(proglets, (String) null, webdoc);
    }

    public static boolean build(String targetDir, boolean webdoc) {
        return build(getProglets(), targetDir, webdoc);
    }

    public static boolean build(String[] proglets) {
        return build(proglets, (String) null, false);
    }

    public static boolean build(String targetDir) {
        return build(getProglets(), targetDir, false);
    }

    public static boolean build(boolean webdoc) {
        return build(getProglets(), (String) null, webdoc);
    }

    public static boolean build() {
        return build(getProglets(), (String) null, false);
    }

    private static void javac(String classPath, String[] javaFiles) {
        try {
            String[] args = new String[javaFiles.length + 3];
            args[0] = "-cp";
            args[1] = classPath;
            args[2] = "-Xlint";
            System.arraycopy(javaFiles, 0, args, 3, javaFiles.length);
            StringWriter out = new StringWriter();
            Class.forName("com.sun.tools.javac.Main").getDeclaredMethod("compile", Class.forName("[Ljava.lang.String;"),
                    Class.forName("java.io.PrintWriter")).invoke((Object) null, args, new PrintWriter(out));
            String sout = out.toString().trim();
            if (sout.length() > 0) {
                System.out.println("Erreur de compilation java:\n" + sout);
                throw new IllegalArgumentException("Erreur de compilation java");
            }
        } catch (Throwable var5) {
            System.err.println("Echec de compilation :" + var5);
            throw new IllegalArgumentException("Erreur de compilation java");
        }
    }

    private static void javadoc(String name, String classPath, String srcDir, String apiDir) throws IOException {
        apiDir = (new File(apiDir)).getCanonicalPath();
        (new File(apiDir)).mkdirs();
        String[] files = FileManager.list(srcDir, ".*\\.java$");
        if (files.length > 0) {
            String argv = "-quiet\t-classpath\t" + classPath + "\t-d\t" + apiDir
                    + "\t-link\thttp://download.oracle.com/javase/6/docs/api"
                    + "\t-public\t-author\t-windowtitle\tJava's Cool v4\t-doctitle\tJava's Cool v4\t-version\t-nodeprecated\t-nohelp\t-nonavbar\t-notree\t-charset\tutf-8\t-Xdoclint:all\t-Xdoclint:-missing";
            String[] var6 = files;
            int var7 = files.length;

            for (int var8 = 0; var8 < var7; ++var8) {
                String f = var6[var8];
                argv = argv + "    " + f;
            }

            try {
                // Main.execute(argv.split("\t"));
            } catch (Throwable var10) {
                throw new IOException(var10);
            }

            Jvs2Html.runDirectory(srcDir, apiDir + File.separator + "proglets" + File.separator + name);
        }

    }

    public static void log(String text, boolean onlyVerbose) {
        if (onlyVerbose) {
            if (verbose) {
                System.out.println(text);
            }
        } else {
            System.out.println(text);
        }

    }

    public static void log(String text) {
        log(text, false);
    }

    static {
        fileRegexSeparator = File.separator.equals("\\") ? "\\\\" : File.separator;
        verbose = false;
    }

    private static class ProgletBuild {
        private String name;
        private String progletSrc;
        private String progletDir;
        private Pml pml;
        private String jarDest;
        private boolean isprocessing;

        public ProgletBuild(String name, String progletDir, Pml pml, String jarDest) {
            this.name = name = name != null ? (new File(name)).getName() : "?";
            Pml var10001 = pml != null ? pml : (new Pml()).load(progletDir + File.separator + "proglet.pml");
            pml = var10001;
            this.pml = var10001;

            try {
                this.progletSrc = progletDir != null ? (new File(progletDir)).getAbsolutePath() : "";
            } catch (Exception var6) {
                throw new RuntimeException("Le dossier source de " + name + " n'existe pas");
            }

            this.jarDest = jarDest = jarDest != null ? (new File(jarDest)).getAbsolutePath() : "";
            this.progletDir = jarDest + "/proglets/".replace("/", File.separator) + name;
            this.isprocessing = pml.getBoolean("processing");
        }

        public ProgletBuild(String name, String progletDir, String jarDest) {
            this(name, progletDir, null, jarDest);
        }

        public void copyFiles() {
            ProgletsBuilder.log("Copie des fichiers de " + this.name, true);

            try {
                (new File(this.progletDir)).mkdirs();
                JarManager.copyFiles(this.progletSrc, this.progletDir);
            } catch (IOException var5) {
                throw new RuntimeException("Erreur lors de la copie des fichiers de " + this.name, var5);
            }

            JarManager.rmDir(new File(this.progletDir, "applet"));
            String[] var1 = FileManager.list(this.progletDir, ".*\\.jar");
            int var2 = var1.length;

            for (int var3 = 0; var3 < var2; ++var3) {
                String jar = var1[var3];
                (new File(jar)).delete();
            }

        }

        public void checkProglet() {
            ProgletsBuilder.log("Vérification de la proglet " + this.name, true);
            boolean error = false;
            if (!this.name.matches("[a-zA-Z][a-zA-Z0-9][a-zA-Z0-9][a-zA-Z0-9]+") || this.name.length() > 20) {
                System.out.println("Le nom de la proglet «" + this.name + "» est bizarre:"
                        + " il ne doit contenir que des lettres faire au moins quatre caractères et au plus seize et démarrer par une lettre minuscule");
                error = true;
            }

            if (!FileManager.exists(this.progletDir + File.separator + "help.xml")) {
                System.out.println("Pas de fichier d'aide pour " + this.name + ", la proglet ne sera pas construite.");
                error = true;
            }

            if (FileManager.exists(this.progletDir + File.separator + "completion.xml")) {
                String err = Xml2Xml
                        .run(FileManager.load(this.progletDir + File.separator + "completion.xml"),
                                FileManager
                                        .load(this.jarDest + "/builder/completionok.xslt".replace("/", File.separator)))
                        .trim();
                if (err.length() > 0) {
                    System.out.println("Il y a une erreur dans le fichier completion.xml : «"
                            + err.replaceAll("\\s+", " ") + "», la proglet ne sera pas construite.");
                    error = true;
                }
            }

            if (!this.pml.isDefined("author")) {
                System.out.println("Le champ «author» n'est pas défini dans " + this.name
                        + "/proglet.pml, la proglet ne sera pas construite.");
                error = true;
            }

            if (!this.pml.isDefined("title")) {
                System.out.println("Le champ «title» n'est pas défini dans " + this.name
                        + "/proglet.pml, la proglet ne sera pas construite.");
                error = true;
            }

            if (this.isprocessing && (!this.pml.isDefined("width") || !this.pml.isDefined("height"))) {
                System.out.println("Les champ «width» et «height» ne sont pas définis dans " + this.name
                        + "/proglet.pml, la proglet processing ne sera pas construite.");
                error = true;
            }

            this.pml.save(this.progletDir + File.separator + "proglet.php");
            if (error) {
                throw new IllegalArgumentException("La proglet ne respecte pas les spécifications");
            }
        }

        public void convertHdocs(boolean webdoc) {
            ProgletsBuilder.log("Convertion des HDocs pour " + this.name, true);
            String[] var2 = FileManager.list(this.progletDir, ".*\\.xml");
            int var3 = var2.length;

            for (int var4 = 0; var4 < var3; ++var4) {
                String doc = var2[var4];
                if (!(new File(doc)).getName().equals("completion.xml")) {
                    try {
                        ProgletsBuilder.log("Convertion de " + (new File(doc)).getName(), true);
                        FileManager.save(doc.replaceFirst("\\.xml", "\\.htm"),
                                Xml2Xml.run(FileManager.load(doc, true),
                                        FileManager.load(
                                                this.jarDest + "/builder/hdoc2htm.xslt".replace("/", File.separator),
                                                true),
                                        "output", webdoc ? "web" : "jvs"),
                                false, true);
                    } catch (IllegalArgumentException var7) {
                        throw new IllegalArgumentException(
                                "dans " + (new File(doc)).getName() + " : " + var7.getMessage());
                    }
                }
            }

            Jvs2Html.runDirectory(this.progletDir, this.progletDir);
        }

        public void createHtmlApplet() {
            ProgletsBuilder.log("Création de l'applet HTML pour " + this.name, true);
            FileManager.save(this.progletDir + File.separator + "applet-tag.htm",
                    "<applet width='560' height='620' code='org.javascool.widgets.PanelApplet' archive='./proglets/"
                            + this.name + "/javascool" + "-proglet-" + this.name
                            + ".jar'><param name='panel' value='org" + ".javascool.proglets." + this.name
                            + ".Panel'/><pre>Impossible " + "de lancer " + this.name
                            + ": Java n'est pas installé ou mal co" + "nfiguré</pre></applet>\n");
        }

        public void javadoc(String classPath) {
            try {
                ProgletsBuilder.log("Création de la javadoc pour " + this.name, true);
                ProgletsBuilder.javadoc(this.name, classPath, this.progletDir,
                        this.progletDir + File.separator + "api");
            } catch (IOException var3) {
                throw new RuntimeException("Erreur lors de la génération de la javadoc");
            }
        }
    }
}
