package org.javascool.builder;

import java.awt.Component;
import java.awt.Dimension;
import java.io.File;
import java.util.ArrayList;
import javax.swing.*;

import org.javascool.gui.About;
import org.javascool.widgets.Console;
import org.javascool.widgets.MainFrame;

public class DialogFrame {
    private static JButton jBuilderButton = null;
    private static JButton jCreatorButton = null;
    private static JLabel jLabel = null;
    private static JProgressBar jProgressBar = null;
    private static JTextField jCreatorMenuDir;
    private static JTextField jCreatorMenuName;
    private static JPopupMenu jBuilderMenu = null;
    private static Console console = Console.newInstance();

    public static void startFrame() {
        jCreatorButton = getConsoleInstance().getToolBar().addTool("Create a new proglet", "icons/new.png",
                new Runnable() {
                    @Override
                    public void run() {
                        DialogFrame.startProgletCreatorMenu();
                    }
                });
        jBuilderButton = getConsoleInstance().getToolBar().addTool("Start builder", "icons/compile.png",
                new Runnable() {
                    @Override
                    public void run() {
                        DialogFrame.startProgletBuilderMenu();
                    }
                });
        getConsoleInstance().getToolBar().addTool("Progress Bar", jProgressBar = new JProgressBar());
        jProgressBar.setSize(new Dimension(100, 25));
        getConsoleInstance().getToolBar().addTool("Status Bar", jLabel = new JLabel());
        getConsoleInstance().getToolBar().addRightTool("HML Converter", new Runnable() {
            @Override
            public void run() {
                DialogFrame.startConvertisseurHML();
            }
        });
        getConsoleInstance().getToolBar().addRightTool(About.getAboutMessage());
        setUpdate("", 0);
        (new MainFrame()).reset("Java's Nice Proglet Buidler", "icons/logo-builder.png", getConsoleInstance());
    }

    public static void setUpdate(String statut, int percent) {
        while (statut.length() < 64) {
            statut += " ";
        }

        if (jLabel != null) {
            jLabel.setText(statut);
        }

        if (jProgressBar != null) {
            jProgressBar.setValue(percent);
        }

    }

    private static void startProgletCreatorMenu() {
        JPopupMenu jCreatorMenu = new JPopupMenu();
        jCreatorMenu.add(new JLabel("Proglet to build:", SwingConstants.LEFT));
        jCreatorMenuDir = new JTextField();
        jCreatorMenuDir.setText(System.getProperty("user.dir") + File.separator);
        jCreatorMenuDir.setEditable(false);
        jCreatorMenu.add(new JPanel() {
            /**
             * 
             */
            private static final long serialVersionUID = -236539671856928833L;

            {
                this.add(DialogFrame.jCreatorMenuDir);
                this.add(DialogFrame.jCreatorMenuName = new JTextField(20));
            }
        });
        JMenuItem menuitem = new JMenuItem("Create folder and extra files");
        jCreatorMenu.add(menuitem);
        menuitem.addActionListener(e -> (new Thread(() -> {
            String name = DialogFrame.jCreatorMenuName.getText();
            if (name != null && name.length() > 0) {
                ProgletCreator.mkdirProglet(DialogFrame.jCreatorMenuDir.getText() + name);
            }
        })).start());
        Component parent = getConsoleInstance().getToolBar();
        jCreatorMenu.show(jCreatorButton, 0, parent.getHeight());
    }

    private static void startProgletBuilderMenu() {
        boolean reload = false;
        if (jBuilderMenu == null || reload) {
            jBuilderMenu = new JPopupMenu();
            if (ProgletsBuilder.getProglets().length > 0) {
                jBuilderMenu.add(new JLabel("Proglets to build:"));
                String[] var1 = ProgletsBuilder.getProglets();

                for (String proglet : var1) {
                    JCheckBox check = new JCheckBox(proglet);
                    check.setSelected(true);
                    jBuilderMenu.add(check);
                }

                jBuilderMenu.addSeparator();
                JMenuItem menuitem = new JMenuItem("Build jar");
                menuitem.addActionListener(e -> (new Thread(() -> {
                    ArrayList<String> proglets = new ArrayList<>();
                    Component[] var2 = DialogFrame.jBuilderMenu.getComponents();

                    for (Component c : var2) {
                        if (c instanceof JCheckBox && ((JCheckBox) c).isSelected()) {
                            proglets.add(((JCheckBox) c).getText());
                        }
                    }

                    DialogFrame.getConsoleInstance().clear();
                    ProgletsBuilder.build(proglets.toArray(new String[proglets.size()]));
                })).start());
                jBuilderMenu.add(menuitem);
            } else {
                jBuilderMenu.add(new JLabel("No proglet to build in this folder"));
            }
        }

        Component parent = getConsoleInstance().getToolBar();
        jBuilderMenu.show(jBuilderButton, 0, parent.getHeight());
    }

    private static void startConvertisseurHML() {
        JPopupMenu jCreatorMenu = new JPopupMenu();
        jCreatorMenu.add(new Htm2Hml());
        Component parent = getConsoleInstance().getToolBar();
        jCreatorMenu.show(jCreatorButton, 0, parent.getHeight());
    }

    private static Console getConsoleInstance() {
        return console;
    }
}
