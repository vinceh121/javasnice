package org.javascool.builder;

import java.net.URI;
import java.net.URL;
import java.util.HashMap;
import java.util.HashSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.javascool.tools.FileManager;

public class LinkCheck {
    private static HashSet<String> links;
    private static HashMap<String, HashSet<String>> anchors;
    private static String root;
    private static boolean loop;
    private static int npages;
    private static int nlinks;
    private static int nbrokens;
    private static final Pattern linkPattern = Pattern.compile("(href|HREF|src|SRC)\\s*=\\s*[\"']");
    private static final Pattern anchorPattern = Pattern.compile("<([aA]\\s+(name|NAME)|[^>]+(id|ID))\\s*=\\s*[\"']");

    private LinkCheck() {
    }

    public static void main(String[] usage) {
        if (usage.length > 0) {
            check(usage[usage.length - 1], "-recursive".equals(usage[0]));
        }
    }

    public static void check(String location, boolean recursive) {
        echoBroken("LINKCHECK: " + getRoot(location));
        links = new HashSet<>();
        anchors = new HashMap<>();
        root = getRoot(location);
        loop = recursive;
        nbrokens = 0;
        nlinks = 0;
        npages = 0;
        check(location);
        echoBroken("  SCANNED PAGES: " + npages + " SCANNED LINKS: " + nlinks + " BROKENS LINKS: " + nbrokens);
        links = null;
        anchors = null;
    }

    private static void check(String location) {
        try {
            String text = FileManager.load(location);
            ++npages;

            for (String s : getLinks(text)) {
                String href = s;
                if (!href.matches("^(https|javascript|rtsp|mailto):.*$")) {
                    try {
                        href = (new URL(new URL(location), href.replaceAll("%2e", "."))).toString();
                        if (!links.contains(href)) {
                            links.add(href);
                            ++nlinks;
                            String anchor = null;
                            int i = href.indexOf("#");
                            if (i != -1) {
                                anchor = href.substring(i + 1);
                                href = href.substring(0, i);
                            }

                            if (!FileManager.exists(href)) {
                                echoBroken("BROKEN  Link in " + location + " -> " + href);
                            } else {
                                if (loop && href.startsWith(root)
                                        && href.matches("^http:.*([?][^?]*|/|\\.(htm|html|shtml|php))$")
                                        && !href.matches("^.*\\.(xslt|java)")) {
                                    check(href);
                                }

                                if (anchor != null) {
                                    if (!anchors.containsKey(href)) {
                                        anchors.put(href, getAnchorSet(FileManager.load(href)));
                                    }

                                    if (!anchors.get(href).contains(anchor)) {
                                        echoBroken("BROKEN  Anchor in " + location + " -> " + href + " #" + anchor);
                                    }
                                }
                            }
                        }
                    } catch (Exception e) {
                        echoBroken("SPURIOUS Link in " + location + " -> " + href + " (" + e + ") ");
                    }
                }
            }
        } catch (Exception e) {
            echoBroken("SPURIOUS   " + location + " (" + e + ") ");
        }

    }

    private static String getRoot(String location) {
        try {
            return (new URI(location)).normalize().toString();
        } catch (Exception ignored) {
            return location;
        }
    }

    private static void echoBroken(String message) {
        System.out.println(message);
        ++nbrokens;
    }

    public static String[] getLinks(String text) {
        HashSet<String> h = new HashSet<>();
        int i = 0;
        int i2;
        for (int l = text.length(); i < l; i = i2) {
            Matcher matcher = linkPattern.matcher(text).region(i, l);
            if (!matcher.find())
                break;

            int i1 = matcher.end();
            i2 = nextQuote(text, i1);
            String link = text.substring(i1, i2);
            if (!link.startsWith("'")) {
                h.add(link);
            }
        }

        return h.toArray(new String[h.size()]);
    }

    public static String[] getAnchors(String text) {
        HashSet<String> a = getAnchorSet(text);
        return a.toArray(new String[a.size()]);
    }

    private static HashSet<String> getAnchorSet(String text) {
        HashSet<String> a = new HashSet<>();
        int i = 0;

        int i2;
        for (int l = text.length(); i < l; i = i2) {
            Matcher matcher = anchorPattern.matcher(text).region(i, l);
            if (!matcher.find()) {
                break;
            }

            int i1 = matcher.end();
            i2 = nextQuote(text, i1);
            a.add(text.substring(i1, i2));
        }

        return a;
    }

    private static int nextQuote(String text, int i1) {
        char c = text.charAt(i1 - 1);
        int i2 = text.indexOf(c, i1);
        return i2 == -1 ? text.length() : i2;
    }
}
