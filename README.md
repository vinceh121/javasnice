# Javasnice

Javasnice is a fork of Javascool V4, a program made to help learn and teach the Java programming language.

Since the original repo of Javascool V4 has been overwritten to let place to Javascool V5 (which never came out of alpha), we've had to decompile to original jar file.

## Building

To build Javasnice you need to install Apache Maven.

Use the following command to use a standard build:
```bash
mvn compile assembly:single
```

Use the following command to build with the builder:
```bash
mvn -P with-builder compile assembly:single
```

## Icons

The new icons are kindly taken from the Papirus Dark icon under the GNU GPL V3

## License

Javasnice is under the GNU GPL V3