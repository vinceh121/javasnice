#!/bin/bash

# Format: generate_po.sh <langcode>

find src/main/java -name "*.java" > files.tmp

# TODO: Added ngettext again

echo Joining po files

touch src/main/resources/i18n/$1.po

xgettext -f files.tmp -L java --from-code UTF-8 -k --keyword="gettext, ngettext:2" -j --output=src/main/resources/i18n/$1.po

rm files.tmp
